﻿#if !MOBILE
using System.Collections.Concurrent;

namespace System.ComponentModel
{
    public class MyServiceProvider : IServiceProvider
    {
        public static readonly MyServiceProvider Instance = new MyServiceProvider();
        readonly ConcurrentDictionary<Type, object> services = new ConcurrentDictionary<Type, object>();
        MyServiceProvider()
        {

        }
        public object GetService(Type serviceType)
        {
            return services.TryGetValue(serviceType, out var value) ? value : null;
        }

        public T GetService<T>()
        {
            return services.TryGetValue(typeof(T), out var value) ? (T)value : default(T);
        }

        public void Register(Type serviceType, object instance)
        {
            services[serviceType] = instance ?? throw new ArgumentNullException(nameof(instance));
        }

        public void Register<T>(T instance)
        {
            Register(typeof(T), instance);
        }
    }
}

#endif