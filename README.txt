﻿.Net içerisinde kullanılmak üzere geliştirilmiş uzantı yöntemler ve yardımcı sınıfları içerir.

Durumsal Derleme Sembolleri

DEV        : MyAppEnv sınıfındaki geliştirme için kullanılan propertyleri aktif hale getirir.
COREFULL   : Tüm semboller için kısayol sembolüdür.
MEF        : MefTypeResolver sınıfını kullanıma açar. Ayrıca System.ComponenetModel.Composition.dll gerektirir.
JSON       : Aşağıdaki sınıflar aktif hale gelir;
             * EnumJsonConverter
			 * EnumPropertyNameAttribute 
			 * EnumTargetTypeAttribute
			 * ToJsonString (uzantı yöntem)
			 * JsonObjectValueProvider (COREDATA sembolü ile)
			 * JsonConvertAndValidateRule (Doğrulama kuralı)
TSQLPARSE  : Aşağıdaki sınıflar aktif hale gelir; (Microsoft.SqlServer.TransactSql.ScriptDom.dll gerekir)
			 * SqlDetectedStatements
			 * SqlSanitizer
			 * SqlScriptVisitor
COREDATA   : System.Data kütüphanesi ile ilgili uzantı sınıflar ve veri nesneleri ile ilgili işler gerçekleştirir.
LEGACYSQL  : Lacus içerisinde kullanılan sınıfları aktif hale getirir. (COREDATA sembolü ile kullanılmalıdır.)
             * SqlHelper
			 * DataHelper
DRAWING    : ImageExtensions.cs sınıfını aktif hale getirir.

TYPESCRIPT : TypeScript niteliklerini aktif hale getirir.
             * TypeScriptIgnore
			 * TypeScriptNamespace
			 * ...
ORACLE     : CtOracleClientFactory sınıfı aktif hale getirir. (COREDATA sembolü ile kullanılmalıdır.)
FIREBIRD   : CtFirebirdSqlClientFactory sınıfı aktif hale getirir. (COREDATA sembolü ile kullanılmalıdır.)
REGISTRY   : Win32 Registry yardımcı sınıfları
