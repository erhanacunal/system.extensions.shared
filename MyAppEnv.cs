﻿#if !MOBILE
#if NETCOREAPP2_0
using Newtonsoft.Json;
using System.Collections.Generic;
#else
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.Custom;
using Newtonsoft.Json;

#if MEF
using System.ComponentModel.Composition; 
#endif
#endif

namespace System
{
    public static class MyAppEnv
    {
#if NETCOREAPP2_0
        static MyAppConfig config;

        public static string WebApiBaseUri => config?.WebApiBaseUri;
        public static string DefaultUrlShortenerServiceName => config?.DefaultUrlShortenerServiceName;
        public static bool UseUrlShortenerService => config?.UseUrlShortenerService ?? false;
        public static IDictionary<string, MySqlConnection> SqlConnections => config?.SqlConnections;
        public static string GoogleUrlShortenerApiKey => config?.GoogleUrlShortenerApiKey;
        public static string TelemetryHost => config?.TelemetryHost;
        public static string TelemetryProductCode => config?.TelemetryProductCode;
        
        public static void Initialize(MyAppConfig cfg)
        {
            config = cfg;
        }
#else

        static string prefix;

        public static void Initialize(string prefx)
        {
            prefix = prefx;
        }

#if DEV
        public static bool IsDevelopmentMachine { get; } = true;
        public static string DevelopmentAfterLoginController => GetUserSpecificValue("DevAfterLoginCtrl");
        public static string DevelopmentAfterLoginControllerAction => GetUserSpecificValue("DevAfterLoginCtrlAction");

        static string GetUserSpecificValue(string key)
            => GetAppSetting($"{key}_{Environment.MachineName}");
#else
        public static bool IsDevelopmentMachine { get; } = false;
        public static string DevelopmentAfterLoginController => "";
        public static string DevelopmentAfterLoginControllerAction => "";
#endif

        public static string GetPrefixedSetting(string key, string defaultValue = null) =>
            GetAppSetting($"{prefix}:{key}").IfNullOrEmpty(defaultValue);

        public static string WebApiBaseUri => GetAppSetting("WebApiBaseUri");
        public static string GoogleUrlShortenerApiKey => GetPrefixedSetting("Google.UrlShortener.ApiKey", "");
        public static string DefaultUrlShortenerServiceName => GetPrefixedSetting("DefaultUrlShortenerService", "");
        public static bool UseUrlShortenerService => GetPrefixedSetting("UseUrlShortenerService", "false").IsOneOfThese("true", "1", "yes");
        public static string HelpSystemFolder => GetPrefixedSetting("HelpSystemFolder", "");
        public static string OneSignalApiKey => GetPrefixedSetting("OneSignalApiKey", "");
        public static string TelemetryHost => GetPrefixedSetting("TelemetryHost", "");
        public static string TelemetryProductCode => GetPrefixedSetting("TelemetryProductCode", "");
        static IUrlShortenerService defaultUrlShortenerService;

#if MEF
        public static IUrlShortenerService DefaultUrlShortenerService
            => LazyInitializer.EnsureInitialized(ref defaultUrlShortenerService,
                () => MefTypeResolver.ResolveOrDefault<IUrlShortenerService>(DefaultUrlShortenerServiceName));
#else
        public static IUrlShortenerService DefaultUrlShortenerService => defaultUrlShortenerService;
#endif
        static string GetAppSetting(string name) => ConfigurationManager.AppSettings[name];
#endif
    }

    public sealed class MyAppConfig
    {
        [JsonProperty("WebApiBaseUri")]
        public string WebApiBaseUri { get; set; }

        [JsonProperty("DefaultUrlShortenerServiceName")]
        public string DefaultUrlShortenerServiceName { get; set; }

        [JsonProperty("UseUrlShortenerService")]
        public bool UseUrlShortenerService { get; set; }

        [JsonProperty("SqlConnections")]
        public Dictionary<string, MySqlConnection> SqlConnections { get; } = new Dictionary<string, MySqlConnection>();

        [JsonProperty("GoogleUrlShortenerApiKey")]
        public string GoogleUrlShortenerApiKey { get; set; }
        [JsonProperty("TelemetryHost")]
        public string TelemetryHost { get; set;}
        [JsonProperty("TelemetryProductCode")]
        public string TelemetryProductCode { get; set; }
    }

    public sealed class MySqlConnection
    {
        public string ConnectionString { get; set; }

        public string ProviderName { get; set; }
    }

}

#endif