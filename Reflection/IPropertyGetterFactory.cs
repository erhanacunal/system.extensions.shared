using System.Reflection;

namespace System.Reflection
{
    public interface IPropertyGetterFactory
    {
        IPropertyGetter Create(PropertyInfo property);
    }
}