﻿using System;
using System.Collections.Generic;

namespace System.Reflection
{
    sealed class ObjectSnapshot
    {
        readonly ObjectInstance instance;
        public Dictionary<PropertyAccessor, object> CapturedProperties { get; } = new Dictionary<PropertyAccessor, object>();

        public ObjectSnapshot(ObjectInstance instance)
        {
            this.instance = instance ?? throw new ArgumentNullException(nameof(instance));
        }

        internal void Capture()
        {
            CapturedProperties.Clear();
            var obj = instance.Instance;
            foreach (var property in instance.Schema.AllUpdatableProperties)
            {
                CapturedProperties.Add(property, property.Getter.GetValue(obj));
            }
        }

        internal void Reject()
        {
            foreach (var capturedProperty in CapturedProperties)
            {
                capturedProperty.Key.Setter.SetValue(instance.Instance, capturedProperty.Value);
            }
            CapturedProperties.Clear();
        }

        internal bool IsModified()
        {
            var obj = instance.Instance;
            foreach (var property in CapturedProperties)
            {
                var currentValue = property.Key.Getter.GetValue(obj);
                if (!Equals(currentValue, property.Value))
                    return true;
            }
            return false;
        }

        internal PropertyAccessor[] GetModifiedProperties()
        {
            var result = new List<PropertyAccessor>();
            var obj = instance.Instance;
            foreach (var property in CapturedProperties)
            {
                var currentValue = property.Key.Getter.GetValue(obj);
                if (!Equals(currentValue, property.Value))
                    result.Add(property.Key);
            }
            return result.ToArray();
        }

        internal bool IsPropertyModified(PropertyAccessor property)
        {
            return !Equals(property.Getter.GetValue(instance.Instance), GetOriginalValue(property));

        }

        internal object GetOriginalValue(PropertyAccessor property)
        {
            return CapturedProperties.TryGetValue(property, out var value)
                ? value
                : property.Getter.GetValue(instance.Instance);
        }

    }
}