﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Utils;
using JetBrains.Annotations;

namespace System.Reflection
{
    /// <summary>
    /// .Net POCO sınıflarının property'lerine daha dinamik şekilde erişmeyi sağlayan sınıf
    /// </summary>
    public sealed class TypeAccessor
    {
        static readonly ConcurrentDictionary<Type, TypeAccessor> Accessors = new ConcurrentDictionary<Type, TypeAccessor>();
        internal static readonly Dictionary<string, string> TypeConnectionNameMapping = new Dictionary<string, string>();
        string defaultConnectionName;
        readonly List<PropertyAccessor> declaredProperties = new List<PropertyAccessor>();
        ReadOnlyCollection<PropertyAccessor> propertiesReadOnly;
        ReadOnlyCollection<PropertyAccessor> allPropertiesReadOnly;
        ReadOnlyCollection<PropertyAccessor> databoundProperties;
        ReadOnlyCollection<PropertyAccessor> primaryKeyProperties;
        readonly List<TypeRelationship> relationships = new List<TypeRelationship>();
        public TypeAccessor BaseTypeAccessor { get; }
        public Type Type { get; }
        public PropertyAccessor IdentityProperty { get; internal set; }
        TypeAccessorOptions options;
        internal TypeAccessorOptions Options => options ?? BaseTypeAccessor?.Options;

        public IReadOnlyCollection<PropertyAccessor> DeclaredProperties
            => LazyInitializer.EnsureInitialized(ref propertiesReadOnly, () => new ReadOnlyCollection<PropertyAccessor>(declaredProperties));

        public IReadOnlyCollection<PropertyAccessor> AllProperties =>
            LazyInitializer.EnsureInitialized(ref allPropertiesReadOnly, () =>
            {
                var lst = new List<PropertyAccessor>();
                if (BaseTypeAccessor != null)
                    lst.AddRange(BaseTypeAccessor.AllProperties);
                lst.AddRange(declaredProperties);
                lst.Sort((a, b) => a.Order.CompareTo(b.Order));
                var index = 0;
                // sırayı tekrar belirle
                foreach (var property in lst)
                    property.Order = index++;
                return new ReadOnlyCollection<PropertyAccessor>(lst);
            });
        public IReadOnlyCollection<TypeRelationship> Relationships =>
            relationships;
        /// <summary>
        /// Identity kolon dahil tüm sahip olduğu alanları verir
        /// </summary>
        public IReadOnlyCollection<PropertyAccessor> MappableProperties => LazyInitializer.EnsureInitialized(ref databoundProperties,
            () => new ReadOnlyCollection<PropertyAccessor>(new List<PropertyAccessor>(AllProperties.Where(item => item.IsMappable))));

        /// <summary>
        /// Tüm birincil anahtarda olan alanları verir
        /// </summary>
        public IReadOnlyCollection<PropertyAccessor> PrimaryKeys => LazyInitializer.EnsureInitialized(ref primaryKeyProperties,
            () => new ReadOnlyCollection<PropertyAccessor>(new List<PropertyAccessor>(AllProperties.Where(item => item.InPrimaryKey))));

        /// <summary>
        /// Tüm güncellenebilir alanları verir, Identity kolon ve bağımsız kolonlar hariç
        /// </summary>
        public IEnumerable<PropertyAccessor> AllUpdatableProperties => AllProperties.Where(item => item.IsMappable && !item.IsIdentity);

        /// <summary>
        /// Bu erişicinin temsil ettiği tip'in namespace'i için bir bağlantı adı atanmış ise onu verir
        /// </summary>
        public string DefaultConnectionName => LazyInitializer.EnsureInitialized(ref defaultConnectionName, () =>
        {
            var ns = Type.Namespace;
            if (string.IsNullOrWhiteSpace(ns)) return string.Empty;
            var mostMatch = string.Empty;
            var foundName = string.Empty;
            foreach (var tcn in TypeConnectionNameMapping)
            {
                if (!ns.StartsWith(tcn.Key) || mostMatch.Length >= tcn.Key.Length) continue;
                foundName = tcn.Value;
                mostMatch = tcn.Key;
            }
            return foundName;
        });
        public string TableName { get; }
        public string TableSchema { get; }
        public TypeRelationship SelfReferencingTypeRelationship { get; }
        TypeAccessor(Type type)
        {
            Type = type;
            TableName = type.Name;
            TableSchema = "dbo";
            if (type.BaseType != typeof(object))
                BaseTypeAccessor = CreateOrGet(type.BaseType);
            foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly))
            {
                var name = property.Name;
                var nameHash = HashUtils.JenkinsHash(name);
                var existingProperty = BaseTypeAccessor?.FindProperty(nameHash);
                if (existingProperty == null)
                {
                    var prop = new PropertyAccessor(this, property, name, nameHash, HashUtils.JenkinsHash(name.ToLowerInvariant()));
                    declaredProperties.Add(prop);
                    continue;
                }
                existingProperty.MergeAttributes(property);
            }
            foreach (var attribute in type.GetCustomAttributes(true))
            {
                switch (attribute)
                {
                    case TableAttribute tableAttribute:
                        TableName = tableAttribute.Name;
                        TableSchema = string.IsNullOrWhiteSpace(tableAttribute.Schema) ? "dbo" : tableAttribute.Schema;
                        break;
                    case TypeForeignKeyAttribute foreignKeyAttribute:
                        var relationship = new TypeRelationship(this, foreignKeyAttribute.ParentType,
                            foreignKeyAttribute.Column);
                        if (SelfReferencingTypeRelationship == null && foreignKeyAttribute.ParentType == type)
                            SelfReferencingTypeRelationship = relationship;
                        else if (SelfReferencingTypeRelationship != null && foreignKeyAttribute.ParentType == type)
                            throw new InvalidOperationException("Too many self referencing columns defined!");
                        relationships.Add(relationship);
                        break;
                }
            }
            if (IdentityProperty == null && BaseTypeAccessor != null)
                IdentityProperty = BaseTypeAccessor.IdentityProperty;
        }

        public static void Configure<T>([NotNull] Action<TypeAccessorOptions> setter)
        {
            if (setter == null) throw new ArgumentNullException(nameof(setter));
            var accessor = CreateOrGet<T>();
            if (accessor.options == null)
                accessor.options = new TypeAccessorOptions();
            setter(accessor.options);
        }

        public PropertyAccessor FindProperty(string name, bool invariant = false)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            var nameHash = HashUtils.JenkinsHash(invariant ? name.ToLowerInvariant() : name);
            return FindProperty(nameHash, invariant);
        }

        public PropertyAccessor FindProperty(uint hash, bool invariant = false)
        {
            foreach (var property in AllProperties)
            {
                if ((invariant && property.InvariantNameHash == hash) || (!invariant && property.NameHash == hash) ||
                    property.ColumnNameHash == hash)
                    return property;
            }
            return null;
        }
        /// <summary>
        /// Kolon adı hash değerine göre bir property erişicisi bulmayı dener
        /// </summary>
        /// <param name="hash">ColumnNameHash ile karşılaştırılacak hash değeri. Bu değer ToLowerInvariant ile elde edilmiş olmalıdır.</param>
        /// <returns>Bulunursa PropertyAccessor örneği yoksa null döner</returns>
        public PropertyAccessor FindPropertyByColumnName(uint hash) =>
            MappableProperties.FirstOrDefault(property => property.ColumnNameHash == hash);
        /// <summary>
        /// Kolon adına göre bir property erişicisi bulmayı dener
        /// </summary>
        /// <param name="columnName">Aranacak kolon adı</param>
        /// <returns>Bulunursa PropertyAccessor örneği yoksa null döner</returns>
        public PropertyAccessor FindPropertyByColumnName(string columnName) =>
            FindPropertyByColumnName(HashUtils.JenkinsHash(columnName.ToLowerInvariant()));

        /// <summary>
        /// Okunabilir propertylerin sıralamasını döndürür
        /// </summary>
        /// <param name="includeNonPublic">Bu sıralamada herkes tarafında erişilebilir olmayanlarında dahil edilip edilmeyeceğini ayarlar</param>
        /// <returns>Property sıralaması</returns>
        public IEnumerable<PropertyAccessor> GetReadableProperties(bool includeNonPublic = false)
        {
            return AllProperties.Where(property => property.Getter != null && (includeNonPublic || property.IsGetterPublic));
        }
        /// <summary>
        /// Yazılabilir propertylerin sıralamasını döndürür
        /// </summary>
        /// <param name="includeNonPublic">Bu sıralamada herkes tarafında erişilebilir olmayanlarında dahil edilip edilmeyeceğini ayarlar</param>
        /// <returns>Property sıralaması</returns>
        public IEnumerable<PropertyAccessor> GetWritableProperties(bool includeNonPublic = false)
        {
            return AllProperties.Where(property => property.Setter != null && (includeNonPublic || property.IsSetterPublic));
        }

        public object this[object instance, string propertyName]
        {
            get
            {
                var prop = FindProperty(propertyName);
                if (prop == null)
                    throw new InvalidOperationException("Property not found -> " + propertyName);
                if (prop.Getter == null)
                    throw new InvalidOperationException("Property is has no getter -> " + propertyName);
                return prop.Getter.GetValue(instance);
            }
            set
            {
                var prop = FindProperty(propertyName);
                if (prop == null)
                    throw new InvalidOperationException("Property not found -> " + propertyName);
                if (prop.Setter == null)
                    throw new InvalidOperationException("Property has no setter -> " + propertyName);
                prop.Setter.SetValue(instance, value);
            }
        }

        public static TypeAccessor CreateOrGet(Type type)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));
            return Accessors.GetOrAdd(type, t => new TypeAccessor(t));
        }

        public static TypeAccessor CreateOrGet<T>() => Accessors.GetOrAdd(typeof(T), t => new TypeAccessor(t));

        public static void RegisterConnectionName(string typeNamespace, string connectionName)
        {
            TypeConnectionNameMapping[typeNamespace] = connectionName;
        }

        /// <summary>
        /// Bu erişicinin verilen erişiden türeyip türemediğini verir
        /// </summary>
        /// <param name="accessor">Ata erişici örneği</param>
        /// <returns>Atası ise true değilse false döner</returns>
        public bool IsDerivedFrom(TypeAccessor accessor)
        {
            var baseAccessor = BaseTypeAccessor;
            while (baseAccessor != null)
            {
                if (baseAccessor == accessor) return true;
                baseAccessor = baseAccessor.BaseTypeAccessor;
            }
            return false;
        }

        /// <summary>
        /// Bu erişiciden başlayarak verilen temel erişiciye kadar olan (dahil değil) tanımlanmış property erişilerini verilen listeye doldurur 
        /// </summary>
        /// <param name="baseAccessor">Bırakılacak temel erişici örneği</param>
        /// <param name="properties">Tanımlı propertylerin aktarılacağı liste</param>
        public void GetPropertiesUntil(TypeAccessor baseAccessor, IList<PropertyAccessor> properties)
        {
            var accessor = this;
            while (true)
            {
                if (accessor == null || accessor == baseAccessor)
                    break;
                properties.AddRange3(accessor.DeclaredProperties.Where(_ => _.IsMappable));
                accessor = accessor.BaseTypeAccessor;
            }
        }

        /// <summary>
        /// İsimleri bu örnekte araştırır bulduklarını <paramref name="targetList"/> listesi örneğine ekler.
        /// </summary>
        /// <param name="targetList">Bulunan property'lerin ekleneceği hedef liste</param>
        /// <param name="propertyNames">Aranacak property'lerin isimleri</param>
        public void GetPropertiesFromNames(IList<PropertyAccessor> targetList, IEnumerable<string> propertyNames)
        {
            foreach (var name in propertyNames)
            {
                var prop = FindProperty(name, true);
                if (prop == null) continue;
                targetList.Add(prop);
            }
        }
        /// <summary>
        /// Verilen isimlerin property değer ayarlayıcıları verilen hedef listeye ekler
        /// </summary>
        /// <param name="targetList">Eklenecek hedef liste</param>
        /// <param name="propertyNames">Aranacak property isimleri</param>
        public void GetPropertySettersFromNames(IList<IPropertySetter> targetList, IEnumerable<string> propertyNames)
        {
            foreach (var name in propertyNames)
            {
                var prop = FindProperty(name, true);
                if (prop == null) continue;
                targetList.Add(prop.Setter);
            }
        }
        /// <summary>
        /// Verilen isimlerin property değer getiricilerini verilen hedef listeye ekler
        /// </summary>
        /// <param name="targetList">Eklenecek hedef liste</param>
        /// <param name="propertyNames">Aranacak property isimleri</param>
        public void GetPropertyGettersFromNames(IList<IPropertyGetter> targetList, IEnumerable<string> propertyNames)
        {
            foreach (var name in propertyNames)
            {
                var prop = FindProperty(name, true);
                if (prop == null) continue;
                targetList.Add(prop.Getter);
            }
        }
        /// <summary>
        /// Verilen <paramref name="b"/> parametresinde olan property'leri içermeyen bir property listesi döndürür
        /// </summary>
        /// <param name="a">Property'leri esas alınacak TypeAccessor örneği</param>
        /// <param name="b">Property'leri çıkarılacak TypeAccessor örneği</param>
        /// <returns>PropertyAccessor listesi örneği</returns>
        public static List<PropertyAccessor> operator -(TypeAccessor a, TypeAccessor b)
        {
            var properties = new List<PropertyAccessor>();
            if (a.IsDerivedFrom(b))
                a.GetPropertiesUntil(b, properties);
            else
            {
                var nameHashes = b.MappableProperties.Select(p => p.ColumnNameHash).ToArray();
                foreach (var property in a.MappableProperties)
                {
                    if (Array.IndexOf(nameHashes, property.ColumnNameHash) != -1) continue;
                    properties.Add(property);
                }
            }
            return properties;
        }

        public override string ToString()
        {
            return string.Concat(Type.Name, BaseTypeAccessor != null ? " : " + BaseTypeAccessor : string.Empty);
        }
    }

    public sealed class TypeAccessorOptions
    {
        public bool IsAuditable { get; set; }
        public string InsertedByProperty { get; set; }
        public string InsertTimeProperty { get; set; }
        public string UpdatedByProperty { get; set; }
        public string UpdateTimeProperty { get; set; }
        public string LockedByProperty { get; set; }
        public string LockTimeProperty { get; set; }
    }

    public sealed class TypeRelationship
    {
        TypeAccessor parenTypeAccessor;
        PropertyAccessor childPropertyAccessor;
        PropertyAccessor parentKeyPropertyAccessor;
        
        readonly Type parentType;
        public TypeAccessor ChildType { get; }
        public string ChildPropertyName { get; }

        public TypeAccessor ParentType =>
            LazyInitializer.EnsureInitialized(ref parenTypeAccessor, () => TypeAccessor.CreateOrGet(parentType));

        public PropertyAccessor ParentKeyProperty =>
            LazyInitializer.EnsureInitialized(ref parentKeyPropertyAccessor, () => ParentType.IdentityProperty);

        public PropertyAccessor ChildProperty =>
            LazyInitializer.EnsureInitialized(ref childPropertyAccessor,
                () => ChildType.FindPropertyByColumnName(ChildPropertyName));

        public TypeRelationship(TypeAccessor childType, Type parentType, string childPropertyName)
        {
            ChildType = childType;
            ChildPropertyName = childPropertyName;
            this.parentType = parentType;
        }
    }
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class TypeForeignKeyAttribute : Attribute
    {
        public Type ParentType { get; }
        public string Column { get; }

        public TypeForeignKeyAttribute(Type parentType, string column)
        {
            ParentType = parentType;
            Column = column;
        }
    }
}

