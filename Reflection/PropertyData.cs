namespace System.Reflection
{
    public struct PropertyData
    {
        public int Size;
        public object Value;

        public PropertyData(object val, bool isSizable)
        {
            Value = val;
            Size = 1;
            if (!isSizable || val == null)
                return;
            var byteArray = val as byte[];
            Size = byteArray?.Length ?? ((string)val).Length;
        }
    }
}