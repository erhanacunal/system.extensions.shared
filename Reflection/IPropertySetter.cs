namespace System.Reflection
{
    public interface IPropertySetter
    {
        void SetValue(object instance, object value);
    }
}