﻿using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
    public sealed class AllUpdatablePropertiesPropertyProvider<T> : PropertyProvider<T> where T : class
    {
        protected override void Initialize(List<PropertyAccessor> properties)
        {
            properties.AddRange(Accessor.AllUpdatableProperties);
        }
    }
} 
