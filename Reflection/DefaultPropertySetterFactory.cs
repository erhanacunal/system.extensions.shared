using System;
using System.Reflection;

namespace System.Reflection
{
    public class DefaultPropertySetterFactory : IPropertySetterFactory
    {
        public IPropertySetter Create(PropertyInfo property)
        {
            var setter = property.GetSetMethod(true);
            if (setter == null)
                return null;
            var propType = property.PropertyType;
            var types = new[] { property.DeclaringType, propType };
            var underlyingType = Nullable.GetUnderlyingType(propType);
            var newType = underlyingType != null && underlyingType.IsEnum
                ? typeof(DefaultNullableEnumPropertySetter<,,>).MakeGenericType(property.DeclaringType, propType, underlyingType)
                : typeof(DefaultPropertySetter<,>).MakeGenericType(types);
            return (IPropertySetter)Activator.CreateInstance(newType, Delegate.CreateDelegate(typeof(Action<,>).MakeGenericType(types), setter));
        }
    }
}