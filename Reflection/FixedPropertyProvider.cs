﻿using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
    public sealed class FixedPropertyProvider<T> : PropertyProvider<T> where T : class
    {
        readonly IList<PropertyAccessor> propertyList;
        public FixedPropertyProvider(params string[] propertyNames)
        {
            Accessor.GetPropertiesFromNames(propertyList = new List<PropertyAccessor>(), propertyNames);
        }

        public FixedPropertyProvider(IEnumerable<string> propertyNames)
        {
            Accessor.GetPropertiesFromNames(propertyList = new List<PropertyAccessor>(), propertyNames);
        }

        public FixedPropertyProvider(IEnumerable<PropertyAccessor> propertyAccessors)
        {
            propertyList = propertyAccessors as IList<PropertyAccessor> ?? propertyAccessors.ToList();
        }

        public FixedPropertyProvider(TypeAccessor accessor, IEnumerable<PropertyAccessor> propertyAccessors) : base(accessor)
        {
            propertyList = propertyAccessors as IList<PropertyAccessor> ?? propertyAccessors.ToList();
        }

        protected override void Initialize(List<PropertyAccessor> properties)
        {
            properties.AddRange(propertyList);
        }
    }
}