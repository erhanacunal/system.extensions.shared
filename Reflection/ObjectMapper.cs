﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Models;
using System.Utils;

namespace System.Reflection
{
    public static class ObjectMapper
    {

        public static TTarget Map<TSource, TTarget>(TSource src, TTarget target, ObjectMapperOptions options, params uint[] hashesOfExcludeProperties)
        {
            return Map(typeof(TSource), src, target, options, hashesOfExcludeProperties);
        }

        public static TTarget Map<TTarget>(Type sourceType, object sourceInstance, TTarget target, ObjectMapperOptions options, params uint[] hashesOfExcludeProperties)
        {
            var targetType = typeof(TTarget);
            var sourceAccessor = TypeAccessor.CreateOrGet(sourceType);
            TypeAccessor targetAccessor;
            if (sourceType == targetType || targetType.BaseType == sourceType)
                targetAccessor = sourceAccessor;
            else if (sourceType.BaseType == targetType)
                targetAccessor = sourceAccessor.BaseTypeAccessor;
            else
                targetAccessor = TypeAccessor.CreateOrGet(targetType);

            var includeNonPublic = IsFlagSet(options, ObjectMapperOptions.IncludeNonPublic);
            var invariantSearch = IsFlagSet(options, ObjectMapperOptions.InvariantSearch);
            var skipVirtualProperties = IsFlagSet(options, ObjectMapperOptions.SkipVirtualProperties);
            foreach (var property in sourceAccessor.GetReadableProperties(includeNonPublic))
            {
                var nameHash = invariantSearch ? property.InvariantNameHash : property.NameHash;
                if ((Array.IndexOf(hashesOfExcludeProperties, nameHash) != -1) || (skipVirtualProperties && property.Property.GetMethod.IsVirtual))
                    continue;
                var targetProperty = targetAccessor.FindProperty(nameHash, invariantSearch);
                targetProperty?.Setter?.SetValue(target, property.Getter.GetValue(sourceInstance));
            }
            return target;
        }

        public static void MapToList<TTarget>(Type sourceType, IEnumerable<object> sources, List<TTarget> targetList, ObjectMapperOptions options, params uint[] hashesOfExcludeProperties) where TTarget : class, new()
        {
            var targetType = typeof(TTarget);
            var sourceAccessor = TypeAccessor.CreateOrGet(sourceType);
            TypeAccessor targetAccessor;
            if (sourceType == targetType || targetType.BaseType == sourceType)
                targetAccessor = sourceAccessor;
            else if (sourceType.BaseType == targetType)
                targetAccessor = sourceAccessor.BaseTypeAccessor;
            else
                targetAccessor = TypeAccessor.CreateOrGet(targetType);

            var includeNonPublic = IsFlagSet(options, ObjectMapperOptions.IncludeNonPublic);
            var invariantSearch = IsFlagSet(options, ObjectMapperOptions.InvariantSearch);
            var skipVirtualProperties = IsFlagSet(options, ObjectMapperOptions.SkipVirtualProperties);
            var propertyList = new List<Tuple<IPropertyGetter, IPropertySetter>>();
            foreach (var property in sourceAccessor.GetReadableProperties(includeNonPublic))
            {
                var nameHash = invariantSearch ? property.InvariantNameHash : property.NameHash;
                if ((Array.IndexOf(hashesOfExcludeProperties, nameHash) != -1) || (skipVirtualProperties && property.Property.GetMethod.IsVirtual))
                    continue;
                var targetProperty = targetAccessor.FindProperty(nameHash, invariantSearch);
                if (targetProperty?.Setter != null)
                    propertyList.Add(Tuple.Create(property.Getter, targetProperty.Setter));
            }
            foreach (var source in sources)
            {
                var target = new TTarget();
                targetList.Add(target);
                foreach (var sourceTargetProperty in propertyList)
                    sourceTargetProperty.Item2.SetValue(target, sourceTargetProperty.Item1.GetValue(source));
            }
        }

        public static void MapToList<TSource, TTarget>(IEnumerable<TSource> sources, List<TTarget> targetList,
            ObjectMapperOptions options, params uint[] hashesOfExcludeProperties) where TTarget : class, new() where TSource : class
        {
            MapToList(typeof(TSource), sources, targetList, options, hashesOfExcludeProperties);
        }

        static bool IsFlagSet(ObjectMapperOptions options, ObjectMapperOptions flag) => (options & flag) == flag;


        public static TTarget Map<TSource, TTarget>(TSource src, TTarget target, ObjectMapperOptions options, params string[] excludeProperties)
        {
            return Map(src, target, options, HashUtils.JenkinsHashes(excludeProperties));
        }

#if COREFULL || COREDATA
        public static void MapFromDataReader<T>(T target, DbDataReader reader, int from = 0, int to = -1) where T : class
        {
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            if (!reader.Read()) return;
            var mapper = new TypeDataReaderMapper(TypeAccessor.CreateOrGet(typeof(T)), reader, from, to);
            mapper.Map(target);
        }

        public static T MapFromDataReader<T>(DbDataReader reader, ObjectFactory<T> factory = null, int from = 0, int to = -1) where T : class, new()
        {
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            if (!reader.Read()) return null;
            TypeDataReaderMapper mapper;
            T target;
            if (factory == null)
            {
                mapper = new TypeDataReaderMapper(TypeAccessor.CreateOrGet(typeof(T)), reader, from, to);
                target = new T();
            }
            else
                target = factory.CreateInstance(reader, out mapper);
            mapper.Map(target);
            return target;
        }
        public static List<T> MapListFromDataReader<T>(DbDataReader reader, ObjectFactory<T> factory = null, int from = 0, int to = -1) where T : class, new()
        {
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            var targetList = new List<T>();
            MapListFromDataReader(targetList.Add, reader, factory, from, to);
            return targetList;
        }

        public static void MapListFromDataReader<T>(IList<T> targetList, DbDataReader reader,
            ObjectFactory<T> factory = null, int from = 0, int to = -1) where T : class, new()
        {
            MapListFromDataReader(targetList.Add, reader, factory, from, to);
        }

        public static void MapListFromDataReaderIntf<T, TIntf>(IList<TIntf> targetList, DbDataReader reader,
            ObjectFactory<T> factory = null, int from = 0, int to = -1) where T : class, TIntf, new()
        {
            MapListFromDataReader(targetList.Add, reader, factory, from, to);
        }

        public static void MapListFromDataReader<T>(Action<T> addAction, DbDataReader reader, ObjectFactory<T> factory = null, int from = 0, int to = -1) where T : class, new()
        {
            if (addAction == null) throw new ArgumentNullException(nameof(addAction));
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            TypeDataReaderMapper mapper;
            if (factory != null)
            {
                while (reader.Read())
                {
                    var target = factory.CreateInstance(reader, out mapper);
                    mapper.Map(target);
                    addAction(target);
                }
                return;
            }
            mapper = new TypeDataReaderMapper(TypeAccessor.CreateOrGet(typeof(T)), reader, from, to);
            while (reader.Read())
            {
                var target = new T();
                mapper.Map(target);
                addAction(target);
            }
        }
#endif

        public static void MapFromDataRow<T>(T instance, DataRow row, Predicate<DataColumn> columnFilter = null)
            => MapFromDataRow(TypeAccessor.CreateOrGet<T>(), instance, row, columnFilter);

        public static void MapFromDataRow(TypeAccessor accessor, object instance, DataRow row, Predicate<DataColumn> columnFilter = null)
        {
            foreach (DataColumn column in row.Table.Columns)
            {
                if (columnFilter != null && !columnFilter(column)) continue;
                var property = accessor.FindPropertyByColumnName(column.ColumnName);
                property?.Setter.SetValue(instance, row[column]);
            }
        }

        public static void MapFromDataRow(TypeAccessor accessor, object instance, DataRow row, DataColumn[] columns)
        {
            foreach (var column in columns)
            {
                var property = accessor.FindPropertyByColumnName(column.ColumnName);
                property?.Setter.SetValue(instance, row[column]);
            }
        }

        public static void MapVerticallyFrom(TypeAccessor accessor, object instance, DbDataReader reader,
            string propertyNameField, string propertyValueField, bool mappableOnly = true)
        {
            var nameOrdinal = reader.GetOrdinal(propertyNameField);
            var valueOrdinal = reader.GetOrdinal(propertyValueField);
            var finder = mappableOnly ? (Func<string, PropertyAccessor>)accessor.FindPropertyByColumnName : (n) => accessor.FindProperty(n, true);
            while (reader.Read())
            {
                var propertyName = reader.GetString(nameOrdinal);
                var property = finder(propertyName);
                if (property?.Setter == null) continue;
                var value = reader.GetValue(valueOrdinal);
                property.Setter.SetValue(instance, value);
            }
        }

        /// <summary>
        /// Dikey tablodan property isimlerine karşılık gelen değerleri verilen örnekte ayarlar.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <param name="reader"></param>
        /// <param name="propertyNameField"></param>
        /// <param name="propertyValueField"></param>
        /// <param name="mappableOnly"></param>
        public static void MapVerticallyFrom<T>(T instance, DbDataReader reader, string propertyNameField,
            string propertyValueField, bool mappableOnly = true)
        {
            MapVerticallyFrom(TypeAccessor.CreateOrGet<T>(), instance, reader, propertyNameField, propertyValueField, mappableOnly);
        }

        public static T MapVerticallyFrom<T>(DbDataReader reader, string propertyNameField,
            string propertyValueField, bool mappableOnly = true) where T : class, new()
        {
            var instance = new T();
            MapVerticallyFrom(TypeAccessor.CreateOrGet<T>(), instance, reader, propertyNameField, propertyValueField, mappableOnly);
            return instance;
        }
    }
}