﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace System.Reflection
{
    public abstract class PropertyProvider<T> where T : class
    {
        readonly List<PropertyAccessor> propertyList;
        List<PropertyAccessor> excludedPropertyList;
        Stack<List<PropertyAccessor>> tempPropertiesStack;
        readonly IReadOnlyCollection<PropertyAccessor> readOnlyList;
        bool initialized;
        public TypeAccessor Accessor { get; }
        internal ObjectInstance Instance { get; set; }
        internal bool OnlyModifiedOrUpdatePrimaryKeys { get; set; }

        protected PropertyProvider()
        {
            Accessor = TypeAccessor.CreateOrGet<T>();
            propertyList = new List<PropertyAccessor>();
            readOnlyList = new ReadOnlyCollection<PropertyAccessor>(propertyList);
        }

        protected PropertyProvider(TypeAccessor accessor)
        {
            Accessor = accessor;
            readOnlyList = new ReadOnlyCollection<PropertyAccessor>(propertyList = new List<PropertyAccessor>());
        }

        protected abstract void Initialize(List<PropertyAccessor> properties);

        public void AddProperty(string name)
        {
            var prop = Accessor.FindPropertyByColumnName(name);
            if (prop == null) return;
            AddProperty(prop);
        }

        public void ExcludeProperties(params PropertyAccessor[] props)
        {
            if (excludedPropertyList == null)
                excludedPropertyList = new List<PropertyAccessor>();
            excludedPropertyList.AddRange(props);
            initialized = false;
            propertyList.Clear();
        }

        public void ExcludeProperties(IEnumerable<PropertyAccessor> props)
        {
            if (excludedPropertyList == null)
                excludedPropertyList = new List<PropertyAccessor>();
            excludedPropertyList.AddRange(props);
            initialized = false;
            propertyList.Clear();
        }

        public void ExcludeProperties(params string[] props)
        {
            if (excludedPropertyList == null)
                excludedPropertyList = new List<PropertyAccessor>();
            Accessor.GetPropertiesFromNames(excludedPropertyList, props);
            initialized = false;
            propertyList.Clear();
        }

        public void AddProperty(PropertyAccessor property)
        {
            propertyList.AddChecked(property);
        }

        public void AddProperties(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
                AddProperty(propertyName);
        }

        public void AddProperties(IEnumerable<string> propertyNames)
        {
            foreach (var propertyName in propertyNames)
                AddProperty(propertyName);
        }

        public static FixedPropertyProvider<T> AllMappableProperties()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return new FixedPropertyProvider<T>(accessor.MappableProperties);
        }

        public static FixedPropertyProvider<T> AllUpdatableProperties()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return new FixedPropertyProvider<T>(accessor.AllUpdatableProperties);
        }

        public IDisposable UseTempProperties(params string[] propertyNames)
        {
            if (tempPropertiesStack == null)
                tempPropertiesStack = new Stack<List<PropertyAccessor>>();
            var lst = new List<PropertyAccessor>();
            Accessor.GetPropertiesFromNames(lst, propertyNames);
            foreach (var accessor in lst)
                propertyList.AddChecked(accessor);
            tempPropertiesStack.Push(lst);
            return new UsingAction(() =>
            {
                var propList = tempPropertiesStack.Pop();
                foreach (var accessor in propList)
                    propertyList.Remove(accessor);
                if (tempPropertiesStack.Count == 0)
                    tempPropertiesStack = null;
            });
        }

        public IReadOnlyCollection<PropertyAccessor> GetProperties()
        {
            if (initialized) return readOnlyList;
            Initialize(propertyList);
            initialized = true;
            return readOnlyList;
        }
    }
}