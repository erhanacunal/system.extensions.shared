﻿using System.Collections.Generic;

namespace System.Reflection
{
    public sealed class ObjectChangeTracker : DisposableBase
    {
        readonly Dictionary<TypeAccessor, List<ObjectInstance>> registrations = new Dictionary<TypeAccessor, List<ObjectInstance>>();

        public void Register<T>(TypeAccessor schema, T obj, bool isNew)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            List<ObjectInstance> instances;
            if (!registrations.TryGetValue(schema, out instances))
            {
                instances = new List<ObjectInstance>();
                registrations.Add(schema, instances);
            }
            instances.Add(new ObjectInstance(schema, obj, isNew));
        }

        internal ObjectInstance GetObjectInstance(object obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            foreach (var reg in registrations)
            {
                foreach (var objectInstance in reg.Value)
                {
                    if (objectInstance.Instance == obj)
                        return objectInstance;
                }
            }
            throw new InvalidOperationException("Instance not registered");
        }

        public bool IsEditing(object obj) => GetObjectInstance(obj).IsEditing;
        public bool IsModified(object obj) => GetObjectInstance(obj).IsModified;

        public void BeginEdit(object instance)
        {
            GetObjectInstance(instance).BeginEdit();
        }

        public void EndEdit(object instance)
        {
            GetObjectInstance(instance).EndEdit();
        }

        public void CancelEdit(object instance)
        {
            GetObjectInstance(instance).CancelEdit();
        }


        protected override void DisposeCore()
        {
            registrations.Clear();
        }
    }

    sealed class ObjectInstance
    {
        public TypeAccessor Schema { get; }
        public object Instance { get; }
        public ObjectSnapshot Snapshot { get; }
        public bool IsEditing { get; set; }
        public bool IsModified => Snapshot.IsModified();
        public bool IsNew { get; set; }
        public ObjectInstance(TypeAccessor schema, object instance, bool isNew)
        {
            if (schema == null) throw new ArgumentNullException(nameof(schema));
            if (instance == null) throw new ArgumentNullException(nameof(instance));
            Schema = schema;
            Instance = instance;
            Snapshot = new ObjectSnapshot(this);
            IsNew = isNew;
        }

        internal void BeginEdit()
        {
            if (IsEditing)
                return;
            Snapshot.Capture();
            IsEditing = true;
        }

        internal void EndEdit()
        {
            if (!IsEditing) return;
            IsEditing = false;
        }

        internal void CancelEdit()
        {
            if (!IsEditing) return;
            IsEditing = false;
            Snapshot.Reject();
        }
    }
}