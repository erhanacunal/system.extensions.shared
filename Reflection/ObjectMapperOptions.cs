﻿using System;

namespace System.Reflection
{
    [Flags]
    public enum ObjectMapperOptions
    {
        IncludeNonPublic,
        SkipVirtualProperties,
        InvariantSearch,
        All = IncludeNonPublic | SkipVirtualProperties | InvariantSearch
    }
} 