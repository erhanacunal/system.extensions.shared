using System;
using System.Utils;

namespace System.Reflection
{
    public sealed class DefaultNullableEnumPropertySetter<TObject, TValue, TNonNullableEnumValue> : IPropertySetter
    {

        readonly Action<TObject, TValue> setter;
        public DefaultNullableEnumPropertySetter(Action<TObject, TValue> setter)
        {
            this.setter = setter;
        }

        public void SetValue(object instance, object value)
        {
            if (value != null)
                value = (TNonNullableEnumValue)value;
            setter((TObject)instance, TypeUtils.CoerceValue<TValue>(value));
        }
    }
}