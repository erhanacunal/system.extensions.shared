namespace System.Reflection
{
    public interface IPropertyGetter
    {
        object GetValue(object instance);
    }
}