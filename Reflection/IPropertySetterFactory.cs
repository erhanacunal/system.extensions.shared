using System.Reflection;

namespace System.Reflection
{
    public interface IPropertySetterFactory
    {
        IPropertySetter Create(PropertyInfo property);
    }
}