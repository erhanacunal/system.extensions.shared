﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Utils;

namespace System.Reflection
{
    public sealed class PropertyAccessor
    {
        int flags;
        TypeCodeEx? typeCode;
        public TypeAccessor TypeAccessor { get; }
        public PropertyInfo Property { get; }
        public IPropertyGetter Getter { get; }
        public IPropertySetter Setter { get; }
        public uint NameHash { get; }
        public uint InvariantNameHash { get; }
        public bool IsGetterPublic => GetFlag(PropertyAccessorFlags.IsGetterPublic);
        public bool IsSetterPublic => GetFlag(PropertyAccessorFlags.IsSetterPublic);
        public string Name { get; }
        public Type UnderlyingType { get; }
        public string ColumnName { get; set; }
        public uint ColumnNameHash { get; set; }
        public int MaxLength { get; set; }
        public int MinLength { get; set; }
        public int Order { get; set; }
        public PropertyAccessorFlags Flags => (PropertyAccessorFlags)flags;
        /// <summary>
        /// String ve Byte[] için true'dur
        /// </summary>
        public bool IsSizable => GetFlag(PropertyAccessorFlags.IsSizable);

        /// <summary>
        /// byte,sbyte,int,uint,short,ushort,long,ulong,float,decimal,double ise true'dur
        /// </summary>
        public bool IsNumericType => GetFlag(PropertyAccessorFlags.IsNumericType);

        /// <summary>
        /// DateTime property'si olup olmadığını verir
        /// </summary>
        public bool IsDateTime => GetFlag(PropertyAccessorFlags.IsDateTime);
        /// <summary>
        /// Birincil anahtar içinde olup olmadığını verir
        /// </summary>
        public bool InPrimaryKey => GetFlag(PropertyAccessorFlags.InPrimaryKey);
        /// <summary>
        /// Identity kolon olup olmadığını verir
        /// </summary>
        public bool IsIdentity => GetFlag(PropertyAccessorFlags.IsIdentity);
        /// <summary>
        /// Sadece okunur alan olup olmadığını verir
        /// </summary>
        public bool IsReadOnly => GetFlag(PropertyAccessorFlags.IsReadOnly);
        /// <summary>
        /// Gözardı edilen alan olup olmadığını verir
        /// </summary>
        public bool IsNotMapped => GetFlag(PropertyAccessorFlags.NotMapped);
        /// <summary>
        /// Eşlenebilir olup olmadığını verir
        /// </summary>
        public bool IsMappable => !GetFlag(PropertyAccessorFlags.NotMapped | PropertyAccessorFlags.IsReadOnly);

        public TypeCodeEx UnderlyingTypeCode => (TypeCodeEx)(typeCode ?? (typeCode = TypeUtils.GetTypeCodeEx(UnderlyingType)));

        internal PropertyAccessor(TypeAccessor typeAccessor, PropertyInfo property, string name, uint nameHash, uint invariantNameHash)

        {
            TypeAccessor = typeAccessor;
            Property = property;
            Name = name;
            NameHash = nameHash;
            InvariantNameHash = invariantNameHash;
            SetFlag(property.CanRead && property.GetMethod.IsPublic, PropertyAccessorFlags.IsGetterPublic);
            SetFlag(property.CanWrite && property.SetMethod.IsPublic, PropertyAccessorFlags.IsSetterPublic);
            Getter = new DefaultPropertyGetterFactory().Create(property);
            Setter = new DefaultPropertySetterFactory().Create(property);
            UnderlyingType = TypeUtils.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;

            /**/
            SetFlag(TypeUtils.IsSizable(UnderlyingType), PropertyAccessorFlags.IsSizable);
            SetFlag(TypeUtils.IsNumericType(UnderlyingType), PropertyAccessorFlags.IsNumericType);
            SetFlag(UnderlyingType == typeof(DateTime), PropertyAccessorFlags.IsDateTime);
            if (!property.CanWrite)
                SetFlag(true, PropertyAccessorFlags.IsReadOnly);
            if (!TypeUtils.IsPrimitiveType(UnderlyingType))
                SetFlag(true, PropertyAccessorFlags.NotMapped);

            ColumnName = string.Empty;
            ColumnNameHash = 0;

            MinLength = MaxLength = 0;
            Order = int.MaxValue;
            ScanAttributes(property.GetCustomAttributes(false));
        }

        void ScanAttributes(object[] attributes)
        {
            foreach (var attribute in attributes)
            {
                if (attribute is KeyAttribute)
                    SetFlag(true, PropertyAccessorFlags.InPrimaryKey);
                else if (attribute is ColumnAttribute)
                {
                    var ca = (ColumnAttribute)attribute;
                    ColumnName = ca.Name.IfNullOrEmpty(Name);
                    ColumnNameHash = HashUtils.JenkinsHash(ColumnName.ToLowerInvariant());
                    if (ca.Order >= 0)
                        Order = ca.Order;
                }
                else if (attribute is DatabaseGeneratedAttribute)
                {
                    var dga = (DatabaseGeneratedAttribute)attribute;
                    if (dga.DatabaseGeneratedOption == DatabaseGeneratedOption.Identity)
                    {
                        SetFlag(true, PropertyAccessorFlags.IsIdentity);
                        TypeAccessor.IdentityProperty = this;
                    }
                    else if (dga.DatabaseGeneratedOption == DatabaseGeneratedOption.Computed)
                        SetFlag(true, PropertyAccessorFlags.IsComputed);
                }
                else if (attribute is NotMappedAttribute)
                    SetFlag(true, PropertyAccessorFlags.NotMapped);
                else if (attribute is MinLengthAttribute)
                    MinLength = ((MinLengthAttribute)attribute).Length;
                else if (attribute is MaxLengthAttribute)
                    MaxLength = ((MaxLengthAttribute)attribute).Length;
                else if (attribute is ReadOnlyAttribute)
                    SetFlag(true, PropertyAccessorFlags.IsReadOnly);
            }

            if (ColumnName.Length != 0) return;
            ColumnName = Name;
            ColumnNameHash = InvariantNameHash;
        }

        internal void MergeAttributes(PropertyInfo property)
        {
            ScanAttributes(property.GetCustomAttributes(false));
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, Type: {Property.PropertyType.Name}";
        }

        bool GetFlag(PropertyAccessorFlags flag)
        {
            var coded = flags & (int)flag;
            return coded != 0;
        }

        void SetFlag(bool value, PropertyAccessorFlags flag)
        {
            if (value)
            {
                flags |= (int)flag;
            }
            else
            {
                flags &= ~(int)flag;
            }
        }

        public bool IsFlagSet(PropertyAccessorFlags flag)
        {
            var val = (int)flag;
            return (flags & val) == val;
        }

        public PropertyData GetPropertyData(object instance)
        {
            var result = new PropertyData(Getter.GetValue(instance), IsSizable);
            if (!IsDateTime) return result;
            var dt = (DateTime?)result.Value;
            if (dt.HasValue && dt.Value < TypeUtils.DefaultDateTime)
                result.Value = DBNull.Value;
            return result;
        }
    }
} 
