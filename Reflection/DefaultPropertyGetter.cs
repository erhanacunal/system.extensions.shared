using System;

namespace System.Reflection
{
    public sealed class DefaultPropertyGetter<TObject, TValue> : IPropertyGetter
    {

        readonly Func<TObject, TValue> getter;

        public DefaultPropertyGetter(Func<TObject, TValue> getter)
        {
            this.getter = getter;
        }

        public object GetValue(object instance) => getter((TObject)instance);
    }
}