using System;
using System.Reflection;

namespace System.Reflection
{
    public class DefaultPropertyGetterFactory : IPropertyGetterFactory
    {
        public IPropertyGetter Create(PropertyInfo property)
        {
            var types = new[] { property.DeclaringType, property.PropertyType };
            var getter = property.GetGetMethod(true);
            if (getter == null)
                return null;
            var newType = typeof(DefaultPropertyGetter<,>).MakeGenericType(types);
            return (IPropertyGetter)Activator.CreateInstance(newType, Delegate.CreateDelegate(typeof(Func<,>).MakeGenericType(types), getter));
        }
    }
}