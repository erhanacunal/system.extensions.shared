﻿using System;

namespace System.Reflection
{
    [Flags]
    public enum PropertyAccessorFlags
    {
        None = 0,
        InPrimaryKey = 1 << 0,
        IsIdentity = 1 << 1,
        IsReadOnly = 1 << 2,
        NotMapped = 1 << 3,
        IsSizable = 1 << 4,
        IsNumericType = 1 << 5,
        IsDateTime = 1 << 6,
        IsComputed = 1 << 7,
        IsGetterPublic = 1 << 8,
        IsSetterPublic = 1 << 9,

        PrimaryKeyIdentity = InPrimaryKey | IsIdentity
    }
} 