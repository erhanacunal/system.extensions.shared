﻿#if !MOBILE
using System.Collections.Generic;
using System.Data;
using System.Custom.Models;

namespace System.Reflection
{
    public sealed class TypeDataReaderMapper
    {
        public TypeAccessor Accessor { get; }
        readonly IDataRecord reader;
        public List<NameValuePair<int, IPropertySetter>> Mappings { get; } = new List<NameValuePair<int, IPropertySetter>>();

        public TypeDataReaderMapper(TypeAccessor accessor, IDataRecord reader, int from = 0, int to = -1)
        {
            Accessor = accessor;
            this.reader = reader;
            if (to == -1)
                to = reader.FieldCount;

            for (var i = from; i < to; i++)
            {
                var field = reader.GetName(i);
                var prop = accessor.FindPropertyByColumnName(field);
                if (prop == null)
                    continue;
                Mappings.Add(new NameValuePair<int, IPropertySetter>(i, prop.Setter));
            }
        }

        public void Map(object instance)
        {
            foreach (var mapping in Mappings)
            {
                var value = reader.GetValue(mapping.Name);
                mapping.Value.SetValue(instance, value);
            }
        }
    }
} 
#endif