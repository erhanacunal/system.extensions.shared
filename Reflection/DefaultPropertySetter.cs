using System;
using System.Utils;

namespace System.Reflection
{
    public sealed class DefaultPropertySetter<TObject, TValue> : IPropertySetter
    {

        readonly Action<TObject, TValue> setter;
        public DefaultPropertySetter(Action<TObject, TValue> setter)
        {
            this.setter = setter;
        }

        public void SetValue(object instance, object value) => setter((TObject)instance, TypeUtils.CoerceValue<TValue>(value));
    }
}