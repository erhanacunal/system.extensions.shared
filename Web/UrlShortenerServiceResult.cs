﻿using System;

namespace System.Web.Custom
{
    public sealed class UrlShortenerServiceResult
    {
        public bool Success { get; set; }
        public string LongUrl { get; set; }
        public string ShortUrl { get; set; }
        public string Status { get; set; }

        public UrlShortenerServiceResult()
        {
            Success = true;
        }

        public UrlShortenerServiceResult(Exception e)
        {
            Status = e.Message;
            Success = false;
        }
    }
}
