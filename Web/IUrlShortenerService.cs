namespace System.Web.Custom
{
    public interface IUrlShortenerService
    {
        UrlShortenerServiceResult ShortUrl(string url);
        UrlShortenerServiceResult ExpandUrl(string url);
    }
}