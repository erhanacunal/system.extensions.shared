﻿namespace System.Diagnostics
{
    /// <summary>
    /// Stopwatch kullanarak using ile çevrelenmiş ifadelerin hızını ölçer
    /// </summary>
    public abstract class PerformanceProfiler : IDisposable
    {
        Stopwatch timer;

        public TimeSpan Elapsed { get; private set; }

        public string Name { get; }

        protected PerformanceProfiler(string name)
        {
            Name = name;
            timer = new Stopwatch();
            timer.Start();
        }

        protected abstract void OnStart();
        protected abstract void OnStop();


        public void Dispose()
        {
            timer.Stop();
            Elapsed = timer.Elapsed;
            timer = null;
            OnStop();
        }
    }

#if !MOBILE
    /// <summary>
    /// Ölçülen değeri Trace ile çıktı penceresine (Output Window) yazar
    /// </summary>
    public sealed class DebugProfiler : PerformanceProfiler
    {

        public DebugProfiler(string name)
            : base(name)
        {
            OnStart();
        }

        protected override void OnStart()
        {
            Trace.WriteLine($"{Name} started");
        }

        protected override void OnStop()
        {
            Trace.WriteLine($"{Name} stopped -> {Elapsed.TotalMilliseconds / 1000d} seconds ({Elapsed.TotalMilliseconds} ms)");
        }

    }

    /// <summary>
    /// Ölçülen değeri Trace ile çıktı penceresine (Output Window) yazar
    /// </summary>
    public sealed class ConsoleProfiler : PerformanceProfiler
    {

        public ConsoleProfiler(string name)
            : base(name)
        {
            OnStart();
        }

        protected override void OnStart()
        {
            Console.WriteLine($"{Name} started");
        }

        protected override void OnStop()
        {
            Console.WriteLine($"{Name} stopped -> {Elapsed.TotalMilliseconds / 1000d} seconds ({Elapsed.TotalMilliseconds} ms)");
        }

    } 
#endif

    public sealed class DelegatePerformanceProfiler : PerformanceProfiler
    {
        readonly Action<double> onCompleted;

        public DelegatePerformanceProfiler(string name, Action<double> onCompleted) : base(name)
        {
            this.onCompleted = onCompleted;
            OnStart();
        }

        protected override void OnStart()
        {

        }

        protected override void OnStop()
        {
            onCompleted?.Invoke(Elapsed.TotalMilliseconds);
        }
    }
    
}
