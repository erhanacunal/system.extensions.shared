﻿#define COMPRESS
#if TELEMETRY
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics.Telemetry.Models;
using System.Custom.Services;
using System.Threading;
using System.Utils;
using Newtonsoft.Json;

namespace System.Diagnostics
{
    public static class MyTelemetryClient
    {
        static readonly object SyncRoot = new object();
        static SendFeatureUsagesTask currentSendFeatureUsagesTask;
        static readonly ConcurrentQueue<TelemetryTask> Tasks = new ConcurrentQueue<TelemetryTask>();

        static MyTelemetryClient()
        {
            var id = HashUtils.JenkinsHash(nameof(MyTelemetryClient));
            MyTaskScheduler.Register(id, PerformTasks, null, 4, MyTaskOptions.Create());
        }

        static async Task PerformTasks(object arg)
        {
            try
            {
                lock (SyncRoot)
                {
                    if (currentSendFeatureUsagesTask != null && (currentSendFeatureUsagesTask.FeatureUsages.Count > 1 || currentSendFeatureUsagesTask.PlannedSendTime <= DateTime.Now))
                    {
                        Tasks.Enqueue(currentSendFeatureUsagesTask);
                        currentSendFeatureUsagesTask = null;
                    }
                }

                if (Tasks.IsEmpty) return;
                if (!Tasks.TryDequeue(out var task)) return;
                if (task.RetryAfter > 0)
                    task.RetryAfter--;
                else
                {
                    var result = await task.Run();
                    switch (result)
                    {
                        case TelemetryTaskStatus.Retrying:
                            // tekrar sıraya al
                            Tasks.Enqueue(task);
                            break;
                        case TelemetryTaskStatus.Failed:
                            task.PersistState();
                            break;
                        case TelemetryTaskStatus.Success:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch
            {
                //
            }
        }

        static HttpClient CreateClient()
        {
            return new HttpClient { BaseAddress = new Uri(MyAppEnv.TelemetryHost) };
        }

        static string BuildUrl(string ctrl, string action, params string[] args)
        {
            var url = $"api/{ctrl}/{action}";
            if (args.Length > 0)
            {
                var qrystr = HttpUtility.ParseQueryString(string.Empty);
                StringUtils.Instance.StrArrayToNameValue(args, qrystr);
                url += "?" + qrystr;
            }
            return url;
        }

        public static void SendException(ExceptionDto exception)
        {
            Tasks.Enqueue(new SendExceptionTask(exception));
        }


        public static void SendLogEntries(IEnumerable<LogEntryDto> logEntries)
        {
            Tasks.Enqueue(new SendLogTask(logEntries));
        }

        public static void SendFeatureUsage<T>(string featureCode, T details)
        {
            lock (SyncRoot)
            {
                if (currentSendFeatureUsagesTask == null)
                    currentSendFeatureUsagesTask = new SendFeatureUsagesTask();
                currentSendFeatureUsagesTask.FeatureUsages.Add(new FeatureUsageDto
                {
                    FeatureCode = featureCode,
                    UsageDetails = JsonConvert.SerializeObject(details)
                });
            }


        }

        abstract class TelemetryTask
        {
            int retryAttempt;

            public int RetryAfter { get; set; }

            public async Task<TelemetryTaskStatus> Run()
            {
                var result = TelemetryTaskStatus.Success;
                try
                {
                    await SendRequest();
                }
                catch (HttpRequestException ex)
                {
                    if (ExceptionUtils.IsTransientHttpError(ex))
                    {
                        result = TelemetryTaskStatus.Retrying;
                        RetryAfter = (int)Math.Pow(2, ++retryAttempt);
                    }
                    else
                        result = TelemetryTaskStatus.Failed;
                }
                catch (Exception)
                {
                    result = TelemetryTaskStatus.Failed;
                }

                return result;
            }

            public abstract void PersistState();

            async Task SendRequest()
            {
                var client = CreateClient();
                using (var ms = new MemoryStream())
                {
                    string postUrl;
#if COMPRESS
                    using (var gzip = new GZipStream(ms, CompressionMode.Compress, true))
                    {
                        PrepareBody(gzip, out postUrl);
                    }
#else
                    PrepareBody(ms, out postUrl);
#endif

                    ms.Position = 0;
                    var streamContent = new StreamContent(ms);
                    streamContent.Headers.Add("Content-Encoding", "gzip");
                    streamContent.Headers.Add("Content-Type", "application/json");
                    await client.PostAsync(postUrl, streamContent);
                }
            }

            protected abstract void PrepareBody(Stream body, out string url);
        }

        enum TelemetryTaskStatus
        {
            Retrying,
            Failed,
            Success
        }

        sealed class SendLogTask : TelemetryTask
        {
            readonly IEnumerable<LogEntryDto> entries;

            public SendLogTask(IEnumerable<LogEntryDto> entries)
            {
                this.entries = entries;
            }

            public override void PersistState()
            {
                try
                {
                    var path = Path.GetTempPath();
                    var fullFile = Path.Combine(path, $"log_{DateTime.Now:yyyyMMddHHmmss}.txt");
                    var json = JsonConvert.SerializeObject(entries);
                    File.WriteAllText(fullFile, json);
                }
                catch
                {
                    //
                }
            }

            protected override void PrepareBody(Stream body, out string url)
            {
                var json = JsonConvert.SerializeObject(entries);
                var jsonData = Encoding.UTF8.GetBytes(json);
                body.Write(jsonData, 0, jsonData.Length);
                url = BuildUrl("Telemetry", "OnLogEntries", "productCode", MyAppEnv.TelemetryProductCode);
            }
        }

        sealed class SendFeatureUsagesTask : TelemetryTask
        {
            public List<FeatureUsageDto> FeatureUsages { get; } = new List<FeatureUsageDto>();
            public DateTime PlannedSendTime { get; } = DateTime.Now.AddSeconds(5);

            public override void PersistState() { }

            protected override void PrepareBody(Stream body, out string url)
            {
                var json = JsonConvert.SerializeObject(FeatureUsages);
                var jsonData = Encoding.UTF8.GetBytes(json);
                body.Write(jsonData, 0, jsonData.Length);
                url = BuildUrl("Telemetry", "OnFeatureUsages", "productCode", MyAppEnv.TelemetryProductCode);
            }
        }

        sealed class SendExceptionTask : TelemetryTask
        {
            readonly ExceptionDto exception;

            public SendExceptionTask(ExceptionDto exception)
            {
                this.exception = exception;
            }

            public override void PersistState() { }

            protected override void PrepareBody(Stream body, out string url)
            {
                var json = JsonConvert.SerializeObject(exception);
                var jsonData = Encoding.UTF8.GetBytes(json);
                body.Write(jsonData, 0, jsonData.Length);
                url = BuildUrl("Telemetry", "OnException");
            }
        }
    }
}

#endif