﻿#if TELEMETRY
using System.Collections.Generic;
using System.Diagnostics.Telemetry.Models;
using System.Custom.Services;
using System.Reflection;
using System.Threading.Tasks;
using System.Utils;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace System.Diagnostics
{
    [Target("MyTelemetry")]
    public sealed class MyTelemetryTarget : TargetWithLayout
    {
        static readonly uint Id;
        readonly List<LogEntryDto> entries = new List<LogEntryDto>();

        static MyTelemetryTarget()
        {
            Id = HashUtils.JenkinsHash("MyTelemetry");
        }

        protected override void Write(LogEventInfo logEvent)
        {
            var logEntry = new LogEntryDto
            {
                LoggerName = logEvent.LoggerName,
                LogLevel = logEvent.Level.Name,
                Message = logEvent.FormattedMessage,
                ProductCode = MyAppEnv.TelemetryProductCode,
                Timestamp = logEvent.TimeStamp
            };
            if (logEvent.Exception != null)
            {
                logEntry.Exception = new ExceptionDto
                {
                    ProductCode = MyAppEnv.TelemetryProductCode,
                    Details = JsonConvert.SerializeObject(logEvent.Exception),
                    ThrowDate = logEvent.TimeStamp
                };
                if (logEvent.Exception is ReflectionTypeLoadException rtle)
                {
                    
                }
            }

            lock (SyncRoot)
            {
                entries.Add(logEntry);
            }
            if (!MyTaskScheduler.Reset(Id))
                MyTaskScheduler.Register(Id, SendEntries, null, 8, MyTaskOptions.Create().SetSingleShot());
        }

        Task SendEntries(object arg)
        {
            LogEntryDto[] copyOfEntries;
            // kopyasını al/temizle ve göndermeyi dene
            // yoksa gönderemediklerini tekrar listeye ekle
            lock (SyncRoot)
            {
                copyOfEntries = entries.ToArray();
                entries.Clear();
            }

            MyTelemetryClient.SendLogEntries(copyOfEntries);
            return Task.FromResult(true);

        }
    }
}

#endif