﻿#if COREFULL || COREDATA
using System;
using System.Collections;
using System.Collections.Generic;
#if MEF
#if NETCOREAPP2_0
using System.Composition;
#else
using System.ComponentModel.Composition;
#endif
#endif
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Data.Models;
using System.Data.Schemas;
using System.Reflection;
using System.Utils;

namespace System.Data
{
#if MEF
    [Export(typeof(IDbHelper))]
#endif
    public sealed class SqlServerDbHelperImpl : IDbHelper
    {

        public T ExecuteReader<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql,
            params object[] parameters)
        {
            var retryCount = 0;
            retry:
            using (var cmd = cnn.CreateCommand(ct, sql, parameters))
            {
                if (RetryAction(cnn, cmd.ExecuteReader, ref retryCount, out var reader))
                    goto retry;
                using (reader)
                    return func(reader);
            }
        }

        public void ExecuteReader(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql,
            params object[] parameters)
        {
            var retryCount = 0;
            retry:
            using (var cmd = cnn.CreateCommand(ct, sql, parameters))
            {
                if (RetryAction(cnn, cmd.ExecuteReader, ref retryCount, out var reader))
                    goto retry;
                using (reader)
                    action(reader);

            }
        }

        #region Async Functions

        public async Task<T> ExecuteReaderAsync<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql, params object[] parameters)
        {
            var args = new RetryActionAsyncArgs<DbDataReader>();
            retry:
            using (var cmd = cnn.CreateCommand(ct, sql, parameters))
            {
                if (await RetryActionAsync(cnn, cmd.ExecuteReaderAsync, args))
                    goto retry;
                using (args.Value)
                    return func(args.Value);

            }
        }

        public async Task ExecuteReaderAsync(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql, params object[] parameters)
        {
            var args = new RetryActionAsyncArgs<DbDataReader>();
            retry:
            using (var cmd = cnn.CreateCommand(ct, sql, parameters))
            {
                if (await RetryActionAsync(cnn, cmd.ExecuteReaderAsync, args))
                    goto retry;
                using (args.Value)
                    action(args.Value);

            }
        }

        public Task<T> ExecuteScalarAsync<T>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return ExecuteReaderAsync(cnn, CommandType.Text,
                _ => _.Read()
                    ? TypeUtils.CoerceValue<T>(_.GetValue(0))
                    : default(T), sql, parameters);
        }

        public Task<T> ExecuteScalar2Async<T>(ConnectionData cnn, string sql, T defaultValue, params object[] parameters)
        {
            return ExecuteReaderAsync(cnn, CommandType.Text,
                _ => _.Read()
                    ? TypeUtils.CoerceValue<T>(_.GetValue(0))
                    : defaultValue, sql, parameters);
        }

        public async Task<int> ExecuteNonQueryAsync(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms)
        {
            var args = new RetryActionAsyncArgs<int>();
            retry:
            using (var cmd = cnn.CreateCommand(cmdType, sql, prms))
            {
                if (await RetryActionAsync(cnn, cmd.ExecuteNonQueryAsync, args))
                    goto retry;
                return args.Value;
            }
        }

        public Task<DataTable> ExecuteDataTableAsync(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms)
        {
            return ExecuteReaderAsync(cnn, cmdType, reader =>
            {
                var result = new DataTable();
                result.Load(reader);
                return result;
            }, sql, prms);
        }

        public Task ExecuteDataTableAsync(ConnectionData cnn, CommandType cmdType, DataTable targetTable, string sql, params object[] prms)
            => ExecuteReaderAsync(cnn, cmdType, targetTable.Load, sql, prms);

        public Task<T> ExecuteObjectAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapFromDataReader<T>(reader), sql, prms);
        }

        public Task<T> ExecuteObject2Async<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapFromDataReader(reader, factory), sql, prms);
        }

        public Task<T> ExecuteObjectVerticalAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapVerticallyFrom<T>(reader, propertyNameField, propertyValueField), sql, prms);
        }

        public Task ExecuteObjectListAsync<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(targetList, reader), sql, prms);
        }

        public Task ExecuteObjectList2Async<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(targetList, reader, factory), sql, prms);
        }

        public Task<List<T>> ExecuteObjectListAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader<T>(reader), sql, prms);
        }

        public Task<List<T>> ExecuteObjectList2Async<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReaderAsync(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(reader, factory), sql, prms);
        }

        #endregion

        public T ExecuteScalar<T>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return ExecuteReader(cnn, CommandType.Text,
                _ => _.Read()
                    ? TypeUtils.CoerceValue<T>(_.GetValue(0))
                    : default(T), sql, parameters);
        }

        public T ExecuteScalar2<T>(ConnectionData cnn, string sql, T defaultValue, params object[] parameters)
        {
            return ExecuteReader(cnn, CommandType.Text, _ => _.Read() ? TypeUtils.CoerceValue<T>(_.GetValue(0)) : defaultValue, sql, parameters);
        }

        public T[] ExecuteArray<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] parameters)
        {
            return ExecuteReader(cnn, cmdType, reader => reader.ReadArray<T>(), sql, parameters);
        }

        public Tuple<T, TK>[] ExecuteTupleArray<T, TK>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T, TK>(), sql, parameters);
        }

        public Tuple<T, TK, TM>[] ExecuteTupleArray<T, TK, TM>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T, TK, TM>(), sql, parameters);
        }

        public Tuple<T, TK, TM, TL>[] ExecuteTupleArray<T, TK, TM, TL>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T, TK, TM, TL>(), sql, parameters);
        }

        public bool LoadVariables<TV1, TV2>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.LoadVariables(ref v1, ref v2);
        }

        public bool LoadVariables<TV1, TV2, TV3>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.LoadVariables(ref v1, ref v2, ref v3);
        }

        public bool LoadVariables<TV1, TV2, TV3, TV4>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.LoadVariables(ref v1, ref v2, ref v3, ref v4);
        }

        public Tuple<TR1, TR2> ExecuteScalarResults<TR1, TR2>(ConnectionData cnn, string sql, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.ReadScalarResults<TR1, TR2>();
        }

        public void LoadScalarResults<TV1, TV2>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                reader.LoadScalarResults(ref v1, ref v2);
        }

        public void LoadScalarResults<TV1, TV2, TV3>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                reader.LoadScalarResults(ref v1, ref v2, ref v3);
        }

        public void LoadScalarResults<TV1, TV2, TV3, TV4>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                reader.LoadScalarResults(ref v1, ref v2, ref v3, ref v4);
        }

        public Tuple<T1, T2> ExecuteTuple<T1, T2>(ConnectionData cnn, string sql, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.ReadTuple<T1, T2>();
        }

        public Tuple<T1, T2, T3> ExecuteTuple<T1, T2, T3>(ConnectionData cnn, string sql, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.ReadTuple<T1, T2, T3>();
        }

        public Tuple<T1, T2, T3, T4> ExecuteTuple<T1, T2, T3, T4>(ConnectionData cnn, string sql, params object[] parameters)
        {
            using (var cmd = cnn.CreateCommand(sql, parameters))
            using (var reader = cmd.ExecuteReader())
                return reader.ReadTuple<T1, T2, T3, T4>();
        }

        public void ExecuteScalarList2<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters)
        {
            ExecuteReader(cnn, CommandType.Text, reader => reader.ReadScalarList(targetList), sql, parameters);
        }

        public void ExecuteScalarListUnionAll<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters)
        {
            ExecuteReader(cnn, CommandType.Text, reader => reader.ReadScalarListUnionAll(targetList), sql, parameters);
        }

        public void ExecuteScalarDictionary2<TK, TV>(ConnectionData cnn, IDictionary<TK, TV> targetDict, string sql, params object[] parameters)
        {
            ExecuteReader(cnn, CommandType.Text, reader => reader.ReadScalarDictionary(targetDict), sql, parameters);
        }

        public string GetTempTableName()
        {
            return "##TMP" + (8).RandomString();
        }

        static bool RetryAction<T>(ConnectionData cnn, Func<T> act, ref int retryCount, out T value)
        {
            try
            {
                value = act();
                return false;
            }
            catch (SqlException ex)
            {
                if (cnn.IsRecoverable(ex, ref retryCount))
                {
                    value = default(T);
                    return true;
                }
                ExceptionUtils.PreserveStackTrace(ex);
                throw;
            }
        }

        static async Task<bool> RetryActionAsync<T>(ConnectionData cnn, Func<Task<T>> act, RetryActionAsyncArgs<T> args)
        {
            try
            {
                args.Value = await act().ConfigureAwait(false);
                return false;
            }
            catch (SqlException ex)
            {
                if (cnn.IsRecoverable(ex, ref args.RetryCount))
                {
                    args.Value = default(T);
                    return true;
                }
                ExceptionUtils.PreserveStackTrace(ex);
                throw;
            }
        }

        sealed class RetryActionAsyncArgs<T>
        {
            internal int RetryCount;
            internal T Value;
        }

        public int ExecuteNonQuery(ConnectionData cnn, string sql, params object[] prms)
        {
            var retryCount = 0;
            retry:
            using (var cmd = cnn.CreateCommand(sql, prms))
            {
                if (RetryAction(cnn, cmd.ExecuteNonQuery, ref retryCount, out var recordsAffected))
                    goto retry;
                return recordsAffected;
            }
        }

        public int ExecuteNonQuerySp(ConnectionData cnn, string sp, params object[] prms)
        {
            var retryCount = 0;
            retry:
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = sp;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.SetCommandParameters(prms);
                if (RetryAction(cnn, cmd.ExecuteNonQuery, ref retryCount, out var recordsAffected))
                    goto retry;
                return recordsAffected;
            }
        }

        public DataTable ExecuteDataTable(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms)
        {
            return ExecuteReader(cnn, cmdType, reader =>
            {
                var result = new DataTable();
                result.Load(reader);
                return result;
            }, sql, prms);
        }

        public void ExecuteDataTable(ConnectionData cnn, CommandType cmdType, DataTable targetTable, string sql, params object[] prms)
            => ExecuteReader(cnn, cmdType, targetTable.Load, sql, prms);

        public DataSet ExecuteDataSet(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms)
        {
            return ExecuteReader(cnn, cmdType, reader =>
            {
                var result = new DataSet();
                do
                {
                    var table = new DataTable();
                    table.Load(reader);
                    result.Tables.Add(table);
                } while (!reader.IsClosed);
                return result;
            }, sql, prms);
        }


        public T ExecuteObject2<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapFromDataReader(reader, factory), sql, prms);
        }

        public T ExecuteObject<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapFromDataReader<T>(reader), sql, prms);
        }

        public T ExecuteObjectVertical<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField, params object[] prms) where T : class, new()
        {
            return ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapVerticallyFrom<T>(reader, propertyNameField, propertyValueField), sql, prms);
        }

        public void ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new()
        {
            ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(targetList, reader), sql, prms);
        }

        public void ExecuteObjectList2<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(targetList, reader, factory), sql, prms);
        }

        public List<T> ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader<T>(reader), sql, prms);
        }

        public List<T> ExecuteObjectList2<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            return ExecuteReader(cnn, cmdType, reader => ObjectMapper.MapListFromDataReader(reader, factory), sql, prms);
        }

        public void ExecuteInsertAll<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return;
            ObjectDbOperations.InsertAll(cnn, sourceList, propertyProvider);
        }

        public async Task ExecuteInsertAllAsync<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return;
            await ObjectDbOperations.InsertAllAsync(cnn, sourceList, propertyProvider);
        }
        /// <summary>
        /// Hierarşik olarak ilişkilendirilmiş tablolarının tek bir işlem ile veritabanına eklenmesini sağlar. (Kendini referans eden tablolar dahil)
        /// </summary>
        /// <returns>Zincirleme çağrının yapılacağı görev örneği</returns>
        public HierarchicalInsertAllBuilder ExecuteHierarchicalInsertAll() => new HierarchicalInsertAllBuilder();

        public RecursiveQueryBuilder<T> ExecuteRecursiveQuery<T>(string parentFieldName, long idOrParentId, bool includeThisId) where T : class, new() =>
            new RecursiveQueryBuilder<T>(this, parentFieldName, idOrParentId, includeThisId);

        public void ExecuteUpdateAll<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return;
            ObjectDbOperations.UpdateAll(cnn, sourceList, propertyProvider);
        }

        public async Task ExecuteUpdateAllAsync<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return;
            await ObjectDbOperations.UpdateAllAsync(cnn, sourceList, propertyProvider);
        }

        public void ExecuteInsert<T>(ConnectionData cnn, T instance, PropertyProvider<T> propertyProvider, bool onlyModified) where T : class
        {
            ObjectDbOperations.Insert(cnn, instance, onlyModified, propertyProvider);
        }

        public void ExecuteUpdate<T>(ConnectionData cnn, T instance, PropertyProvider<T> propertyProvider, bool updatePrimaryKeys) where T : class
        {
            ObjectDbOperations.Update(cnn, instance, updatePrimaryKeys, propertyProvider);
        }

        /// <inheritdoc />
        public SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(ConnectionData cnn, string sql,
            params object[] prms) => new SingleQueryMultipleResultsBuilder(cnn, sql, prms);

        public SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(string cnnName, string sql,
            params object[] prms) => new SingleQueryMultipleResultsBuilder(cnnName, sql, prms);

        /// <summary>
        /// Toplu insert/update işlerini gerçekleştirir
        /// </summary>
        /// <returns></returns>
        public BatchUpsertEntitiesBuilder ExecuteBatchUpsert() => new BatchUpsertEntitiesBuilder();

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnn">Sql bağlantı adı</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        public ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(ConnectionData cnn, IList<T> targetList, string sql,
            params object[] prms) where T : class, new() => new ListFromMultipleResult<T>(cnn, sql, targetList, prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnnName">Sql bağlantı adı</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        public ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(string cnnName, IList<T> targetList, string sql,
            params object[] prms) where T : class, new() => new ListFromMultipleResult<T>(cnnName, sql, targetList, prms);

        public bool DataExists2(ConnectionData cnn, string sql, params object[] prms)
        {
            return ExecuteReader(cnn, CommandType.Text, reader => reader.Read(), sql, prms);
        }

        public Task<bool> DataExistsAsync(ConnectionData cnn, string sql, params object[] prms)
        {
            return ExecuteReaderAsync(cnn, CommandType.Text, reader => reader.Read(), sql, prms);
        }

        public T ExecuteObjectAnonymous<T>(ConnectionData cnn, CommandType cmdType, string sql, T type, params object[] prms)
        {
            return ExecuteReader(cnn, cmdType, reader => reader.ReadAnonymousObject(type), sql, prms);
        }

        public IList<T> ExecuteListAnonymous<T>(ConnectionData cnn, CommandType cmdType, string sql, T type, params object[] prms)
        {
            return ExecuteReader(cnn, cmdType, reader => reader.ReadListAnonymous(type), sql, prms);
        }

        public bool HasColumn(ConnectionData cnn, string tableName, string columnName)
        {
            return ExecuteScalar<int>(cnn, @"SELECT COUNT(c.name) FROM sys.columns c
  WHERE c.object_id = OBJECT_ID(@p1) AND c.name = @p2", tableName, columnName) > 0;
        }

        public bool HasTable(ConnectionData cnn, string tableName)
        {
            return ExecuteScalar<int>(cnn, "SELECT OBJECT_ID(@p1)", tableName) > 0;
        }

        void ExecuteCustomInternal(CommandType type, ConnectionData cnn, string sql, Action<DbDataReader> customAction, params object[] parameters)
        {
            ExecuteReader(cnn, type, customAction, sql, parameters);
        }

        public void ExecuteCustomSp(ConnectionData cnn, string sql, Action<DbDataReader> customAction, params object[] parameters)
        {
            ExecuteCustomInternal(CommandType.StoredProcedure, cnn, sql, customAction, parameters);
        }

        public void ExecuteCustom(ConnectionData cnn, string sql, Action<DbDataReader> customAction, params object[] parameters)
        {
            ExecuteCustomInternal(CommandType.Text, cnn, sql, customAction, parameters);
        }

        public string CreateTableScriptFrom(DataTable tbl)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("CREATE TABLE [{0}] (", tbl.TableName);
            for (var i = 0; i < tbl.Columns.Count; i++)
            {
                if (i > 0)
                    sb.Append(",");
                var col = tbl.Columns[i];
                var propValue = col.ExtendedProperties["DBTYPE"];
                var sti = propValue == null ? SqlServerTypeInfo.GetByType(col.DataType) : SqlServerTypeInfo.GetByName((string)propValue);
                sb.AppendFormat("[{0}] ", col.ColumnName);
                if (sti.IsSizable)
                    sb.AppendFormat("{0}({1})", sti.Name, col.MaxLength == 0 ? "MAX" : col.MaxLength + "");
                else if (sti.IsNumeric && sti.HasPrecisionScale)
                {
                    sb.AppendFormat("{0}({1},{2})", sti.Name,
                        col.ExtendedProperties.GetValue("PRECISION", 19),
                        col.ExtendedProperties.GetValue("SCALE", 2));
                }
                else
                    sb.Append(sti.Name);
                foreach (DictionaryEntry kv in col.ExtendedProperties)
                {
                    if ("COLLATION".Equals(kv.Key))
                        sb.AppendFormat(" COLLATE {0}", kv.Value);
                    else if ("IDENTITY".Equals(kv.Key))
                        sb.AppendFormat(" IDENTITY(1,1)");
                }
            }
            sb.Append(")");
            return sb.ToString();
        }

        public void CreateTable(ConnectionData connection, DataTable tbl)
        {
            if (!(connection.Connection is SqlConnection sqlcnn))
                throw new ArgumentNullException(nameof(sqlcnn));
            var trn = connection.Transaction as SqlTransaction;
            using (var sbc = new SqlBulkCopy(sqlcnn, SqlBulkCopyOptions.Default, trn))
            {
                sbc.DestinationTableName = tbl.TableName;
                foreach (DataColumn column in tbl.Columns)
                {
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                }
                using (var cmd = sqlcnn.CreateCommand())
                {
                    cmd.Transaction = trn;
                    cmd.CommandText = CreateTableScriptFrom(tbl);
                    cmd.ExecuteNonQuery();
                }
                sbc.WriteToServer(tbl);
            }
        }

        public async Task CreateTableAsync(ConnectionData connection, DataTable tbl)
        {
            if (!(connection.Connection is SqlConnection sqlcnn))
                throw new ArgumentNullException(nameof(sqlcnn));
            var trn = connection.Transaction as SqlTransaction;
            using (var sbc = new SqlBulkCopy(sqlcnn, SqlBulkCopyOptions.Default, trn))
            {
                sbc.DestinationTableName = tbl.TableName;
                foreach (DataColumn column in tbl.Columns)
                {
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                }
                using (var cmd = sqlcnn.CreateCommand())
                {
                    cmd.Transaction = trn;
                    cmd.CommandText = CreateTableScriptFrom(tbl);
                    await cmd.ExecuteNonQueryAsync();
                }
                await sbc.WriteToServerAsync(tbl);
            }
        }

        public DataTable CreateDataTableFromSchema<T>(SqlTableSchema table, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            var result = new DataTable(GetTempTableName());
            var properties = propertyProvider.GetProperties();
            foreach (var property in properties)
            {
                var column = table.FindColumn(property.ColumnNameHash);
                if (column == null) continue;
                var dataColumn = result.Columns.Add(column.Name, property.UnderlyingType);
                var type = column.TypeInfo;
                dataColumn.ExtendedProperties.Add("DBTYPE", type.Name);
                if (type.IsSizable)
                {
                    dataColumn.MaxLength = column.MaxLength;
                    if (property.UnderlyingTypeCode == TypeCodeEx.String)
                        dataColumn.ExtendedProperties.Add("COLLATE", column.CollationName);
                }
                else if (type.IsNumeric && type.HasPrecisionScale)
                {
                    dataColumn.ExtendedProperties.Add("PRECISION", column.Precision);
                    dataColumn.ExtendedProperties.Add("SCALE", column.Scale);
                }
                if (column.IsIdentity)
                    dataColumn.ExtendedProperties.Add("IDENTITY", true);
            }

            var colCount = result.Columns.Count;

            foreach (var item in sourceList)
            {
                var row = new object[colCount];
                var i = 0;
                foreach (var property in properties) 
                    row[i++] = property.Getter.GetValue(item);
                result.Rows.Add(row);
            }

            return result;
        }

        public void BulkInsert(ConnectionData connection, DataTable tbl)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            if (tbl == null) throw new ArgumentNullException(nameof(tbl));
            if (!(connection.Connection is SqlConnection sqlcnn))
                throw new ArgumentNullException(nameof(sqlcnn));
            var trn = connection.Transaction as SqlTransaction;
            using (var sbc = new SqlBulkCopy(sqlcnn, SqlBulkCopyOptions.Default, trn))
            {
                sbc.DestinationTableName = tbl.TableName;
                foreach (DataColumn column in tbl.Columns)
                {
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                }
                sbc.WriteToServer(tbl);
            }
        }
        public async Task BulkInsertAsync(ConnectionData connection, DataTable tbl)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            if (tbl == null) throw new ArgumentNullException(nameof(tbl));
            if (!(connection.Connection is SqlConnection sqlcnn))
                throw new ArgumentNullException(nameof(sqlcnn));
            var trn = connection.Transaction as SqlTransaction;
            using (var sbc = new SqlBulkCopy(sqlcnn, SqlBulkCopyOptions.Default, trn))
            {
                sbc.DestinationTableName = tbl.TableName;
                foreach (DataColumn column in tbl.Columns)
                {
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                }
                await sbc.WriteToServerAsync(tbl);
            }
        }

        public Task ExecuteScalarListAsync<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters)
        {
            return ExecuteReaderAsync(cnn, CommandType.Text, reader => reader.ReadScalarList(targetList), sql, parameters);
        }
    }
}
#endif