﻿#if COREFULL || (COREDATA && LEGACYSQL)
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace System.Data
{
    public static class DataHelper
    {
        public static DataTable RunQuery(string ConnectionString_, string Query_)
        {
            using (SqlConnection cnn_ = new SqlConnection(ConnectionString_))
            {
                return SqlHelper.ExecuteDataset(cnn_, CommandType.Text, Query_).Tables[0];
            }
        }

        public static DataTable RunQueryOracle(string ConnectionString_, string Query_)
        {

            using (OracleConnection cnn_ = new OracleConnection(ConnectionString_))
            {
                return SqlHelper.ExecuteDataset(cnn_, CommandType.Text, Query_).Tables[0];
            }
        }

        public static DataSet ExcelToDataset(string fileName)
        {
            string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties = 'Excel 12.0 Xml;HDR=YES'", fileName);

            DataSet data = new DataSet();

            foreach (var sheetName in GetExcelSheetNames(connectionString))
            {
                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    var dataTable = new DataTable();
                    string query = string.Format("SELECT * FROM [{0}]", sheetName);
                    con.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter(query, con);
                    adapter.Fill(dataTable);
                    data.Tables.Add(dataTable);
                }
            }

            return data;
        }

        static string[] GetExcelSheetNames(string connectionString)
        {
            using (var con = new OleDbConnection(connectionString))
            {
                con.Open();
                using (var dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null))
                {
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheetNames = new String[dt.Rows.Count];
                    int i = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheetNames[i] = row["TABLE_NAME"].ToString();
                        i++;
                    }

                    return excelSheetNames;
                }
            }
        }
    }
}

#endif