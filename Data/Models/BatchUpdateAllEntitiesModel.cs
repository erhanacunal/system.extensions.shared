﻿#if COREFULL||COREDATA
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace System.Data.Models
{
    sealed class BatchUpdateAllEntitiesModel<T> : BatchUpsertEntitiesModel where T : class
    {
        readonly IList<T> sourceList;
        readonly PropertyProvider<T> propertyProvider;

        public BatchUpdateAllEntitiesModel(IList<T> sourceList, PropertyProvider<T> propertyProvider)
        {
            this.sourceList = sourceList;
            this.propertyProvider = propertyProvider;
        }

        public override void Execute(ConnectionData cnn)
        {
            ObjectDbOperations.UpdateAll(cnn, sourceList, propertyProvider);
        }
    }
} 
#endif