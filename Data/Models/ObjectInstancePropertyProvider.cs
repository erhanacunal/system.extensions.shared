﻿#if COREFULL || COREDATA
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace System.Data.Models
{
    sealed class ObjectInstancePropertyProvider<T> : PropertyProvider<T> where T : class
    {
        readonly bool forInsert;

        public ObjectInstancePropertyProvider(ObjectInstance instance, bool forInsert, bool onlyModifiedOrUpdatePrimaryKeys)
        {
            Instance = instance;
            this.forInsert = forInsert;
            OnlyModifiedOrUpdatePrimaryKeys = onlyModifiedOrUpdatePrimaryKeys;
        }

        protected override void Initialize(List<PropertyAccessor> properties)
        {
            if (forInsert)
            {
                if (OnlyModifiedOrUpdatePrimaryKeys && Instance != null && Instance.IsEditing)
                {
                    properties.AddRange(Instance.Snapshot.GetModifiedProperties());
                    return;
                }
                properties.AddRange(Accessor.AllUpdatableProperties);
                return;
            }
            if (Instance != null && Instance.IsEditing)
            {
                properties.AddRange(Instance
                    .Snapshot
                    .GetModifiedProperties()
                    .Where(prop => OnlyModifiedOrUpdatePrimaryKeys || !prop.InPrimaryKey));
                return;
            }
            properties.AddRange(Accessor
                .MappableProperties
                .Where(prop => OnlyModifiedOrUpdatePrimaryKeys || !prop.InPrimaryKey));
        }
    }
} 
#endif