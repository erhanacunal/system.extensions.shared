﻿#if COREFULL||COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace System.Data.Models
{
    public sealed class BatchUpsertEntitiesBuilder
    {
        readonly List<BatchUpsertEntitiesModel> models = new List<BatchUpsertEntitiesModel>();
        HierarchicalInsertAllBuilder builder;

        public BatchUpsertEntitiesBuilder InsertAll<T>(IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return this;
            models.Add(new BatchInsertAllEntitiesModel<T>(sourceList, propertyProvider));
            return this;
        }

        public BatchUpsertEntitiesBuilder UpdateAll<T>(IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (sourceList.Count == 0) return this;
            models.Add(new BatchUpdateAllEntitiesModel<T>(sourceList, propertyProvider));
            return this;
        }

        public BatchUpsertEntitiesBuilder InsertAllHierarchical<T>(IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            if (builder == null)
                builder = new HierarchicalInsertAllBuilder();
            builder.InsertAll(sourceList, propertyProvider);
            return this;
        }

        /// <summary>
        /// İki tabloyu birbiriyle mantıksal olarak ilişkilendirir.
        /// </summary>
        /// <param name="data">ÜstTablo=AltTablo.Alan; Örn: Company=Department.CompanyID</param>
        /// <returns></returns>
        public BatchUpsertEntitiesBuilder AddRelationship(string data)
        {
            if (builder == null)
                throw new InvalidOperationException("Method cannot be called before InsertAllHierarchical() method");
            builder.AddRelationship(data);
            return this;
        }

        public void Execute(string cnnName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                foreach (var model in models)
                    model.Execute(cnn);
                builder?.Execute(cnn);
            }
        }
    }
} 
#endif