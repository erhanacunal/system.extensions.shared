#if COREFULL ||COREDATA
using System;

namespace System.Data.Models
{
    sealed class Relationship
    {
        public string ParentTable { get; }
        public string ChildTable { get; }
        public string ChildColumn { get; }
        public IInsertAllModel ParentTableModel { get; internal set; }
        public IInsertAllModel ChildTableModel { get; internal set; }

        public Relationship(string parentTable, string childTable, string childColumn)
        {
            if (string.IsNullOrEmpty(parentTable)) throw new ArgumentException("Value cannot be null or empty.", nameof(parentTable));
            if (string.IsNullOrEmpty(childTable)) throw new ArgumentException("Value cannot be null or empty.", nameof(childTable));
            if (string.IsNullOrEmpty(childColumn)) throw new ArgumentException("Value cannot be null or empty.", nameof(childColumn));
            ParentTable = parentTable;
            ChildTable = childTable;
            ChildColumn = childColumn;
        }

        public Relationship(string pack)
        {
            var parts = pack.Split('.', '=');
            if (parts.Length != 3)
                throw new InvalidOperationException("Relationship definition format is invalid. Format must be 'ParentTable=ChildTable.Column'");
            ParentTable = parts[0];
            ChildTable = parts[1];
            ChildColumn = parts[2];
        }
    }
} 
#endif