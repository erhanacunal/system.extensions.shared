﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Reflection;
using System.Utils;

namespace System.Data.Models
{
    public sealed class ObjectDbContext : DisposableBase
    {
        ConnectionData connection;
        internal ObjectChangeTracker ChangeTracker { get; private set; }

        public ObjectDbContext(string connectionName, bool withChangeTracking = false)
        {
            connection = ConnectionManager.Get(connectionName);
            if (withChangeTracking)
                ChangeTracker = new ObjectChangeTracker();
        }

        public int ExecuteNonQuery(string sql, params object[] prms)
        {
            using (var cmd = connection.CreateCommand(sql, prms))
                return cmd.ExecuteNonQuery();
        }

        public T GetById<T>(int oid) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var columns = accessor.GetSelectableColumns();
            var sql = $"SELECT {columns} FROM {accessor.TableName} WHERE {accessor.IdentityProperty.ColumnName} = @p1";
            return DbHelpers.ExecuteReader(connection, CommandType.Text, reader =>
            {
                var obj = default(T);
                if (reader.Read())
                {
                    obj = new T();
                    ChangeTracker?.Register(accessor, obj, false);
                    new TypeDataReaderMapper(accessor, reader).Map(obj);
                }
                return obj;
            }, sql, oid);
        }

        public T ExecuteObject<T>(CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteReader(connection, CommandType.Text, reader =>
            {
                if (!reader.Read()) return null;
                var obj = new T();
                ChangeTracker?.Register(accessor, obj, false);
                new TypeDataReaderMapper(accessor, reader).Map(obj);
                return obj;
            }, sql, prms);
        }

        public void ExecuteList<T>(CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            DbHelpers.ExecuteReader(connection, cmdType, reader =>
            {
                var mapper = new TypeDataReaderMapper(accessor, reader);
                while (reader.Read())
                {
                    var obj = new T();
                    ChangeTracker?.Register(accessor, obj, false);
                    mapper.Map(obj);
                    targetList.Add(obj);
                }
            }, sql, prms);
        }

        public void ExecuteList<T>(CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            DbHelpers.ExecuteReader(connection, cmdType, reader =>
            {
                while (reader.Read())
                {
                    TypeDataReaderMapper mapper;
                    var obj = factory.CreateInstance(reader, out mapper);
                    ChangeTracker?.Register(mapper.Accessor, obj, false);
                    mapper.Map(obj);
                    targetList.Add(obj);
                }
            }, sql, prms);
        }

        public IList<T> ExecuteList<T>(CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            var result = new List<T>();
            ExecuteList(cmdType, result, sql, prms);
            return result;
        }

        /// <summary>
        /// Verilen örneği verilen bağlantıdaki veritabanından siler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="instance">Silinecek HPO örneği</param>
        public void Delete<T>(T instance) where T : class
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var retryCount = 0;
            retry:
            using (var cmd = connection.CreateCommand())
            {
                var sb = new StringBuilder();
                sb.AppendFormat("DELETE FROM {0}", accessor.TableName);
                sb.Append(" WHERE ");
                if (accessor.IdentityProperty != null)
                {
                    sb.AppendFormat("{0} = @p1", accessor.IdentityProperty.ColumnName);
                    cmd.AddWithValue("@p1", accessor.IdentityProperty.GetPropertyData(instance).Value);
                }
                else
                {
                    throw new NotSupportedException("Identity kolon olmadan silme desteklenmiyor");
                }
                cmd.CommandText = sb.ToString();
                try
                {
                    cmd.ExecuteNonQuery();
                    var objInst = ChangeTracker?.GetObjectInstance(instance);
                    if (objInst != null)
                        objInst.IsNew = true;
                }
                catch (SqlException ex)
                {
                    if (!connection.IsRecoverable(ex, ref retryCount))
                    {
                        ExceptionUtils.PreserveStackTrace(ex);
                        throw;
                    }
                    goto retry;
                }
            }
        }

        /// <summary>
        /// Verilen örnekleri SqlBulkCopy ile geçici bir tabloya insert eder
        /// daha sonra gerçek tabloyu bu tabloyla günceller
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="sourceList">Örnekler</param>
        /// <param name="propertyProvider"></param>
        /// <remarks>
        /// Eklencekler ile güncellenecekleri birbirinden ayırır
        /// </remarks>
        public void UpdateAll<T>(IEnumerable<T> sourceList, PropertyProvider<T> propertyProvider = null) where T : class
        {
            ObjectDbOperations.UpdateAll(connection, sourceList, propertyProvider, ChangeTracker);
        }

        public void Save<T>(T instance, bool onlyModifiedOrUpdatePrimaryKeys = false, PropertyProvider<T> propertyProvider = null) where T : class
        {
            var objectInstance = ChangeTracker?.GetObjectInstance(instance);
            propertyProvider = propertyProvider ??
                               new ObjectInstancePropertyProvider<T>(objectInstance, objectInstance?.IsNew ?? false,
                                   onlyModifiedOrUpdatePrimaryKeys);

            if (objectInstance != null && objectInstance.IsNew)
                ObjectDbOperations.InsertCore(connection, instance, propertyProvider);
            else
                ObjectDbOperations.UpdateCore(connection, instance, propertyProvider);


        }

        /// <summary>
        /// Tek bir Obje'den türeyen sınıfı verilen bağlantıdaki veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="instance">Eklenecek örnek</param>
        /// <param name="onlyModified"></param>
        /// <param name="propertyProvider"></param>
        public void Insert<T>(T instance, bool onlyModified = false, PropertyProvider<T> propertyProvider = null) where T : class
        {
            ObjectDbOperations.Insert(connection, instance, onlyModified, propertyProvider, ChangeTracker);
        }

        /// <summary>
        /// Verilen tüm Obje'den türeyen sınıf örneklerini verilen bağlantıdaki veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="sourceList">Eklenecek örnek</param>
        /// <param name="propertyProvider"></param>
        public void InsertAll<T>(IList<T> sourceList, PropertyProvider<T> propertyProvider = null) where T : class
        {
            ObjectDbOperations.InsertAll(connection, sourceList, propertyProvider, ChangeTracker);
        }

        /// <summary>
        /// Tek bir Obje'den türeyen sınıfı verilen bağlantıdaki veritabanına günceller
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="instance">Güncellenecek örnek</param>
        /// <param name="updatePrimaryKeys"></param>
        /// <param name="propertyProvider"></param>
        public void Update<T>(T instance, bool updatePrimaryKeys = false, PropertyProvider<T> propertyProvider = null) where T : class
        {
            ObjectDbOperations.Update(connection, instance, updatePrimaryKeys, propertyProvider, ChangeTracker);
        }

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        public ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(IList<T> targetList, string sql,
            params object[] prms) where T : class, new()
        {
            return new ListFromMultipleResult<T>(connection, sql, targetList, prms);
        }

        /// <summary>
        /// Sql ifadesinden gelen birden fazla sonucu, birbirinden bağımsız belirtilen biçimlerde aktarır 
        /// </summary>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>.
        public SingleQueryMultipleResultsBuilder ExecuteMultipleTargetsFromMultipleResults(string sql,
            params object[] prms)
        {
            return new SingleQueryMultipleResultsBuilder(connection, sql, prms);
        }

        protected override void DisposeCore()
        {
            ChangeTracker?.Dispose();
            ChangeTracker = null;
            connection.Dispose();
            connection = null;
        }
    }
}
#endif