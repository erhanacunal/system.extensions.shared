﻿using System.Data;

#if COREFULL || COREDATA
namespace System.Data.Models
{
    public abstract class BatchUpsertEntitiesModel
    {
        public abstract void Execute(ConnectionData cnn);
    }
}

#endif