﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using System.Reflection;
using System.Utils;

namespace System.Data.Models
{
    public sealed class SingleQueryMultipleResultsBuilder
    {
        readonly ConnectionData connection;
        readonly string sql;
        readonly object[] prms;
        readonly Queue<ResultType> types = new Queue<ResultType>();
        readonly bool disposeConnection;

        public SingleQueryMultipleResultsBuilder(ConnectionData connection, string sql, params object[] prms)
        {
            this.connection = connection;
            this.sql = sql;
            this.prms = prms;
            disposeConnection = false;
        }

        public SingleQueryMultipleResultsBuilder(string cnnName, string sql, params object[] prms)
        {
            connection = ConnectionManager.Get(cnnName);
            this.sql = sql;
            this.prms = prms;
            disposeConnection = true;
        }

        public SingleQueryMultipleResultsBuilder ObjectList<T>(IList<T> targetList, ObjectFactory<T> factory = null) where T : class, new()
        {
            types.Enqueue(new ResultObjectList<T>(targetList, factory));
            return this;
        }

        public SingleQueryMultipleResultsBuilder Object<T>(Action<T> setter, ObjectFactory<T> factory = null) where T : class, new()
        {
            types.Enqueue(new ResultObjectValue<T>(setter, factory));
            return this;
        }

        public SingleQueryMultipleResultsBuilder ScalarDictionary<TK, TV>(IDictionary<TK, TV> targetDictionary)
        {
            types.Enqueue(new ResultScalarDictionaryType<TK, TV>(targetDictionary));
            return this;
        }

        public SingleQueryMultipleResultsBuilder ScalarList<T>(IList<T> targetDictionary)
        {
            types.Enqueue(new ResultScalarListType<T>(targetDictionary));
            return this;
        }

        public SingleQueryMultipleResultsBuilder ScalarValue<T>(Action<T> setter)
        {
            types.Enqueue(new ResultScalarValue<T>(setter));
            return this;
        }

        public SingleQueryMultipleResultsBuilder TupleValue<T1, T2>(Action<Tuple<T1, T2>> setter)
        {
            types.Enqueue(new ResultTuple2Value<T1, T2>(setter));
            return this;
        }

        public SingleQueryMultipleResultsBuilder TupleValue<T1, T2, T3>(Action<Tuple<T1, T2, T3>> setter)
        {
            types.Enqueue(new ResultTuple3Value<T1, T2, T3>(setter));
            return this;
        }

        public void Execute()
        {
            if (types.Count == 0)
                return;
            try
            {
                DbHelpers.ExecuteReader(connection, CommandType.Text, reader =>
                {
                    do
                    {
                        types.Dequeue().Execute(reader);
                    } while (types.Count > 0 && reader.NextResult());
                }, sql, prms);
            }
            finally
            {
                if (disposeConnection)
                    connection.Dispose();
            }
        }

        public async Task ExecuteAsync()
        {
            if (types.Count == 0)
                return;
            try
            {
                await DbHelpers.ExecuteReaderAsync(connection, CommandType.Text, reader =>
                {
                    do
                    {
                        types.Dequeue().Execute(reader);
                    } while (types.Count > 0 && reader.NextResult());
                }, sql, prms);
            }
            finally
            {
                if (disposeConnection)
                    connection.Dispose();
            }
        }

        abstract class ResultType
        {
            internal abstract void Execute(DbDataReader reader);
        }

        sealed class ResultObjectList<T> : ResultType where T : class, new()
        {
            readonly IList<T> targetList;
            readonly ObjectFactory<T> factory;

            public ResultObjectList(IList<T> targetList, ObjectFactory<T> factory)
            {
                this.targetList = targetList;
                this.factory = factory;
            }

            internal override void Execute(DbDataReader reader)
            {
                ObjectMapper.MapListFromDataReader(targetList, reader, factory);
            }
        }

        sealed class ResultScalarDictionaryType<TK, TV> : ResultType
        {
            readonly IDictionary<TK, TV> targetDictionary;

            public ResultScalarDictionaryType(IDictionary<TK, TV> targetDictionary)
            {
                this.targetDictionary = targetDictionary;
            }

            internal override void Execute(DbDataReader reader)
            {
                reader.ReadScalarDictionary(targetDictionary);
            }
        }

        sealed class ResultScalarListType<T> : ResultType
        {
            readonly IList<T> targetList;

            public ResultScalarListType(IList<T> targetList)
            {
                this.targetList = targetList;
            }

            internal override void Execute(DbDataReader reader)
            {
                reader.ReadScalarList(targetList);
            }
        }

        sealed class ResultScalarValue<T> : ResultType
        {
            readonly Action<T> setter;

            public ResultScalarValue(Action<T> setter)
            {
                this.setter = setter;
            }

            internal override void Execute(DbDataReader reader)
            {
                if (!reader.Read()) return;
                setter(TypeUtils.CoerceValue<T>(reader.GetValue(0)));
            }
        }

        sealed class ResultObjectValue<T> : ResultType where T : class, new()
        {
            readonly Action<T> setter;
            readonly ObjectFactory<T> factory;

            public ResultObjectValue(Action<T> setter, ObjectFactory<T> factory)
            {
                this.setter = setter;
                this.factory = factory;
            }

            internal override void Execute(DbDataReader reader)
            {
                setter(ObjectMapper.MapFromDataReader(reader, factory));
            }
        }

        sealed class ResultTuple2Value<T1, T2> : ResultType
        {
            readonly Action<Tuple<T1, T2>> setter;

            public ResultTuple2Value(Action<Tuple<T1, T2>> setter)
            {
                this.setter = setter;
            }

            internal override void Execute(DbDataReader reader)
            {
                setter(reader.ReadTuple<T1, T2>());
            }
        }

        sealed class ResultTuple3Value<T1, T2, T3> : ResultType
        {
            readonly Action<Tuple<T1, T2, T3>> setter;

            public ResultTuple3Value(Action<Tuple<T1, T2, T3>> setter)
            {
                this.setter = setter;
            }

            internal override void Execute(DbDataReader reader)
            {
                setter(reader.ReadTuple<T1, T2, T3>());
            }
        }
    }
}
#endif