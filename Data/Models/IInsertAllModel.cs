#if COREFULL || COREDATA
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Reflection;

namespace System.Data.Models
{
    interface IInsertAllModel
    {
        void GeneratePushSql(ConnectionData cnn, SqlCommand cmd, StringBuilder sql);
        void GeneratePullSql(StringBuilder sql);
        void ProcessResult(DbDataReader reader);
        string TableName { get; }
        TypeAccessor Accessor { get; }
        void ReIndex();
        long? FindRowIndexByIdentityValue(object value);
    }
} 
#endif