﻿#if COREFULL ||COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Reflection;

namespace System.Data.Models
{
    public sealed class RecursiveQueryBuilder<T> where T : class
    {
        readonly IDbHelper helper;
        readonly string parentFieldName;
        readonly TypeAccessor accessor;
        readonly List<string> fields = new List<string>();
        readonly SqlParamBuilder sqlParamBuilder = new SqlParamBuilder(true);
        string initialCondition;
        string tempTableName;
        string processResultSql;
        internal RecursiveQueryBuilder(IDbHelper helper, string parentFieldName, long idOrParentId, bool includeThisId)
        {
            if (string.IsNullOrWhiteSpace(parentFieldName)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(parentFieldName));
            if (idOrParentId <= 0) throw new ArgumentOutOfRangeException(nameof(idOrParentId));
            this.helper = helper;
            this.parentFieldName = parentFieldName;
            accessor = TypeAccessor.CreateOrGet<T>();
            sqlParamBuilder.Add2("@idOrParentId", idOrParentId);
            fields.Add(accessor.IdentityProperty.ColumnName);
            fields.Add(parentFieldName);
            initialCondition = includeThisId
                ? $"( {accessor.IdentityProperty.ColumnName} = @idOrParentId )"
                : "( ( $PF$ = @idOrParentId ) OR ( $PF$ IS NULL AND @idOrParentId IS NULL ) )";
        }

        public RecursiveQueryBuilder<T> SetInitialCondition(string condition, bool append = false)
        {
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            initialCondition = append ? string.Concat(initialCondition, " ", condition) : condition;
            return this;
        }

        public RecursiveQueryBuilder<T> AddFields(params string[] fieldNames)
        {
            fields.AddRange(fieldNames);
            return this;
        }

        public RecursiveQueryBuilder<T> AddSqlParameter(string name, object value)
        {
            sqlParamBuilder.Add2(name, value);
            return this;
        }

        public RecursiveQueryBuilder<T> SetTempTableVariable(string name)
        {
            tempTableName = name ?? throw new ArgumentNullException(nameof(name));
            return this;
        }

        public RecursiveQueryBuilder<T> SetProcessResultSql(string sql)
        {
            processResultSql = sql ?? throw new ArgumentNullException(nameof(sql));
            return this;
        }

        public Tuple<string, SqlParamBuilder> BuildSql(ConnectionData cnn)
        {
            if (string.IsNullOrWhiteSpace(processResultSql))
                throw new ArgumentNullException(nameof(processResultSql));
            var sql = new StringBuilder();
            var whereCondition = string.IsNullOrWhiteSpace(initialCondition)
                ? string.Empty
                : string.Concat(" WHERE ", initialCondition.Replace("$PF$", parentFieldName));
            var tgtFields = string.Join(", ", fields.Select(f => string.Concat("tgt.", f)));
            var allFields = string.Join(", ", fields);

            if (!string.IsNullOrWhiteSpace(tempTableName))
            {
                var properties = new List<PropertyAccessor>();
                accessor.GetPropertiesFromNames(properties, fields);
                var metadata = cnn.DbConnectionName.Database.GetCachedTable(accessor.TableName);
                sql.AppendFormat("\r\nDECLARE @{0} TABLE (\r\n", tempTableName);
                var i = 0;
                foreach (var property in properties)
                {
                    var column = metadata.FindColumn(property.ColumnNameHash);
                    if (column == null) continue;
                    if (i++ > 0) sql.Append(",\r\n");
                    sql.Append($"  {column.GetDefinition()}");
                }
                sql.AppendLine("\r\n);");
            }

            sql.AppendFormat(@"WITH cte({0}) AS (
  SELECT {0} FROM {1}{2}
  UNION ALL
  SELECT {3} FROM {1} tgt INNER JOIN cte c ON tgt.{4} = c.ID)", allFields, accessor.TableName, whereCondition, tgtFields, parentFieldName);
            if (!string.IsNullOrWhiteSpace(tempTableName))
                sql.AppendLine($"\r\nINSERT INTO @{tempTableName}({allFields}) SELECT {allFields} FROM cte;");

            sql.AppendLine(processResultSql.Replace("$TABLE$", accessor.TableName));
            return Tuple.Create(sql.ToString(), sqlParamBuilder);
        }

        public void ExecuteNonQuery(string connectionName)
        {
            using (var cnn = ConnectionManager.Get(connectionName))
            {
                var sql = BuildSql(cnn);
                helper.ExecuteNonQuery(cnn, sql.Item1, sql.Item2.Build());
            }
        }

        public void ExecuteObjectList<TK>(string connectionName, IList<TK> targetList) where TK : class, new()
        {
            using (var cnn = ConnectionManager.Get(connectionName))
            {
                var sql = BuildSql(cnn);
                helper.ExecuteObjectList(cnn, CommandType.Text, targetList, sql.Item1, sql.Item2.Build());
            }
        }

    }
}

#endif