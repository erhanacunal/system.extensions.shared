﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;

namespace System.Data.Models
{
    public sealed class HierarchicalInsertAllBuilder
    {
        readonly List<IInsertAllModel> models = new List<IInsertAllModel>();
        readonly List<Relationship> relationships = new List<Relationship>();
        bool tablesOrdered;
        /// <summary>
        /// Yeni bir toplu ekleme işlemi ekler, Eğer bir alt tablo ise ilişkilendirilmiş alandaki değer bir üst tablo listesindeki indeksini göstermelidir.
        /// </summary>
        /// <typeparam name="T">Eklenecek varlık tipi</typeparam>
        /// <param name="sourceList">Kaynak liste</param>
        /// <param name="properties">Hangi property'ler eklenecek</param>
        /// <returns></returns>
        public HierarchicalInsertAllBuilder InsertAll<T>(IList<T> sourceList, PropertyProvider<T> properties) where T : class
        {
            models.Add(new InsertAllModel<T>(this, sourceList, properties));
            return this;
        }
        /// <summary>
        /// İki tabloyu birbiriyle mantıksal olarak ilişkilendirir.
        /// </summary>
        /// <param name="data">ÜstTablo=AltTablo.Alan; Örn: Company=Department.CompanyID</param>
        /// <returns></returns>
        public HierarchicalInsertAllBuilder AddRelationship(string data)
        {
            relationships.Add(new Relationship(data));
            return this;
        }

        public HierarchicalInsertAllBuilder ReIndex()
        {
            ReorderTables();
            foreach (var model in models)
                model.ReIndex();
            return this;
        }

        //IInsertAllModel FindModelByTableName(string tableName) => models.Find(_ => string.Equals(_.TableName, tableName, StringComparison.OrdinalIgnoreCase));
        int FindIndexOfModelByTableName(string tableName) => models.FindIndex(_ => string.Equals(_.TableName, tableName, StringComparison.OrdinalIgnoreCase));

        void ReorderTables()
        {
            if (tablesOrdered) return;
            foreach (var relationship in relationships)
            {
                var parentIndex = FindIndexOfModelByTableName(relationship.ParentTable);
                if (parentIndex == -1)
                    throw new InvalidOperationException(relationship.ParentTable + " table must be in insert queue");
                var childIndex = FindIndexOfModelByTableName(relationship.ChildTable);
                if (childIndex == -1)
                    throw new InvalidOperationException(relationship.ChildTable + " table must be in insert queue");
                relationship.ParentTableModel = models[parentIndex];
                relationship.ChildTableModel = models[childIndex];
                if (childIndex >= parentIndex) continue;
                models.RemoveAt(parentIndex);
                models.Insert(childIndex, relationship.ParentTableModel);
            }
            tablesOrdered = true;
        }

        public void Execute(string cnnName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Execute(cnn);
        }
        public void Execute(ConnectionData cnn)
        {
            ReorderTables();
            using (var cmd = (SqlCommand)cnn.CreateCommand())
            {
                var sql = new StringBuilder();
                foreach (var model in models)
                    model.GeneratePushSql(cnn, cmd, sql);
                foreach (var model in models)
                    model.GeneratePullSql(sql);
                cmd.CommandText = sql.ToString();
                using (var reader = cmd.ExecuteReader())
                {
                    var i = 0;
                    do
                    {
                        models[i++].ProcessResult(reader);
                    } while (reader.NextResult());
                }
            }

        }
        /// <summary>
        /// Verilen tablo isminin diğer tablolarda çocuk olarak ayarlanmış ilişkileri döndür
        /// </summary>
        /// <param name="childTableName">Aranacak tablo adı</param>
        /// <returns></returns>
        internal Relationship[] GetMyParentRelationships(string childTableName) => relationships
            .Where(_ => string.Equals(_.ChildTable, childTableName, StringComparison.OrdinalIgnoreCase))
            .ToArray();
    }
}

#endif