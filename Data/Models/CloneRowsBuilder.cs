﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.Extensions;
using System.Reflection;
using System.Text;
using System.Utils;
using JetBrains.Annotations;

namespace System.Data.Models
{
    public sealed class CloneRowsBuilder<TEntity> where TEntity : class, new()
    {
        readonly string cnnName;
        readonly Dictionary<string, string> customColumnValues = new Dictionary<string, string>();
        readonly SqlParamBuilder paramBuilder = new SqlParamBuilder(true);
        readonly List<uint> excludedColumns = new List<uint>();
        readonly TypeAccessor accessor;
        Mode mode = Mode.None;
        string whereClause;
        public CloneRowsBuilder([NotNull] string cnnName)
        {
            if (string.IsNullOrWhiteSpace(cnnName)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(cnnName));
            this.cnnName = cnnName;
            accessor = TypeAccessor.CreateOrGet<TEntity>();
        }

        void EnsureModeNotSet()
        {
            if (mode != Mode.None) throw new InvalidOperationException("Mode is already specified!");
        }

        void EnsureModeSet()
        {
            if (mode == Mode.None)
                throw new InvalidOperationException("Please use one of these : BySourceIds or ByWhereClause");
        }

        public CloneRowsBuilder<TEntity> BySourceIds<TId>(IEnumerable<TId> sourceIds) where TId : struct
        {
            EnsureModeNotSet();
            if (sourceIds == null) throw new ArgumentNullException(nameof(sourceIds));
            paramBuilder.Add2("@sourceIds", sourceIds.ToSingleColumnDataTable("dbo.ArrayOfBigInt", "Value"));
            mode = Mode.SourceIds;
            return this;
        }

        public CloneRowsBuilder<TEntity> ByWhereClause(string clause, params object[] prms)
        {
            EnsureModeNotSet();
            whereClause = clause;
            mode = Mode.WhereClause;
            paramBuilder.AddRange(prms);
            return this;
        }

        public CloneRowsBuilder<TEntity> ChangeColumnValue([NotNull] string columnName, [NotNull] string value)
        {
            if (columnName == null) throw new ArgumentNullException(nameof(columnName));
            customColumnValues[columnName] = value ?? throw new ArgumentNullException(nameof(value));
            return this;
        }

        public CloneRowsBuilder<TEntity> SqlParameter([NotNull] string name, [NotNull] object value)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            paramBuilder.Add2(name, value);
            return this;
        }

        public CloneRowsBuilder<TEntity> SqlParameters(params object[] prms)
        {
            paramBuilder.AddRange(prms);
            return this;
        }

        public CloneRowsBuilder<TEntity> ExcludeColumns(params string[] columns)
        {
            excludedColumns.AddRange(HashUtils.JenkinsHashes(columns));
            return this;
        }

        void BuildSql(StringBuilder sql, ConnectionData cnn)
        {

            var schema = cnn.DbConnectionName.Database.GetCachedTable(accessor.TableName);
            sql.AppendLine("DECLARE @sourceRecords TABLE (");
            var i = 0;
            var columnNames = new List<string>(schema.Columns.Count - excludedColumns.Count);
            foreach (var column in schema.Columns)
            {
                if (excludedColumns.IndexOf(column.NameHash) != -1) continue;
                if (i++ > 0)
                    sql.Append(", ");
                cnn.SqlDialect.GetColumnDefinition(sql, column, false);
                columnNames.Add(column.Name);
            }

            sql.AppendLine(");");
            sql.AppendFormat("INSERT @sourceRecords ({0})\r\n", string.Join(", ", columnNames));
            sql.AppendFormat("  SELECT {0} FROM {1} tbl",
                StringUtils.Instance.AddPrefixThenJoin(columnNames, "tbl."), accessor.TableName);
            if (mode == Mode.SourceIds)
                sql.Append(" INNER JOIN @sourceIds ids ON tbl.ID = ids.Value;");
            else if (mode == Mode.WhereClause)
                sql.Append(" WHERE ").Append(whereClause);
            sql.AppendLine($@" MERGE {accessor.TableName} USING @sourceRecords AS src ON 1 = 0
WHEN NOT MATCHED THEN
    INSERT (");
            i = 0;
            // insert kolonları
            foreach (var property in accessor.AllUpdatableProperties)
            {
                if (excludedColumns.IndexOf(property.NameHash) != -1 || excludedColumns.IndexOf(property.ColumnNameHash) != -1) continue;
                if (i++ > 0)
                    sql.Append(", ");
                sql.Append(property.ColumnName);
            }
            sql.AppendLine(")");
            sql.Append("  VALUES (");
            i = 0;
            // insert değerleri
            foreach (var property in accessor.AllUpdatableProperties)
            {
                if (excludedColumns.IndexOf(property.NameHash) != -1 || excludedColumns.IndexOf(property.ColumnNameHash) != -1) continue;
                if (i++ > 0)
                    sql.Append(", ");
                sql.Append(customColumnValues.TryGetValue(property.Name, out var expr)
                    ? expr
                    : "src." + property.ColumnName
                );
            }
            sql.Append(")");
        }

        public Tuple<long, long>[] ExecuteAndReturnOldAndNewIds()
        {
            EnsureModeSet();
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var sql = new StringBuilder();
                BuildSql(sql, cnn);
                var identity = accessor.IdentityProperty;
                sql.AppendLine($" OUTPUT src.{identity.ColumnName}, INSERTED.{identity.ColumnName};");
                return DbHelpers.ExecuteTupleArray<long, long>(cnn, sql.ToString(), paramBuilder.Build());
            }
        }

        public List<TEntity> ExecuteAndReturnNewEntities()
        {
            EnsureModeSet();
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var sql = new StringBuilder();
                sql.AppendLine("DECLARE @oldAndNewIds TABLE ([OLD_ID] bigint,[NEW_ID] bigint);");
                BuildSql(sql, cnn);
                var identity = accessor.IdentityProperty;
                sql.AppendLine($" OUTPUT src.{identity.ColumnName}, INSERTED.{identity.ColumnName} INTO @oldAndNewIds;");
                sql.AppendFormat("SELECT {0} FROM {1} tbl INNER JOIN @oldAndNewIds ids ON tbl.{2} = ids.[NEW_ID];",
                    accessor.GetSelectableColumns(excludedColumns.ToArray()), accessor.TableName, identity.ColumnName);
                return DbHelpers.ExecuteObjectList<TEntity>(cnn, CommandType.Text, sql.ToString(), paramBuilder.Build());
            }
        }

        enum Mode
        {
            None,
            SourceIds,
            WhereClause
        }
    }
}

#endif