#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Reflection;

namespace System.Data.Models
{
    sealed class InsertAllModel<T> : IInsertAllModel where T : class
    {
        readonly HierarchicalInsertAllBuilder builder;
        readonly IList<T> sourceList;
        readonly List<string> parentColumns = new List<string>();
        readonly PropertyProvider<T> propertyProvider;
        readonly string tempTable;
        bool selfReferencing;
        string selfReferencingColumn;
        bool reIndexed;

        public InsertAllModel(HierarchicalInsertAllBuilder builder, IList<T> sourceList, PropertyProvider<T> properties)
        {
            this.builder = builder;
            this.sourceList = sourceList;
            propertyProvider = properties;
            Accessor = TypeAccessor.CreateOrGet<T>();
            TableName = Accessor.TableName;
            tempTable = DbHelpers.GetTempTableName();
        }

        public void GeneratePushSql(ConnectionData cnn, SqlCommand cmd, StringBuilder sql)
        {
            var tableMetadata = cnn.DbConnectionName.Database.GetCachedTable(TableName);
            var tblInfo = TableInfo.Create(sourceList, Accessor, tableMetadata, propertyProvider.GetProperties(), true);
            // e�er de�erler alanlara s��m�yorsa tabloyu d�zenle
            tblInfo.AlterTableIfRequired(cmd, TableName);
            // ge�ici tabloyu olu�tur
            cmd.CommandText = tblInfo.CreateTableScript(tempTable, true);
            cmd.ExecuteNonQuery();
            // ge�ici tabloya insert et
            using (var sbc = new SqlBulkCopy(cmd.Connection, SqlBulkCopyOptions.Default, cmd.Transaction))
            {
                sbc.DestinationTableName = tempTable;
                var dt = new DataTable(tempTable);
                foreach (var col in tblInfo.Columns)
                {
                    var prop = col.Property;
                    dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);
                    sbc.ColumnMappings.Add(prop.ColumnName, prop.ColumnName);
                }
                // fazladan bir de indeks kolonu ekliyoruz
                // ekledi�imiz s�rada bize d�ns�n diye
                // bu s�ralama ile �rneklerin ID de�erlerini g�ncelleyece�iz
                dt.Columns.Add("IDX", typeof(int));
                sbc.ColumnMappings.Add("IDX", "IDX");
                foreach (var row in tblInfo.Rows)
                    dt.Rows.Add(row);
                // g�nder
                sbc.WriteToServer(dt);
            }
            // �imdi insert etti�imiz tabloyu, �st tablolar�n de�erlerine g�ncelle
            var parentRelationships = builder.GetMyParentRelationships(TableName);
            selfReferencing = false;
            selfReferencingColumn = null;
            foreach (var rel in parentRelationships)
            {
                // buradaki toplanan alanlar� e�leme tablosuna ekleyece�iz modeli g�ncellemek i�in gerekiyor.
                parentColumns.Add(rel.ChildColumn);
                // �st tablo ve �ocuk tablo adlar� ayn� ise bu kendini referans eden bir tablo ili�kisi
                if (string.Equals(rel.ParentTable, rel.ChildTable, StringComparison.OrdinalIgnoreCase))
                {
                    if (selfReferencing)
                        throw new InvalidOperationException("Too many self referencing columns defined");
                    selfReferencing = true;
                    selfReferencingColumn = rel.ChildColumn;
                    continue;
                }

                // yeni insert etti�imiz temp tablonun alanlar�n� g�ncelle
                // Not: bir �st tablo @tmpTabloAd� �eklinde �nceden bir ge�ici tablo 
                // olu�turmu� oluyor
                sql.AppendFormat(@"
-- Mant�ksal indekslerin yerine {1} tablosuna eklenmi� Id de�erlerine g�ncelle 

UPDATE tgt SET {2} = parTable.{3} FROM {0} tgt 
  INNER JOIN @tmp{1} parTable ON tgt.{2} = parTable.IDX;", tempTable, rel.ParentTable, rel.ChildColumn, rel.ParentTableModel.Accessor.IdentityProperty.ColumnName);
            }
            var idCol = tableMetadata.FindColumn(Accessor.IdentityProperty.ColumnNameHash);
            sql.AppendFormat("\r\n-- {0} i�in bir e�leme tablosu olu�tur\r\n\r\nDECLARE @tmp{0} TABLE ( IDX int, {1}", TableName, idCol.GetDefinition());
            foreach (var column in parentColumns)
            {
                var pCol = tblInfo.FindColumnByName(column);
                if (pCol == null)
                    throw new InvalidOperationException($"Parent column '{column}' could not be found");
                sql.Append(", ").Append(pCol.GetColumnDefinition());
            }
            sql.Append(" );\r\n");
            var insertColumns = string.Join(",", tblInfo.Columns.Select(_ => string.Concat("[", _.Property.ColumnName, "]")));
            var valuesStmts = string.Join(",", tblInfo.Columns.Select(GetInsertValueStatement));
            var outputStmts = string.Join(",", parentColumns.Select(_ => string.Concat("temp.[", _, "]")));
            var outputColumns = string.Join(",", parentColumns.Select(_ => string.Concat("[", _, "]")));
            sql.AppendFormat(@"
-- Temp tablodaki verileri as�l {0} tablosuna aktar.

MERGE INTO {0} USING {1} AS temp ON 1 = 0
WHEN NOT MATCHED THEN
    INSERT ({2})
    VALUES ({3}) ", TableName, tempTable, insertColumns, valuesStmts);
            sql.AppendFormat("OUTPUT temp.IDX, inserted.{1}{2} INTO @tmp{0} (IDX, {1}{3});",
                TableName, idCol.Name, outputStmts.PrependIfNotEmpty(", "), outputColumns.PrependIfNotEmpty(", "));
            if (selfReferencing)
            {
                sql.AppendFormat(@"
-- �st Id de�erlerini e�leme tablosundan bakarak g�ncelle

UPDATE tgt SET {2} = src.{1} FROM {0} tgt
	INNER JOIN @tmp{0} lnk ON tgt.{1} = lnk.{1}
	INNER JOIN @tmp{0} src ON lnk.{2} = src.IDX;
", TableName, idCol.Name, selfReferencingColumn);
            }
            sql.AppendFormat("\r\nDROP TABLE [{0}];\r\n", tempTable);
        }

        public void GeneratePullSql(StringBuilder sql)
        {

            if (selfReferencing)
            {
                var columns = string.Join(",", parentColumns.Select(_ => string.Concat("src.[", _, "]")));
                sql.AppendFormat(@"SELECT tgt.{0},{2} FROM @tmp{1} tgt
    INNER JOIN {1} src ON tgt.ID = src.ID ORDER BY IDX;",
                    Accessor.IdentityProperty.ColumnName, TableName, columns);
            }
            else
            {
                var otherColumns = string.Join(",", parentColumns.Select(_ => string.Concat("[", _, "]")));
                sql.AppendFormat("SELECT {0}{2} FROM @tmp{1} ORDER BY IDX;\r\n", Accessor.IdentityProperty.ColumnName, TableName, otherColumns.PrependIfNotEmpty(","));
            }
        }

        string GetInsertValueStatement(ColumnInfo column)
        {
            var columnName = column.Property.ColumnName;
            // kendini referans eden kolon ise bu alan�n de�eri NULL olarak ge�meli ki foreign key hatas�na neden olmas�n.
            return string.Equals(columnName, selfReferencingColumn, StringComparison.OrdinalIgnoreCase)
                ? "NULL"
                : string.Concat("temp.[", columnName, "]");
        }

        public void ProcessResult(DbDataReader reader)
        {
            var i = 0;
            var idSetter = Accessor.IdentityProperty.Setter;
            var otherSetters = new List<IPropertySetter>(parentColumns.Count);
            Accessor.GetPropertySettersFromNames(otherSetters, parentColumns);

            while (reader.Read())
            {
                var item = sourceList[i++];
                idSetter.SetValue(item, reader.GetValue(0));
                for (var j = 1; j <= parentColumns.Count; j++)
                    otherSetters[j - 1].SetValue(item, reader.GetValue(j));
            }
        }

        public void ReIndex()
        {
            if (reIndexed) return;
            foreach (var relationship in builder.GetMyParentRelationships(TableName))
            {
                var parent = relationship.ParentTableModel;
                var thisProperty = Accessor.FindPropertyByColumnName(relationship.ChildColumn);
                foreach (var item in sourceList)
                {
                    var idx = parent.FindRowIndexByIdentityValue(thisProperty.Getter.GetValue(item));
                    thisProperty.Setter.SetValue(item, idx);
                }
            }
            reIndexed = true;
        }

        public long? FindRowIndexByIdentityValue(object value)
        {
            if (value == null) return null;
            var idProp = Accessor.IdentityProperty;
            long idx = 0;
            foreach (var item in sourceList)
            {
                if (Equals(value, idProp.Getter.GetValue(item)))
                    return idx;
                idx++;
            }
            return null;
        }


        public string TableName { get; }

        public TypeAccessor Accessor { get; }

        public override string ToString()
        {
            return Accessor.ToString();
        }
    }
} 
#endif