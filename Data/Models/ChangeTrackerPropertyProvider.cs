﻿#if COREFULL || COREDATA
using System.Collections.Generic;
using System.Reflection;

namespace System.Data.Models
{
    sealed class ChangeTrackerPropertyProvider<T> : PropertyProvider<T> where T : class
    {
        readonly ObjectChangeTracker changeTracker;
        readonly IList<T> items;

        public ChangeTrackerPropertyProvider(ObjectChangeTracker changeTracker, IList<T> items)
        {
            this.changeTracker = changeTracker;
            this.items = items;
        }

        protected override void Initialize(List<PropertyAccessor> properties)
        {
            foreach (var prop in Accessor.AllUpdatableProperties)
            {
                foreach (var obj in items)
                {
                    var objInst = changeTracker.GetObjectInstance(obj);
                    if (objInst != null && objInst.IsEditing)
                    {
                        if (!objInst.Snapshot.IsPropertyModified(prop))
                            continue;
                        properties.Add(prop);
                        break;
                    }
                    properties.Add(prop);
                    break;
                }
            }
        }
    }
} 
#endif