﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace System.Data.Models
{
    public abstract class ObjectFactory<T> where T : class, new()
    {
        readonly Dictionary<Type, TypeDataReaderMapper> mappers = new Dictionary<Type, TypeDataReaderMapper>();

        protected T1 GetMapperOf<T1>(DbDataReader reader, out TypeDataReaderMapper mapper) where T1 : T, new()
        {
            var type = typeof(T1);
            if (mappers.TryGetValue(type, out mapper)) return new T1();
            var accessor = TypeAccessor.CreateOrGet(type);
            mapper = new TypeDataReaderMapper(accessor, reader);
            mappers.Add(type, mapper);
            return new T1();
        }

        public abstract T CreateInstance(DbDataReader reader, out TypeDataReaderMapper mapper);

    }
} 
#endif