﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace System.Data.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Liste eleman tipi</typeparam>
    public sealed class ListFromMultipleResult<T> where T : class
    {
        readonly ConnectionData connection;
        readonly string sql;
        readonly IList<T> targetList;
        readonly object[] prms;
        readonly Queue<Tuple<Func<T>, TypeAccessor>> types = new Queue<Tuple<Func<T>, TypeAccessor>>();
        readonly bool disposeConnection;
        internal ListFromMultipleResult(ConnectionData connection, string sql, IList<T> targetList, object[] prms)
        {
            this.connection = connection;
            this.sql = sql;
            this.targetList = targetList;
            this.prms = prms;
            disposeConnection = false;
        }

        internal ListFromMultipleResult(string cnnName, string sql, IList<T> targetList, object[] prms)
        {
            connection = ConnectionManager.Get(cnnName);
            this.sql = sql;
            this.targetList = targetList;
            this.prms = prms;
            disposeConnection = true;
        }
        /// <summary>
        /// Bir sonraki sonuçta kullanılacak sınıf yapılandırıcı delegesi. 
        /// </summary>
        /// <typeparam name="TDerived">Türeyen sınıfın tipi</typeparam>
        /// <param name="ctor">Yapılandırıcı delegesi</param>
        /// <returns>Bu sınıfın örneği</returns>
        public ListFromMultipleResult<T> NextResult<TDerived>(Func<T> ctor) where TDerived : T
        {
            types.Enqueue(Tuple.Create(ctor, TypeAccessor.CreateOrGet<TDerived>()));
            return this;
        }
        /// <summary>
        /// Sorguyu çalıştırır ve dönen sonuçlara göre listeyi doldurur
        /// </summary>
        public void Execute()
        {
            if (types.Count == 0)
                return;
            try
            {
                DbHelpers.ExecuteReader(connection, CommandType.Text, reader =>
                {
                    do
                    {
                        var type = types.Dequeue();
                        while (reader.Read())
                        {
                            var obj = type.Item1();
                            new TypeDataReaderMapper(type.Item2, reader).Map(obj);
                            targetList.Add(obj);
                        }
                    } while (types.Count > 0 && reader.NextResult());
                }, sql, prms);
            }
            finally
            {
                if (disposeConnection)
                    connection.Dispose();
            }
        }
    }
} 
#endif