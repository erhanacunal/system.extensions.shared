﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;

namespace System.Data
{
    /// <summary>
    /// Sql parametrelerini inşa etmeyi kolaylaştıran sınıf
    /// </summary>
    public sealed class SqlParamBuilder
    {
        readonly bool useNamedParameters;
        readonly List<SqlParamInfo> parameters = new List<SqlParamInfo>();
        /// <summary>
        /// Yeni bir <see cref="SqlParamBuilder"/> örneği başlatır.
        /// </summary>
        /// <param name="useNamedParameters">Parametreler oluşturulurken hariçten isim belirtilip belirtilmeyeceğini ayarlar</param>
        public SqlParamBuilder(bool useNamedParameters = false)
        {
            this.useNamedParameters = useNamedParameters;
        }
        /// <summary>
        /// Ardışık isim kullanarak bir parametre ekler ve <see cref="SqlParamInfo"/> örneği döndürür
        /// </summary>
        /// <param name="value">Parametre değeri</param>
        /// <returns><see cref="SqlParamInfo"/> örneği</returns>
        public SqlParamInfo Add2(object value)
        {
            if (useNamedParameters)
                throw new InvalidOperationException("Use New(name,value) method instead");
            var sqlParamInfo = new SqlParamInfo(parameters.Count, value);
            parameters.Add(sqlParamInfo);
            return sqlParamInfo;
        }
        /// <summary>
        /// İsimli parametre ekler ve <see cref="SqlParamInfo"/> örneği döndürür
        /// </summary>
        /// <param name="name">Parametrenin ismi</param>
        /// <param name="value">Parametrenin değeri</param>
        /// <returns><see cref="SqlParamInfo"/> örneği</returns>
        /// <exception cref="InvalidOperationException">Bu örneği useNamedParameters = <c>false</c> olarak başlatırsanız bu istisna fırlatılır</exception>
        public SqlParamInfo Add2(string name, object value)
        {
            if (!useNamedParameters)
                throw new InvalidOperationException("Use New(value) method instead");
            var sqlParamInfo = new SqlParamInfo(name, value);
            parameters.Add(sqlParamInfo);
            return sqlParamInfo;
        }
        /// <summary>
        /// Ardışık isim kullanarak bir parametre ekler ve ismini döndürür
        /// </summary>
        /// <param name="value">Parametre değeri</param>
        /// <returns>Parametrenin ismini döndürür</returns>
        public string Add(object value)
        {
            return Add2(value).Name;
        }
        /// <summary>
        /// Çoklu parametre ekler. Eğer bu sınıf başlatılırken useNamedParameters <c>true</c> ise dizilim isim,değer,isim,değer şeklinde sıralanmalıdır.
        /// Diğer durumda parametreler ardışık isimlendirme kullanılarak eklenir.
        /// </summary>
        /// <param name="prms">Eklenecek parametre verileri</param>
        /// <returns><see cref="SqlParamBuilder"/> örneği</returns>
        public SqlParamBuilder AddRange(params object[] prms)
        {
            if (useNamedParameters)
            {
                if (prms.Length % 2 != 0)
                    throw new InvalidOperationException("Parameter notation must be name, value, name, value, ...");
                for (var i = 0; i < prms.Length; i += 2)
                    parameters.Add(new SqlParamInfo((string)prms[i], prms[i + 1]));
                return this;
            }
            foreach (var prm in prms)
                parameters.Add(new SqlParamInfo(parameters.Count, prm));
            return this;
        }

        /// <summary>
        /// Eklenmiş parametre bilgilerini sql çağrımında kullanılabilecek tek bir dizi haline getirir.
        /// </summary>
        /// <returns><see cref="object"/> dizisi</returns>
        public object[] Build()
        {
            object[] result;
            if (useNamedParameters)
            {
                result = new object[parameters.Count * 2 + 1];
                result[0] = DbSwitches.UseNamedParams;
                for (var i = 0; i < parameters.Count; i++)
                    parameters[i].Write(result, i * 2 + 1);
                return result;
            }
            result = new object[parameters.Count];
            for (var i = 0; i < parameters.Count; i++)
                result[i] = parameters[i].Value;
            return result;
        }

        public bool IsDefined(string paramName)
        {
            if (string.IsNullOrWhiteSpace(paramName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(paramName));
            return parameters.Exists(_ => string.Equals(_.Name, paramName));
        }

        public string GetUniqueParamName(string paramName)
        {
            if (string.IsNullOrWhiteSpace(paramName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(paramName));
            var i = 1;
            var currentName = paramName;
            while (true)
            {
                if (parameters.Exists(_ => string.Equals(currentName, _.Name, StringComparison.OrdinalIgnoreCase)))
                    currentName = paramName + i++;
                else
                    break;
            }

            return currentName;
        }

    }

    public sealed class SqlParamInfo
    {
        /// <summary>
        /// Parametrenini adını verir
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Parametrenin değerini verir
        /// </summary>
        public object Value { get; }

        internal SqlParamInfo(string name, object value)
        {
            Name = name;
            Value = value;
        }

        internal SqlParamInfo(int idx, object value)
        {
            Name = "@p" + (idx + 1);
            Value = value;
        }

        internal void Write(object[] result, int idx)
        {
            result[idx] = Name;
            result[idx + 1] = Value;
        }
    }
}

#endif