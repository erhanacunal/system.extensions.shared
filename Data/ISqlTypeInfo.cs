#if COREFULL || COREDATA
using System;
using System.Data;

namespace System.Data
{
    public interface ISqlTypeInfo
    {
        string Name { get; }
        bool IsSizable { get; }
        bool IsUnicode { get; }
        bool IsBinary { get; }
        bool IsNumeric { get; }
        bool HasPrecisionScale { get; }
        string DefaultValue { get; }
        Type CodeType { get; }
        DbType DbType { get; }
        bool IsString { get; }

        SqlConversionResult IsConvertibleTo(ISqlTypeInfo to);
    }
} 
#endif