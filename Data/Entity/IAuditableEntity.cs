﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#if TYPESCRIPT
using System.Extensions.TypeScript;
#endif
#if JSON
using Newtonsoft.Json;
#endif

namespace System.Data.Entity
{
#if TYPESCRIPT
    [TypeScriptIgnore]
#endif
    public interface ISupportsIdentity
    {
        long IdValue { get; set; }
    }
#if TYPESCRIPT
    [TypeScriptIgnore]
#endif
    public interface IAuditableEntity : ISupportsIdentity
    {
        bool Deleted { get; set; }
        DateTime EntryDateTime { get; set; }
        long EnteredBy { get; set; }
        DateTime? UpdatedDateTime { get; set; }
        long? UpdatedBy { get; set; }
    }

    public abstract class AuditableEntity : IAuditableEntity
    {
#if JSON
        [JsonIgnore]
#endif
#if !MOBILE
        [NotMapped] 
#endif
        public long IdValue { get => ID; set => ID = value; }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; } // Simple
#if JSON
        [JsonIgnore] 
#endif
        public DateTime EntryDateTime { get; set; } // Simple
#if JSON
        [JsonIgnore]
#endif
        public long EnteredBy { get; set; } // Simple
#if JSON
        [JsonIgnore]
#endif
        public DateTime? UpdatedDateTime { get; set; } // Simple
#if JSON
        [JsonIgnore]
#endif
        public long? UpdatedBy { get; set; } // Simple
#if JSON
        [JsonIgnore]
#endif
        public bool Deleted { get; set; } // Simple
    }
}