﻿using System.Collections.Generic;
using Newtonsoft.Json;
#if TYPESCRIPT
using System.Extensions.TypeScript;
#endif

namespace System.Data.Entity
{
#if TYPESCRIPT
    [TypeScriptNamespace("Common.Models")] 
#endif
    public sealed class BulkModelChange<T> where T : class, IAuditableEntity
    {
#if JSON
        [JsonProperty("inserted")] 
#endif
        public List<T> Inserted { get; } = new List<T>();

#if JSON
        [JsonProperty("updated")] 
#endif
        public List<T> Updated { get; } = new List<T>();

#if JSON
        [JsonProperty("deleted")] 
#endif
        public List<T> Deleted { get; } = new List<T>();
    }
}
