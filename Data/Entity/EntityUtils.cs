﻿#if !MOBILE
using System.Collections.Generic;
using System.Data.Models;
using System.Linq;
using System.MyResources;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Utils;

namespace System.Data.Entity
{
    public static class EntityUtils
    {
        public static readonly uint[] ExcludedProperties;

        static EntityUtils()
        {
            ExcludedProperties = HashUtils.JenkinsHashes("enteredby", "entrydatetime", "deleted", "updatedby", "updateddatetime");
        }

        public static PropertyProvider<T> GetPropertiesForInserting<T>(bool includeDeleted = false) where T : class, IAuditableEntity
        {
            var result = new FixedPropertyProvider<T>();
            foreach (var property in result.Accessor.MappableProperties)
            {
                if (property.IsIdentity || Array.IndexOf(ExcludedProperties, property.ColumnNameHash, includeDeleted ? 3 : 2) != -1) continue;
                result.AddProperty(property);
            }
            return result;
        }

        public static PropertyProvider<T> GetPropertiesForUpdating<T>(bool includeDeleted = false, bool includeIdentity = false)
            where T : class, IAuditableEntity
        {
            var result = new FixedPropertyProvider<T>();
            foreach (var property in result.Accessor.MappableProperties)
            {
                if (Array.IndexOf(ExcludedProperties, property.ColumnNameHash, 0, includeDeleted ? 2 : 3) != -1 ||
                    !includeIdentity && property.IsIdentity) continue;
                result.AddProperty(property);
            }
            return result;
        }

        public static void InitListForInserting<T>(IList<T> source, long userId) where T : class, IAuditableEntity
        {
            foreach (var item in source)
            {
                item.IdValue = 0;
                item.EntryDateTime = DateTime.Now;
                item.EnteredBy = userId;
                item.UpdatedBy = null;
                item.UpdatedDateTime = null;
            }
        }

        public static void InitEntityForInserting<T>(T source, long userId) where T : class, IAuditableEntity
        {
            source.IdValue = 0;
            source.EntryDateTime = DateTime.Now;
            source.EnteredBy = userId;
            source.UpdatedBy = null;
            source.UpdatedDateTime = null;

        }

        public static void DeleteEntityById<T>(string cnnName, long auditUserId, long id)
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            DbHelpers.ExecuteNonQuery(cnnName, $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE ID = @p2 AND Deleted = 0",
                auditUserId, id);
        }

        public static Task DeleteEntityByIdAsync<T>(string cnnName, long auditUserId, long id)
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteNonQueryAsync(cnnName, CommandType.Text, $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE ID = @p2 AND Deleted = 0",
                auditUserId, id);
        }

        public static void DeleteEntitiesBy<T>(string cnnName, long auditUserId, string propertyName, object value)
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var columnName = accessor.FindProperty(propertyName, true);
            if (columnName == null)
                throw new ArgumentException(nameof(propertyName));
            var sqlText = value is DataTable
                ? $@"UPDATE tgt SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() FROM {accessor.TableName} tgt
    INNER JOIN @p2 inList ON tgt.{columnName.ColumnName} = inList.Value
WHERE tgt.Deleted = 0"
                : $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE {columnName.ColumnName} = @p2 AND Deleted = 0";
            DbHelpers.ExecuteNonQuery(cnnName, sqlText, auditUserId, value);
        }

        public static async Task DeleteEntitiesByAsync<T>(string cnnName, long auditUserId, string propertyName, object value)
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var columnName = accessor.FindProperty(propertyName, true);
            if (columnName == null)
                throw new ArgumentException(nameof(propertyName));
            var sqlText = value is DataTable
                ? $@"UPDATE tgt SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() FROM {accessor.TableName} tgt
    INNER JOIN @p2 inList ON tgt.{columnName.ColumnName} = inList.Value
WHERE tgt.Deleted = 0"
                : $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE {columnName.ColumnName} = @p2 AND Deleted = 0";
            await DbHelpers.ExecuteNonQueryAsync(cnnName, CommandType.Text, sqlText, auditUserId, value);
        }

        public static void DeleteAllEntities<T>(string cnnName, long auditUserId, string extraWhereClause, params object[] args)
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            DbHelpers.ExecuteNonQuery(cnnName, $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE Deleted = 0 {extraWhereClause}",
                ArrayUtils.Concat(new object[] { auditUserId }, args));
        }

        public static List<T> GetAllEntities<T>(string cnnName, string extraWhereClause, params object[] args) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteObjectList<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(ExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 {extraWhereClause}", args);
        }

        public static Task<List<T>> GetAllEntitiesAsync<T>(string cnnName, string extraWhereClause, params object[] args) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteObjectListAsync<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(ExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 {extraWhereClause}", args);
        }

        public static T GetEntityById<T>(string cnnName, long id, string[] excludedProperties = null) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return DbHelpers.ExecuteObject<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(allExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND ID = @p1",
                id);
        }

        public static async Task<T> GetEntityByIdAsync<T>(string cnnName, long id, string[] excludedProperties = null) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await GetEntityByIdAsync<T>(cnn, id, excludedProperties);
        }

        public static Task<T> GetEntityByIdAsync<T>(ConnectionData cnn, long id, string[] excludedProperties = null) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var idColumn = accessor.IdentityProperty.ColumnName;
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return DbHelpers.ExecuteObjectAsync<T>(cnn, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(allExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND {idColumn} = @p1",
                id);
        }

        public static async Task<T> GetEntityBySqlAsync<T>(string cnnName, string extraWhere, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await GetEntityBySqlAsync<T>(cnn, extraWhere, prms);
        }

        public static Task<T> GetEntityBySqlAsync<T>(ConnectionData cnn, string extraWhere, params object[] prms) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteObjectAsync<T>(cnn, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(ExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND {extraWhere}",
                prms);
        }

        public static T GetEntityBySql<T>(string cnnName, string sql, params object[] prms) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            return DbHelpers.ExecuteObject<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(ExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND {sql}",
                prms);
        }

        public static string GetSelectableColumns(TypeAccessor accessor, string[] excludedProperties = null)
        {
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return accessor.GetSelectableColumns(allExcludedProperties);
        }

        public static string GetSelectableColumns<T>(string[] excludedProperties = null)
        {
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return TypeAccessor.CreateOrGet<T>().GetSelectableColumns(allExcludedProperties);
        }

        public static List<T> GetEntitiesByParentId<T>(string cnnName, string parentPropertyName, long parentPropertyValue, string[] excludedProperties = null) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var parentColumn = accessor.FindProperty(parentPropertyName);
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return DbHelpers.ExecuteObjectList<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(allExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND {parentColumn.ColumnName} = @p1",
                parentPropertyValue);
        }

        public static Task<List<T>> GetEntitiesByParentIdAsync<T>(string cnnName, string parentPropertyName, long parentPropertyValue, string[] excludedProperties = null) where T : class, new()
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            var parentColumn = accessor.FindProperty(parentPropertyName);
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            return DbHelpers.ExecuteObjectListAsync<T>(cnnName, CommandType.Text,
                $"SELECT {accessor.GetSelectableColumns(allExcludedProperties)} FROM {accessor.TableName} WHERE Deleted = 0 AND {parentColumn.ColumnName} = @p1",
                parentPropertyValue);
        }

        public static long UpsertUniqueEntity<T>(string cnnName, long auditUserId, T item, string duplicateCheckSql = null, string displayMember = null, string[] excludedProperties = null) where T : IAuditableEntity
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var prms = new List<object> { DbSwitches.UseNamedParams, "@auditUserId", auditUserId, "@id", item.IdValue };
            var sql = CreateUpsertUniqueEntitySql(item, duplicateCheckSql, excludedProperties, accessor, prms);
            item.IdValue = DbHelpers.ExecuteScalar<long>(cnnName, sql, prms.ToArray());
            if (item.IdValue == 0L)
                throw new InvalidOperationException(string.Format(SR.AlreadyExists, string.IsNullOrEmpty(displayMember)
                    ? item.IdValue
                    : accessor.FindProperty(displayMember).Getter.GetValue(item)));
            return item.IdValue;
        }

        public static async Task<long> UpsertUniqueEntityAsync<T>(string cnnName, long auditUserId, T item, string duplicateCheckSql = null, string displayMember = null, string[] excludedProperties = null) where T : IAuditableEntity
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var prms = new List<object> { DbSwitches.UseNamedParams, "@auditUserId", auditUserId, "@id", item.IdValue };
            var sql = CreateUpsertUniqueEntitySql(item, duplicateCheckSql, excludedProperties, accessor, prms);
            item.IdValue = await DbHelpers.ExecuteScalarAsync<long>(cnnName, sql, prms.ToArray());
            if (item.IdValue == 0L)
                throw new InvalidOperationException(string.Format(SR.AlreadyExists, string.IsNullOrEmpty(displayMember)
                    ? item.IdValue
                    : accessor.FindProperty(displayMember).Getter.GetValue(item)));
            return item.IdValue;
        }

        public static string CreateUpsertUniqueEntitySql<T>(T item, string duplicateCheckSql, string[] excludedProperties, TypeAccessor accessor,
            IList<object> prms) where T : IAuditableEntity
        {
            var sb = StringBuilderCache.Allocate();
            sb.AppendLine("DECLARE @result bigint;");
            sb.AppendLine("SET @result = 1;");
            var checkDuplication = duplicateCheckSql.IsNotNullOrWhitespace();
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            if (checkDuplication)
            {
                sb.AppendFormat(
                    "IF ( EXISTS( SELECT ID FROM {0} WHERE ( ID <> @id ) AND ( Deleted = 0 ) AND ( {1} ) ) ) SET @result = 0;\r\n ",
                    accessor.TableName, duplicateCheckSql);
                sb.AppendLine("IF @result = 1 BEGIN");
            }


            if (item.IdValue == 0L)
            {
                sb.AppendFormat("INSERT INTO {0} ({1},Deleted,EnteredBy,EntryDateTime) VALUES({2},0,@auditUserId,GETDATE());\r\n",
                    accessor.TableName,
                    accessor.GetInsertColumns(allExcludedProperties),
                    accessor.GetInsertValuesWithParams(item, prms, allExcludedProperties));
                sb.AppendLine("SET @result = SCOPE_IDENTITY();");
            }
            else
            {
                sb.AppendFormat("UPDATE {0} SET {1}, UpdatedBy=@auditUserId, UpdatedDateTime = GETDATE() WHERE ID = @id;\r\n", accessor.TableName,
                    accessor.GetUpdateSetExpressionsWithParams(item, prms, allExcludedProperties));
                sb.AppendLine("SET @result = @id;");
            }

            if (checkDuplication)
                sb.AppendLine("END");
            sb.AppendLine("SELECT @result");
            return StringBuilderCache.ReturnAndFree(sb);
        }

        public static bool UpsertIndeterministicEntity<T>(string cnnName, long userId, T item, string idValueCondition, string[] excludedProperties = null)
            where T : class, IAuditableEntity
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (string.IsNullOrWhiteSpace(idValueCondition))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(idValueCondition));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var sb = new StringBuilder();
            var prms = new List<object> { DbSwitches.UseNamedParams, "@auditUserId", userId };
            var allExcludedProperties = ArrayUtils.Concat(ExcludedProperties, excludedProperties != null ? HashUtils.JenkinsHashes(excludedProperties) : null);
            if (item.IdValue == 0)
            {
                // id value yoksa verilen sql ile bulma ifadelerini ekle
                sb.AppendLine("DECLARE @id bigint;");
                sb.AppendFormat("SELECT @id = ID FROM {0} WHERE Deleted = 0 AND {1};\r\n ", accessor.TableName,
                    idValueCondition);
                sb.AppendLine("IF @@ROWCOUNT = 0 BEGIN");
                sb.AppendFormat(
                    "  INSERT INTO {0} ({1},Deleted,EnteredBy,EntryDateTime) VALUES({2},0,@auditUserId,GETDATE());\r\n",
                    accessor.TableName,
                    accessor.GetInsertColumns(allExcludedProperties),
                    accessor.GetInsertValuesWithParams(item, prms, allExcludedProperties));
                sb.AppendLine("  SET @id = SCOPE_IDENTITY();");
                sb.AppendLine("END ELSE ");
            }
            else // id parametresini ekle çünkü bulma işlemleri yapılmıyor
                prms.AddRange2("@id", item.IdValue);
            sb.AppendFormat("  UPDATE {0} SET {1}, UpdatedBy=@auditUserId, UpdatedDateTime = GETDATE() WHERE ID = @id;\r\n", accessor.TableName,
                accessor.GetUpdateSetExpressionsWithParams(item, item.IdValue != 0 ? prms : null, allExcludedProperties));
            sb.AppendLine("SELECT @id");
            item.IdValue = DbHelpers.ExecuteScalar<long>(cnnName, sb.ToString(), prms.ToArray());
            return item.IdValue != 0L;
        }
        /// <summary>
        /// Verilen kaynak id değerlerine göre tablo satırlarını kopyalar.
        /// </summary>
        /// <typeparam name="T">Satırları kopyalanacak tablo varlığı</typeparam>
        /// <param name="cnnName">Bağlantı adı</param>
        /// <param name="userId">İşlemi yapan kullanıcı id değeri</param>
        /// <param name="ids">Kopyalanacak id dizisi</param>
        /// <returns>Eski ve Yeni Id değerlerinin bulunduğu demet dizisi</returns>
        public static Tuple<long, long>[] CloneRowsByIdArray<T>(string cnnName, long userId, long[] ids) where T : class, IAuditableEntity, new()
        {
            return DbHelpers
                .BeginCloneRows<T>(cnnName)
                .BySourceIds(ids)
                .ChangeColumnValue(nameof(AuditableEntity.EnteredBy), "@auditUserId")
                .ChangeColumnValue(nameof(AuditableEntity.EntryDateTime), "GETDATE()")
                .ChangeColumnValue(nameof(AuditableEntity.Deleted), "0")
                .SqlParameters("@auditUserId", userId)
                .ExecuteAndReturnOldAndNewIds();
        }

        /// <summary>
        /// Toplu kaydetme işlerini gerçekleştirir, (insert,update,delete)
        /// </summary>
        /// <typeparam name="T">Entity tipi</typeparam>
        /// <param name="cnnName">Bağlantı adı</param>
        /// <param name="userId">İşlemi gerçekleştiren kullanıcı id değeri</param>
        /// <param name="request">Toplu kaydetme isteği örneği</param>
        /// <param name="insertedIds">Eklenen öğelerin id değerlerinin aktarılacağı liste</param>
        /// <returns>Başarılı ise Success=true değilse false döner</returns>
        public static void PerformBulkSaveRequest<T>(string cnnName, long userId, BulkModelChange<T> request, List<long> insertedIds)
            where T : class, IAuditableEntity
        {
            var crasit = TypeAccessor.CreateOrGet<AuditableEntity>();
            var accessor = TypeAccessor.CreateOrGet<T>();
            var properties = new FixedPropertyProvider<T>(accessor, accessor - crasit);
            foreach (var entity in request.Inserted)
            {
                entity.EnteredBy = userId;
                entity.EntryDateTime = DateTime.Now;
            }
            foreach (var entity in request.Updated)
            {
                entity.UpdatedBy = userId;
                entity.UpdatedDateTime = DateTime.Now;
                entity.Deleted = false;
            }
            foreach (var entity in request.Deleted)
            {
                entity.UpdatedBy = userId;
                entity.UpdatedDateTime = DateTime.Now;
                entity.Deleted = true;
                request.Updated.Add(entity);
            }
            using (var ctx = new ObjectDbContext(cnnName))
            {
                if (request.Inserted.Count > 0)
                {
                    using (properties.UseTempProperties(
                        nameof(AuditableEntity.EnteredBy),
                        nameof(AuditableEntity.EntryDateTime)))
                    {
                        ctx.InsertAll(request.Inserted, properties);
                        insertedIds.AddRange3(request.Inserted.Select(_ => _.IdValue));
                    }
                }
                if (request.Updated.Count <= 0) return;
                using (properties.UseTempProperties(
                    nameof(AuditableEntity.UpdatedBy),
                    nameof(AuditableEntity.UpdatedDateTime),
                    nameof(AuditableEntity.Deleted)))
                {
                    ctx.UpdateAll(request.Updated, properties);
                }
            }
        }

        /// <summary>
        /// Toplu kaydetme işlerini gerçekleştirir, (insert,update,delete)
        /// </summary>
        /// <typeparam name="T">Entity tipi</typeparam>
        /// <param name="cnnName">Bağlantı adı</param>
        /// <param name="userId">İşlemi gerçekleştiren kullanıcı id değeri</param>
        /// <param name="request">Toplu kaydetme isteği örneği</param>
        /// <param name="insertedIds">Eklenen öğelerin id değerlerinin aktarılacağı liste</param>
        /// <returns>Başarılı ise Success=true değilse false döner</returns>
        public static async System.Threading.Tasks.Task PerformBulkSaveRequestAsync<T>(string cnnName, long userId, BulkModelChange<T> request, List<long> insertedIds)
            where T : class, IAuditableEntity
        {
            var crasit = TypeAccessor.CreateOrGet<AuditableEntity>();
            var accessor = TypeAccessor.CreateOrGet<T>();
            var properties = new FixedPropertyProvider<T>(accessor, accessor - crasit);
            foreach (var entity in request.Inserted)
            {
                entity.EnteredBy = userId;
                entity.EntryDateTime = DateTime.Now;
            }
            foreach (var entity in request.Updated)
            {
                entity.UpdatedBy = userId;
                entity.UpdatedDateTime = DateTime.Now;
                entity.Deleted = false;
            }
            foreach (var entity in request.Deleted)
            {
                entity.UpdatedBy = userId;
                entity.UpdatedDateTime = DateTime.Now;
                entity.Deleted = true;
                request.Updated.Add(entity);
            }

            using (var cnn = ConnectionManager.Get(cnnName))
            {
                if (request.Inserted.Count > 0)
                {
                    using (properties.UseTempProperties(
                        nameof(AuditableEntity.EnteredBy),
                        nameof(AuditableEntity.EntryDateTime)))
                    {
                        await ObjectDbOperations.InsertAllAsync(cnn, request.Inserted, properties);
                        insertedIds.AddRange3(request.Inserted.Select(_ => _.IdValue));
                    }
                }
                if (request.Updated.Count <= 0) return;
                using (properties.UseTempProperties(
                    nameof(AuditableEntity.UpdatedBy),
                    nameof(AuditableEntity.UpdatedDateTime),
                    nameof(AuditableEntity.Deleted)))
                {
                    await ObjectDbOperations.UpdateAllAsync(cnn, request.Updated, properties);
                }
            }
        }

    }
}
#endif