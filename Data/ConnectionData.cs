﻿#if COREFULL || COREDATA
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.Data.Providers;

namespace System.Data
{
    /// <summary>
    /// Sql bağlantısının verimli kullanılabilmesi için gerekli önbellekleme sınıfı
    /// </summary>
    public sealed class ConnectionData : IDisposable
    {
        readonly bool useMasterDbConnection;
        const int MAX_RETRY = 2;
        DbConnection connection;

        /*static int[] RetryableErrors = new int[]{
            -1,   // SqlConnectionBroken
            -2,   // SqlTimeout
            701,  // SqlOutOfMemory
            1204, // SqlOutOfLocks
            1205, // SqlDeadlockVictim
            1222, // SqlLockRequestTimeout
            8645, // SqlTimeoutWaitingForMemoryResource
            8651, // SqlLowMemoryCondition
            30053 // SqlWordbreakerTimeout
        };
        */
        /// <summary>
        /// Veritabanı bağlantı ismi örneğini verir
        /// </summary>
        public IDbConnectionName DbConnectionName { get; }

        /// <summary>
        /// Tahsis edilen bağlantıyı verir
        /// </summary>
        public DbConnection Connection => connection ??
                                           (connection = useMasterDbConnection ? DbConnectionName.GetMasterDbConnection() : DbConnectionName.GetConnection());

        public DbTransaction Transaction { get; private set; }
        public string Name { get; }

        public ISqlDialect SqlDialect { get; }

        internal IReservedConnection ReservedConnection;

        internal ConnectionData(string name, IDbConnectionName dbConnectionName, bool useMaster)
        {
            useMasterDbConnection = useMaster;
            Name = name;
            DbConnectionName = dbConnectionName;
            SqlDialect = dbConnectionName.SqlClientFactory.SqlDialect;
        }

        public DbCommand CreateCommand(DbTransaction trans = null)
        {
            var cmd = Connection.CreateCommand();
            cmd.Transaction = trans ?? Transaction;
            cmd.CommandTimeout = ConnectionManager.CommandTimeout;
            DbConnectionName.SqlClientFactory.CustomizeCommand(cmd);
            return cmd;
        }

        public DbCommand CreateCommand(string sql, params object[] prms)
        {
            var cmd = Connection.CreateCommand();
            cmd.Transaction = Transaction;
            cmd.CommandTimeout = ConnectionManager.CommandTimeout;
            cmd.CommandText = sql;
            DbConnectionName.SqlClientFactory.CustomizeCommand(cmd);
            cmd.SetCommandParameters(prms);
            return cmd;
        }

        public DbCommand CreateCommand(CommandType cmdType, string sql, params object[] prms)
        {
            var cmd = Connection.CreateCommand();
            cmd.Transaction = Transaction;
            cmd.CommandTimeout = ConnectionManager.CommandTimeout;
            cmd.CommandType = cmdType;
            cmd.CommandText = sql;
            DbConnectionName.SqlClientFactory.CustomizeCommand(cmd);
            cmd.SetCommandParameters(prms);
            
            return cmd;
        }

        /// <summary>
        /// Yeni bir transaction başlatır.
        /// </summary>
        /// <returns>IDbTransaction</returns>
        public DbTransaction BeginTransaction()
        {
            if (Transaction == null)
                return Transaction = Connection.BeginTransaction();
            else
                return Connection.BeginTransaction();
        }

        /// <summary>
        /// Yeni bir transaction başlatır.
        /// </summary>
        /// <returns>IDbTransaction</returns>
        public DbTransaction BeginTransaction(IsolationLevel level)
        {
            if (Transaction == null)
                return Transaction = Connection.BeginTransaction(level);
            else
                return Connection.BeginTransaction(level);
        }

        public void CommitTransaction()
        {
            if (Transaction == null)
                return;
            Transaction.Commit();
            Transaction.Dispose();
            Transaction = null;
        }

        public void RollbackTransaction()
        {
            if (Transaction == null)
                return;
            Transaction.Rollback();
            Transaction.Dispose();
            Transaction = null;
        }

        public void Dispose()
        {
            // ayrılmış bir bağlantı değilse, bağlantıyı kapat
            // ayrılmış bağlantılarda, ayrılmış bağlantı nesnesi dispose edilene kadar bağlantı kapatılmamalı
            if (ReservedConnection != null) return;
            connection?.Dispose();
            connection = null;
            Transaction = null;
        }

        public bool IsRecoverable(SqlException ex, ref int retryCount)
        {
            bool allowRetry;
            var e = ex.Number;
            if (e == -1 || e == 1205)
            {
                // yeni bir bağlantı almasını sağla
                Connection.Dispose();
                connection = null;
                Transaction = null;
                allowRetry = true;
            }
            else allowRetry = e.In(-2, 701, 1204, 1222, 8645, 8651, 30053);
            if (!allowRetry)
                return false;
            retryCount++;
            if (retryCount > MAX_RETRY)
                return false;
            Thread.Sleep(ex.Number == -2 /*Timeout*/ ? 5000 : 500);
            return true;
        }

    }
} 
#endif