#if COREFULL || COREDATA
namespace System.Data
{
    public enum SqlConversionResult : byte
    {
        Allowed = 1,
        Explicit = 2,
        Implicit = 3,
        NotAllowed = 4,
        RequiresCast = 5,
        XmlConversion = 6
    }
} 
#endif