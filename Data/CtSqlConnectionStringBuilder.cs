﻿#if COREFULL || COREDATA
using System;
using System.Text;
using System.Utils;

namespace System.Data
{
    public sealed class CtSqlConnectionStringBuilder
    {

        public string DataSource { get; set; }
        public string InitialCatalog { get; set; }
        public bool IntegratedSecurity { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public int? MinPoolSize { get; set; }
        public int? MaxPoolSize { get; set; }
        public string ApplicationName { get; set; }
        public bool MultipleActiveResultSets { get; set; }
        public bool ContextConnection { get; set; }

        public string ConnectionString
        {
            get => Build();
            set => Parse(value);
        }

        public CtSqlConnectionStringBuilder()
        {

        }

        public CtSqlConnectionStringBuilder(string connectionString, string overrideCatalog = null, string overrideApplicationName = null)
        {
            Parse(connectionString, overrideCatalog, overrideApplicationName);
        }

        void Parse(string value, string overrideCatalog = null, string overrideApplicationName = null)
        {
            var properties = StringUtils.Instance.ToDictionary<string, string>(value);
            foreach (var kv in properties)
            {
                switch (kv.Key.ToLowerInvariant())
                {
                    case "data source":
                        DataSource = kv.Value;
                        break;
                    case "initial catalog":
                        if (overrideCatalog.IsNullOrEmpty())
                            InitialCatalog = kv.Value;
                        else
                            InitialCatalog = overrideCatalog;
                        break;
                    case "integrated security":
                        IntegratedSecurity = kv.Value.IsOneOfThese(StringComparison.InvariantCultureIgnoreCase, "SSPI", "true");
                        break;
                    case "user id":
                        IntegratedSecurity = false;
                        UserID = kv.Value;
                        break;
                    case "password":
                        Password = kv.Value;
                        break;
                    case "min pool size":
                        MinPoolSize = kv.Value.ToInt32(0);
                        break;
                    case "max pool size":
                        MaxPoolSize = kv.Value.ToInt32(0);
                        break;
                    case "application name":
                        ApplicationName = kv.Value;
                        break;
                    case "multipleactiveresultsets":
                        MultipleActiveResultSets = bool.Parse(kv.Value);
                        break;
                    case "context connection":
                        ContextConnection = true;
                        break;
                }
            }
            if (!string.IsNullOrEmpty(overrideApplicationName))
                ApplicationName = overrideApplicationName;
        }

        string Build()
        {
            if (ContextConnection)
                return "context connection=true";
            var sb = new StringBuilder();
            if (!IntegratedSecurity)
                sb.AppendFormat("Persist Security Info=True;User ID={0};Password={1};", UserID, Password);
            else
                sb.Append("Integrated Security=SSPI;");
            sb.AppendFormat("Data Source={0};Initial Catalog={1}", DataSource, InitialCatalog);
            if (MinPoolSize.HasValue)
                sb.Append(string.Concat(";Min Pool Size=", MinPoolSize.Value));
            if (MaxPoolSize.HasValue)
                sb.Append(string.Concat(";Max Pool Size=", MaxPoolSize.Value));
            if (ApplicationName.IsNotNullOrEmpty())
                sb.Append(string.Concat(";Application Name=", ApplicationName));
            if (MultipleActiveResultSets)
                sb.Append(";MultipleActiveResultSets=True");
            return sb.ToString();
        }

        public string GetTransientString(string catalogName)
        {
            var tmp = InitialCatalog;
            var tmp1 = MinPoolSize;
            InitialCatalog = catalogName;
            MinPoolSize = null;
            try
            {
                return Build();
            }
            finally
            {
                InitialCatalog = tmp;
                MinPoolSize = tmp1;
            }
        }

        public CtSqlConnectionStringBuilder Clone()
        {
            return new CtSqlConnectionStringBuilder()
            {
                ApplicationName = ApplicationName,
                DataSource = DataSource,
                InitialCatalog = InitialCatalog,
                IntegratedSecurity = IntegratedSecurity,
                MaxPoolSize = MaxPoolSize,
                MinPoolSize = MinPoolSize,
                UserID = UserID,
                Password = Password,
                MultipleActiveResultSets = MultipleActiveResultSets,
                ContextConnection =  ContextConnection
            };
        }



    }
}

#endif