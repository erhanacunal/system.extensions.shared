﻿#if COREFULL || COREDATA
using System;
using System.Data.Common;
using System.Threading;
using System.Data;
using System.Data.Schemas;
using System.Utils;

namespace System.Data.Providers
{
    public abstract class DbConnectionNameBase : DisposableBase, IDbConnectionName
    {

        protected CtSqlClientFactory ProviderFactory;
        DbEngineType engineType;
        SqlDatabaseSchema database;

        public CtSqlClientFactory SqlClientFactory
        {
            get
            {
                if (ProviderFactory == null)
                    Interlocked.CompareExchange(ref ProviderFactory, CtSqlClientFactories.Find(EngineType), null);
                return ProviderFactory;
            }
        }

        #region Default c'tor

        protected DbConnectionNameBase(string name, DbEngineType engineType)
        {
            Name = name;
            NameHash = HashUtils.JenkinsHash(name.ToLowerInvariant());
            EngineType = engineType;
            IsTransient = false;
        }

        #endregion

        #region IDatabaseConnectionProvider Members

        public string Name { get; }
        public uint NameHash { get; }
        public string ConnectionString { get; protected set; }

        public DbEngineType EngineType
        {
            get => engineType;
            protected set
            {
                engineType = value;
                ProviderFactory = null;
            }
        }

        public bool IsTransient { get; set; }
        public abstract DbConnection GetConnection();
        public abstract DbConnection GetMasterDbConnection();
        public abstract DbConnection GetConnection(params object[] prms);

        public abstract bool TestConnection();
        public SqlDatabaseSchema Database => database ?? (database = new SqlDatabaseSchema(this));

        public abstract void Initialize();

        public virtual string GetPropertyValue(string name)
        {
            var parts = ConnectionString.Split('=', ';');
            for (int i = 0; i < parts.Length; i += 2)
            {
                if (parts[i].Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return parts[i + 1];
            }
            return "";
        }

        #endregion

        protected override void DisposeCore()
        {
            if (IsTransient)
                ConnectionManager.Remove(Name);
        }
    }
}

#endif