﻿#if ( COREFULL || COREDATA) && DB2
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using IBM.Data.DB2;

namespace System.Data.Providers
{
    public sealed class CtDb2SqlClientFactory: CtSqlClientFactory
    {
        public CtDb2SqlClientFactory()
        {
            Engine = DbEngineType.Db2;
            SqlDialect = new SqlServerDialect();
        }
        public override DbConnection CreateConnection()
        {
            return new DB2Connection();
        }

        public override DbConnection CreateConnection(string connectionString)
        {
            return new DB2Connection(connectionString);
        }

        public override ISqlDialect SqlDialect { get; }

        public override void CustomizeCommand(DbCommand cmd)
        {
            
        }
    }
}
#endif