#if COREFULL || COREDATA
using System.Configuration;
using System.Data.Common;
using System.Data;
using System.Utils;

namespace System.Data.Providers
{
    public class AppConfigDbConnectionName : DbConnectionNameBase
    {
        #region IDatabaseConnectionProvider Members

        public override DbConnection GetConnection()
        {
            var cnn = SqlClientFactory.CreateConnection(ConnectionString);
            cnn.Open();
            return cnn;
        }

        public override DbConnection GetConnection(params object[] prms)
        {
            var cnn = SqlClientFactory.CreateConnection(string.Format(ConnectionString, args: prms));
            cnn.Open();
            return cnn;
        }

        public override DbConnection GetMasterDbConnection()
        {
            var sb = ConnStrUtils.Parse(ConnectionString);
            sb.InitialCatalog = "master";
            var cnn = SqlClientFactory.CreateConnection(sb.ConnectionString);
            cnn.Open();
            return cnn;
        }

        public override void Initialize()
        {
#if NETCOREAPP2_0
            var cnn = MyAppEnv.SqlConnections[Name];
            var providerName = cnn.ProviderName;
            ConnectionString = cnn.ConnectionString;
#else
            var cs = ConfigurationManager.ConnectionStrings[Name]; 
            ConnectionString = cs.ConnectionString;
            var providerName = cs.ProviderName;
#endif

            if (ConnectionString.StartsWith("metadata=res://"))
            {
                // entity framework connection
                var entity = new MyEntityConnectionStringBuilder(ConnectionString);
                var sb = ConnStrUtils.Parse(entity.ProviderConnectionString);
                ConnectionString = sb.ConnectionString;
            }
            if (string.IsNullOrEmpty(providerName))
                return;
            switch (providerName.ToLower())
            {
                case "mssql":
                    EngineType = DbEngineType.SqlServer;
                    var sb = ConnStrUtils.Parse(ConnectionString);
                    ConnectionString = sb.ConnectionString;
                    break;
#if DB2
                case "db2":
                    EngineType = DbEngineType.Db2;
                    break; 
#endif

            }
        }

        #endregion

        #region Default c'tor

        public AppConfigDbConnectionName(string name, DbEngineType engineType) : base(name, engineType) { }

        #endregion

        #region Public Methods

        public override bool TestConnection()
        {
            if (string.IsNullOrWhiteSpace(ConnectionString)) return false;
            try
            {
                using (var cnn = SqlClientFactory.CreateConnection(string.Format(ConnectionString)))
                {
                    cnn.Open();
                }
                return true;
            }
            catch { return false; }
        }

        #endregion

    }
} 
#endif