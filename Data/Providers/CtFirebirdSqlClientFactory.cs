﻿#if ( COREFULL || COREDATA) && FIREBIRD
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Data;
using FirebirdSql.Data.FirebirdClient;

namespace System.Data.Providers
{
    public sealed class CtFirebirdSqlClientFactory : CtSqlClientFactory
    {

        public CtFirebirdSqlClientFactory()
        {
            Engine = DbEngineType.Firebird;
            SqlDialect = new SqlServerDialect();
        }
        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection() => new FbConnection();

        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection(string connectionString) => new FbConnection(connectionString);

        public override ISqlDialect SqlDialect { get; }
        public override void CustomizeCommand(DbCommand cmd)
        {
            
        }
    }
} 
#endif