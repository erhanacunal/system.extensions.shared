﻿#if ( COREFULL || COREDATA) && MYSQL
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace System.Data.Providers
{
    public sealed class CtMySqlSqlClientFactory : CtSqlClientFactory
    {
        public CtMySqlSqlClientFactory()
        {
            Engine = DbEngineType.MySql;
            SqlDialect = new SqlServerDialect();
        }
        public override DbConnection CreateConnection()
        {
            return new MySqlConnection();
        }

        public override DbConnection CreateConnection(string connectionString)
        {
            return new MySqlConnection(connectionString);
        }

        public override ISqlDialect SqlDialect { get; }

        public override void CustomizeCommand(DbCommand cmd)
        {

        }
    }
}
#endif