﻿#if COREFULL || COREDATA
using System.Data.Common;
using System.Data;

namespace System.Data.Providers
{
    public abstract class CtSqlClientFactory
    {
        public DbEngineType Engine { get; protected set; }
        public abstract DbConnection CreateConnection();
        public abstract DbConnection CreateConnection(string connectionString);
        public abstract ISqlDialect SqlDialect { get; }
        public abstract void CustomizeCommand(DbCommand cmd);
    }
} 
#endif