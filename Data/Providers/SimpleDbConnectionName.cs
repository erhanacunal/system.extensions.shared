﻿#if COREFULL || COREDATA
using System.Data.Common;
using System.Data;
using System.Utils;

namespace System.Data.Providers
{

    public class SimpleDbConnectionName : DbConnectionNameBase
    {
        #region Default c'tor

        public SimpleDbConnectionName(DbEngineType provType, string name, string connectionString) : this(provType, null, name, connectionString) { }

        public SimpleDbConnectionName(DbEngineType engineType, CtSqlClientFactory factory, string name, string connectionString) : base(name, engineType)
        {
            ProviderFactory = factory;
            if (engineType == DbEngineType.SqlServer && !connectionString.StartsWith("#"))
            {
                var sb = ConnStrUtils.Parse(connectionString);
                ConnectionString = sb.ConnectionString;
            }
            else
                ConnectionString = connectionString;

        }

        #endregion

        #region IDatabaseConnectionProvider Members

        public override DbConnection GetConnection()
        {
            var cnn = SqlClientFactory.CreateConnection(ConnectionString);
            cnn.Open();
            return cnn;
        }

        public override DbConnection GetConnection(params object[] prms)
        {
            var cnn = SqlClientFactory.CreateConnection(string.Format(ConnectionString, prms));
            cnn.Open();
            return cnn;
        }

        public override DbConnection GetMasterDbConnection()
        {
            var parts = ConnectionString.Split('=', ';');
            StringUtils.Instance.ReplaceValue(ref parts, "Initial Catalog", "master");
            var cnnStr = StringUtils.Instance.JoinNameValuePairs(parts);
            var cnn = SqlClientFactory.CreateConnection(cnnStr);
            cnn.Open();
            return cnn;
        }

        public override void Initialize()
        {

        }

        public override bool TestConnection()
        {
            if (ConnectionString.IsNullOrWhitespace()) return false;
            try
            {
                using (var cnn = SqlClientFactory.CreateConnection(string.Format(ConnectionString)))
                {
                    cnn.Open();
                }
                return true;
            }
            catch { return false; }
        }

        #endregion


    }
}

#endif