﻿#if COREFULL || COREDATA
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Data;

namespace System.Data.Providers
{
    public class CtSqlServerClientFactory : CtSqlClientFactory
    {

        public CtSqlServerClientFactory()
        {
            Engine = DbEngineType.SqlServer;
            SqlDialect = new SqlServerDialect();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection()
        {
            return new SqlConnection();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        public override ISqlDialect SqlDialect { get; }
        public override void CustomizeCommand(DbCommand cmd)
        {

        }
    }
}
#endif