#if COREFULL || COREDATA
using System.Configuration;
using System.Data;

namespace System.Data.Providers
{
    public sealed class AppConfigDbConnectionNameFactory : IDbConnectionNameFactory
    {
        public bool TryCreate(string name, out IDbConnectionName instance)
        {
            instance = null;
#if NETCOREAPP2_0
            if (!MyAppEnv.SqlConnections.TryGetValue(name, out var cnn))
                return false;
#else
            var cnn = ConfigurationManager.ConnectionStrings[name];
            if (cnn == null) return false; 
#endif
            instance = new AppConfigDbConnectionName(name, DbEngineType.SqlServer);
            return true;
        }
    }
} 
#endif