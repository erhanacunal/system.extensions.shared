#if (COREFULL || COREDATA) && ORACLE
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Data;
using Oracle.DataAccess.Client;

namespace System.Data.Providers
{
    public class CtOracleClientFactory : CtSqlClientFactory
    {

        public CtOracleClientFactory()
        {
            Engine = DbEngineType.Oracle;
            SqlDialect = new OracleDialect();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection()
        {
            return new OracleConnection();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public override DbConnection CreateConnection(string connectionString)
        {
            return new OracleConnection(connectionString);
        }

        public override ISqlDialect SqlDialect { get; }
        public override void CustomizeCommand(DbCommand cmd)
        {
            if (!(cmd is OracleCommand oraCmd)) return;
            oraCmd.BindByName = true;
        }
    }
}
#endif