﻿#if COREFULL || COREDATA
using System;
using System.Data.Common;
using System.Data;
using System.Data.Schemas;

namespace System.Data.Providers
{
    public interface IDbConnectionName : IDisposable
    {
        string Name { get; }
        uint NameHash { get; }
        DbConnection GetConnection();
        DbConnection GetMasterDbConnection();
        DbConnection GetConnection(params object[] prms);
        DbEngineType EngineType { get; }
        CtSqlClientFactory SqlClientFactory { get; }
        void Initialize();
        string GetPropertyValue(string name);
        string ConnectionString { get; }
        bool TestConnection();
        SqlDatabaseSchema Database { get; }

    }
}

#endif