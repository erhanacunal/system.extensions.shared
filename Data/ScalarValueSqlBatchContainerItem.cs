﻿using System.Data.Common;
using System.Utils;
using JetBrains.Annotations;

namespace System.Data
{
    sealed class ScalarValueSqlBatchContainerItem<T> : SqlDataReaderBatchContainerItem
    {
        readonly Action<T> setterAction;
        readonly T defaultValue;
        readonly bool hasDefault;

        public ScalarValueSqlBatchContainerItem([NotNull] Action<T> setterAction, CommandType cmdType, string sqlText, object[] prms) : base(cmdType, sqlText, prms)
        {
            this.setterAction = setterAction ?? throw new ArgumentNullException(nameof(setterAction));
            hasDefault = false;
        }

        public ScalarValueSqlBatchContainerItem([NotNull] Action<T> setterAction) 
        {
            this.setterAction = setterAction ?? throw new ArgumentNullException(nameof(setterAction));
            hasDefault = false;
        }

        public ScalarValueSqlBatchContainerItem([NotNull] Action<T> setterAction, T defaultValue, CommandType cmdType, string sqlText, object[] prms) : base(cmdType, sqlText, prms)
        {
            this.setterAction = setterAction ?? throw new ArgumentNullException(nameof(setterAction));
            this.defaultValue = defaultValue;
            hasDefault = true;
        }

        public ScalarValueSqlBatchContainerItem([NotNull] Action<T> setterAction, T defaultValue) 
        {
            this.setterAction = setterAction ?? throw new ArgumentNullException(nameof(setterAction));
            this.defaultValue = defaultValue;
            hasDefault = true;
        }
        public override void ProcessDataReader(DbDataReader reader)
        {
            if (!reader.Read())
            {
                if (hasDefault)
                    setterAction(defaultValue);
                return;
            }
            setterAction(TypeUtils.CoerceValue<T>(reader.GetValue(0)));
        }
    }
}