﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.Data
{
    sealed class SqlBulkCopyBatchContainer : SqlBatchContainer
    {
        readonly List<SqlBulkCopyBatchContainerItem> items = new List<SqlBulkCopyBatchContainerItem>();
        public override void AddItem(SqlBatchContainerItem containerItem)
        {
            if (!(containerItem is SqlBulkCopyBatchContainerItem drTask))
                throw new ArgumentException("task, SqlBulkCopyBatchContainerItem tipinde olmalı!", nameof(containerItem));
            items.Add(drTask);
        }

        internal override async Task Execute(ConnectionData cnn)
        {
            foreach (var item in items)
                await cnn.SqlDialect.BulkInsertAsync(cnn, item.Table, item.Options);
        }
    }
}