﻿#if COREFULL || COREDATA || MOBILE
#if JSON
using Newtonsoft.Json; 
#endif

namespace System.Data
{
    public interface ISqlQueryParameter
    {
#if JSON
        [JsonProperty("name")] 
#endif
        string Name { get; }
#if !MOBILE
#if JSON
        [JsonIgnore]
#endif
        object SqlValue { get; } 
#endif
    }
}

#endif