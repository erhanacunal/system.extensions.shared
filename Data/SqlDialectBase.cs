﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Data.Providers;
using System.Data.Schemas;
using System.Reflection;
using System.Threading.Tasks;

namespace System.Data
{
    public abstract class SqlDialectBase : ISqlDialect
    {
        public abstract SqlTableSchema GetTableWithColumns(IDbConnectionName cnnName, string tableName);
        public abstract SqlTableSchema GetTable(IDbConnectionName cnnName, string tableName);
        public abstract IList<SqlTableColumnSchema> GetTableColumns(IDbConnectionName cnnName, SqlTableSchema table);
        public abstract IList<SqlForeignKeySchema> GetTableForeignKeys(IDbConnectionName cnnName, SqlTableSchema table);
        public abstract ISqlTypeInfo GetTypeInfo(string typeName);
        public abstract ISqlTypeInfo GetTypeInfo(int id);
        public abstract ISqlTypeInfo GetTypeInfo(Type type);
        public abstract object[] ConvertSqlQueryParameters(IEnumerable<ISqlQueryParameter> parameters, Action<SqlParamBuilder> builderAction = null);
        public abstract string GetColumnDefinition(SqlTableColumnSchema column,bool emitCollation = true);
        public abstract void GetColumnDefinition(StringBuilder sb, SqlTableColumnSchema column, bool emitCollation = true);
        public abstract void GetColumnDefinition(TextWriter writer, SqlTableColumnSchema column, bool emitCollation = true);
        public abstract string DbTypeToTypeName(DbType dbType);
        public abstract string CreateTableScriptFromDataTable(string tableName, DataTable table, bool memoryTable);
        public abstract void CreateTableScriptFromDataTable(TextWriter writer, string tableName, DataTable table, bool memoryTable);
        public abstract void CreateTableScriptFromSchema(TextWriter writer, SqlTableSchema table, bool memoryTable, string tableName = null);
        public abstract string GetAutoParameterName();
        public abstract string GenerateInsertScript<T>(IDbConnectionName cnnName,T instance, PropertyProvider<T> propertyProvider, 
            out bool requiresReader, out object[] prms, bool identityInsert = false) where T : class;

        public abstract string GenerateUpdateScript<T>(IDbConnectionName cnnName, T instance, PropertyProvider<T> propertyProvider,
            out object[] prms) where T : class;

        public abstract Task BulkInsertAsync(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options);
        public abstract void BulkInsert(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options);

        public abstract IList<SqlTableIndexSchema> GetTableIndexes(IDbConnectionName cnnName, SqlTableSchema table);
        public abstract IList<SqlTableIndexColumnSchema> GetTableIndexColumns(IDbConnectionName cnnName, SqlTableIndexSchema index);
    }
}

#endif