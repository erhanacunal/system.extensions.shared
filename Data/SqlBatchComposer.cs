﻿using System;
using System.Collections.Generic;
using System.Data.Providers;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace System.Data
{

    /*
     * SqlBatchComposer
     * |--- SqlBatchTargetConnection
     *      |--- SqlBatchContainer
     *           |--- SqlBatchContainerItem
     *           |--- SqlDataReaderBatchContainer
     *                |--- SqlDataReaderBatchContainerItem
     *                |--- ObjectListSqlBatchContainerItem
     */

    public sealed class SqlBatchComposer
    {
        readonly IDictionary<IDbConnectionName, SqlBatchTargetConnection> connections = new Dictionary<IDbConnectionName, SqlBatchTargetConnection>();

        public SqlBatchTargetConnection BeginConnection(string cnnName)
        {
            var dbCnnName = ConnectionManager.Find(cnnName);
            if (connections.TryGetValue(dbCnnName, out var targetConnection)) 
                return targetConnection;
            targetConnection = new SqlBatchTargetConnection(this, dbCnnName);
            connections.Add(dbCnnName, targetConnection);
            return targetConnection;
        }

        public async Task Execute()
        {
            foreach (var connection in connections.Values)
                await connection.Execute();
        }
    }
}
