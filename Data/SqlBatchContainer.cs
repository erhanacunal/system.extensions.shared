﻿using System.Threading.Tasks;

namespace System.Data
{
    /// <summary>
    /// Toplu sql işlerini tutan soyut sınıf
    /// Bu sınıfın altında SqlBatchTask'lar toplanır
    /// </summary>
    abstract class SqlBatchContainer
    {
        public abstract void AddItem(SqlBatchContainerItem containerItem);

        internal abstract Task Execute(ConnectionData cnn);
    }
}