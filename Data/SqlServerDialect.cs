﻿#if COREFULL || COREDATA
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
#if MEF
using System.ComponentModel.Composition;
#endif
using System.Data.Providers;
using System.Data.Schemas;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Utils;
using JetBrains.Annotations;

namespace System.Data
{
    public sealed class SqlServerDialect : SqlDialectBase
    {
        static readonly IDbHelper MyDbHelper;

        static SqlServerDialect()
        {
#if MEF
            MyDbHelper = MefTypeResolver.Resolve<IDbHelper>();
#else
            MyDbHelper = new SqlServerDbHelperImpl();
#endif
        }

        public override SqlTableSchema GetTableWithColumns(IDbConnectionName cnnName, string tableName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                SqlTableSchema table = null;
                var columns = new List<SqlTableColumnSchema>();
                MyDbHelper.ExecuteSingleQueryMultipleResults(cnn, GET_TABLE_AND_COLUMNS, tableName)
                    .Object<SqlTableSchema>(t => table = t)
                    .ObjectList(columns)
                    .Execute();
                if (table == null) return table;
                table.Database = cnnName.Database;
                foreach (var column in columns)
                {
                    column.Table = table;
                    column.TypeInfo = GetTypeInfo(column.TypeName);
                    if (column.MaxLength > 0 && column.TypeInfo.IsUnicode)
                        column.MaxLength /= 2;
                }
                table.Columns.Load(columns);
                return table;
            }
        }

        public override SqlTableSchema GetTable(IDbConnectionName cnnName, string tableName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var table = MyDbHelper.ExecuteObject<SqlTableSchema>(cnn, CommandType.Text, GET_TABLE, tableName);
                if (table == null) return null;
                table.Database = cnnName.Database;
                return table;
            }
        }

        public override IList<SqlTableColumnSchema> GetTableColumns(IDbConnectionName cnnName, SqlTableSchema table)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var columns = MyDbHelper.ExecuteObjectList<SqlTableColumnSchema>(cnn, CommandType.Text, GET_TABLE_COLUMNS, table.ObjectId);
                foreach (var column in columns)
                {
                    column.Table = table;
                    column.TypeInfo = GetTypeInfo(column.TypeName);
                    if (column.MaxLength > 0 && column.TypeInfo.IsUnicode)
                        column.MaxLength /= 2;
                }
                return columns;
            }
        }

        public override IList<SqlForeignKeySchema> GetTableForeignKeys(IDbConnectionName cnnName, SqlTableSchema table)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var fkeys = MyDbHelper.ExecuteObjectList<SqlForeignKeySchema>(cnn, CommandType.Text, GET_TABLE_FOREIGN_KEYS, table.ObjectId);
                foreach (var fkey in fkeys)
                    fkey.ForeignTable = table;
                return fkeys;
            }
        }

        public override object[] ConvertSqlQueryParameters(IEnumerable<ISqlQueryParameter> parameters, Action<SqlParamBuilder> builderAction = null)
        {
            var builder = new SqlParamBuilder(true);
            foreach (var parameter in parameters)
            {
                var name = parameter.Name;
                if (name.IsNullOrWhitespace()) continue;
                if (name.StartsWithLetter())
                    name = string.Concat("@", name);
                else if (name[0] == ':')
                    name = string.Concat("@", name.Substring(1));

                builder.Add2(name, parameter.SqlValue);
            }
            builderAction?.Invoke(builder);
            return builder.Build();
        }

        public override string GetColumnDefinition(SqlTableColumnSchema column, bool emitCollation = true)
        {
            var sb = new StringBuilder();
            GetColumnDefinition(sb, column, emitCollation);
            return sb.ToString();
        }

        public override void GetColumnDefinition(StringBuilder sb, SqlTableColumnSchema column, bool emitCollation = true)
        {
            sb.AppendFormat("[{0}] {1}", column.Name, column.TypeName);
            if (column.TypeInfo.IsSizable)
            {
                var size = column.MaxLength == -1 ? (object)"MAX" : column.MaxLength;
                if (column.TypeInfo.IsString && column.CollationName.IsNotNullOrWhitespace() && emitCollation)
                    sb.AppendFormat("({0}) COLLATE {1}", size, column.CollationName);
                else
                    sb.AppendFormat("({0})", size);
            }
            else if (column.TypeInfo.IsNumeric && column.TypeInfo.HasPrecisionScale)
            {
                sb.AppendFormat("({0},{1})", column.Precision, column.Scale);
            }
        }

        public override void GetColumnDefinition(TextWriter writer, SqlTableColumnSchema column, bool emitCollation = true)
        {
            writer.Write("[{0}] {1}", column.Name, column.TypeName);
            if (column.TypeInfo.IsSizable)
            {
                var size = column.MaxLength == -1 ? (object)"MAX" : column.MaxLength;
                if (column.TypeInfo.IsString && column.CollationName.IsNotNullOrWhitespace() && emitCollation)
                    writer.Write("({0}) COLLATE {1}", size, column.CollationName);
                else
                    writer.Write("({0})", size);
            }
            else if (column.TypeInfo.IsNumeric && column.TypeInfo.HasPrecisionScale)
            {
                writer.Write("({0},{1})", column.Precision, column.Scale);
            }
        }

        public override string GetAutoParameterName()
        {
            return "@auto_" + (8).RandomString();
        }

        public override string GenerateInsertScript<T>(IDbConnectionName cnnName, T instance, [NotNull] PropertyProvider<T> propertyProvider, out bool requiresReader, out object[] prms, bool identityInsert = false)
        {
            if (propertyProvider == null) throw new ArgumentNullException(nameof(propertyProvider));
            var sql = new StringBuilder(200);
            var accessor = propertyProvider.Accessor;
            var identityProperty = accessor.IdentityProperty;
            requiresReader = false;
            if (identityProperty != null)
            {
                if (identityInsert)
                {
                    sql.AppendFormat("SET IDENTITY_INSERT {0} ON;\r\n", accessor.TableName);
                    propertyProvider.AddProperty(identityProperty);
                }
                else
                {
                    propertyProvider.ExcludeProperties(identityProperty);
                    requiresReader = true;
                }
            }

            var props = propertyProvider.GetProperties();
            prms = null;
            if (props.Count == 0)
                throw new InvalidOperationException($"There is no columns to insert for type {accessor.Type.Name}");
            var tableMetadata = cnnName.Database.GetCachedTable(accessor.TableName);
            sql.AppendFormat("INSERT INTO {0}(", accessor.TableName);
            sql.Append(string.Join(",", props.Select(_ => string.Concat("[", _.ColumnName, "]"))));
            sql.Append(") VALUES (");
            var i = 0;
            prms = new object[props.Count];
            foreach (var prop in props)
            {
                if (i++ > 0)
                    sql.Append(',');
                sql.AppendFormat("@p{0}", i);
                var fd = prop.GetPropertyData(instance);
                var val = fd.Value ?? DBNull.Value;
                var size = fd.Size;
                var mt = tableMetadata.FindColumn(prop.ColumnNameHash);
                if (mt.TypeName.IsOneOfThese(TableInfo.SizableTypes) &&
                    (mt.MaxLength > 0) && (size > mt.MaxLength))
                {
                    if (prop.UnderlyingTypeCode == TypeCodeEx.String)
                        val = ((string) val).Substring(0, mt.MaxLength);
                    else if (prop.UnderlyingTypeCode == TypeCodeEx.ByteArray)
                    {
                        var byteArray = (byte[]) val;
                        Array.Resize(ref byteArray, mt.MaxLength);
                        val = byteArray;
                    }
                    else
                        throw new NotImplementedException();
                }

                prms[i - 1] = val;
            }
            sql.Append(");");
            if (identityProperty != null)
            {
                if (identityInsert)
                    sql.AppendFormat("SET IDENTITY_INSERT {0} OFF;\r\n", accessor.TableName);
                else
                    sql.AppendLine("SELECT SCOPE_IDENTITY();");
            }

            return sql.ToString();
        }

        public override string GenerateUpdateScript<T>(IDbConnectionName cnnName, T instance, PropertyProvider<T> propertyProvider,
            out object[] prms)
        {
            if (propertyProvider == null) throw new ArgumentNullException(nameof(propertyProvider));
            var sql = new StringBuilder(200);
            var accessor = propertyProvider.Accessor;
            propertyProvider.ExcludeProperties(accessor.PrimaryKeys);
            var props = propertyProvider.GetProperties();
            prms = null;
            if (props.Count == 0)
                throw new InvalidOperationException($"There is no columns to insert for type {accessor.Type.Name}");
            var tableMetadata = cnnName.Database.GetCachedTable(accessor.TableName);
            sql.AppendFormat("UPDATE [{0}] SET ", accessor.TableName);
            var i = 0;
            prms = new object[props.Count + accessor.PrimaryKeys.Count];
            foreach (var prop in props)
            {
                if (i++ > 0)
                    sql.Append(",");
                var fd = prop.GetPropertyData(instance);
                sql.AppendFormat("{0} = @p{1}", prop.ColumnName, i);
                var val = fd.Value;
                var size = fd.Size;
                var mt = tableMetadata.FindColumn(prop.ColumnNameHash);
                if (mt.TypeName.IsOneOfThese(TableInfo.SizableTypes) &&
                    (mt.MaxLength > 0) && (size > mt.MaxLength))
                {
                    if (prop.UnderlyingTypeCode == TypeCodeEx.String)
                        val = ((string) val).Substring(0, mt.MaxLength);
                    else if (prop.UnderlyingTypeCode == TypeCodeEx.ByteArray)
                    {
                        var byteArray = (byte[]) val;
                        Array.Resize(ref byteArray, mt.MaxLength);
                        val = byteArray;
                    }
                    else
                        throw new NotImplementedException();
                }
                prms[i - 1] = val;
            }
            sql.Append(" WHERE ");
            var j = 0;
            foreach (var pk in accessor.PrimaryKeys)
            {
                if (j++ > 0)
                    sql.Append(" AND ");
                i++;
                sql.AppendFormat("( {0} = @p{1} )", pk.ColumnName, i);
                prms[i - 1] = pk.Getter.GetValue(instance);
            }
            return sql.ToString();
        }

        public override async Task BulkInsertAsync(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options)
        {
            using (var sbc = new SqlBulkCopy((SqlConnection) cnn.Connection, (SqlBulkCopyOptions)(int) options,
                (SqlTransaction) cnn.Transaction))
            {
                sbc.DestinationTableName = table.TableName;
                sbc.BulkCopyTimeout = ConnectionManager.CommandTimeout;
                foreach (DataColumn column in table.Columns)
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                await sbc.WriteToServerAsync(table);
            }
        }

        public override void BulkInsert(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options)
        {
            using (var sbc = new SqlBulkCopy((SqlConnection)cnn.Connection, (SqlBulkCopyOptions)(int)options,
                (SqlTransaction)cnn.Transaction))
            {
                sbc.DestinationTableName = table.TableName;
                sbc.BulkCopyTimeout = ConnectionManager.CommandTimeout;
                foreach (DataColumn column in table.Columns)
                    sbc.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                sbc.WriteToServer(table);
            }
        }

        public override string DbTypeToTypeName(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.AnsiString:
                    return "varchar";
                case DbType.Binary:
                    return "binary";
                case DbType.Byte:
                    return "tinyint";
                case DbType.Boolean:
                    return "bit";
                case DbType.Currency:
                    return "money";
                case DbType.Date:
                    return "date";
                case DbType.DateTime:
                    return "datetime";
                case DbType.Decimal:
                    return "decimal";
                case DbType.Double:
                    return "float";
                case DbType.Guid:
                    return "uniqueidentifier";
                case DbType.Int16:
                    return "smallint";
                case DbType.Int32:
                    return "int";
                case DbType.Int64:
                    return "bigint";
                case DbType.Object:
                    return "sql_variant";
                case DbType.SByte:
                    return "tinyint";
                case DbType.Single:
                    return "real";
                case DbType.String:
                    return "nvarchar";
                case DbType.Time:
                    return "time";
                case DbType.UInt16:
                    return "smallint";
                case DbType.UInt32:
                    return "int";
                case DbType.UInt64:
                    return "bigint";
                case DbType.VarNumeric:
                    throw new InvalidOperationException("Not valid dbType");
                case DbType.AnsiStringFixedLength:
                    return "char";
                case DbType.StringFixedLength:
                    return "nchar";
                case DbType.Xml:
                    return "xml";
                case DbType.DateTime2:
                    return "datetime2";
                case DbType.DateTimeOffset:
                    return "datetimeoffset";
                default:
                    throw new ArgumentOutOfRangeException(nameof(dbType), dbType, null);
            }
        }

        public override string CreateTableScriptFromDataTable(string tableName, DataTable table, bool memoryTable)
        {
            using (var writer = new StringWriter())
            {
                CreateTableScriptFromDataTable(writer, tableName, table, memoryTable);
                return writer.ToString();
            }
        }

        public override void CreateTableScriptFromDataTable(TextWriter writer, string tableName, DataTable table, bool memoryTable)
        {
            writer.WriteLine(memoryTable
                ? "DECLARE {0} TABLE ("
                : "CREATE TABLE [{0}] (", tableName);
            for (var i = 0; i < table.Columns.Count; i++)
            {
                if (i > 0)
                    writer.WriteLine(",");
                var col = table.Columns[i];
                var propValue = col.ExtendedProperties["DBTYPE"];
                var sti = propValue == null ? SqlServerTypeInfo.GetByType(col.DataType) : SqlServerTypeInfo.GetByName((string)propValue);
                writer.Write("[{0}] ", col.ColumnName);
                if (sti.IsSizable)
                    writer.Write("{0}({1})", sti.Name, col.MaxLength == 0 ? "MAX" : col.MaxLength + "");
                else if (sti.IsNumeric && sti.HasPrecisionScale)
                {
                    writer.Write("{0}({1},{2})", sti.Name,
                        col.ExtendedProperties.GetValue("PRECISION", 19),
                        col.ExtendedProperties.GetValue("SCALE", 2));
                }
                else
                    writer.Write(sti.Name);
                foreach (DictionaryEntry kv in col.ExtendedProperties)
                {
                    if ("COLLATION".Equals(kv.Key))
                        writer.Write(" COLLATE {0}", kv.Value);
                    else if ("IDENTITY".Equals(kv.Key))
                        writer.Write(" IDENTITY(1,1)");
                }
            }
            writer.Write("\r\n);");
        }

        public override void CreateTableScriptFromSchema(TextWriter writer, SqlTableSchema table, bool memoryTable, string tableName = null)
        {
            tableName = tableName ?? table.Name;
            writer.WriteLine(memoryTable
                ? "DECLARE {0} TABLE ("
                : "CREATE TABLE [{0}] (", tableName);
            for (int i = 0; i < table.Columns.Count; i++)
            {
                var column = table.Columns[i];
                if (i > 0)
                    writer.WriteLine(",");
                GetColumnDefinition(writer, column);
            }
            writer.Write("\r\n);");
        }

        public override IList<SqlTableIndexSchema> GetTableIndexes(IDbConnectionName cnnName, SqlTableSchema table)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var indexes = MyDbHelper.ExecuteObjectList<SqlTableIndexSchema>(cnn, CommandType.Text, GET_TABLE_INDEXES, table.ObjectId);
                foreach (var index in indexes)
                    index.Table = table;
                return indexes;
            }
        }

        public override IList<SqlTableIndexColumnSchema> GetTableIndexColumns(IDbConnectionName cnnName, SqlTableIndexSchema index)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
            {
                var columns = MyDbHelper.ExecuteObjectList<SqlTableIndexColumnSchema>(cnn, CommandType.Text, GET_INDEX_COLUMNS, index.Table.ObjectId, index.IndexId);
                foreach (var column in columns)
                    column.Index = index;
                return columns;
            }
        }

        public override ISqlTypeInfo GetTypeInfo(string typeName) => SqlServerTypeInfo.GetByName(typeName);

        public override ISqlTypeInfo GetTypeInfo(int id) => SqlServerTypeInfo.GetById(id);

        public override ISqlTypeInfo GetTypeInfo(Type type) => SqlServerTypeInfo.GetByType(type);

        //public override void GetAllForeignKeys(IDbConnectionName cnnName, IList<SqlForeignKeyMetadata> metadatas)
        //{
        //    if (metadatas == null) throw new ArgumentNullException(nameof(metadatas));
        //    using (var cnn = ConnectionManager.Get(cnnName))
        //    {
        //        MyDbHelper.ExecuteObjectList(cnn, CommandType.Text, metadatas, FOREIGN_KEYS_SQL);
        //        foreach (var metadata in metadatas)
        //            metadata.Database = cnnName.Database;
        //    }
        //}

        //public override void GetAllTables(IDbConnectionName cnnName, IList<SqlTableMetadata> tables)
        //{
        //    if (tables == null) throw new ArgumentNullException(nameof(tables));

        //    using (var cnn = ConnectionManager.Get(cnnName))
        //    {
        //        MyDbHelper.ExecuteReader(cnn, CommandType.Text, reader =>
        //        {
        //            var accessor = TypeAccessor.CreateOrGet<SqlColumnMetadata>();
        //            var mapper = new TypeDataReaderMapper(accessor, reader, 1);
        //            string curTableName = null;
        //            SqlTableMetadata curTable = null;
        //            while (reader.Read())
        //            {
        //                var tableName = reader.GetString(0) ?? string.Empty;
        //                if (!Equals(tableName, curTableName))
        //                {
        //                    curTableName = tableName;
        //                    curTable = new SqlTableMetadata { Database = cnnName.Database, Name = tableName };
        //                    tables.Add(curTable);
        //                }
        //                var column = new SqlColumnMetadata();
        //                mapper.Map(column);
        //                column.Table = curTable;

        //                curTable.Columns.Add(column);
        //            }
        //        }, GET_ALL_TABLES_SQL);
        //    }
        //}


        const string GET_ALL_TABLES_SQL = @"SELECT TableName=t.name, Name=c.name,TypeName=ty.name,CollationName=c.collation_name,Precision=c.[precision],Scale=c.[scale],MaxLength=c.max_length FROM sys.columns c 
  INNER JOIN sys.tables t ON c.object_id = t.object_id AND t.type = 'U'
  INNER JOIN sys.types ty ON c.user_type_id=ty.user_type_id
ORDER BY t.name";

        const string FOREIGN_KEYS_SQL = @"--DECLARE @pktable_id int = object_id(quotename(@p1))
select        
    MasterTableName         = convert(sysname,o1.name),
    MasterTableColumnName   = convert(sysname,c1.name),        
    ForeignTableName        = convert(sysname,o2.name),
    ForeignTableColumnName  = convert(sysname,c2.name),
    Name                    = convert(sysname,object_name(f.object_id)),
    PrimaryKeyName          = convert(sysname,i.name)    
from
    sys.objects o1,
    sys.objects o2,
    sys.columns c1,
    sys.columns c2,
    sys.foreign_keys f inner join
    sys.foreign_key_columns k on (k.constraint_object_id = f.object_id) inner join
    sys.indexes i on (f.referenced_object_id = i.object_id and f.key_index_id = i.index_id)
where
    o1.object_id = f.referenced_object_id and
    --(o1.object_id = @pktable_id) and
    o2.object_id = f.parent_object_id and
    c1.object_id = f.referenced_object_id and
    c2.object_id = f.parent_object_id and
    c1.column_id = k.referenced_column_id and
    c2.column_id = k.parent_column_id
order by 1,2,3,4";

        const string GET_TABLE_FOREIGN_KEYS = @"select        
    MasterTableName         = convert(sysname,o1.name),
    MasterTableColumnName   = convert(sysname,c1.name),        
    ForeignTableName        = convert(sysname,o2.name),
    ForeignTableColumnName  = convert(sysname,c2.name),
    Name                    = convert(sysname,object_name(f.object_id)),
    PrimaryKeyName          = convert(sysname,i.name)    
from
    sys.objects o1,
    sys.objects o2,
    sys.columns c1,
    sys.columns c2,
    sys.foreign_keys f inner join
    sys.foreign_key_columns k on (k.constraint_object_id = f.object_id) inner join
    sys.indexes i on (f.referenced_object_id = i.object_id and f.key_index_id = i.index_id)
where
    o1.object_id = f.referenced_object_id and    
    o2.object_id = f.parent_object_id and
	o2.object_id = @p1 and 
    c1.object_id = f.referenced_object_id and
    c2.object_id = f.parent_object_id and
    c1.column_id = k.referenced_column_id and
    c2.column_id = k.parent_column_id
order by 1,2,3,4";

        const string GET_TABLE_AND_COLUMNS = @"DECLARE @objId int = OBJECT_ID(@p1);
SELECT 
  Name=o.name ,
  ObjectId=object_id,
  IdentitySeed=IDENT_SEED(sc.name + '.' + o.name),
  IdentityIncrement=IDENT_INCR(sc.name + '.' + o.name),  
  SchemaName=sc.name 
FROM sys.objects o 
  INNER JOIN sys.schemas sc on o.schema_id = sc.schema_id
WHERE type = 'U' and o.object_id=@objId;

-- columns

SELECT 
    ObjectId=c.object_id,
    Name=c.name,
    ColumnId=c.column_id,
    MaxLength=c.max_length,
    Precision=c.precision,
    Scale=c.scale,
    IsNullable=c.is_nullable,
    IsIdentity=c.is_identity,
    IsComputed=c.is_computed,
    TypeName=t.name,
    TypeMaxLength=t.max_length,
    CollationName=c.collation_name 
from sys.columns c
  inner join sys.types t on c.user_type_id = t.user_type_id where c.object_id = @objId";

        const string GET_TABLE = @"DECLARE @objId int = OBJECT_ID(@p1);
SELECT 
  Name=o.name ,
  ObjectId=object_id,
  IdentitySeed=IDENT_SEED(sc.name + '.' + o.name),
  IdentityIncrement=IDENT_INCR(sc.name + '.' + o.name),  
  SchemaName=sc.name 
FROM sys.objects o 
  INNER JOIN sys.schemas sc on o.schema_id = sc.schema_id
WHERE type = 'U' and o.object_id=@objId;";

        const string GET_TABLE_COLUMNS = @"SELECT 
    ObjectId=c.object_id,
    Name=c.name,
    ColumnId=c.column_id,
    MaxLength=c.max_length,
    Precision=c.precision,
    Scale=c.scale,
    IsNullable=c.is_nullable,
    IsIdentity=c.is_identity,
    IsComputed=c.is_computed,
    TypeName=t.name,
    TypeMaxLength=t.max_length,
    CollationName=c.collation_name 
from sys.columns c
  inner join sys.types t on c.user_type_id = t.user_type_id where c.object_id = @p1";

        const string GET_TABLE_INDEXES = @"select ObjectId=i.object_id,Name=i.name,IndexId=i.index_id,TypeDescription=i.type,IsUnique=i.is_unique,IsUniqueConstraint=i.is_unique_constraint,IsPrimaryKey=i.is_primary_key,IsDisabled=i.is_disabled from sys.indexes i  
where i.object_id = @p1 and i.type in (1,2)";

        const string GET_INDEX_COLUMNS = @"select ColumnId=ic.column_id,IsDescendingKey=ic.is_descending_key,IsIncludedColumn=ic.is_included_column,
KeyOrdinal=ic.key_ordinal,IndexColumnId=ic.index_column_id from sys.index_columns ic 
where ic.object_id = @p1 AND ic.index_id = @p2";
    }
}

#endif