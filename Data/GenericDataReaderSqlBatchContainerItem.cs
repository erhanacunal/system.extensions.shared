﻿using System.Data.Common;
using JetBrains.Annotations;

namespace System.Data
{
    sealed class GenericDataReaderSqlBatchContainerItem : SqlDataReaderBatchContainerItem
    {
        readonly Action<DbDataReader> readerAction;


        public GenericDataReaderSqlBatchContainerItem([NotNull] Action<DbDataReader> readerAction, CommandType cmdType, string sqlText, object[] prms) : base(cmdType, sqlText, prms)
        {
            this.readerAction = readerAction ?? throw new ArgumentNullException(nameof(readerAction));
        }

        public GenericDataReaderSqlBatchContainerItem([NotNull] Action<DbDataReader> readerAction)
        {
            this.readerAction = readerAction ?? throw new ArgumentNullException(nameof(readerAction));
        }

        public override void ProcessDataReader(DbDataReader reader) => readerAction(reader);
    }
}