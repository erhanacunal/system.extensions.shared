﻿#if COREFULL || COREDATA
namespace System.Data
{
    public enum DbEngineType
    {
        SqlServer = 1,
#if (COREFULL || COREDATA) && ORACLE
        Oracle = 2, 
#endif
#if (COREFULL || COREDATA) && FIREBIRD
        Firebird = 3,
#endif
#if (COREFULL || COREDATA) && DB2
        Db2 = 4,
#endif
#if (COREFULL || COREDATA) && MYSQL
        MySql = 5
#endif
    }
}

#endif