﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using System.Data.Models;
using System.Data.Schemas;
using System.Reflection;

namespace System.Data
{
    public interface IDbHelper
    {
        T ExecuteReader<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql, params object[] parameters);
        void ExecuteReader(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql, params object[] parameters);
        T ExecuteScalar<T>(ConnectionData cnn, string sql, params object[] parameters);
        T ExecuteScalar2<T>(ConnectionData cnn, string sql, T defaultValue, params object[] parameters);
        T[] ExecuteArray<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] parameters);
        Tuple<T, TK>[] ExecuteTupleArray<T, TK>(ConnectionData cnn, string sql, params object[] parameters);
        Tuple<T, TK, TM>[] ExecuteTupleArray<T, TK, TM>(ConnectionData cnn, string sql, params object[] parameters);
        Tuple<T, TK, TM, TL>[] ExecuteTupleArray<T, TK, TM, TL>(ConnectionData cnn, string sql, params object[] parameters);
        bool LoadVariables<TV1, TV2>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters);
        bool LoadVariables<TV1, TV2, TV3>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters);
        bool LoadVariables<TV1, TV2, TV3, TV4>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters);
        Tuple<TR1, TR2> ExecuteScalarResults<TR1, TR2>(ConnectionData cnn, string sql, params object[] parameters);
        void LoadScalarResults<TV1, TV2>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters);
        void LoadScalarResults<TV1, TV2, TV3>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters);
        void LoadScalarResults<TV1, TV2, TV3, TV4>(ConnectionData cnn, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters);
        Tuple<T1, T2> ExecuteTuple<T1, T2>(ConnectionData cnn, string sql, params object[] parameters);
        Tuple<T1, T2, T3> ExecuteTuple<T1, T2, T3>(ConnectionData cnn, string sql, params object[] parameters);
        Tuple<T1, T2, T3, T4> ExecuteTuple<T1, T2, T3, T4>(ConnectionData cnn, string sql, params object[] parameters);
        void ExecuteScalarList2<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters);
        void ExecuteScalarListUnionAll<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters);
        void ExecuteScalarDictionary2<TK, TV>(ConnectionData cnn, IDictionary<TK, TV> targetDict, string sql, params object[] parameters);
        string GetTempTableName();
        int ExecuteNonQuery(ConnectionData cnn, string sql, params object[] prms);
        int ExecuteNonQuerySp(ConnectionData cnn, string sp, params object[] prms);
        DataTable ExecuteDataTable(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms);
        void ExecuteDataTable(ConnectionData cnn, CommandType cmdType, DataTable targetTable, string sql, params object[] prms);
        DataSet ExecuteDataSet(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms);
        T ExecuteObject<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new();
        T ExecuteObject2<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        T ExecuteObjectVertical<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField, params object[] prms) where T : class, new();
        void ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new();
        void ExecuteObjectList2<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        List<T> ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new();
        List<T> ExecuteObjectList2<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        void ExecuteInsertAll<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class;
        Task ExecuteInsertAllAsync<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class;

        /// <summary>
        /// Hierarşik olarak ilişkilendirilmiş tablolarının tek bir işlem ile veritabanına eklenmesini sağlar. (Kendini referans eden tablolar dahil)
        /// </summary>
        /// <returns>Zincirleme çağrının yapılacağı görev örneği</returns>
        HierarchicalInsertAllBuilder ExecuteHierarchicalInsertAll();

        RecursiveQueryBuilder<T> ExecuteRecursiveQuery<T>(string parentFieldName, long idOrParentId, bool includeThisId) where T : class, new();
        void ExecuteUpdateAll<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class;
        Task ExecuteUpdateAllAsync<T>(ConnectionData cnn, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class;
        void ExecuteInsert<T>(ConnectionData cnn, T instance, PropertyProvider<T> propertyProvider, bool onlyModified) where T : class;
        void ExecuteUpdate<T>(ConnectionData cnn, T instance, PropertyProvider<T> propertyProvider, bool updatePrimaryKeys) where T : class;

        /// <summary>
        /// Sql ifadesinden gelen birden fazla sonucu, birbirinden bağımsız belirtilen biçimlerde aktarır 
        /// </summary>
        /// <param name="cnn">Sql bağlantı verisi</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>.
        SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(ConnectionData cnn, string sql, params object[] prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla sonucu, birbirinden bağımsız belirtilen biçimlerde aktarır 
        /// </summary>
        /// <param name="cnnName">Sql bağlantı adı</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>.
        SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(string cnnName, string sql, params object[] prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnn">Sql bağlantı verisi</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(ConnectionData cnn, IList<T> targetList, string sql,
            params object[] prms) where T : class, new();

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnnName">Sql bağlantı adı</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(string cnnName, IList<T> targetList, string sql,
            params object[] prms) where T : class, new();

        BatchUpsertEntitiesBuilder ExecuteBatchUpsert();

        bool DataExists2(ConnectionData cnn, string sql, params object[] prms);
        T ExecuteObjectAnonymous<T>(ConnectionData cnn, CommandType cmdType, string sql, T type, params object[] prms);
        IList<T> ExecuteListAnonymous<T>(ConnectionData cnn, CommandType cmdType, string sql, T type, params object[] prms);
        bool HasColumn(ConnectionData cnn, string tableName, string columnName);
        bool HasTable(ConnectionData cnn, string tableName);
        string CreateTableScriptFrom(DataTable tbl);
        void CreateTable(ConnectionData connection, DataTable tbl);
        Task CreateTableAsync(ConnectionData connection, DataTable tbl);
        void BulkInsert(ConnectionData connection, DataTable tbl);
        DataTable CreateDataTableFromSchema<T>(SqlTableSchema table, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class;


        // async //
        Task ExecuteReaderAsync(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql, params object[] parameters);
        Task<T> ExecuteReaderAsync<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql, params object[] parameters);
        Task<T> ExecuteScalarAsync<T>(ConnectionData cnn, string sql, params object[] parameters);
        Task<T> ExecuteScalar2Async<T>(ConnectionData cnn, string sql, T defaultValue, params object[] parameters);
        Task<int> ExecuteNonQueryAsync(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms);
        Task<DataTable> ExecuteDataTableAsync(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms);
        Task ExecuteDataTableAsync(ConnectionData cnn, CommandType cmdType, DataTable targetTable, string sql, params object[] prms);
        Task<T> ExecuteObjectAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new();
        Task<T> ExecuteObject2Async<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        Task<T> ExecuteObjectVerticalAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField, params object[] prms) where T : class, new();
        Task ExecuteObjectListAsync<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new();
        Task ExecuteObjectList2Async<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        Task<List<T>> ExecuteObjectListAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new();
        Task<List<T>> ExecuteObjectList2Async<T>(ConnectionData cnn, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new();
        Task BulkInsertAsync(ConnectionData connection, DataTable tbl);
        Task<bool> DataExistsAsync(ConnectionData cnn, string sql, params object[] prms);
        Task ExecuteScalarListAsync<T>(ConnectionData cnn, IList<T> targetList, string sql, params object[] parameters);
    }
}
#endif