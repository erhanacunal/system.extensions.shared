﻿using System.Text;

namespace System.Data
{
    /// <summary>
    /// Toplu sql iş görevi - Sql çalıştırıldıktan sonra sonucu kullanacak/değerlendirecek sınıf 
    /// </summary>
    abstract class SqlBatchContainerItem
    {
        public CommandType CmdType { get; }
        public string SqlText { get; }
        public object[] Prms { get; }
        public bool HasNamedParams { get; }
        public int ParamStartIndex { get; }

        protected SqlBatchContainerItem(CommandType cmdType, string sqlText, object[] prms)
        {
            CmdType = cmdType;
            SqlText = sqlText;
            Prms = prms;
            ParamStartIndex = -1;
            if (prms == null || prms.Length == 0) return;
            HasNamedParams = cmdType == CommandType.StoredProcedure || prms[0] == DbSwitches.UseNamedParams;
            ParamStartIndex = cmdType == CommandType.StoredProcedure ? 0 : (HasNamedParams ? 1 : 0);
        }

        protected SqlBatchContainerItem()
        {

        }

        protected internal void MergeSqlAndParams(StringBuilder sql, SqlParamBuilder builder)
        {
            if (CmdType == CommandType.StoredProcedure)
            {
                // EXEC dbo.xxx şeklinde yazacağız
                sql.AppendFormat("EXEC {0} ", SqlText);
                if (ParamStartIndex == -1)
                {
                    sql.AppendLine(";");
                    return;
                }

                for (var i = 0; i < Prms.Length; i += 2)
                {
                    if (i > 0)
                        sql.Append(", ");
                    var value = Prms[i + 1];
                    sql.Append(builder.Add(value));
                }

                sql.AppendLine(";");
                return;
            }
            if (ParamStartIndex == -1)
            {
                sql.Append(SqlText);
                return;
            }

            var sqlText = SqlText;

            if (HasNamedParams)
            {
                for (var i = ParamStartIndex; i < Prms.Length; i += 2)
                {
                    var name = (string)Prms[i];
                    var value = Prms[i + 1];
                    var newName = builder.Add(value);
                    sqlText = sqlText.Replace(name, newName);
                }

                sql.AppendLine(sqlText);
                return;
            }

            var paramIndex = 1;
            foreach (var prm in Prms)
            {
                var newName = builder.Add(prm);
                sqlText = sqlText.Replace("@p" + paramIndex++, newName);
            }
            sql.AppendLine(sqlText);
        }
    }
}