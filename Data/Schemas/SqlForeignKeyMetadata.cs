﻿#if COREFULL || COREDATA
using System;
using System.Threading;
using JetBrains.Annotations;

namespace System.Data.Schemas
{
    public sealed class SqlForeignKeySchema : SqlNamedSchemaObject
    {
        SqlTableSchema masterTable;
        SqlTableColumnSchema masterColumn;
        SqlTableColumnSchema foreignTableColumn;

        public SqlTableSchema ForeignTable { get; set; }

        public SqlTableSchema MasterTable => LazyInitializer.EnsureInitialized(ref masterTable,
            () => ForeignTable.Database.GetCachedTable(MasterTableName));

        public SqlTableColumnSchema MasterTableColumn => LazyInitializer.EnsureInitialized(ref masterColumn, () => MasterTable.FindColumn(MasterTableColumnName));
        public SqlTableColumnSchema ForeignTableColumn => LazyInitializer.EnsureInitialized(ref foreignTableColumn, () => ForeignTable.FindColumn(ForeignTableColumnName));
        public string MasterTableName { get; set; }
        public string MasterTableColumnName { get; set; }
        public string ForeignTableName { get; set; }
        public string ForeignTableColumnName { get; set; }
        public string PrimaryKeyName { get; set; }
    }

    public class SqlForeignKeyCollection : SqlOwnedCollection<SqlForeignKeySchema>
    {
        public SqlTableSchema Owner { get; }

        public SqlForeignKeyCollection([NotNull] SqlTableSchema owner)
        {
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
        }

        protected override void RefreshCore()
        {
            Load(Owner.Database.SqlDialect.GetTableForeignKeys(Owner.Database.ConnectionName, Owner));}
    }
}
#endif