﻿#if COREFULL || COREDATA
using System;
using JetBrains.Annotations;

namespace System.Data.Schemas
{
    public class SqlTableColumnCollection : SqlOwnedCollection<SqlTableColumnSchema>
    {
        public SqlTableSchema Owner { get; }

        internal SqlTableColumnCollection([NotNull] SqlTableSchema owner)
        {
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
        }
        protected override void RefreshCore()
        {
            Load(Owner.Database.SqlDialect.GetTableColumns(Owner.Database.ConnectionName, Owner));
        }
    }
} 
#endif