﻿#if COREFULL || COREDATA
using System;
using JetBrains.Annotations;

namespace System.Data.Schemas
{
    public class SqlTableIndexColumnCollection : SqlOwnedCollection<SqlTableIndexColumnSchema>
    {

        [NotNull]
        public SqlTableIndexSchema Owner { get; }

        public SqlTableIndexColumnCollection([NotNull] SqlTableIndexSchema owner)
        {
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
        }

        protected override void RefreshCore()
        {
            Load(Owner.Table.Database.SqlDialect.GetTableIndexColumns(Owner.Table.Database.ConnectionName, Owner));
        }
    }
} 
#endif