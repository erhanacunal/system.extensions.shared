﻿#if COREFULL || COREDATA
using System.Text;
using System.Threading;
using System.Data;
using System.Utils;

namespace System.Data.Schemas
{
    public sealed class SqlTableColumnSchema : SqlNamedSchemaObject
    {
        public static readonly SqlTableColumnSchema Empty = new SqlTableColumnSchema();

        uint nameHash;
        /// <summary>
        /// Kolon adının hash değeri
        /// </summary>
        public uint NameHash => nameHash == 0 ? (nameHash = HashUtils.JenkinsHash(Name.ToLowerInvariant())) : nameHash;
        /// <summary>
        /// Veritabanı tipi, varchar,bit... gibi
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// String alanlar için karşılaştırıcı ismi
        /// </summary>
        public string CollationName { get; set; }
        /// <summary>
        /// Tam sayı kısmı uzunluğu
        /// </summary>
        public byte Precision { get; set; }
        /// <summary>
        /// Ondalık kısım uzunluğu
        /// </summary>
        public byte Scale { get; set; }
        /// <summary>
        /// String veya binary tip ise uzunluğu
        /// </summary>
        public int MaxLength { get; set; }
        /// <summary>
        /// Hangi tablonun kolonu olduğunu verir
        /// </summary>
        public SqlTableSchema Table { get; set; }
        /// <summary>
        /// Sql tip bilgisini verir
        /// </summary>
        public ISqlTypeInfo TypeInfo { get; set; }
        /// <summary>
        /// Kolon id değeri
        /// </summary>
        public int ColumnId { get; set; }
        /// <summary>
        /// Null geçilip geçilmeyeceğini verir
        /// </summary>
        public bool IsNullable { get; set; }
        /// <summary>
        /// Hesaplanan bir alan olup olmadığını verir
        /// </summary>
        public bool IsComputed { get; set; }
        /// <summary>
        /// Tipin izin verdiği en fazla veri uzunluğu
        /// </summary>
        public int TypeMaxLength { get; set; }
        /// <summary>
        /// Identity kolon olup olmadığını verir.
        /// </summary>
        public bool IsIdentity { get; set; }

        public string GetDefinition(bool emitCollation = true)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("[{0}] {1}", Name, TypeName);
            if (TypeInfo.IsSizable)
            {
                if (TypeInfo.IsString && emitCollation)
                    sb.AppendFormat("({0}) COLLATE {1} \r\n", MaxLength, CollationName);
                else
                    sb.AppendFormat("({0})\r\n", MaxLength);
            }
            else if (TypeInfo.IsNumeric && TypeInfo.HasPrecisionScale)
            {
                sb.AppendFormat("({0},{1})\r\n", Precision, Scale);
            }
            return sb.ToString();
        }

    }
}
#endif