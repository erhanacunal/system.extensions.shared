﻿#if COREFULL || COREDATA
using System;

namespace System.Data.Schemas
{
    public sealed class SqlTableSchema : SqlNamedSchemaObject
    {
        public SqlTableSchema()
        {
            Columns = new SqlTableColumnCollection(this);
            Indexes = new SqlTableIndexCollection(this);
            ForeignKeys = new SqlForeignKeyCollection(this);
        }

        public SqlTableColumnCollection Columns { get; }
        public SqlTableIndexCollection Indexes { get; }
        public SqlForeignKeyCollection ForeignKeys { get; }
        public int IdentitySeed { get; set; }
        public int IdentityIncrement { get; set; }
        public string SchemaName { get; set; }
        public SqlDatabaseSchema Database { get; set; }

        public SqlTableColumnSchema FindColumn(uint hash) => Columns.Find(c => c.NameHash == hash);
        public SqlTableColumnSchema FindColumn(string name) => Columns.Find(c => string.Equals(name, c.Name, StringComparison.OrdinalIgnoreCase));
        public SqlTableColumnSchema FindColumnById(int id) => Columns.Find(c => c.ColumnId == id);
    }
}
#endif