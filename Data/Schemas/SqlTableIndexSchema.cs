﻿#if COREFULL || COREDATA
namespace System.Data.Schemas
{
    public sealed class SqlTableIndexSchema : SqlNamedSchemaObject
    {
        public SqlTableIndexColumnCollection Columns { get; }
        public bool IsPrimaryKey { get; set; }
        public bool IsUnique { get; set; }
        public bool IsUniqueConstraint { get; set; }
        public bool IsDisabled { get; set; }
        public int IndexId { get; set; }
        public int TypeId { get; set; }
        public SqlTableSchema Table { get; set; }

        public SqlTableIndexSchema()
        {
            Columns = new SqlTableIndexColumnCollection(this);
        }

    }
} 
#endif