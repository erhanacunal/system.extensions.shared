﻿#if COREFULL || COREDATA
namespace System.Data.Schemas
{
    public abstract class SqlSchemaObject
    {
        public int ObjectId { get; set; }
    }

    public abstract class SqlNamedSchemaObject : SqlSchemaObject
    {
        /// <summary>
        /// Nesnenin adı
        /// </summary>
        public string Name { get; set; }
    }

    public sealed class SqlSchemaObjectDescription : SqlNamedSchemaObject
    {
        public string Description { get; set; }
    }

}

#endif