﻿#if COREFULL || COREDATA
using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Data.Schemas
{
    public abstract class SqlOwnedCollection<T> : IList<T>
    {
        protected readonly List<T> InnerList = new List<T>();

        public IEnumerator<T> GetEnumerator()
        {
            return InnerList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)InnerList).GetEnumerator();
        }

        public void Add(T item)
        {
            InnerList.Add(item);
        }

        public void Clear()
        {
            InnerList.Clear();
        }

        public bool Contains(T item)
        {
            return InnerList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            InnerList.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return InnerList.Remove(item);
        }

        public int Count => InnerList.Count;

        public bool IsReadOnly => ((IList<T>)InnerList).IsReadOnly;

        public int IndexOf(T item)
        {
            return InnerList.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            InnerList.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            InnerList.RemoveAt(index);
        }

        public T this[int index]
        {
            get => InnerList[index];
            set => InnerList[index] = value;
        }

        public T Find(Predicate<T> match) => InnerList.Find(match);

        public void Refresh(bool force = false)
        {
            if (force || InnerList.Count == 0)
                RefreshCore();
        }

        protected abstract void RefreshCore();

        internal void Load(IEnumerable<T> items)
        {
            InnerList.Clear();
            InnerList.AddRange(items);
        }
    }
} 
#endif