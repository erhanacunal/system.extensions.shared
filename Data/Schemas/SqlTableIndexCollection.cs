﻿#if COREFULL || COREDATA
using System;
using JetBrains.Annotations;

namespace System.Data.Schemas
{
    public class SqlTableIndexCollection : SqlOwnedCollection<SqlTableIndexSchema>
    {

        [NotNull]
        public SqlTableSchema Owner { get; }

        public SqlTableIndexCollection([NotNull] SqlTableSchema owner)
        {
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
        }

        protected override void RefreshCore()
        {
            Load(Owner.Database.SqlDialect.GetTableIndexes(Owner.Database.ConnectionName, Owner));
        }
    }
} 
#endif