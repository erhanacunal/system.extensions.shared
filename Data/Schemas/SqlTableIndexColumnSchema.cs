﻿#if COREFULL || COREDATA
namespace System.Data.Schemas
{
    public sealed class SqlTableIndexColumnSchema : SqlNamedSchemaObject
    {
        SqlTableColumnSchema column;
        public SqlTableIndexSchema Index { get; set; }

        public int ColumnId { get; set; }
        public bool IsDescendingKey { get; set; }
        public bool IsIncludedColumn { get; set; }
        public int KeyOrdinal { get; set; }
        public int IndexColumnId { get; set; }
        public SqlTableColumnSchema Column => column ?? (column = Index.Table.FindColumnById(ColumnId));
    }
} 
#endif