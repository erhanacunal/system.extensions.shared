﻿#if COREFULL || COREDATA
using System.Collections.Concurrent;
using System.Data.Providers;
using System.Data;
using System.Utils;

namespace System.Data.Schemas
{
    public sealed class SqlDatabaseSchema : SqlNamedSchemaObject
    {
        readonly ConcurrentDictionary<uint, SqlTableSchema> tables = new ConcurrentDictionary<uint, SqlTableSchema>();
        public IDbConnectionName ConnectionName { get; }
        public ISqlDialect SqlDialect { get; }

        public SqlDatabaseSchema(IDbConnectionName connectionName)
        {
            ConnectionName = connectionName;
            SqlDialect = connectionName.SqlClientFactory.SqlDialect;
        }

        public SqlTableSchema GetCachedTable(string tableName)
        {
            var hash = HashUtils.JenkinsHash(tableName.ToLowerInvariant());
            return tables.GetOrAdd(hash, h => SqlDialect.GetTableWithColumns(ConnectionName, tableName));
        }

        public SqlTableSchema GetTableNoCache(string tableName) 
            => SqlDialect.GetTableWithColumns(ConnectionName, tableName);
    }
} 
#endif