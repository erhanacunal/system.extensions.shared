﻿#if (COREFULL || COREDATA) && ORACLE
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Utils;

namespace System.Data
{
    public sealed class OracleTypeInfo : ISqlTypeInfo
    {
        static readonly List<OracleTypeInfo> TypeInfos;
        readonly uint nameHash;

        public int Id { get; }
        public string Name { get; }
        public bool IsSizable { get; }
        public bool IsUnicode { get; }
        public bool IsBinary { get; }
        public bool IsNumeric { get; }
        public bool HasPrecisionScale { get; }
        public string DefaultValue { get; }
        public Type CodeType { get; }
        public DbType DbType { get; }
        public bool IsString { get; }

        static OracleTypeInfo()
        {

            TypeInfos = new List<OracleTypeInfo>
            {
                new OracleTypeInfo(96,"char",true, DbType.AnsiStringFixedLength, typeof(char[])),
                new OracleTypeInfo(96,"nchar",true, DbType.StringFixedLength,typeof(char[])),
                new OracleTypeInfo(1,"varchar2",true, DbType.AnsiString,typeof(char[])),
                new OracleTypeInfo(1,"nvarchar2",true, DbType.String,typeof(string)),
                new OracleTypeInfo(23,"raw",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(24,"longraw",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(8,"long",true, DbType.Binary,typeof(string)),
                new OracleTypeInfo(2,"number",false, DbType.Decimal,typeof(decimal)),
                new OracleTypeInfo(2,"numeric",false, DbType.Decimal,typeof(decimal)),
                new OracleTypeInfo(2,"float",false, DbType.Single,typeof(float)),
                new OracleTypeInfo(2,"double precision",false, DbType.Double,typeof(double)),
                new OracleTypeInfo(2,"dec",false, DbType.Decimal,typeof(decimal)),
                new OracleTypeInfo(2,"decimal",false, DbType.Decimal,typeof(decimal)),
                new OracleTypeInfo(2,"integer",false, DbType.Int64,typeof(long)),
                new OracleTypeInfo(2,"int",false, DbType.Int32,typeof(long)),
                new OracleTypeInfo(2,"smallint",false, DbType.Int16,typeof(long)),
                new OracleTypeInfo(100,"binary_float",false, DbType.Single,typeof(float)),
                new OracleTypeInfo(101,"binary_double",false, DbType.Double,typeof(double)),
                new OracleTypeInfo(12,"date",false, DbType.Date,typeof(DateTime)),
                new OracleTypeInfo(180,"timestamp",false, DbType.Time,typeof(TimeSpan)),
                new OracleTypeInfo(113,"blob",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(112,"clob",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(112,"nclob",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(114,"bfile",true, DbType.Binary,typeof(byte[])),
                new OracleTypeInfo(69,"rowid",true, DbType.String,typeof(string)),
                new OracleTypeInfo(208,"urowid",true, DbType.String,typeof(string)),

            };
        }

        OracleTypeInfo(int id, string name, bool sizable, DbType dbType, Type codeType)
        {
            Id = id;
            Name = name;
            IsSizable = sizable;
            IsUnicode = dbType.IsDbTypeUnicodeString();
            IsBinary = dbType.IsDbTypeBinary();
            IsNumeric = dbType.IsDbTypeNumeric();
            HasPrecisionScale = dbType.IsDbTypeNumeric();
            DefaultValue = string.Empty;
            CodeType = codeType;
            DbType = dbType;
            IsString = dbType.IsDbTypeString();
            nameHash = HashUtils.JenkinsHash(name.ToLowerInvariant());
        }

        public SqlConversionResult IsConvertibleTo(ISqlTypeInfo to)
        {
            return SqlConversionResult.NotAllowed;
        }

        public static OracleTypeInfo GetById(int id)
        {
            return TypeInfos.FirstOrDefault(_ => _.Id == id);
        }

        public static OracleTypeInfo GetByName(string name)
        {
            var hash = HashUtils.JenkinsHash(name.ToLowerInvariant());
            return TypeInfos.FirstOrDefault(_ => _.nameHash == hash);
        }

        public static OracleTypeInfo GetByType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            restart:
            foreach (var ti in TypeInfos)
            {
                if (ti.CodeType == type)
                    return ti;
            }
            if (type.IsEnum)
            {
                type = type.GetEnumUnderlyingType();
                goto restart;
            }
            type = Nullable.GetUnderlyingType(type);
            if (type != null)
                goto restart;

            return null;
        }
    }
}

#endif