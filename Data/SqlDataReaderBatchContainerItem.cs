﻿using System.Data.Common;

namespace System.Data
{
    /// <summary>
    /// Tek bir datareader ile ilgili işi yapacak görev
    /// </summary>
    abstract class SqlDataReaderBatchContainerItem : SqlBatchContainerItem
    {
        protected SqlDataReaderBatchContainerItem(CommandType cmdType, string sqlText, object[] prms) : base(cmdType,
            sqlText, prms)
        {
            UsesNextResult = false;
        }
        public bool UsesNextResult { get; }
        protected SqlDataReaderBatchContainerItem()
        {
            UsesNextResult = true;
        }

        public abstract void ProcessDataReader(DbDataReader reader);
    }
}