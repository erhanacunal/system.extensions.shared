﻿#if COREFULL || COREDATA
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Data.Providers;
using System.Utils;

namespace System.Data
{
    /*
     * Nasıl Çalışıyor
     * 
     * IDatabaseConnectionProvider bir IDbConnection sağlayıcısı
     * 
     * ConnectionManager sınıfının doğru çalışabilmesi için bu interface'i sunan bir provider a ihtiyaç duyar.
     * 
     * GetCachedConnection yöntemi şu şekilde çalışır;
     * 
     * Verilen isimde bir ProviderConnCache sınıfı elde eder
     * sonra bu örnekten bir bağlantı ister ( Bu bağlantı daha önce kullanılmış veya yeni olabilir )
     * İsteyen kişiye CachedConnection sınıfının bir örneği verilir
     * İşi bittiğinde de CachedConnection örneğinin Dispose yöntemi çağrılır ve bağlantı bilgisi (SharedConnectionInfo)
     * bağlı olduğu ProviderConnCache örneğine geri verilir.
     * 
     
     
     */


    public static class ConnectionManager
    {

        #region Private & Protected Fields

        static readonly List<IDbConnectionName> Names = new List<IDbConnectionName>();
        static WeakList<ReservedConnection> reservedConnections;
        static readonly List<IDbConnectionNameFactory> NameFactories = new List<IDbConnectionNameFactory>();


        public static int CommandTimeout = 900;

        #endregion

        #region Default c'tor

        static ConnectionManager()
        {
        }

        #endregion

        #region Private & Protected Methods

        #endregion

        #region Public Methods

        public static void Initialize()
        {
            foreach (var prv in Names)
            {
                prv.Initialize();
            }
        }
        public static void RegisterNameFactory(IDbConnectionNameFactory factory)
        {
            NameFactories.AddChecked(factory);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IDbConnectionName Find(string name, bool throwIfNotFound = true)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is null or empty.", nameof(name));
            var bareName = name;
            if (bareName.StartsWith("*"))
                bareName = bareName.Substring(1);
            var nameHash = HashUtils.JenkinsHash(bareName.ToLowerInvariant());
            var found = Names.FirstOrDefault(prov => prov.NameHash == nameHash);
            if (found != null)
                return found;
            foreach (var nameFactory in NameFactories)
            {
                if (nameFactory.TryCreate(bareName, out found))
                {
                    found.Initialize();
                    Names.Add(found);
                    return found;
                }
            }
            if (throwIfNotFound)
                throw new InvalidOperationException("Sağlayıcı bulunamadı! : " + bareName);
            return null;
        }

        static bool TryGetReservedConnection(string name, out ReservedConnection cnn)
        {
            cnn = null;

            if (reservedConnections == null)
                return false;

            foreach (var rc in reservedConnections)
            {
                if (rc != null && rc.ConnectionName.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    cnn = rc;
                    return true;
                }
            }

            return false;
        }

        public static ConnectionData Get(string providerName, bool forceNew = false)
        {
            if (string.IsNullOrEmpty(providerName))
                throw new ArgumentException("providerName is null or empty.", nameof(providerName));
            return TryGetReservedConnection(providerName, out var reservedConnection)
                ? reservedConnection.Connection :
                new ConnectionData(providerName, Find(providerName), providerName.StartsWith("*"));
        }

        public static ConnectionData Get(IDbConnectionName provider) 
            => new ConnectionData(provider.Name, provider, provider.Name.StartsWith("*"));

        public static void Remove(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is null or empty.", nameof(name));
            Names.RemoveAll(_ => _.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static IDbConnectionName Add(DbEngineType provType, string name, string connectionString, bool transient = false)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is null or empty.", nameof(name));
            //if ( String.IsNullOrEmpty( connectionString ) )
            //    throw new ArgumentException( "connectionString is null or empty.", "connectionString" );
            Remove(name);
            var result = new SimpleDbConnectionName(provType, name, connectionString)
            {
                IsTransient = transient
            };
            Names.Add(result);
            return result;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static IReservedConnection GetReservedConnection(string name)
        {

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is null or empty.", nameof(name));

            var prov = Find(name);

            var reservedConnection = new ReservedConnection(prov);

            if (reservedConnections == null)
                reservedConnections = new WeakList<ReservedConnection>();

            reservedConnections.Add(reservedConnection);

            return reservedConnection;
        }

        #endregion

        sealed class ReservedConnection : DisposableBase, IReservedConnection
        {
            public string ConnectionName { get; }
            public ConnectionData Connection { get; }

            public ReservedConnection(IDbConnectionName baseName)
            {
                ConnectionName = Guid.NewGuid().ToString();
                Connection = new ConnectionData(ConnectionName, baseName, false)
                {
                    ReservedConnection = this
                };
            }

            public IDbTransaction BeginTransaction()
            {
                return Connection.BeginTransaction();
            }

            public void CommitTransaction()
            {
                Connection.CommitTransaction();
            }

            public void RollbackTransaction()
            {
                Connection.RollbackTransaction();
            }

            protected override void DisposeCore()
            {
                Connection.ReservedConnection = null;
                Connection.Dispose();
            }
        }
    }
}

#endif