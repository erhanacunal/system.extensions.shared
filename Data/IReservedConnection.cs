#if COREFULL || COREDATA
using System;
using System.Data;

namespace System.Data
{
    public interface IReservedConnection : IDisposable
    {

        string ConnectionName { get; }
        ConnectionData Connection { get; }

        IDbTransaction BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
} 
#endif