#if COREFULL || COREDATA
using System.Data.Providers;

namespace System.Data
{
    public interface IDbConnectionNameFactory
    {
        bool TryCreate(string name, out IDbConnectionName instance);
    }
} 
#endif