﻿#if (COREFULL || COREDATA) && ORACLE
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Data.Providers;
using System.Data.Schemas;

namespace System.Data
{
    public sealed class OracleDialect : SqlDialectBase
    {
        public override SqlTableSchema GetTableWithColumns(IDbConnectionName cnnName, string tableName)
        {
            throw new NotImplementedException();
        }

        public override SqlTableSchema GetTable(IDbConnectionName cnnName, string tableName)
        {
            throw new NotImplementedException();
        }

        public override IList<SqlTableColumnSchema> GetTableColumns(IDbConnectionName cnnName, SqlTableSchema table)
        {
            throw new NotImplementedException();
        }

        public override string GenerateInsertScript<T>(T instance, PropertyProvider<T> propertyProvider = null)
        {
            throw new NotImplementedException();
        }

        public override object[] ConvertSqlQueryParameters(IEnumerable<ISqlQueryParameter> parameters, Action<SqlParamBuilder> builderAction = null)
        {
            var builder = new SqlParamBuilder(true);
            foreach (var parameter in parameters)
            {
                var name = parameter.Name;
                if (name.IsNullOrWhitespace()) continue;
                if (name[0] == '@')
                    name = string.Concat(":", name.Substring(1));
                else if (name[0] != ':')
                    name = string.Concat(":", name);
                builder.Add2(name, parameter.SqlValue);
            }
            builderAction?.Invoke(builder);
            return builder.Build();
        }

        public override string GetColumnDefinition(SqlTableColumnSchema column, bool emitCollation = true)
        {
            throw new NotImplementedException();
        }

        public override void GetColumnDefinition(StringBuilder sb, SqlTableColumnSchema column, bool emitCollation = true)
        {
            throw new NotImplementedException();
        }

        public override void GetColumnDefinition(TextWriter writer, SqlTableColumnSchema column, bool emitCollation = true)
        {
            throw new NotImplementedException();
        }

        public override string DbTypeToTypeName(DbType dbType)
        {
            throw new NotImplementedException();
        }

        public override string CreateTableScriptFromDataTable(string tableName, DataTable table, bool memoryTable)
        {
            throw new NotImplementedException();
        }

        public override void CreateTableScriptFromDataTable(TextWriter writer, string tableName, DataTable table, bool memoryTable)
        {
            throw new NotImplementedException();
        }

        public override void CreateTableScriptFromSchema(TextWriter writer, SqlTableSchema table, bool memoryTable, string tableName = null)
        {
            throw new NotImplementedException();
        }

        public override string GetAutoParameterName()
        {
            return ":auto_" + (8).RandomString();
        }

        public override IList<SqlTableIndexSchema> GetTableIndexes(IDbConnectionName cnnName, SqlTableSchema table)
        {
            throw new NotImplementedException();
        }

        public override IList<SqlTableIndexColumnSchema> GetTableIndexColumns(IDbConnectionName cnnName, SqlTableIndexSchema index)
        {
            throw new NotImplementedException();
        }

        public override IList<SqlForeignKeySchema> GetTableForeignKeys(IDbConnectionName cnnName, SqlTableSchema table)
        {
            throw new NotImplementedException();
        }

        public override ISqlTypeInfo GetTypeInfo(string typeName) => OracleTypeInfo.GetByName(typeName);

        public override ISqlTypeInfo GetTypeInfo(int id) => OracleTypeInfo.GetById(id);

        public override ISqlTypeInfo GetTypeInfo(Type type) => OracleTypeInfo.GetByType(type);
    }
}

#endif