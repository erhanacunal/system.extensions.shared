﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace System.Data
{
    /// <summary>
    /// DataReader grubu
    /// </summary>
    sealed class SqlDataReaderBatchContainer : SqlBatchContainer
    {
        readonly Queue<SqlDataReaderBatchContainerItem> items = new Queue<SqlDataReaderBatchContainerItem>();

        public override void AddItem(SqlBatchContainerItem containerItem)
        {
            if (!(containerItem is SqlDataReaderBatchContainerItem drTask))
                throw new ArgumentException("task, SqlDataReaderBatchContainerItem tipinde olmalı!", nameof(containerItem));
            items.Enqueue(drTask);
        }

        internal override async Task Execute(ConnectionData cnn)
        {
            var sql = new StringBuilder();
            var sqlParamBuilder = new SqlParamBuilder();
            foreach (var task in items)
                task.MergeSqlAndParams(sql, sqlParamBuilder);
            Console.WriteLine(sql.ToString());
            using (var cmd = cnn.CreateCommand(sql.ToString(), sqlParamBuilder.Build()))
            using (var reader = await cmd.ExecuteReaderAsync())
            {
                do
                {
                    items.Dequeue().ProcessDataReader(reader);
                } while (items.Count > 0 && reader.NextResult());
            }
        }
    }
}