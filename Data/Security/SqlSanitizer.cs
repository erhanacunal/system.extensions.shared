﻿#if COREFULL || TSQLPARSE
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dom = Microsoft.SqlServer.TransactSql.ScriptDom;

namespace System.Data.Security
{
    public static class SqlSanitizer
    {

        public static bool IsSafeForDataRetrieval(string sqlText, out IList<Dom.ParseError> errors)
        {
            var parser = new Dom.TSql120Parser(false);
            using (var reader = new StringReader(sqlText))
            {
                var fragment = parser.ParseStatementList(reader, out errors);
                if (errors.Count > 0)
                    return false;
                return IsSafeForDataRetrieval(fragment, errors);
            }
        }

        public static bool IsSafeForDataRetrieval(Dom.TSqlFragment fragment, IList<Dom.ParseError> errors)
        {
            var visitor = new SqlScriptVisitor(errors);
            fragment.Accept(visitor);
            if (visitor.IsFlagSet(SqlDetectedStatements.SAFE_FOR_DATA_RETRIEVAL))
            {
                if (visitor.IsFlagSet(SqlDetectedStatements.CREATE_TABLE) &&
                    visitor.CreatedTables.Any(_ => !_.StartsWith("#")))
                {
                    errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde geçici tablo dışında başka bir tablo oluşturulmaya çalışılıyor"));
                    return false;
                }
                if (visitor.IsFlagSet(SqlDetectedStatements.DROP_OBJECT) &&
                    visitor.DroppedObjects.Any(_ => !_.StartsWith("#")))
                {
                    errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde geçici tablo dışında başka bir nesne bırakılmaya çalışılıyor"));
                    return false;
                }
                if (visitor.IsFlagSet(SqlDetectedStatements.UPDATE) &&
                    visitor.UpdatedTables.Any(_ => !_.StartsWith("#")))
                {
                    errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde geçici tablo dışında başka bir tablo güncellenmeye çalışılıyor"));
                    return false;
                }
                if (visitor.IsFlagSet(SqlDetectedStatements.DELETE) &&
                    visitor.DeletedTables.Any(_ => !_.StartsWith("#")))
                {
                    errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde geçici tablo dışında başka bir tablo içindeki veri silinmeye çalışılıyor"));
                    return false;
                }
                if (visitor.IsFlagSet(SqlDetectedStatements.INSERT) &&
                    visitor.InsertedTables.Any(_ => !_.StartsWith("#")))
                {
                    errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde geçici tablo dışında başka bir tabloya veri eklenmeye çalışılıyor"));
                    return false;
                }
                return true;
            }
            errors.Add(new Dom.ParseError(0, 0, 0, 0, "Sorgu içerisinde güvenilmeyen ifadeler var!"));
            return false;
        }
    }
}

#endif