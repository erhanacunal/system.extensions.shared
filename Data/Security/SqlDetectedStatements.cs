#if COREFULL || TSQLPARSE
namespace System.Data.Security
{
    public static class SqlDetectedStatements
    {
        public const int SELECT = 1 << 0;
        public const int UPDATE = 1 << 1;
        public const int DELETE = 1 << 2;
        public const int TRUNCATE = 1 << 3;
        public const int ALTER_TABLE = 1 << 4;
        public const int DYNAMIC_SQL = 1 << 5;
        public const int CTE = 1 << 6;
        public const int DBCC = 1 << 7;
        public const int DROP_DATABASE = 1 << 8;
        public const int CREATE_OR_ALTER_DATABASE = 1 << 9;
        public const int DROP_OBJECT = 1 << 10;
        public const int USER_OBJECT_CHANGING = 1 << 11;
        public const int CREATE_OR_ALTER_TRIGGER = 1 << 12;
        public const int CREATE_OR_ALTER_VIEW = 1 << 13;
        public const int CREATE_OR_ALTER_INDEX = 1 << 14;
        public const int DROP_UNOWNED_OBJECTS = 1 << 15;
        public const int CREATE_TABLE = 1 << 16;
        public const int KILL = 1 << 17;
        public const int DROP_INDEX = 1 << 18;
        public const int CREATE_OR_ALTER_SP_OR_FN = 1 << 19;
        public const int ALTER_LOGIN = 1 << 20;
        public const int CREATE_OR_ALTER_LOGIN = 1 << 21;
        public const int CREATE_TYPE = 1 << 22;
        public const int CREATE_SCHEMA = 1 << 23;
        public const int INSERT = 1 << 24;
        public const int EXECUTE_SP = 1 << 25;
        public const int USE_DATABASE = 1 << 26;

        public const int SAFE_FOR_DATA_RETRIEVAL = SELECT | UPDATE | DELETE | CREATE_TABLE | DROP_OBJECT | CTE | INSERT | EXECUTE_SP;
    }
} 
#endif