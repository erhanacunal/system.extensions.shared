#if COREFULL || TSQLPARSE
using System;
using System.Collections.Generic;
using System.Linq;
using Dom = Microsoft.SqlServer.TransactSql.ScriptDom;

namespace System.Data.Security
{
    sealed class SqlScriptVisitor : Dom.TSqlFragmentVisitor
    {
        readonly IList<Dom.ParseError> errors;

        internal int DetectedStatements { get; private set; }
        public List<string> UpdatedTables { get; } = new List<string>();
        public List<string> InsertedTables { get; } = new List<string>();
        public List<string> DeletedTables { get; } = new List<string>();
        public List<string> DroppedObjects { get; } = new List<string>();
        public List<string> CreatedTables { get; } = new List<string>();
        public List<string> ExecutedStoredProcedures { get; } = new List<string>();

        public SqlScriptVisitor(IList<Dom.ParseError> errors)
        {
            this.errors = errors;
        }
        public override void Visit(Dom.SelectStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.SELECT;
            base.Visit(node);
        }

        public override void Visit(Dom.UpdateStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.UPDATE;
            if (node.UpdateSpecification.Target is Dom.NamedTableReference ntr)
            {
                // UPDATE x SET ... FROM AA x gibi
                var alias = ntr.SchemaObject.BaseIdentifier.Value;
                if (node.UpdateSpecification.FromClause != null)
                {
                    var table = node.UpdateSpecification.FromClause.TableReferences.OfType<Dom.NamedTableReference>()
                        .FirstOrDefault(_ => _.Alias.Value.Equals(alias, StringComparison.OrdinalIgnoreCase));
                    if (table != null)
                        UpdatedTables.Add(table.SchemaObject.BaseIdentifier.Value);
                    else
                        errors.Add(new Dom.ParseError(0, 0, 0, 0, "Update referans edilen tablo bulunamadư -> " + alias));
                    return;
                }
                UpdatedTables.Add(alias);

            }
            base.Visit(node);
        }

        internal bool IsFlagSet(int flag) => (DetectedStatements & flag) != 0;

        public override void Visit(Dom.DeleteStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DELETE;
            if (node.DeleteSpecification.Target is Dom.NamedTableReference ntr)
            {
                // UPDATE x SET ... FROM AA x gibi
                var alias = ntr.SchemaObject.BaseIdentifier.Value;
                if (node.DeleteSpecification.FromClause != null)
                {
                    var table = node.DeleteSpecification.FromClause.TableReferences.OfType<Dom.NamedTableReference>()
                        .FirstOrDefault(_ => _.Alias.Value.Equals(alias, StringComparison.OrdinalIgnoreCase));
                    if (table != null)
                        UpdatedTables.Add(table.SchemaObject.BaseIdentifier.Value);
                    else
                        errors.Add(new Dom.ParseError(0, 0, 0, 0, "Delete referans edilen tablo bulunamadư -> " + alias));
                    return;
                }
                DeletedTables.Add(alias);
            }
            base.Visit(node);
        }

        public override void Visit(Dom.InsertStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.INSERT;
            if (node.InsertSpecification.Target is Dom.NamedTableReference ntr)
            {
                // UPDATE x SET ... FROM AA x gibi
                var alias = ntr.SchemaObject.BaseIdentifier.Value;
                InsertedTables.Add(alias);
            }
            base.Visit(node);
        }

        public override void Visit(Dom.AlterTableStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.ALTER_TABLE;
            base.Visit(node);
        }

        public override void Visit(Dom.ExecuteStatement node)
        {
            if (node.ExecuteSpecification.ExecutableEntity is Dom.ExecutableProcedureReference sp)
            {
                var spRef = sp.ProcedureReference.ProcedureReference;
                var spName = spRef.Name.BaseIdentifier.Value;
                if (spName.Equals("sp_executesql"))
                {
                    DetectedStatements |= SqlDetectedStatements.DYNAMIC_SQL;
                    return;
                }
                if (spRef.Name.SchemaIdentifier != null)
                    spName = string.Concat(spRef.Name.SchemaIdentifier.Value, ".", spName);
                else
                    spName = "dbo." + spName;
                ExecutedStoredProcedures.Add(spName);
                DetectedStatements |= SqlDetectedStatements.EXECUTE_SP;
                return;
            }
            if (node.ExecuteSpecification.ExecutableEntity is Dom.ExecutableStringList)
            {
                DetectedStatements |= SqlDetectedStatements.DYNAMIC_SQL;
                return;
            }

            base.Visit(node);
        }

        public override void Visit(Dom.ExecuteAsStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DYNAMIC_SQL;
            base.Visit(node);
        }

        public override void Visit(Dom.WithCtesAndXmlNamespaces node)
        {
            DetectedStatements |= SqlDetectedStatements.CTE;
            base.Visit(node);
        }

        public override void Visit(Dom.DbccStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DBCC;
            base.Visit(node);
        }

        public override void Visit(Dom.DropDatabaseStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_DATABASE;
            base.Visit(node);
        }

        public override void Visit(Dom.AlterDatabaseStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_DATABASE;
            base.Visit(node);
        }

        public override void Visit(Dom.CreateDatabaseStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_DATABASE;
            base.Visit(node);
        }

        public override void Visit(Dom.DropObjectsStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_OBJECT;
            foreach (var objectName in node.Objects)
            {
                DroppedObjects.Add(objectName.BaseIdentifier.Value);
            }
            base.Visit(node);
        }

        public override void Visit(Dom.IndexStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_INDEX;
            base.Visit(node);
        }

        public override void Visit(Dom.UserStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.USER_OBJECT_CHANGING;
            base.Visit(node);
        }

        public override void Visit(Dom.TriggerStatementBody node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_TRIGGER;
            base.Visit(node);
        }

        public override void Visit(Dom.ViewStatementBody node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_VIEW;
            base.Visit(node);
        }

        public override void Visit(Dom.DropUnownedObjectStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_UNOWNED_OBJECTS;
            base.Visit(node);
        }

        public override void Visit(Dom.KillStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.KILL;
            base.Visit(node);
        }
        public override void Visit(Dom.DropIndexStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_INDEX;
            base.Visit(node);
        }

        public override void Visit(Dom.TruncateTableStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.TRUNCATE;
            base.Visit(node);
        }

        public override void Visit(Dom.DropSchemaStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_OBJECT;
            DroppedObjects.Add(node.Schema.BaseIdentifier.Value);
            base.Visit(node);
        }

        public override void Visit(Dom.DropTypeStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.DROP_OBJECT;
            DroppedObjects.Add(node.Name.BaseIdentifier.Value);
            base.Visit(node);
        }

        public override void Visit(Dom.ProcedureStatementBodyBase node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_SP_OR_FN;
            base.Visit(node);
        }

        public override void Visit(Dom.AlterLoginStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_LOGIN;
            base.Visit(node);
        }

        public override void Visit(Dom.CreateLoginStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_OR_ALTER_LOGIN;
            base.Visit(node);
        }

        public override void Visit(Dom.CreateTypeStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_TYPE;
            base.Visit(node);
        }

        public override void Visit(Dom.CreateSchemaStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_SCHEMA;
            base.Visit(node);
        }

        public override void Visit(Dom.CreateTableStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.CREATE_TABLE;
            CreatedTables.Add(node.SchemaObjectName.BaseIdentifier.Value);
            base.Visit(node);
        }

        public override void Visit(Dom.UseStatement node)
        {
            DetectedStatements |= SqlDetectedStatements.USE_DATABASE;
            base.Visit(node);
        }
    }
} 
#endif