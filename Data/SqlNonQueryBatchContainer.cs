﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace System.Data
{
    sealed class SqlNonQueryBatchContainer : SqlBatchContainer
    {
        readonly List<SqlNonQueryBatchContainerItem> items = new List<SqlNonQueryBatchContainerItem>();
        public override void AddItem(SqlBatchContainerItem containerItem)
        {
            if (!(containerItem is SqlNonQueryBatchContainerItem drTask))
                throw new ArgumentException("task, SqlDataReaderBatchContainerItem tipinde olmalı!", nameof(containerItem));
            items.Add(drTask);
        }

        internal override async Task Execute(ConnectionData cnn)
        {
            var sql = new StringBuilder();
            var sqlParamBuilder = new SqlParamBuilder();
            foreach (var item in items)
                item.MergeSqlAndParams(sql, sqlParamBuilder);
            Console.WriteLine(sql.ToString());
            using (var cmd = cnn.CreateCommand(sql.ToString(), sqlParamBuilder.Build()))
                await cmd.ExecuteNonQueryAsync();
        }
    }
}