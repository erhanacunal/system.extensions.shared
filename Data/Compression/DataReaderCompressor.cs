﻿#if COREFULL
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Utils;

namespace System.Data.Compression
{
    // Dosya formatı:
    //
    // FieldCount StringCacheCount Int32CacheCount Int64CacheCount RowCount <-- bu değerler 7 bit encoded değerler
    // (FieldName,FieldTypeCode) * FieldCount
    // ( Id, Value ) * StringCacheCount
    // ( Id, Value ) * Int32CacheCount
    // ( Id, Value ) * Int64CacheCount
    // (NullMask, CacheMask, CellValues[maskelere göre ya veri ya da cache id değeri, null olanlar yazılmaz]) * RowCount
    //
    // Üç tip için önbellek kullanıyoruz
    // Bunlar string, int32 ve int64.
    // Sıkıştırma işi; Alanlarda sık geçen değerleri birkaç sözlükte tutuyoruz. Aynı değerleri devamlı yazmak yerine bi kere önbelleğe atıyoruz. 
    // Sütunla da önbellek öğesini ilişkilendiriyoruz.
    // Null olanlar ve önbelleklenmesi engellenen alanlar dışındaki belirlenen tiplere uyan sütunları sıkıştırma işlemine tabi tutuyoruz.
    // Her satır için bir null maskesi kullanıyoruz ( yani hangi alanlar null ( decompress ederken kullanılıyor ) )
    // Her satır için bir önbellek maskesi kullanıyoruz ( yani hangi alanlar önbellekten alınacak ( decompress ederken kullanılıyor ) )
    public sealed class DataReaderCompressor
    {
        static readonly ReadWriteObjectExOptions ReadWriteOptions = new ReadWriteObjectExOptions
        {
            DateTimeConversion = DateTimeConversionMethod.Ticks,
            EncodeString = false,
            WriteWithTypeCode = false
        };

        static int lastId = 0;

        readonly DataReaderCompressorOptions options;
        readonly Dictionary<string, StringValueCache> stringCache = new Dictionary<string, StringValueCache>(StringComparer.OrdinalIgnoreCase);
        readonly Dictionary<int, Int32ValueCache> int32Cache = new Dictionary<int, Int32ValueCache>();
        readonly Dictionary<long, Int64ValueCache> int64Cache = new Dictionary<long, Int64ValueCache>();
        List<FieldInfo> fields;
        readonly List<RowInfo> rows = new List<RowInfo>();

        public DataReaderCompressor(DataReaderCompressorOptions options)
        {
            this.options = options;
            if (options.SplitRecords && options.PacketSize == 0)
                throw new ArgumentException("SplitRecords = true ise, PacketSize alanı 0 dan büyük olmalıdır");
            if (options.NewFileNameFactory == null)
                throw new ArgumentNullException(nameof(options.NewFileNameFactory));
        }

        public void Compress(DbDataReader reader)
        {
            var schema = reader.GetSchemaTable();
            if (schema == null)
                throw new InvalidOperationException("Provider does not support DbDataReader.GetSchemaTable()!");
            var columnSize = schema.Columns["ColumnSize"];
            var columnOrdinal = schema.Columns["ColumnOrdinal"];
            fields = new List<FieldInfo>(reader.FieldCount);
            for (var i = 0; i < reader.FieldCount; i++)
            {
                var fieldName = reader.GetName(i);
                var row = schema.FindRowByColumnValue(columnOrdinal, i);
                if (row == null)
                    throw new InvalidOperationException("Internal error: Schema row could not be found!");
                fields.Add(new FieldInfo(i,
                    fieldName,
                    reader.GetFieldType(i),
                    options.SkipCachingFields.Contains(fieldName, StringComparer.OrdinalIgnoreCase),
                    TypeUtils.CoerceValue<int>(row[columnSize]),
                    19, 4)
                );
            }
            while (reader.Read())
            {
                var row = new RowInfo();
                rows.Add(row);
                CompressRow(row, reader);
                if (options.SplitRecords && rows.Count >= options.PacketSize)
                    WriteToFile();
            }
            if (rows.Count == 0) return;
            WriteToFile();
        }

        void Reset()
        {
            rows.Clear();
            stringCache.Clear();
            int32Cache.Clear();
            int64Cache.Clear();
            lastId = 0;
        }

        void WriteToFile()
        {
            using (var file = File.Create(options.NewFileNameFactory()))
            using (var writer = new BinaryWriter(file, Encoding.UTF8))
            {
                writer.WriteInt32Encoded(fields.Count);
                writer.WriteInt32Encoded(stringCache.Count);
                writer.WriteInt32Encoded(int32Cache.Count);
                writer.WriteInt32Encoded(int64Cache.Count);
                writer.WriteInt32Encoded(rows.Count);
                foreach (var field in fields)
                    field.Write(writer);
                foreach (var cache in stringCache.Values)
                {
                    writer.WriteInt32Encoded(cache.Id);
                    cache.WriteValue(writer);
                }
                foreach (var cache in int32Cache.Values)
                {
                    writer.WriteInt32Encoded(cache.Id);
                    cache.WriteValue(writer);
                }
                foreach (var cache in int64Cache.Values)
                {
                    writer.WriteInt32Encoded(cache.Id);
                    cache.WriteValue(writer);
                }
                foreach (var row in rows)
                    row.Write(writer);
            }
            Reset();
        }

        public DataTable Decompress(string inputFileName)
        {
            Reset();
            using (var file = File.OpenRead(inputFileName))
            using (var reader = new BinaryReader(file, Encoding.UTF8))
            {
                var fieldCount = reader.ReadInt32Encoded();
                var stringCacheCount = reader.ReadInt32Encoded();
                var int32CacheCount = reader.ReadInt32Encoded();
                var int64CacheCount = reader.ReadInt32Encoded();
                var rowCount = reader.ReadInt32Encoded();
                fields = new List<FieldInfo>(fieldCount);
                for (var i = 0; i < fieldCount; i++)
                    fields.Add(FieldInfo.FromReader(reader, i));
                for (var i = 0; i < stringCacheCount; i++)
                    CreateCacheItem(stringCache, reader, StringValueCache.FromReader);
                for (var i = 0; i < int32CacheCount; i++)
                    CreateCacheItem(int32Cache, reader, Int32ValueCache.FromReader);
                for (var i = 0; i < int64CacheCount; i++)
                    CreateCacheItem(int64Cache, reader, Int64ValueCache.FromReader);
                for (var i = 0; i < rowCount; i++)
                {
                    var row = new RowInfo();
                    rows.Add(row);
                    DecompressRow(row, reader);
                }
            }

            var dataTable = new DataTable();
            foreach (var field in fields)
            {
                var column = dataTable.Columns.Add(field.Name, field.Type);
                if (field.MaxLength > 0 && TypeUtils.IsSizable(field.Type))
                    column.MaxLength = field.MaxLength;
                column.ExtendedProperties.Add("PRECISION", (int)field.NumericPrecision);
                column.ExtendedProperties.Add("SCALE", (int)field.NumericScale);

            }
            foreach (var row in rows)
            {
                var rowData = new object[fields.Count];
                foreach (var cell in row.Cells)
                    rowData[cell.Field.Index] = cell.Value;
                dataTable.Rows.Add(rowData);
            }
            return dataTable;

            void CreateCacheItem<T, TCache>(IDictionary<T, TCache> cache, BinaryReader reader, Func<BinaryReader, TCache> factory) where TCache : ValueCache<T>
            {
                var cacheItem = factory(reader);
                cache.Add(cacheItem.Value, cacheItem);
            }
        }

        void CompressRow(RowInfo row, DbDataReader reader)
        {
            var cells = row.Cells;
            foreach (var field in fields)
            {
                var isNull = reader.IsDBNull(field.Index);
                if (field.SkipCaching || isNull)
                    cells.Add(new CellData(field, isNull ? null : reader.GetValue(field.Index)));
                else
                {
                    if (field.IsString)
                        CreateCellData(stringCache, field, reader.GetString(field.Index), v => new StringValueCache(v));
                    else if (field.IsInt32)
                        CreateCellData(int32Cache, field, reader.GetInt32(field.Index), v => new Int32ValueCache(v));
                    else if (field.IsInt64)
                        CreateCellData(int64Cache, field, reader.GetInt64(field.Index), v => new Int64ValueCache(v));
                    else if (field.IsDateTime)
                    {
                        var dateValue = reader.GetDateTime(field.Index);
                        // sadece 00:00:00.0 olanları alıyoruz. diğer durumda önbellek şişebilir.
                        if (dateValue.TimeOfDay.Ticks == 0)
                            CreateCellData(int64Cache, field, dateValue.Ticks, v => new Int64ValueCache(v));
                        else
                            cells.Add(new CellData(field, reader.GetValue(field.Index)));
                    }
                    else
                        cells.Add(new CellData(field, reader.GetValue(field.Index)));
                }
            }

            void CreateCellData<T, TCache>(IDictionary<T, TCache> cache, FieldInfo field, T value, Func<T, TCache> factory) where TCache : ValueCache<T>
            {
                if (!cache.TryGetValue(value, out var cachedValue))
                {
                    cachedValue = factory(value);
                    cache.Add(value, cachedValue);
                }
                cells.Add(new CellData(field, cachedValue));
            }
        }

        void DecompressRow(RowInfo row, BinaryReader reader)
        {
            var maskLength = (fields.Count - 1) / 8 + 1;
            var nullMaskData = reader.ReadBytes(maskLength);
            var cacheMaskData = reader.ReadBytes(maskLength);
            var nullMask = new BitArray(nullMaskData);
            var cacheMask = new BitArray(cacheMaskData);
            var cells = row.Cells;
            foreach (var field in fields)
            {
                if (nullMask.Get(field.Index))
                    cells.Add(new CellData(field, (object)null));
                else if (cacheMask.Get(field.Index))
                {
                    var cacheId = reader.ReadInt32Encoded();
                    if (field.IsString)
                        CreateCellDataFromCache(stringCache, field, cacheId);
                    else if (field.IsInt32)
                        CreateCellDataFromCache(int32Cache, field, cacheId);
                    else if (field.IsInt64 || field.IsDateTime)
                        CreateCellDataFromCache(int64Cache, field, cacheId);
                }
                else
                {
                    cells.Add(new CellData(field, reader.ReadObjectEx(field.TypeCode, ReadWriteOptions)));
                }
            }

            void CreateCellDataFromCache<T, TCache>(IDictionary<T, TCache> cache, FieldInfo field, int cacheId)
                where TCache : ValueCache<T>
            {
                foreach (var cacheItem in cache.Values)
                {
                    if (cacheItem.Id != cacheId) continue;
                    cells.Add(new CellData(field, cacheItem));
                    return;
                }
                throw new InvalidOperationException("Önbellek id değeri bulunamadı! -> " + cacheId);
            }
        }

        sealed class RowInfo
        {
            public List<CellData> Cells { get; } = new List<CellData>();

            public void Write(BinaryWriter writer)
            {
                var nullMask = new BitArray(Cells.Count);
                var cacheMask = new BitArray(Cells.Count);
                foreach (var cell in Cells)
                {
                    if (cell.IsNull)
                        nullMask.Set(cell.Field.Index, true);
                    if (cell.FromCache)
                        cacheMask.Set(cell.Field.Index, true);
                }
                var maskLength = (Cells.Count - 1) / 8 + 1;
                var nullMaskData = new byte[maskLength];
                nullMask.CopyTo(nullMaskData, 0);
                var cacheMaskData = new byte[maskLength];
                cacheMask.CopyTo(cacheMaskData, 0);
                writer.Write(nullMaskData);
                writer.Write(cacheMaskData);
                foreach (var cell in Cells)
                {
                    // null olanları geçiyoruz
                    if (cell.IsNull) continue;
                    cell.Write(writer);
                }
            }

        }

        sealed class CellData
        {
            readonly object value;
            readonly ValueCache cache;
            public FieldInfo Field { get; }
            public object Value
            {
                get
                {
                    if (cache == null) return value;
                    var v = cache.GetValue();
                    if (Field.IsDateTime)
                        v = new DateTime((long)cache.GetValue());
                    return v;
                }
            }

            public bool IsNull { get; }
            public bool FromCache { get; }


            public CellData(FieldInfo field, object value)
            {
                Field = field;
                this.value = value;
                cache = null;
                IsNull = value == null || Equals(value, DBNull.Value);
                FromCache = false;
            }

            public CellData(FieldInfo field, ValueCache cache)
            {
                Field = field;
                this.cache = cache;
                IsNull = false;
                FromCache = true;
            }

            public void Write(BinaryWriter writer)
            {
                if (FromCache)
                {
                    writer.WriteInt32Encoded(cache.Id);
                    return;
                }
                writer.WriteObjectEx(Value, ReadWriteOptions);
            }
        }

        sealed class FieldInfo
        {
            public int Index { get; }
            public Type Type { get; }
            public string Name { get; }
            public bool SkipCaching { get; }
            public int MaxLength { get; }
            public short NumericPrecision { get; }
            public short NumericScale { get; }
            public bool IsString { get; }
            public bool IsInt32 { get; }
            public bool IsInt64 { get; }
            public bool IsDateTime { get; }
            public TypeCodeEx TypeCode { get; }

            public FieldInfo(int index, string name, Type type, bool skipCaching, int maxLength, short numericPrecision, short numericScale)
            {
                Index = index;
                Name = name;
                Type = type;
                SkipCaching = skipCaching;
                MaxLength = maxLength;
                NumericPrecision = numericPrecision;
                NumericScale = numericScale;
                IsString = type == typeof(string);
                IsInt32 = type == typeof(int);
                IsInt64 = type == typeof(long);
                IsDateTime = type == typeof(DateTime);
                TypeCode = TypeUtils.GetTypeCodeEx(type);
            }

            FieldInfo(int index, string name, TypeCodeEx typeCode, int maxLength, short numericPrecision, short numericScale)
            {
                Index = index;
                Name = name;
                Type = TypeUtils.GetTypeFromTypeCodeEx(typeCode);
                SkipCaching = false;
                IsString = typeCode == TypeCodeEx.String;
                IsInt32 = typeCode == TypeCodeEx.Int32;
                IsInt64 = typeCode == TypeCodeEx.Int64;
                IsDateTime = typeCode == TypeCodeEx.DateTime;
                TypeCode = typeCode;
                MaxLength = maxLength;
                NumericPrecision = numericPrecision;
                NumericScale = numericScale;
            }

            public void Write(BinaryWriter writer)
            {
                var typeCode = TypeUtils.GetTypeCodeEx(Type);
                writer.Write(Name);
                writer.WriteInt32Encoded((int)typeCode);
                writer.WriteInt32Encoded(MaxLength);
                writer.Write(NumericPrecision);
                writer.Write(NumericScale);
            }

            public static FieldInfo FromReader(BinaryReader reader, int index)
            {
                var name = reader.ReadString();
                var typeCode = (TypeCodeEx)reader.ReadInt32Encoded();
                var maxLength = reader.ReadInt32Encoded();
                var precision = reader.ReadInt16();
                var scale = reader.ReadInt16();
                return new FieldInfo(index, name, typeCode, maxLength, precision, scale);
            }
        }

        #region Cache Types

        abstract class ValueCache
        {
            public int Id { get; protected set; }
            public abstract object GetValue();

            protected ValueCache()
            {
                Id = lastId++;
            }

            public abstract void WriteValue(BinaryWriter writer);
        }

        abstract class ValueCache<T> : ValueCache
        {
            public T Value { get; }

            protected ValueCache(T value)
            {
                Value = value;

            }

            public override object GetValue() => Value;
        }

        sealed class StringValueCache : ValueCache<string>
        {
            public StringValueCache(string value) : base(value)
            {
            }

            public override void WriteValue(BinaryWriter writer) => writer.Write(Value);

            public static StringValueCache FromReader(BinaryReader reader)
            {
                var id = reader.ReadInt32Encoded();
                var value = reader.ReadString();
                return new StringValueCache(value) { Id = id };
            }
        }

        sealed class Int32ValueCache : ValueCache<int>
        {
            public Int32ValueCache(int value) : base(value)
            {
            }

            public override void WriteValue(BinaryWriter writer) => writer.WriteInt32Encoded(Value);

            public static Int32ValueCache FromReader(BinaryReader reader)
            {
                var id = reader.ReadInt32Encoded();
                var value = reader.ReadInt32Encoded();
                return new Int32ValueCache(value) { Id = id };
            }
        }

        sealed class Int64ValueCache : ValueCache<long>
        {
            public Int64ValueCache(long value) : base(value)
            {
            }

            public override void WriteValue(BinaryWriter writer) => writer.WriteInt64Encoded(Value);

            public static Int64ValueCache FromReader(BinaryReader reader)
            {
                var id = reader.ReadInt32Encoded();
                var value = reader.ReadInt64Encoded();
                return new Int64ValueCache(value) { Id = id };
            }
        }
        #endregion
    }

    public sealed class DataReaderCompressorOptions
    {
        /// <summary>
        /// Önbelleğe alınmayacak alanların listesini verir.
        /// </summary>
        public List<string> SkipCachingFields { get; } = new List<string>();
        /// <summary>
        /// DbDataReader'dan dönen kayıtların parça parça mı yoksa tek bir dosya da mı oluşacağını verir veya ayarlar.
        /// </summary>
        public bool SplitRecords { get; set; }
        /// <summary>
        /// SplitRecords = true ise, dosya içerisinde ne kadar satır içereceğini verir veya ayarlar.
        /// </summary>
        public int PacketSize { get; set; }
        /// <summary>
        /// Oluşacak dosya ismini türeten işlevi verir veya ayarlar
        /// </summary>
        public Func<string> NewFileNameFactory { get; set; }
    }
}

#endif