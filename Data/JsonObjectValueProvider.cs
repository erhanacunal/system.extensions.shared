﻿#if COREFULL || (COREDATA && JSON)
using System.Reflection;
using Newtonsoft.Json;

namespace System.Data
{
    public sealed class JsonObjectValueProvider<T> : ObjectValueProvider
    {
        public T Value { get; }

        public JsonObjectValueProvider(string value)
        {
            Value = JsonConvert.DeserializeObject<T>(value);
        }
    }
}

#endif