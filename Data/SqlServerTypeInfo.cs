﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Utils;

namespace System.Data
{
    public class SqlServerTypeInfo : ISqlTypeInfo
    {

        const byte A = 1; // Allowed
        const byte E = 2; // Explict Conversion
        const byte I = 3; // Implicit Conversion
        const byte X = 4; // Conversion not Allowed
        const byte C = 5; // Requires CAST
        const byte N = 6;// XmlConversion

        static byte[,] conversionTable;

        byte[,] ConversionTable
        {
            get
            {
                if (conversionTable == null)
                    conversionTable = new byte[30, 30] {
                       //                          
                       //                          
                       //                                     s
                       //                         d n         m
                       // b                       e u     b   a
                       // i                       c m f   i   l
                       // n                       i e l r g   l
                       // a                       m r o e i i i
                       // r                       a i a a n n n
                       // y                       l c t l t t t    
                        { A,I,I,I,I,I,I,I,I,I,I,I,I,I,X,X,I,I,I,I,I,I,I,I,I,I,X,X,I,I }, // binary
                        { I,A,I,I,I,I,I,I,I,I,I,I,I,I,X,X,I,I,I,I,I,I,I,I,I,I,X,X,I,I }, // varbinary
                        { E,E,A,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,E,I,I,I,I,I,I }, // char
                        { E,E,I,A,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,E,I,I,I,I,I,I }, // varchar
                        { E,E,I,I,A,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,E,I,X,I,I,I,I }, // nchar
                        { E,E,I,I,I,A,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,I,E,I,X,I,I,I,I }, // nvarchar
                        { E,E,I,I,I,I,A,I,I,I,I,I,E,E,E,E,E,E,E,E,E,E,E,E,X,X,X,X,I,X }, // datetime
                        { E,E,I,I,I,I,I,A,I,I,I,I,E,E,E,E,E,E,E,E,E,E,E,E,X,X,X,X,I,X }, // smalldatetime
                        { E,E,I,I,I,I,I,I,A,X,I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,X }, // date
                        { E,E,I,I,I,I,I,I,X,A,I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,X }, // time
                        { E,E,I,I,I,I,I,I,I,I,A,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,X }, // datetimeoffset
                        { E,E,I,I,I,I,I,I,I,I,I,A,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,X }, // date
                        { I,I,I,I,I,I,I,I,X,X,X,X,C,C,I,I,I,I,I,I,I,I,I,I,X,X,X,X,I,X }, // decimal
                        { I,I,I,I,I,I,I,I,X,X,X,X,C,C,I,I,I,I,I,I,I,I,I,I,X,X,X,X,I,X }, // numeric
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,A,I,I,I,I,I,I,I,I,X,X,X,X,X,I,X }, // float
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,A,I,I,I,I,I,I,I,X,X,X,X,X,I,X }, // real
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,A,I,I,I,I,I,I,I,X,X,X,X,I,X }, // bigint
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,A,I,I,I,I,I,I,X,X,X,X,I,X }, // int
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,I,A,I,I,I,I,I,X,X,X,X,I,X }, // smallint
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,I,I,A,I,I,I,I,X,X,X,X,I,X }, // tinyint
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,I,I,I,A,I,I,I,X,X,X,X,I,X }, // money
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,I,I,I,I,A,I,I,X,X,X,X,I,X }, // smallmoney
                        { I,I,I,I,I,I,I,I,X,X,X,X,I,I,I,I,I,I,I,I,I,I,A,I,X,X,X,X,I,X }, // bit
                        { I,I,I,I,X,X,I,I,X,X,X,X,I,I,X,X,I,I,I,I,I,I,I,A,X,I,X,X,X,X }, // timestamp
                        { I,I,I,I,I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,A,X,X,X,I,X }, // uniqueidentifier
                        { I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,X,A,X,X,X,X }, // image
                        { X,X,I,I,I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,A,I,X,I }, // ntext
                        { X,X,I,I,I,I,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,I,A,X,I }, // ntext
                        { E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,X,E,X,X,X,A,X }, // text
                        { E,E,E,E,E,E,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,N }  // sqlvariant
                    };
                return conversionTable;
            }
        }

        static readonly List<SqlServerTypeInfo> TypeInfos;
        readonly uint nameHash;
        public int Id { get; }
        public string Name { get; private set; }
        public bool IsSizable { get; private set; }
        public bool IsUnicode { get; private set; }
        public bool IsBinary { get; private set; }
        public bool IsNumeric { get; private set; }
        public bool HasPrecisionScale { get; private set; }
        public string DefaultValue { get; private set; }
        public Type CodeType { get; private set; }
        public DbType DbType { get; private set; }
        public bool IsString => CodeType == typeof(string);

        static SqlServerTypeInfo()
        {
            TypeInfos = new List<SqlServerTypeInfo>() {
                new SqlServerTypeInfo(173,"binary",           true  ) { DbType = DbType.Binary,IsBinary = true,  IsSizable = true, DefaultValue = "0x0", CodeType = typeof(byte[]) },
                new SqlServerTypeInfo(165,"varbinary",        true  ) { DbType = DbType.Binary,IsBinary = true,   IsSizable = true, DefaultValue = "0x0", CodeType = typeof(byte[]) },
                new SqlServerTypeInfo(175,"char",             true  ) { DbType = DbType.AnsiStringFixedLength,IsSizable = true, DefaultValue = "''", CodeType = typeof(char[]) },
                new SqlServerTypeInfo(167,"varchar",          true  ) { DbType = DbType.AnsiString,IsSizable = true, DefaultValue = "''", CodeType = typeof(string) },
                new SqlServerTypeInfo(239,"nchar",            true  ) { IsUnicode = true, DbType = DbType.StringFixedLength,IsSizable = true,DefaultValue = "''", CodeType = typeof(string) },
                new SqlServerTypeInfo(231,"nvarchar",         true  ) { IsUnicode = true,DbType = DbType.String,IsSizable = true,DefaultValue = "''", CodeType = typeof(string) },
                new SqlServerTypeInfo(61 ,"datetime",         false ) { DbType = DbType.DateTime,DefaultValue="'1900-01-01'", CodeType = typeof(DateTime) },
                new SqlServerTypeInfo(58 ,"smalldatetime",    false ) { DbType = DbType.DateTime,DefaultValue="'1900-01-01'", CodeType = typeof(DateTime) },
                new SqlServerTypeInfo(40 ,"date",             false ) { DbType = DbType.Date, DefaultValue="'1900-01-01'", CodeType = typeof(DateTime) },
                new SqlServerTypeInfo(41 ,"time",             false ) { DbType = DbType.Time, DefaultValue = "'00:00:00'", CodeType = typeof(TimeSpan)},
                new SqlServerTypeInfo(43 ,"datetimeoffset",   false ) { DbType = DbType.DateTimeOffset,CodeType = typeof(DateTimeOffset)},
                new SqlServerTypeInfo(42 ,"datetime2",        false ) { DbType = DbType.DateTime2,DefaultValue="'1900-01-01'", CodeType = typeof(DateTime) },
                new SqlServerTypeInfo(106,"decimal",          false ) { DbType = DbType.Decimal,IsNumeric = true, HasPrecisionScale = true,DefaultValue = "0", CodeType = typeof(decimal) },
                new SqlServerTypeInfo(108,"numeric",          false ) { DbType = DbType.Decimal,IsNumeric = true, HasPrecisionScale = true,DefaultValue = "0", CodeType = typeof(decimal) },
                new SqlServerTypeInfo(62 ,"float",            false ) { DbType = DbType.Double,IsNumeric = true,DefaultValue = "0.0", CodeType = typeof(double) },
                new SqlServerTypeInfo(59 ,"real",             false ) { DbType = DbType.Single,IsNumeric = true,DefaultValue = "0.0", CodeType = typeof(Single) },
                new SqlServerTypeInfo(127,"bigint",           false ) { DbType = DbType.Int64,IsNumeric = true,DefaultValue = "0", CodeType = typeof(long) },
                new SqlServerTypeInfo(56 ,"int",              false ) { DbType = DbType.Int32,IsNumeric = true,DefaultValue = "0", CodeType = typeof(int) },
                new SqlServerTypeInfo(52 ,"smallint",         false ) { DbType = DbType.Int16,IsNumeric = true,DefaultValue = "0", CodeType = typeof(short) },
                new SqlServerTypeInfo(48 ,"tinyint",          false ) { DbType = DbType.Byte,IsNumeric = true,DefaultValue = "0", CodeType = typeof(byte) },
                new SqlServerTypeInfo(60 ,"money",            false ) { DbType = DbType.Decimal,IsNumeric = true,DefaultValue = "0.0", CodeType = typeof(decimal) },
                new SqlServerTypeInfo(122,"smallmoney",       false ) { DbType = DbType.Decimal,IsNumeric = true,DefaultValue = "0", CodeType = typeof(decimal) },
                new SqlServerTypeInfo(104,"bit",              false ) { DbType = DbType.Boolean,DefaultValue = "0", CodeType = typeof(bool)},
                new SqlServerTypeInfo(189,"timestamp",        false ) ,
                new SqlServerTypeInfo(36 ,"uniqueidentifier", false ) { DbType = DbType.Guid, DefaultValue = "NEWID()", CodeType = typeof(Guid) },
                new SqlServerTypeInfo(34 ,"image",            false ) { DbType = DbType.Binary, IsBinary = true, DefaultValue = "0x0", CodeType = typeof(byte[]) },
                new SqlServerTypeInfo(99 ,"ntext",            false ) { IsUnicode = true, DbType = DbType.String,CodeType = typeof(string) } ,
                new SqlServerTypeInfo(35 ,"text",             false ) { DbType = DbType.String, IsBinary = true, DefaultValue="''", CodeType = typeof(string) },
                new SqlServerTypeInfo(98 ,"sql_variant",      true  ) { DbType = DbType.Object,CodeType = typeof(object) } ,
                new SqlServerTypeInfo(241,"xml",              false ) { DbType = DbType.Xml, CodeType = typeof(System.Xml.Linq.XDocument)},
                new SqlServerTypeInfo(128,"hierarchyid",      false ) ,

                new SqlServerTypeInfo(129,"geometry",         false ) ,
                new SqlServerTypeInfo(130,"geography",        false ) ,
                new SqlServerTypeInfo(256,"sysname",          false ) { DbType = DbType.String,CodeType = typeof(string)}
            };
        }

        SqlServerTypeInfo(int id, string name, bool isSizable)
        {
            Id = id;
            Name = name;
            nameHash = HashUtils.JenkinsHash(name.ToLowerInvariant());
            IsSizable = isSizable;
        }

        public static SqlServerTypeInfo GetById(int id)
        {
            return TypeInfos.FirstOrDefault(_ => _.Id == id);
        }

        public static SqlServerTypeInfo GetByName(string name)
        {
            var hash = HashUtils.JenkinsHash(name.ToLowerInvariant());
            return TypeInfos.FirstOrDefault(_ => _.nameHash == hash);
        }

        public static SqlServerTypeInfo GetByType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            restart:
            foreach (var ti in TypeInfos)
            {
                if (ti.CodeType == type)
                    return ti;
            }
            if (type.IsEnum)
            {
                type = type.GetEnumUnderlyingType();
                goto restart;
            }
            type = Nullable.GetUnderlyingType(type);
            if (type != null)
                goto restart;

            return null;
        }

        public static void CopyTo(IList<SqlServerTypeInfo> list)
        {
            foreach (var item in TypeInfos)
            {
                list.Add(item);
            }
        }

        public static implicit operator SqlServerTypeInfo(string src)
        {
            return GetByName(src);
        }

        public static implicit operator SqlServerTypeInfo(Type type)
        {
            return GetByType(type);
        }

        public SqlConversionResult IsConvertibleTo(ISqlTypeInfo to)
        {
            var rowIdx = TypeInfos.FindIndex(_ => ReferenceEquals(_, this));
            var colIdx = TypeInfos.FindIndex(_ => ReferenceEquals(_, to));
            if (rowIdx >= 30) return SqlConversionResult.NotAllowed;
            if (colIdx >= 30) return SqlConversionResult.NotAllowed;
            return (SqlConversionResult)ConversionTable[rowIdx, colIdx];
        }

        public override string ToString()
        {
            var csType = "";
            if (CodeType != null)
                csType = " -> " + TypeUtils.GetTypeAlias(CodeType);
            if (HasPrecisionScale)
                return $"{Name}(X,X){csType}";
            if (IsSizable)
                return $"{Name}(X){csType}";
            return $"{Name}{csType}";
        }
    }


}

#endif