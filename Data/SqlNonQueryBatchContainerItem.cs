﻿namespace System.Data
{
    sealed class SqlNonQueryBatchContainerItem : SqlBatchContainerItem
    {
        public SqlNonQueryBatchContainerItem(CommandType cmdType, string sqlText, object[] prms) : base(cmdType, sqlText, prms) { }
    }
}