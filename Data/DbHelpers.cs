﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
#if MEF
using System.ComponentModel.Composition;
#endif
using System.Data.Models;
using System.Data.Schemas;
using System.Text.Parsers;
using System.Reflection;

namespace System.Data
{
    public static class DbHelpers
    {
#if MEF
        static readonly IDbHelper Helper = MefTypeResolver.Resolve<IDbHelper>();
#else
        static readonly IDbHelper Helper = new SqlServerDbHelperImpl();
#endif
        static DbHelpers()
        {

        }

        #region Reader

        public static T ExecuteReader<T>(string cnnName, CommandType ct, Func<DbDataReader, T> func, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteReader(cnn, ct, func, sql, parameters);
        }

        public static T ExecuteReader<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql,
            params object[] parameters)
        {
            return Helper.ExecuteReader(cnn, ct, func, sql, parameters);
        }

        public static void ExecuteReader(string cnnName, CommandType ct, Action<DbDataReader> action, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteReader(cnn, ct, action, sql, parameters);
        }

        public static void ExecuteReader(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql,
            params object[] parameters)
        {
            Helper.ExecuteReader(cnn, ct, action, sql, parameters);
        }

        #endregion

        public static T ExecuteScalar<T>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteScalar<T>(cnn, sql, parameters);
        }


        public static T ExecuteScalar2<T>(string cnnName, string sql, T defaultValue, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteScalar2(cnn, sql, defaultValue, parameters);
        }

        public static T[] ExecuteArray<T>(string cnnName, CommandType cmdType, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteArray<T>(cnn, cmdType, sql, parameters);
        }

        public static T[] ExecuteArray<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] parameters)
        {
            return Helper.ExecuteArray<T>(cnn, cmdType, sql, parameters);
        }

        public static Tuple<T1, T2>[] ExecuteTupleArray<T1, T2>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T1, T2>(), sql, parameters);
        }

        public static Tuple<T1, T2>[] ExecuteTupleArray<T1, T2>(ConnectionData cnn, string sql, params object[] parameters)
        {
            return Helper.ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T1, T2>(), sql, parameters);
        }

        public static Tuple<T1, T2, T3>[] ExecuteTupleArray<T1, T2, T3>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T1, T2, T3>(), sql, parameters);
        }

        public static Tuple<T1, T2, T3, T4>[] ExecuteTupleArray<T1, T2, T3, T4>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteReader(cnn, CommandType.Text, reader => reader.ReadTupleArray<T1, T2, T3, T4>(), sql, parameters);
        }

        public static bool LoadVariables<TV1, TV2>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.LoadVariables(cnn, sql, ref v1, ref v2, parameters);
        }

        public static bool LoadVariables<TV1, TV2, TV3>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.LoadVariables(cnn, sql, ref v1, ref v2, ref v3, parameters);
        }

        public static bool LoadVariables<TV1, TV2, TV3, TV4>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.LoadVariables(cnn, sql, ref v1, ref v2, ref v3, ref v4, parameters);
        }

        public static Tuple<TR1, TR2> ExecuteScalarResults<TR1, TR2>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteScalarResults<TR1, TR2>(cnn, sql, parameters);
        }

        public static void LoadScalarResults<TV1, TV2>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.LoadScalarResults(cnn, sql, ref v1, ref v2);
        }

        public static void LoadScalarResults<TV1, TV2, TV3>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.LoadScalarResults(cnn, sql, ref v1, ref v2, ref v3);
        }

        public static void LoadScalarResults<TV1, TV2, TV3, TV4>(string cnnName, string sql, ref TV1 v1, ref TV2 v2, ref TV3 v3, ref TV4 v4, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.LoadScalarResults(cnn, sql, ref v1, ref v2, ref v3, ref v4);
        }

        public static Tuple<T1, T2> ExecuteTuple<T1, T2>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteTuple<T1, T2>(cnn, sql, parameters);
        }

        public static Tuple<T1, T2, T3> ExecuteTuple<T1, T2, T3>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteTuple<T1, T2, T3>(cnn, sql, parameters);
        }

        public static Tuple<T1, T2, T3, T4> ExecuteTuple<T1, T2, T3, T4>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteTuple<T1, T2, T3, T4>(cnn, sql, parameters);
        }

        public static void ExecuteScalarList2<T>(string cnnName, IList<T> targetList, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteScalarList2(cnn, targetList, sql, parameters);
        }

        public static void ExecuteScalarListUnionAll<T>(string cnnName, IList<T> targetList, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteScalarListUnionAll(cnn, targetList, sql, parameters);
        }

        public static void ExecuteScalarDictionary2<TK, TV>(string cnnName, IDictionary<TK, TV> targetDict, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteScalarDictionary2(cnn, targetDict, sql, parameters);
        }

        public static string GetTempTableName()
        {
            return Helper.GetTempTableName();
        }

        public static int ExecuteNonQuery(string cnnName, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteNonQuery(cnn, sql, prms);
        }

        public static int ExecuteNonQuerySp(string cnnName, string sp, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteNonQuerySp(cnn, sp, prms);
        }

        public static DataTable ExecuteDataTable(string cnnName, CommandType cmdType, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteDataTable(cnn, cmdType, sql, prms);
        }

        public static DataTable ExecuteDataTable(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms)
        {
            return Helper.ExecuteDataTable(cnn, cmdType, sql, prms);
        }

        public static void ExecuteDataTable(string cnnName, CommandType cmdType, DataTable targetTable, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteReader(cnn, cmdType, targetTable.Load, sql, prms);
        }

        public static void ExecuteDataTable(ConnectionData cnn, CommandType cmdType, DataTable targetTable, string sql, params object[] prms)
        {
            Helper.ExecuteReader(cnn, cmdType, targetTable.Load, sql, prms);
        }

        public static DataSet ExecuteDataSet(string cnnName, CommandType cmdType, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteDataSet(cnn, cmdType, sql, prms);
        }

        public static T ExecuteObject<T>(string cnnName, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObject<T>(cnn, cmdType, sql, prms);
        }

        public static T ExecuteObject2<T>(string cnnName, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObject2(cnn, cmdType, factory, sql, prms);
        }

        public static T ExecuteObject<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
            => Helper.ExecuteObject<T>(cnn, cmdType, sql, prms);

        public static T ExecuteObjectVertical<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField,
            params object[] prms) where T : class, new()
        {
            return Helper.ExecuteObjectVertical<T>(cnn, cmdType, sql, propertyNameField, propertyValueField, prms);
        }

        public static T ExecuteObjectVertical<T>(string cnnName, CommandType cmdType, string sql, string propertyNameField, string propertyValueField,
            params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObjectVertical<T>(cnn, cmdType, sql, propertyNameField, propertyValueField, prms);
        }

        public static void ExecuteObjectList<T>(string cnnName, CommandType cmdType, IList<T> targetList, string sql, params object[] prms)
            where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteObjectList(cnn, cmdType, targetList, sql, prms);
        }

        public static void ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, IList<T> targetList, string sql, params object[] prms)
            where T : class, new()
        {
            Helper.ExecuteObjectList(cnn, cmdType, targetList, sql, prms);
        }

        public static void ExecuteObjectList2<T>(string cnnName, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteObjectList2(cnn, cmdType, targetList, factory, sql, prms);
        }

        public static List<T> ExecuteObjectList<T>(string cnnName, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObjectList<T>(cnn, cmdType, sql, prms);
        }

        public static List<T> ExecuteObjectList<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return Helper.ExecuteObjectList<T>(cnn, cmdType, sql, prms);
        }

        public static List<T> ExecuteObjectList2<T>(string cnnName, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObjectList2(cnn, cmdType, factory, sql, prms);
        }

        public static void ExecuteInsertAll<T>(string cnnName, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteInsertAll(cnn, sourceList, propertyProvider);
        }

        public static async Task ExecuteInsertAllAsync<T>(string cnnName, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteInsertAllAsync(cnn, sourceList, propertyProvider);
        }

        /// <summary>
        /// Hierarşik olarak ilişkilendirilmiş tablolarının tek bir işlem ile veritabanına eklenmesini sağlar. (Kendini referans eden tablolar dahil)
        /// </summary>
        /// <returns>Zincirleme çağrının yapılacağı görev örneği</returns>
        public static HierarchicalInsertAllBuilder ExecuteHierarchicalInsertAll()
        {
            return Helper.ExecuteHierarchicalInsertAll();
        }

        public static RecursiveQueryBuilder<T> ExecuteRecursiveQuery<T>(string parentFieldName, long idOrParentId, bool includeThisId) where T : class, new()
        {
            return Helper.ExecuteRecursiveQuery<T>(parentFieldName, idOrParentId, includeThisId);
        }

        public static void ExecuteUpdateAll<T>(string cnnName, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteUpdateAll(cnn, sourceList, propertyProvider);
        }

        public static async Task ExecuteUpdateAllAsync<T>(string cnnName, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteUpdateAllAsync(cnn, sourceList, propertyProvider);
        }

        public static void ExecuteInsert<T>(string cnnName, T instance, PropertyProvider<T> propertyProvider, bool onlyModified) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteInsert(cnn, instance, propertyProvider, onlyModified);
        }

        public static void ExecuteUpdate<T>(string cnnName, T instance, PropertyProvider<T> propertyProvider, bool updatePrimaryKeys) where T : class
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.ExecuteUpdate(cnn, instance, propertyProvider, updatePrimaryKeys);
        }

        public static CloneRowsBuilder<TEntity> BeginCloneRows<TEntity>(string cnnName) where TEntity : class, new()
            => new CloneRowsBuilder<TEntity>(cnnName);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla sonucu, birbirinden bağımsız belirtilen biçimlerde aktarır 
        /// </summary>
        /// <param name="cnnName">Sql bağlantı adı</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>.
        public static SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(string cnnName, string sql,
            params object[] prms) => Helper.ExecuteSingleQueryMultipleResults(cnnName, sql, prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla sonucu, birbirinden bağımsız belirtilen biçimlerde aktarır 
        /// </summary>
        /// <param name="cnn">Sql bağlantı bilgisi</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>.
        public static SingleQueryMultipleResultsBuilder ExecuteSingleQueryMultipleResults(ConnectionData cnn, string sql,
            params object[] prms) => Helper.ExecuteSingleQueryMultipleResults(cnn, sql, prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnnName">Sql bağlantı adı</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        public static ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(string cnnName, IList<T> targetList, string sql,
            params object[] prms) where T : class, new() => Helper.ExecuteListFromMultipleResult(cnnName, targetList, sql, prms);

        /// <summary>
        /// Sql ifadesinden gelen birden fazla dönen sonucu, tek bir listeye belirtilen tipden türeyen örnekler ile ekler
        /// </summary>
        /// <typeparam name="T">Temel tip</typeparam>
        /// <param name="cnn">Sql bağlantı bilgisi</param>
        /// <param name="targetList">Hedef liste</param>
        /// <param name="sql">Çalıştırılacak sql komutu</param>
        /// <param name="prms">Sql için gerekli parametreler</param>
        /// <returns>Fluent api için gerekli sınıfın örneği</returns>
        public static ListFromMultipleResult<T> ExecuteListFromMultipleResult<T>(ConnectionData cnn, IList<T> targetList, string sql,
            params object[] prms) where T : class, new() => Helper.ExecuteListFromMultipleResult(cnn, targetList, sql, prms);

        public static bool DataExists2(string cnnName, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.DataExists2(cnn, sql, prms);
        }

        public static T ExecuteObjectAnonymous<T>(string cnnName, CommandType cmdType, string sql, T type, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteObjectAnonymous(cnn, cmdType, sql, type, prms);
        }

        public static IList<T> ExecuteListAnonymous<T>(string cnnName, CommandType cmdType, string sql, T type, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.ExecuteListAnonymous(cnn, cmdType, sql, type, prms);
        }

        public static bool HasColumn(string cnnName, string tableName, string columnName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.HasColumn(cnn, tableName, columnName);
        }

        public static bool HasTable(string cnnName, string tableName)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return Helper.HasTable(cnn, tableName);
        }

        public static string CreateTableScriptFrom(DataTable tbl)
        {
            return Helper.CreateTableScriptFrom(tbl);
        }

        public static void CreateTable(ConnectionData connection, DataTable tbl)
        {
            Helper.CreateTable(connection, tbl);
        }

        public static Task CreateTableAsync(ConnectionData connection, DataTable tbl)
        {
            return Helper.CreateTableAsync(connection, tbl);
        }

        /// <summary>
        /// TABLE {TabloAdı} (
        ///   KolonAdı {TİPİ/varchar/int,...} [({Size})|({Precision,Scale})|COLLATE {Collation adı}|IDENTITY], 
        ///   ...
        /// )
        /// </summary>
        /// <param name="sqlScript"></param>
        /// <returns></returns>
        public static DataTable CreateDataTableFromScript(string sqlScript)
        {
            var st = new GenericLexer(new GenericLexerOptions
            {
                AllowHashedIdentifiers = true,
                ParseQualifiedIdentifier = true,
                ParseBracketIdentifiers = true
            }, sqlScript);
            string colName;
            st.ExpectIdentifier("TABLE");
            if (st.IsToken(TokenKind.LParen))
                colName = GetTempTableName();
            else
                st.Expect(TokenKind.Identifier, out colName);
            st.Expect(TokenKind.LParen);
            var dt = new DataTable(colName);
            do
            {
                st.Expect(TokenKind.Identifier, out colName);
                st.Expect(TokenKind.Identifier, out var typeName);
                var sti = SqlServerTypeInfo.GetByName(typeName);
                var col = dt.Columns.Add(colName, sti.CodeType);
                col.ExtendedProperties.Add("DBTYPE", typeName);
                if (sti.IsSizable)
                {
                    st.Expect(TokenKind.LParen);
                    col.MaxLength = Convert.ToInt32(st.ParseLiteralValue());
                    st.Expect(TokenKind.RParen);
                    if (st.IsIdentifierToken("COLLATE"))
                    {
                        st.NextToken();
                        st.Expect(TokenKind.Identifier, out colName);
                        col.ExtendedProperties.Add("COLLATION", colName);
                    }
                }
                else if (sti.IsNumeric && sti.HasPrecisionScale)
                {
                    st.Expect(TokenKind.LParen);
                    col.ExtendedProperties.Add("PRECISION", st.ParseLiteralValue());
                    st.Expect(TokenKind.Comma);
                    col.ExtendedProperties.Add("SCALE", st.ParseLiteralValue());
                    st.Expect(TokenKind.RParen);
                }
                if (st.IsIdentifierToken("IDENTITY"))
                {
                    st.NextToken();
                    col.ExtendedProperties.Add("IDENTITY", true);
                }
            } while (st.TrySkipToken(TokenKind.Comma));
            st.Expect(TokenKind.RParen);
            return dt;
        }

        public static DataTable CreateDataTableFromSchema<T>(SqlTableSchema table, IList<T> sourceList, PropertyProvider<T> propertyProvider) where T : class
            => Helper.CreateDataTableFromSchema(table, sourceList, propertyProvider);

        public static void BulkInsert(string cnnName, DataTable tbl)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                Helper.BulkInsert(cnn, tbl);
        }

        public static void BulkInsert(ConnectionData connection, DataTable tbl)
        {
            Helper.BulkInsert(connection, tbl);
        }

        public static Task BulkInsertAsync(ConnectionData connection, DataTable tbl)
        {
            return Helper.BulkInsertAsync(connection, tbl);
        }

        public static async Task<int> ExecuteNonQueryAsync(string cnnName, CommandType cmdType, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteNonQueryAsync(cnn, cmdType, sql, prms);
        }

        public static async Task<DataTable> ExecuteDataTableAsync(string cnnName, CommandType cmdType, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteDataTableAsync(cnn, cmdType, sql, prms);
        }

        public static async Task ExecuteDataTableAsync(string cnnName, CommandType cmdType, DataTable targetTable, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteDataTableAsync(cnn, cmdType, targetTable, sql, prms);
        }

        public static async Task<T> ExecuteObjectAsync<T>(string cnnName, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteObjectAsync<T>(cnn, cmdType, sql, prms);
        }

        public static async Task<T> ExecuteObject2Async<T>(string cnnName, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteObject2Async(cnn, cmdType, factory, sql, prms);
        }

        public static async Task<T> ExecuteObjectAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            return await Helper.ExecuteObjectAsync<T>(cnn, cmdType, sql, prms);
        }

        public static async Task<T> ExecuteObjectVerticalAsync<T>(ConnectionData cnn, CommandType cmdType, string sql, string propertyNameField, string propertyValueField,
            params object[] prms) where T : class, new()
        {
            return await Helper.ExecuteObjectVerticalAsync<T>(cnn, cmdType, sql, propertyNameField, propertyValueField, prms);
        }

        public static async Task<T> ExecuteObjectVerticalAsync<T>(string cnnName, CommandType cmdType, string sql, string propertyNameField, string propertyValueField, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteObjectVerticalAsync<T>(cnn, cmdType, sql, propertyNameField, propertyValueField, prms);
        }

        public static async Task ExecuteObjectListAsync<T>(string cnnName, CommandType cmdType, IList<T> targetList, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteObjectListAsync(cnn, cmdType, targetList, sql, prms);
        }

        public static async Task ExecuteObjectList2Async<T>(string cnnName, CommandType cmdType, IList<T> targetList, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteObjectList2Async(cnn, cmdType, targetList, factory, sql, prms);
        }

        public static async Task<List<T>> ExecuteObjectListAsync<T>(string cnnName, CommandType cmdType, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteObjectListAsync<T>(cnn, cmdType, sql, prms);
        }

        public static async Task<List<T>> ExecuteObjectList2Async<T>(string cnnName, CommandType cmdType, ObjectFactory<T> factory, string sql, params object[] prms) where T : class, new()
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteObjectList2Async(cnn, cmdType, factory, sql, prms);
        }
        // async //

        public static async Task<T> ExecuteReaderAsync<T>(string cnnName, CommandType ct, Func<DbDataReader, T> func, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteReaderAsync(cnn, ct, func, sql, parameters);
        }

        public static async Task<T> ExecuteReaderAsync<T>(ConnectionData cnn, CommandType ct, Func<DbDataReader, T> func, string sql,
            params object[] parameters)
        {
            return await Helper.ExecuteReaderAsync(cnn, ct, func, sql, parameters);
        }

        public static async Task ExecuteReaderAsync(string cnnName, CommandType ct, Action<DbDataReader> action, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteReaderAsync(cnn, ct, action, sql, parameters);
        }

        public static Task ExecuteReaderAsync(ConnectionData cnn, CommandType ct, Action<DbDataReader> action, string sql,
            params object[] parameters)
        {
            return Helper.ExecuteReaderAsync(cnn, ct, action, sql, parameters);
        }

        public static async Task<T> ExecuteScalarAsync<T>(string cnnName, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteScalarAsync<T>(cnn, sql, parameters);
        }

        public static async Task<T> ExecuteScalar2Async<T>(string cnnName, string sql, T defaultValue, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.ExecuteScalar2Async(cnn, sql, defaultValue, parameters);
        }

        public static async Task<bool> DataExistsAsync(string cnnName, string sql, params object[] prms)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                return await Helper.DataExistsAsync(cnn, sql, prms);
        }

        public static async Task ExecuteScalarListAsync<T>(string cnnName, IList<T> targetList, string sql, params object[] parameters)
        {
            using (var cnn = ConnectionManager.Get(cnnName))
                await Helper.ExecuteScalarListAsync(cnn, targetList, sql, parameters);
        }
    }
}

#endif