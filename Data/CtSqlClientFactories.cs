﻿#if COREFULL || COREDATA
using System.Collections.Generic;
using System.Linq;
using System.Data.Providers;

namespace System.Data
{
    public static class CtSqlClientFactories
    {

        static readonly List<CtSqlClientFactory> Factories = new List<CtSqlClientFactory>();

        static CtSqlClientFactories()
        {
            Factories.Add(new CtSqlServerClientFactory());
#if ORACLE
            Factories.Add(new CtOracleClientFactory()); 
#endif
#if FIREBIRD
            Factories.Add(new CtFirebirdSqlClientFactory());
#endif
#if DB2
            Factories.Add(new CtDb2SqlClientFactory());
#endif
#if MYSQL
            Factories.Add(new CtMySqlSqlClientFactory());
#endif
        }

        public static void RegisterSqlClientFactory(CtSqlClientFactory factory)
        {
            if (!Factories.Contains(factory))
                Factories.Add(factory);
        }

        public static CtSqlClientFactory Find(DbEngineType type) => Factories.FirstOrDefault(f => f.Engine == type);
    }
}

#endif