﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Models;
using System.Reflection;
using System.Utils;

namespace System.Data
{
    using System.Data.Schemas;
    public static class ObjectDbOperations
    {
        /// <summary>
        /// Tek bir Obje'den türeyen sınıfı verilen bağlantıdaki veritabanına günceller
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="connection">Sql ifadesinin çalıştırılacağı bağlantı örneği</param>
        /// <param name="instance">Güncellenecek örnek</param>
        /// <param name="updatePrimaryKeys"></param>
        /// <param name="propertyProvider"></param>
        /// <param name="tracker"></param>
        public static void Update<T>(ConnectionData connection, T instance, bool updatePrimaryKeys = false, PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            var objectInstance = tracker?.GetObjectInstance(instance);
            UpdateCore(connection, instance, propertyProvider ?? new ObjectInstancePropertyProvider<T>(objectInstance, false, updatePrimaryKeys));
            if (objectInstance == null) return;
            objectInstance.IsNew = false;
            objectInstance.EndEdit();
        }

        internal static void UpdateCore<T>(ConnectionData connection, T instance, PropertyProvider<T> propertyProvider) where T : class
        {
            var accessor = propertyProvider.Accessor;
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(accessor.TableName);
            using (var cmd = connection.CreateCommand())
            {
                var fieldsToUpdate = propertyProvider.GetProperties();
                if (fieldsToUpdate.Count == 0)
                    return;
                var sb = new StringBuilder();
                var alterTableScripts = new Dictionary<string, string>();
                sb.AppendFormat("UPDATE [{0}] SET ", accessor.TableName);
                var i = 0;
                foreach (var prop in fieldsToUpdate)
                {
                    if (i++ > 0)
                        sb.Append(",");
                    var fd = prop.GetPropertyData(instance);
                    sb.AppendFormat("{0} = @p{1}", prop.ColumnName, i);
                    var val = fd.Value;
                    var size = fd.Size;
                    var mt = tableMetadata.FindColumn(prop.ColumnNameHash);
                    if (mt.TypeName.IsOneOfThese(TableInfo.SizableTypes) &&
                        (mt.MaxLength > 0) && (size > mt.MaxLength))
                    {
                        alterTableScripts[prop.ColumnName] = $"ALTER TABLE [{accessor.TableName}] ALTER COLUMN [{prop.ColumnName}] {mt.TypeName}({size})";
                        mt.MaxLength = size;
                    }
                    cmd.AddWithValue("@p" + i, val);
                }
                sb.Append(" WHERE ");
                if (accessor.IdentityProperty != null)
                {
                    i++;
                    sb.AppendFormat("{0} = @p{1}", accessor.IdentityProperty.ColumnName, i);
                    cmd.AddWithValue("@p" + i, accessor.IdentityProperty.GetPropertyData(instance).Value);
                }
                else
                {
                    var j = 0;
                    foreach (var fd in accessor.PrimaryKeys)
                    {
                        if (j++ > 0)
                            sb.Append(" AND ");
                        i++;
                        sb.AppendFormat("( {0} = @p{1} )", fd.ColumnName, i);
                        if (propertyProvider.OnlyModifiedOrUpdatePrimaryKeys && propertyProvider.Instance != null && propertyProvider.Instance.IsEditing)
                            cmd.AddWithValue("@p" + i, propertyProvider.Instance.Snapshot.GetOriginalValue(fd));
                        else
                            cmd.AddWithValue("@p" + i, fd.Getter.GetValue(instance));
                    }
                }

                foreach (var kv in alterTableScripts)
                {
                    cmd.CommandText = kv.Value;
                    cmd.ExecuteNonQuery();
                }

                cmd.CommandText = sb.ToString();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Tek bir Obje'den türeyen sınıfı verilen bağlantıdaki veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="connection">Sql ifadesinin çalıştırılacağı bağlantı örneği</param>
        /// <param name="instance">Eklenecek örnek</param>
        /// <param name="onlyModified"></param>
        /// <param name="propertyProvider"></param>
        /// <param name="tracker"></param>
        public static void Insert<T>(ConnectionData connection, T instance, bool onlyModified = false, PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            var objectInstance = tracker?.GetObjectInstance(instance);
            propertyProvider = propertyProvider ?? new ObjectInstancePropertyProvider<T>(objectInstance, true, onlyModified);
            InsertCore(connection, instance, propertyProvider, tracker);
            if (objectInstance == null) return;
            objectInstance.IsNew = false;
            objectInstance.EndEdit();
        }

        internal static void InsertCore<T>(ConnectionData connection, T instance, PropertyProvider<T> propertyProvider, ObjectChangeTracker tracker = null) where T : class
        {
            var props = propertyProvider.GetProperties();
            if (props.Count == 0) return;
            var accessor = propertyProvider.Accessor;
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(accessor.TableName);
            using (var cmd = connection.CreateCommand())
            {
                // İşin özeti
                // * Eklenecek alanları üret
                // * Değer parametrelerini üret, değerlerini ayarla
                // * Sql ifadesini çalıştır
                var alterTableScripts = new Dictionary<string, string>();
                var sb = new StringBuilder();
                sb.AppendFormat("INSERT INTO {0}(", accessor.TableName);
                sb.Append(string.Join(",", props.Select(_ => string.Concat("[", _.ColumnName, "]"))));
                sb.Append(") VALUES (");
                var i = 0;
                foreach (var prop in props)
                {
                    if (i++ > 0)
                        sb.Append(',');
                    sb.AppendFormat("@p{0}", i);
                    var fd = prop.GetPropertyData(instance);
                    var val = fd.Value ?? DBNull.Value;
                    var size = fd.Size;
                    var mt = tableMetadata.FindColumn(prop.ColumnNameHash);
                    if (mt.TypeName.IsOneOfThese(TableInfo.SizableTypes) &&
                        (mt.MaxLength > 0) && (size > mt.MaxLength))
                    {
                        alterTableScripts[prop.ColumnName] = $"ALTER TABLE [{accessor.TableName}] ALTER COLUMN [{prop.ColumnName}] {mt.TypeName}({size})";
                        mt.MaxLength = size;
                    }

                    cmd.AddWithValue("@p" + i, val);
                }
                sb.Append(");");
                if (accessor.IdentityProperty != null)
                    sb.Append("SELECT SCOPE_IDENTITY()");

                foreach (var kv in alterTableScripts)
                {
                    cmd.CommandText = kv.Value;
                    cmd.ExecuteNonQuery();
                }

                cmd.CommandText = sb.ToString();
                if (accessor.IdentityProperty != null)
                    accessor.IdentityProperty.Setter.SetValue(instance, cmd.ExecuteScalar());
                else
                    cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Verilen örnekleri SqlBulkCopy ile geçici bir tabloya insert eder
        /// daha sonra gerçek tabloyu bu tabloyla günceller
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="connection">Sql ifadesinin çalıştırılacağı bağlantı örneği</param>
        /// <param name="sourceList">Örnekler</param>
        /// <param name="propertyProvider"></param>
        /// <param name="tracker">Nesne izleyici örneği</param>
        /// <remarks>
        /// Eklencekler ile güncellenecekleri birbirinden ayırır
        /// </remarks>
        public static void UpdateAll<T>(ConnectionData connection, IEnumerable<T> sourceList, PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            var willBeUpdated = sourceList as IList<T> ?? sourceList.ToList();
            if (willBeUpdated.Count == 0) return;
            var accessor = TypeAccessor.CreateOrGet<T>();
            if (propertyProvider == null)
                propertyProvider = tracker == null
                    ? (PropertyProvider<T>)new AllUpdatablePropertiesPropertyProvider<T>()
                    : new ChangeTrackerPropertyProvider<T>(tracker, willBeUpdated);
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(accessor.TableName);
            var modifiedProperties = propertyProvider.GetProperties();
            // değişen alan yoksa çık
            if (modifiedProperties.Count == 0)
                return;
            using (var cmd = connection.CreateCommand())
            {
                if (!(connection.Connection is SqlConnection))
                    return;
                // hangi alanlar değişmiş onları bul

                var allPks = accessor.PrimaryKeys;

                // hiç pk alan yoksa çık
                if (allPks.Count == 0)
                    return;

                var tmpTableName = DbHelpers.GetTempTableName();
                var tblInfo = TableInfo.Create(willBeUpdated, accessor, tableMetadata, allPks.Concat(modifiedProperties), false);
                tblInfo.AlterTableIfRequired(cmd, accessor.TableName);
                // temp tabloyu oluştur
                cmd.CommandText = tblInfo.CreateTableScript(tmpTableName, false);
                cmd.ExecuteNonQuery();

                var updateSql = new StringBuilder();
                updateSql.Append("UPDATE tgt SET ");

                var idProp = accessor.IdentityProperty;
                if (idProp == null) throw new InvalidOperationException("Identity property required for this operation");

                using (var sbc = new SqlBulkCopy((SqlConnection)cmd.Connection, SqlBulkCopyOptions.Default, (SqlTransaction)connection.Transaction))
                {
                    sbc.DestinationTableName = tmpTableName;
                    sbc.BulkCopyTimeout = 300;
                    var dt = new DataTable(tmpTableName);

                    dt.Columns.Add(idProp.ColumnName, idProp.UnderlyingType);
                    sbc.ColumnMappings.Add(idProp.ColumnName, idProp.ColumnName);

                    // geri kalan değişenleri ekle
                    var i = 0;
                    foreach (var prop in modifiedProperties)
                    {
                        dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);
                        sbc.ColumnMappings.Add(prop.ColumnName, prop.ColumnName);
                        if (i++ > 0)
                            updateSql.Append(", ");
                        updateSql.AppendFormat("[{0}] = src.[{0}]", prop.ColumnName);
                    }

                    updateSql
                        .AppendFormat(" FROM {0} tgt INNER JOIN [{1}] src ON ", accessor.TableName, tmpTableName)
                        .AppendFormat("( tgt.[{0}] = src.[{0}] )", idProp.ColumnName);

                    foreach (var row in tblInfo.Rows)
                    {
                        dt.Rows.Add(row);
                    }
                    // gönder
                    sbc.WriteToServer(dt);
                }
                updateSql
                    .Append("; DROP TABLE ")
                    .Append(tmpTableName);

                cmd.CommandText = updateSql.ToString();
                cmd.ExecuteNonQuery();
                if (tracker == null)
                    return;
                foreach (var item in willBeUpdated)
                {
                    var objInst = tracker.GetObjectInstance(item);
                    if (objInst == null)
                        continue;
                    objInst.IsNew = false;
                    objInst.EndEdit();
                }
            }
        }

        /// <summary>
        /// Verilen örnekleri SqlBulkCopy ile geçici bir tabloya insert eder
        /// daha sonra gerçek tabloyu bu tabloyla günceller
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="connection">Sql ifadesinin çalıştırılacağı bağlantı örneği</param>
        /// <param name="sourceList">Örnekler</param>
        /// <param name="propertyProvider"></param>
        /// <param name="tracker">Nesne izleyici örneği</param>
        /// <remarks>
        /// Eklencekler ile güncellenecekleri birbirinden ayırır
        /// </remarks>
        public static async Task UpdateAllAsync<T>(ConnectionData connection, IEnumerable<T> sourceList, PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            var willBeUpdated = sourceList as IList<T> ?? sourceList.ToList();
            if (willBeUpdated.Count == 0) return;
            var accessor = TypeAccessor.CreateOrGet<T>();
            if (propertyProvider == null)
                propertyProvider = tracker == null
                    ? (PropertyProvider<T>)new AllUpdatablePropertiesPropertyProvider<T>()
                    : new ChangeTrackerPropertyProvider<T>(tracker, willBeUpdated);
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(accessor.TableName);
            var modifiedProperties = propertyProvider.GetProperties();
            // değişen alan yoksa çık
            if (modifiedProperties.Count == 0)
                return;
            using (var cmd = connection.CreateCommand())
            {
                if (!(connection.Connection is SqlConnection))
                    return;
                // hangi alanlar değişmiş onları bul

                var allPks = accessor.PrimaryKeys;

                // hiç pk alan yoksa çık
                if (allPks.Count == 0)
                    return;

                var tmpTableName = DbHelpers.GetTempTableName();
                var tblInfo = TableInfo.Create(willBeUpdated, accessor, tableMetadata, allPks.Concat(modifiedProperties), false);
                tblInfo.AlterTableIfRequired(cmd, accessor.TableName);
                // temp tabloyu oluştur
                cmd.CommandText = tblInfo.CreateTableScript(tmpTableName, false);
                await cmd.ExecuteNonQueryAsync();

                var updateSql = new StringBuilder();
                updateSql.Append("UPDATE tgt SET ");

                var idProp = accessor.IdentityProperty;
                if (idProp == null) throw new InvalidOperationException("Identity property required for this operation");

                using (var sbc = new SqlBulkCopy((SqlConnection)cmd.Connection, SqlBulkCopyOptions.Default, (SqlTransaction)connection.Transaction))
                {
                    sbc.DestinationTableName = tmpTableName;
                    sbc.BulkCopyTimeout = 300;
                    var dt = new DataTable(tmpTableName);

                    dt.Columns.Add(idProp.ColumnName, idProp.UnderlyingType);
                    sbc.ColumnMappings.Add(idProp.ColumnName, idProp.ColumnName);

                    // geri kalan değişenleri ekle
                    var i = 0;
                    foreach (var prop in modifiedProperties)
                    {
                        dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);
                        sbc.ColumnMappings.Add(prop.ColumnName, prop.ColumnName);
                        if (i++ > 0)
                            updateSql.Append(", ");
                        updateSql.AppendFormat("[{0}] = src.[{0}]", prop.ColumnName);
                    }

                    updateSql
                        .AppendFormat(" FROM {0} tgt INNER JOIN [{1}] src ON ", accessor.TableName, tmpTableName)
                        .AppendFormat("( tgt.[{0}] = src.[{0}] )", idProp.ColumnName);

                    foreach (var row in tblInfo.Rows)
                    {
                        dt.Rows.Add(row);
                    }
                    // gönder
                    await sbc.WriteToServerAsync(dt);
                }
                updateSql
                    .Append("; DROP TABLE ")
                    .Append(tmpTableName);

                cmd.CommandText = updateSql.ToString();
                await cmd.ExecuteNonQueryAsync();
                if (tracker == null)
                    return;
                foreach (var item in willBeUpdated)
                {
                    var objInst = tracker.GetObjectInstance(item);
                    if (objInst == null)
                        continue;
                    objInst.IsNew = false;
                    objInst.EndEdit();
                }
            }
        }

        /// <summary>
        /// Verilen tüm Obje'den türeyen sınıf örneklerini verilen bağlantıdaki veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="connection">Sql ifadesinin çalıştırılacağı bağlantı örneği</param>
        /// <param name="sourceList">Eklenecek örnek</param>
        /// <param name="propertyProvider"></param>
        /// <param name="tracker"></param>
        public static void InsertAll<T>(ConnectionData connection, IList<T> sourceList,
            PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            if (sourceList.Count == 0) return;
            if (propertyProvider == null)
                propertyProvider = tracker == null
                    ? (PropertyProvider<T>)new AllUpdatablePropertiesPropertyProvider<T>()
                    : new ChangeTrackerPropertyProvider<T>(tracker, sourceList);
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(propertyProvider.Accessor.TableName);
            using (var cmd = connection.CreateCommand())
            {
                if (cmd is SqlCommand)
                    InsertAllBulk(propertyProvider, tableMetadata, cmd as SqlCommand, sourceList);
                else
                    InsertAllParametric(propertyProvider, tableMetadata, cmd, sourceList);
            }

        }

        public static async Task InsertAllAsync<T>(ConnectionData connection, IList<T> sourceList,
            PropertyProvider<T> propertyProvider = null, ObjectChangeTracker tracker = null) where T : class
        {
            if (sourceList.Count == 0) return;
            if (propertyProvider == null)
                propertyProvider = tracker == null
                    ? (PropertyProvider<T>)new AllUpdatablePropertiesPropertyProvider<T>()
                    : new ChangeTrackerPropertyProvider<T>(tracker, sourceList);
            var tableMetadata = connection.DbConnectionName.Database.GetCachedTable(propertyProvider.Accessor.TableName);
            using (var cmd = connection.CreateCommand())
            {
                if (cmd is SqlCommand)
                    await InsertAllBulkAsync(propertyProvider, tableMetadata, cmd as SqlCommand, sourceList);
                else
                    await InsertAllParametricAsync(propertyProvider, tableMetadata, cmd, sourceList);
            }

        }

        /// <summary>
        /// Sadece iç kullanım için, Verilen örnekleri SqlBulkCopy ile geçici bir tabloya insert eder
        /// daha sonra gerçek tabloya insert eder ve üretilen id'leri sonuç olarak döndürür
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="propertyProvider"></param>
        /// <param name="tableSchema">Tablo ile ilgili üst veri</param>
        /// <param name="cmd">Kullanılacak komut</param>
        /// <param name="sourceList">Nesne örnekleri</param>
        /// <param name="tracker"></param>
        static void InsertAllBulk<T>(PropertyProvider<T> propertyProvider, SqlTableSchema tableSchema, SqlCommand cmd, IList<T> sourceList, ObjectChangeTracker tracker = null) where T : class
        {
            var accessor = propertyProvider.Accessor;
            var idProp = accessor.IdentityProperty;
            var useTempTable = idProp != null;
            var tmpTable = accessor.TableName;
            int i;
            StringBuilder columnNames = null;
            StringBuilder valuesStatement = null;
            var tblInfo = TableInfo.Create(sourceList, accessor, tableSchema, propertyProvider.GetProperties(), useTempTable);
            tblInfo.AlterTableIfRequired(cmd, tmpTable);
            if (useTempTable)
            {
                // identity kolon var ise
                // ilk önce kayıtları geçici bir tabloya insert ediyoruz
                // sonra buradan dönen identity değerlerini model'e yazıyoruz
                // geçici tablo ismi
                tmpTable = DbHelpers.GetTempTableName();
                // geçici tabloyu oluştur
                cmd.CommandText = tblInfo.CreateTableScript(tmpTable, true);
                cmd.ExecuteNonQuery();
                // modelin gerçek propertylerinden kolon isimlerini toplamak için kullanılıyor
                columnNames = new StringBuilder();
                valuesStatement = new StringBuilder();

            }
            using (var sbc = new SqlBulkCopy(cmd.Connection, SqlBulkCopyOptions.Default, cmd.Transaction))
            {
                sbc.DestinationTableName = tmpTable;
                sbc.BulkCopyTimeout = ConnectionManager.CommandTimeout;
                var dt = new DataTable(tmpTable);
                i = 0;
                foreach (var col in tblInfo.Columns)
                {
                    var prop = col.Property;
                    dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);
                    sbc.ColumnMappings.Add(prop.ColumnName, prop.ColumnName);
                    if (!useTempTable)
                        continue;
                    if (i++ > 0)
                    {
                        columnNames.Append(',');
                        valuesStatement.Append(',');
                    }
                    columnNames.Append('[').Append(prop.ColumnName).Append(']');
                    valuesStatement.Append("temp.[").Append(prop.ColumnName).Append(']');
                }
                if (useTempTable)
                {
                    // fazladan bir de indeks kolonu ekliyoruz
                    // eklediğimiz sırada bize dönsün diye
                    // bu sıralama ile örneklerin ID değerlerini güncelleyeceğiz
                    dt.Columns.Add("IDX", typeof(int));
                    sbc.ColumnMappings.Add("IDX", "IDX");
                }
                foreach (var row in tblInfo.Rows)
                    dt.Rows.Add(row);
                // gönder
                sbc.WriteToServer(dt);
            }
            if (useTempTable)
            {
                // eski output inserted
                // ID değerleri benim insert ettiğim sırada gelmiyor, bu yüzden id'ler ve değerler birbirine giriyor
                //cmd.CommandText = string.Format("INSERT INTO {0} ({1}) OUTPUT INSERTED.{2} SELECT {1} FROM {3} ORDER BY IDX",
                //    accessor.TableName, columnNames, accessor.IdentityProperty.ColumnName, tmpTable);
                // yeni MERGE ifadesi, burada idx değerini de döndürebiliyoruz.

                cmd.CommandText = $@"MERGE INTO {accessor.TableName} USING {tmpTable} AS temp ON 1 = 0
WHEN NOT MATCHED THEN 
    INSERT ({columnNames})
    VALUES ({valuesStatement}) OUTPUT INSERTED.{idProp.ColumnName},temp.IDX;";
                using (var reader = cmd.ExecuteReader())
                {
                    var idSetter = idProp.Setter;
                    // gelen id leri modele yaz
                    while (reader.Read())
                    {
                        var item = sourceList[reader.GetInt32(1)];
                        idSetter.SetValue(item, reader.GetValue(0));
                    }
                }
                // geçici tabloyu bırak
                cmd.CommandText = "DROP TABLE " + tmpTable;
                cmd.ExecuteNonQuery();
            }
            if (tracker == null)
                return;
            foreach (var item in sourceList)
            {
                var objInst = tracker.GetObjectInstance(item);
                if (objInst == null)
                    continue;
                objInst.IsNew = false;
                objInst.EndEdit();
            }
        }

        /// <summary>
        /// Sadece iç kullanım için, Verilen örnekleri SqlBulkCopy ile geçici bir tabloya insert eder
        /// daha sonra gerçek tabloya insert eder ve üretilen id'leri sonuç olarak döndürür
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="propertyProvider"></param>
        /// <param name="tableSchema">Tablo ile ilgili üst veri</param>
        /// <param name="cmd">Kullanılacak komut</param>
        /// <param name="sourceList">Nesne örnekleri</param>
        /// <param name="tracker"></param>
        static async Task InsertAllBulkAsync<T>(PropertyProvider<T> propertyProvider, SqlTableSchema tableSchema, SqlCommand cmd, IList<T> sourceList, ObjectChangeTracker tracker = null) where T : class
        {
            var accessor = propertyProvider.Accessor;
            var idProp = accessor.IdentityProperty;
            var useTempTable = idProp != null;
            var tmpTable = accessor.TableName;
            StringBuilder columnNames = null;
            StringBuilder valuesStatement = null;
            var tblInfo = TableInfo.Create(sourceList, accessor, tableSchema, propertyProvider.GetProperties(), useTempTable);
            tblInfo.AlterTableIfRequired(cmd, tmpTable);
            if (useTempTable)
            {
                // identity kolon var ise
                // ilk önce kayıtları geçici bir tabloya insert ediyoruz
                // sonra buradan dönen identity değerlerini model'e yazıyoruz
                // geçici tablo ismi
                tmpTable = DbHelpers.GetTempTableName();
                // geçici tabloyu oluştur
                cmd.CommandText = tblInfo.CreateTableScript(tmpTable, true);
                await cmd.ExecuteNonQueryAsync();
                // modelin gerçek propertylerinden kolon isimlerini toplamak için kullanılıyor
                columnNames = new StringBuilder();
                valuesStatement = new StringBuilder();

            }
            using (var sbc = new SqlBulkCopy(cmd.Connection, SqlBulkCopyOptions.Default, cmd.Transaction))
            {
                sbc.DestinationTableName = tmpTable;
                var dt = new DataTable(tmpTable);
                var i = 0;
                foreach (var col in tblInfo.Columns)
                {
                    var prop = col.Property;
                    dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);
                    sbc.ColumnMappings.Add(prop.ColumnName, prop.ColumnName);
                    if (!useTempTable)
                        continue;
                    if (i++ > 0)
                    {
                        columnNames.Append(',');
                        valuesStatement.Append(',');
                    }
                    columnNames.Append('[').Append(prop.ColumnName).Append(']');
                    valuesStatement.Append("temp.[").Append(prop.ColumnName).Append(']');
                }
                if (useTempTable)
                {
                    // fazladan bir de indeks kolonu ekliyoruz
                    // eklediğimiz sırada bize dönsün diye
                    // bu sıralama ile örneklerin ID değerlerini güncelleyeceğiz
                    dt.Columns.Add("IDX", typeof(int));
                    sbc.ColumnMappings.Add("IDX", "IDX");
                }
                foreach (var row in tblInfo.Rows)
                    dt.Rows.Add(row);
                // gönder
                await sbc.WriteToServerAsync(dt);
            }
            if (useTempTable)
            {
                // eski output inserted
                // ID değerleri benim insert ettiğim sırada gelmiyor, bu yüzden id'ler ve değerler birbirine giriyor
                //cmd.CommandText = string.Format("INSERT INTO {0} ({1}) OUTPUT INSERTED.{2} SELECT {1} FROM {3} ORDER BY IDX",
                //    accessor.TableName, columnNames, accessor.IdentityProperty.ColumnName, tmpTable);
                // yeni MERGE ifadesi, burada idx değerini de döndürebiliyoruz.

                cmd.CommandText = $@"MERGE INTO {accessor.TableName} USING {tmpTable} AS temp ON 1 = 0
WHEN NOT MATCHED THEN 
    INSERT ({columnNames})
    VALUES ({valuesStatement}) OUTPUT INSERTED.{idProp.ColumnName},temp.IDX;";
                using (var reader = cmd.ExecuteReader())
                {
                    var idSetter = idProp.Setter;
                    // gelen id leri modele yaz
                    while (reader.Read())
                    {
                        var item = sourceList[reader.GetInt32(1)];
                        idSetter.SetValue(item, reader.GetValue(0));
                    }
                }
                // geçici tabloyu bırak
                cmd.CommandText = "DROP TABLE " + tmpTable;
                await cmd.ExecuteNonQueryAsync();
            }
            if (tracker == null)
                return;
            foreach (var item in sourceList)
            {
                var objInst = tracker.GetObjectInstance(item);
                if (objInst == null)
                    continue;
                objInst.IsNew = false;
                objInst.EndEdit();
            }
        }

        /// <summary>
        /// Sadece iç kullanım için, Verilen tüm Nesne örneklerini komut ile parametrik olarak verilen bağlantıdaki
        /// veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="propertyProvider"></param>
        /// <param name="tableSchema">Tablo ile ilgili üst veri</param>
        /// <param name="cmd">Kullanılacak komut</param>
        /// <param name="sourceList">Sınıf örnekleri</param>
        /// <param name="tracker">Nesne izleyici örneği</param>
        static void InsertAllParametric<T>(PropertyProvider<T> propertyProvider, SqlTableSchema tableSchema, DbCommand cmd, IList<T> sourceList, ObjectChangeTracker tracker = null) where T : class
        {
            if (sourceList.Count == 0) return;
            var accessor = propertyProvider.Accessor;
            var sb = new StringBuilder();
            sb.AppendFormat("INSERT INTO [{0}](", accessor.TableName);
            var i = 0;
            var valueParams = new List<Tuple<string, DbParameter, PropertyAccessor>>();
            foreach (var prop in propertyProvider.GetProperties())
            {
                if (i++ > 0)
                    sb.Append(",");
                sb.Append('[').Append(prop.ColumnName).Append(']');
                var pname = "@p" + i;
                var prm = cmd.CreateParameter();
                var colMetadata = tableSchema.FindColumn(prop.ColumnNameHash);
                prm.ParameterName = pname;
                prm.DbType = colMetadata.TypeInfo.DbType;
                valueParams.Add(Tuple.Create(pname, prm, prop));
                cmd.Parameters.Add(prm);
            }
            sb.AppendFormat(") VALUES({0});", string.Join(",", valueParams.Select(_ => _.Item1)));
            if (accessor.IdentityProperty != null)
                sb.Append("SELECT SCOPE_IDENTITY()");
            cmd.CommandText = sb.ToString();
            // max Length hesapla
            foreach (var insData in valueParams)
            {
                var colMetadata = tableSchema.FindColumn(insData.Item3.ColumnNameHash);
                var prm = (IDbDataParameter)insData.Item2;
                var dbType = prm.DbType;
                var prop = insData.Item3;
                if (colMetadata.TypeInfo.HasPrecisionScale)
                {
                    prm.Precision = colMetadata.Precision;
                    prm.Scale = colMetadata.Scale;
                    continue;
                }
                if (dbType != DbType.String && dbType != DbType.Binary && dbType != DbType.StringFixedLength) continue;
                var maxLength = 1;
                foreach (var item in sourceList)
                {
                    var val = prop.GetPropertyData(item);
                    maxLength = Math.Max(maxLength, val.Size);
                }
                // ALTER TABLE gerekli buraya henüz yapamadık
                if (colMetadata.MaxLength > 0 && maxLength > colMetadata.MaxLength)
                    maxLength = colMetadata.MaxLength;
                prm.Size = Math.Max(maxLength, 1);
            }
            cmd.Prepare();
            foreach (var item in sourceList)
            {
                foreach (var insData in valueParams)
                {
                    var prop = insData.Item3;
                    var val = prop.GetPropertyData(item);
                    insData.Item2.Value = val.Value ?? DBNull.Value;
                }
                if (accessor.IdentityProperty != null)
                    accessor.IdentityProperty.Setter.SetValue(item, cmd.ExecuteScalar());
                else
                    cmd.ExecuteNonQuery();
                var objInst = tracker?.GetObjectInstance(item);
                if (objInst == null)
                    continue;
                objInst.IsNew = false;
                objInst.EndEdit();
            }
            valueParams.Clear();
        }

        /// <summary>
        /// Sadece iç kullanım için, Verilen tüm Nesne örneklerini komut ile parametrik olarak verilen bağlantıdaki
        /// veritabanına ekler
        /// </summary>
        /// <typeparam name="T">Obje'den türeyen sınıf</typeparam>
        /// <param name="propertyProvider"></param>
        /// <param name="tableSchema">Tablo ile ilgili üst veri</param>
        /// <param name="cmd">Kullanılacak komut</param>
        /// <param name="sourceList">Sınıf örnekleri</param>
        /// <param name="tracker">Nesne izleyici örneği</param>
        static async Task InsertAllParametricAsync<T>(PropertyProvider<T> propertyProvider, SqlTableSchema tableSchema, DbCommand cmd, IList<T> sourceList, ObjectChangeTracker tracker = null) where T : class
        {
            if (sourceList.Count == 0) return;
            var accessor = propertyProvider.Accessor;
            var sb = new StringBuilder();
            sb.AppendFormat("INSERT INTO [{0}](", accessor.TableName);
            var i = 0;
            var valueParams = new List<Tuple<string, DbParameter, PropertyAccessor>>();
            foreach (var prop in propertyProvider.GetProperties())
            {
                if (i++ > 0)
                    sb.Append(",");
                sb.Append('[').Append(prop.ColumnName).Append(']');
                var pname = "@p" + i;
                var prm = cmd.CreateParameter();
                var colMetadata = tableSchema.FindColumn(prop.ColumnNameHash);
                prm.ParameterName = pname;
                prm.DbType = colMetadata.TypeInfo.DbType;
                valueParams.Add(Tuple.Create(pname, prm, prop));
                cmd.Parameters.Add(prm);
            }
            sb.AppendFormat(") VALUES({0});", string.Join(",", valueParams.Select(_ => _.Item1)));
            if (accessor.IdentityProperty != null)
                sb.Append("SELECT SCOPE_IDENTITY()");
            cmd.CommandText = sb.ToString();
            // max Length hesapla
            foreach (var insData in valueParams)
            {
                var colMetadata = tableSchema.FindColumn(insData.Item3.ColumnNameHash);
                var prm = (IDbDataParameter)insData.Item2;
                var dbType = prm.DbType;
                var prop = insData.Item3;
                if (colMetadata.TypeInfo.HasPrecisionScale)
                {
                    prm.Precision = colMetadata.Precision;
                    prm.Scale = colMetadata.Scale;
                    continue;
                }
                if (dbType != DbType.String && dbType != DbType.Binary && dbType != DbType.StringFixedLength) continue;
                var maxLength = 1;
                foreach (var item in sourceList)
                {
                    var val = prop.GetPropertyData(item);
                    maxLength = Math.Max(maxLength, val.Size);
                }
                // ALTER TABLE gerekli buraya henüz yapamadık
                if (colMetadata.MaxLength > 0 && maxLength > colMetadata.MaxLength)
                    maxLength = colMetadata.MaxLength;
                prm.Size = Math.Max(maxLength, 1);
            }
            cmd.Prepare();
            foreach (var item in sourceList)
            {
                foreach (var insData in valueParams)
                {
                    var prop = insData.Item3;
                    var val = prop.GetPropertyData(item);
                    insData.Item2.Value = val.Value ?? DBNull.Value;
                }
                if (accessor.IdentityProperty != null)
                    accessor.IdentityProperty.Setter.SetValue(item, await cmd.ExecuteScalarAsync());
                else
                    await cmd.ExecuteNonQueryAsync();
                var objInst = tracker?.GetObjectInstance(item);
                if (objInst == null)
                    continue;
                objInst.IsNew = false;
                objInst.EndEdit();
            }
            valueParams.Clear();
        }
    }

    /// <summary>
    /// Satırları analiz eder ve oluşturur
    /// </summary>
    sealed class TableInfo
    {

        internal static readonly string[] SizableTypes;
        internal static readonly string[] DecimalTypes;
        readonly TypeAccessor accessor;
        public List<object[]> Rows;
        internal List<ColumnInfo> Columns;
        readonly SqlTableSchema tableSchema;

        TableInfo(TypeAccessor accessor, SqlTableSchema tableSchema)
        {
            this.accessor = accessor;
            this.tableSchema = tableSchema;
        }

        static TableInfo()
        {
            SizableTypes = new[] { "nvarchar", "varchar", "char", "nchar", "varbinary" };
            DecimalTypes = new[] { "numeric", "decimal" };
        }

        internal ColumnInfo FindColumnByName(string name)
        {
            var hash = HashUtils.JenkinsHash(name.ToLowerInvariant());
            return Columns.Find(_ => _.NameHash == hash);

        }

        internal ColumnInfo FindColumnByNameHash(uint nameHash) => Columns.Find(_ => _.NameHash == nameHash);

        internal static TableInfo Create<T>(ICollection<T> sourceList, TypeAccessor accessor, SqlTableSchema tableSchema, IEnumerable<PropertyAccessor> items, bool hasId) where T : class
        {
            var ti = new TableInfo(accessor, tableSchema);

            var propList = items as PropertyAccessor[] ?? items.ToArray();
            var updateIdColumn = hasId;
            var createRows = true;
            var lastIdx = propList.Length;
            ti.Rows = new List<object[]>(sourceList.Count);
            ti.Columns = new List<ColumnInfo>(propList.Length);
            var columnCount = propList.Length + (hasId ? 1 : 0);
            foreach (var prop in propList)
            {
                var ci = new ColumnInfo(tableSchema, prop) { Index = ti.Columns.Count };
                ti.Columns.Add(ci);
                var rowIdx = 0;
                foreach (var item in sourceList)
                {
                    object[] row;
                    if (createRows)
                    {
                        row = new object[columnCount];
                        row.Fill(DBNull.Value);
                        ti.Rows.Add(row);
                    }
                    else
                        row = ti.Rows[rowIdx];

                    var fd = prop.GetPropertyData(item);

                    if (ci.IsSizable && fd.Size > ci.MeasuredSize)
                        ci.MeasuredSize = fd.Size;
                    row[ci.Index] = fd.Value;

                    if (updateIdColumn)
                        // id kolonunu güncelle
                        row[lastIdx] = rowIdx;
                    rowIdx++;
                }
                updateIdColumn = false;
                createRows = false;
            }
            return ti;
        }

        internal string CreateTableScript(string name, bool includeIdx)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("CREATE TABLE [{0}] (", name);
            var i = 0;
            foreach (var col in Columns)
            {
                if (i++ > 0)
                    sb.Append(", ");
                col.WriteColumn(sb);
            }
            if (includeIdx)
            {
                sb.AppendFormat(", IDX int\r\n");
                //var mt = tableMetadata.FindColumn(accessor.IdentityProperty.ColumnNameHash);
                //if (mt.TypeName.IsOneOfThese(StringComparison.OrdinalIgnoreCase, DecimalTypes))
                //    sb.AppendFormat(", IDX {0}({1},{2})\r\n", mt.TypeName, mt.Precision, mt.Scale);
                //else
                //    sb.AppendFormat(",IDX {0}\r\n", mt.TypeName);

            }
            sb.Append(')');
            return sb.ToString();
        }

        internal void AlterTableIfRequired(DbCommand cmd, string tableName)
        {
            var sb = new StringBuilder();
            foreach (var col in Columns)
            {
                if (!col.IsAlterColumnRequired)
                    continue;
                var colMetadata = tableSchema.FindColumn(col.Property.ColumnNameHash);
                sb.Clear();
                sb.Append("ALTER TABLE ").Append(tableName).Append(" ALTER COLUMN ");
                col.WriteColumn(sb, false);
                sb.AppendLine(";");
                colMetadata.MaxLength = col.MeasuredSize;
            }
            if (sb.Length <= 0)
                return;
            cmd.CommandText = sb.ToString();
            cmd.ExecuteNonQuery();
        }


    }

    sealed class ColumnInfo
    {
        internal readonly PropertyAccessor Property;
        public int MeasuredSize;
        internal readonly bool IsSizable;
        readonly bool isNumeric;
        readonly bool isTextualColumn;
        public int Index;
        internal readonly SqlTableColumnSchema Schema;
        internal readonly uint NameHash;

        public bool IsAlterColumnRequired => IsSizable &&
                                             (Schema.MaxLength > 0) && (MeasuredSize > Schema.MaxLength);

        internal void WriteColumn(StringBuilder sb, bool emitCollation = true)
        {
            sb.AppendFormat("[{0}] {1}", Schema.Name, Schema.TypeName);
            if (IsSizable)
            {
                var size = Schema.MaxLength == -1 ? (object)"MAX" : MeasuredSize;
                if (isTextualColumn && emitCollation)
                    sb.AppendFormat("({0}) COLLATE {1} \r\n", size, Schema.CollationName);
                else
                    sb.AppendFormat("({0})\r\n", size);
            }
            else if (isNumeric)
            {
                sb.AppendFormat("({0},{1})\r\n", Schema.Precision, Schema.Scale);
            }
        }

        public ColumnInfo(SqlTableSchema tableSchema, PropertyAccessor prop)
        {
            Property = prop;
            Schema = tableSchema.FindColumn(prop.ColumnNameHash);
            IsSizable = Schema.TypeName.IsOneOfThese(TableInfo.SizableTypes);
            isTextualColumn = IsSizable && prop.UnderlyingType == typeof(string);
            isNumeric = !IsSizable && Schema.TypeName.IsOneOfThese(TableInfo.DecimalTypes);
            MeasuredSize = 1;
            NameHash = prop.ColumnNameHash;
        }

        internal string GetColumnDefinition()
        {
            var sb = new StringBuilder();
            WriteColumn(sb, false);
            return sb.ToString();
        }


    }
}

#endif