﻿#if COREFULL || COREDATA
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Data.Providers;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace System.Data
{
    using System.Data.Schemas;
    public interface ISqlDialect
    {
        SqlTableSchema GetTableWithColumns(IDbConnectionName cnnName, string tableName);
        SqlTableSchema GetTable(IDbConnectionName cnnName, string tableName);
        IList<SqlTableColumnSchema> GetTableColumns(IDbConnectionName cnnName, SqlTableSchema table);
        IList<SqlTableIndexSchema> GetTableIndexes(IDbConnectionName cnnName, SqlTableSchema table);
        IList<SqlTableIndexColumnSchema> GetTableIndexColumns(IDbConnectionName cnnName, SqlTableIndexSchema index);
        IList<SqlForeignKeySchema> GetTableForeignKeys(IDbConnectionName cnnName, SqlTableSchema table);
        ISqlTypeInfo GetTypeInfo(string typeName);
        ISqlTypeInfo GetTypeInfo(int id);
        ISqlTypeInfo GetTypeInfo(Type type);
        object[] ConvertSqlQueryParameters(IEnumerable<ISqlQueryParameter> parameters, Action<SqlParamBuilder> builderAction = null);
        string GetColumnDefinition(SqlTableColumnSchema column, bool emitCollation = true);
        void GetColumnDefinition(StringBuilder sb, SqlTableColumnSchema column, bool emitCollation = true);
        void GetColumnDefinition(TextWriter writer, SqlTableColumnSchema column, bool emitCollation = true);
        string DbTypeToTypeName(DbType dbType);
        string CreateTableScriptFromDataTable(string tableName, DataTable table, bool memoryTable);
        void CreateTableScriptFromDataTable(TextWriter writer, string tableName, DataTable table, bool memoryTable);

        void CreateTableScriptFromSchema(TextWriter writer, SqlTableSchema table, bool memoryTable,
            string tableName = null);

        string GetAutoParameterName();
        string GenerateInsertScript<T>(IDbConnectionName cnnName, T instance, PropertyProvider<T> propertyProvider, out bool requiresReader, out object[] prms, bool identityInsert = false) where T : class;
        string GenerateUpdateScript<T>(IDbConnectionName cnnName, T instance, PropertyProvider<T> propertyProvider, out object[] prms) where T : class;

        Task BulkInsertAsync(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options);
        void BulkInsert(ConnectionData cnn, DataTable table, SqlDialectBulkCopyOptions options);
    }

    public enum SqlDialectBulkCopyOptions
    {
        Default = 0,
        KeepIdentity = 1,
        CheckConstraints = 2,
        TableLock = 4,
        KeepNulls = 8,
        FireTriggers = 16, // 0x00000010
        UseInternalTransaction = 32, // 0x00000020
        AllowEncryptedValueModifications = 64, // 0x00000040
    }
}

#endif