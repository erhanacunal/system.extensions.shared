﻿#if COREFULL || COREDATA
using System;
using System.Globalization;

namespace System.Data
{
    public static class TSql
    {
        static readonly string[] PhobitedWords = {
            "ALTER ",
            "CREATE ",
            "DROP ",
            "SELECT ",
            "DELETE ",
            "UPDATE ",
            "INSERT "
        };

        public static bool DisablePhobitedWordControl = false;



        public static string ConvertSqlValue(object value, string format = null, bool isRaw = false)
        {
            string result;
            if (ReferenceEquals(value, DBNull.Value))
                return "NULL";
            if (value == null)
                return "NULL";
            if (value is string)
            {
                if (!isRaw)
                {
                    var str = value.ToString().Replace("'", "''");
                    if (string.IsNullOrEmpty(format))
                        result = "'" + str + "'";
                    else
                        result = "'" + string.Format(format, str) + "'";
                }
                else
                {
                    var str = value.ToString().Replace("'", "''");
                    result = string.IsNullOrEmpty(format) ? str : string.Format(format, str);
                }
            }
            else if (value is DateTime)
            {
                if (Convert.ToDateTime(value) == DateTime.MinValue)
                {
                    result = "NULL";
                }
                else if (string.IsNullOrEmpty(format))
                    result = "'" + Convert.ToDateTime(value).ToString("yyyyMMdd HH:mm:ss.fff") + "'";
                else
                    result = "'" + string.Format(format, value) + "'";
            }
            else if (value is bool)
            {
                var val = Convert.ToBoolean(value);
                if (string.IsNullOrEmpty(format))
                    result = val ? "1" : "0";
                else
                    result = string.Format(format, val ? "1" : "0");
            }
            else if (value is byte[])
            {
                var bytes = (byte[])(object)value;
                result = bytes.ToHex();
            }
            else if (value is Guid)
            {
                result = $"'{(Guid)value}'";
            }
            else if (value is double)
            {
                result = ((double)value).ToString(CultureInfo.InvariantCulture.NumberFormat);
            }
            else if (value is Enum)
            {
                result = Convert.ToInt32(value).ToString();
            }
            else if (string.IsNullOrEmpty(format))
                result = value.ToString();
            else
                result = string.Format(format, value);
            if (DisablePhobitedWordControl) return result;
            foreach (var pw in PhobitedWords)
            {
                var idx = result.IndexOf(pw, StringComparison.InvariantCultureIgnoreCase);
                if (idx == -1) continue;
                result = string.Empty;
                break;
            }
            return result;
        }


        public static string ApplyLikeWildcard(string arg, LikeWildcard criteria)
        {
            if (string.IsNullOrEmpty(arg))
                return "%";
            switch (criteria)
            {
                case LikeWildcard.StartsWith:
                    return string.Concat(arg, "%");
                case LikeWildcard.Contains:
                    return string.Concat("%", arg, "%");
                case LikeWildcard.EndsWith:
                    return string.Concat("%", arg);
                default:
                    return arg;
            }
        }
    }

    public enum LikeWildcard
    {
        StartsWith,
        Contains,
        EndsWith
    }
}

#endif