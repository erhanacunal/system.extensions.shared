﻿namespace System.Data
{
    sealed class SqlBulkCopyBatchContainerItem : SqlBatchContainerItem
    {
        public DataTable Table { get; }
        public SqlDialectBulkCopyOptions Options { get; }


        public SqlBulkCopyBatchContainerItem(DataTable table, SqlDialectBulkCopyOptions options)
        {
            Table = table;
            Options = options;
        }
    }
}