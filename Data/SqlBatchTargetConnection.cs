﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Models;
using System.Data.Providers;
using System.Data.Schemas;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Utils;
using JetBrains.Annotations;

namespace System.Data
{
    /// <summary>
    /// Tek bir bağlantıda gerçekleşecek toplu sql işlerini çalıştırır.
    /// </summary>
    public sealed class SqlBatchTargetConnection : DisposableBase
    {
        readonly SqlBatchComposer composer;
        internal readonly IDbConnectionName CnnName;
        readonly List<SqlBatchContainer> batches = new List<SqlBatchContainer>();
        SqlBatchContainer currentBatchContainer;
        ConnectionData activeConnection;
        readonly bool disposeConnection = true;
        //readonly Guid debugId = Guid.NewGuid();

        public ConnectionData ActiveConnection =>
            activeConnection ?? (activeConnection = ConnectionManager.Get(CnnName));

        public SqlBatchTargetConnection(SqlBatchComposer composer, IDbConnectionName cnnName)
        {
            this.composer = composer;
            CnnName = cnnName;
            //Console.WriteLine("{0} constructed by connection name", debugId);
        }

        public SqlBatchTargetConnection(ConnectionData activeConnection)
        {
            this.activeConnection = activeConnection;
            CnnName = activeConnection.DbConnectionName;
            composer = null;
            disposeConnection = false;
            //Console.WriteLine("{0} constructed by activeConnection", debugId);
        }

        public SqlBatchComposer EndConnection() => composer;

        public DbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return ActiveConnection.BeginTransaction(isolationLevel);
        }

        public SqlBatchTargetConnection GetAllObjectList<T>([NotNull] IList<T> targetList, string additionalWhereClause, params object[] prms) where T : class, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var sql = StringBuilderCache.Allocate();
            sql.AppendFormat("SELECT * FROM {0}", accessor.TableName);
            if (additionalWhereClause.IsNotNullOrWhitespace())
                sql.Append(" WHERE ").Append(additionalWhereClause);
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReader(targetList, reader), CommandType.Text, StringBuilderCache.ReturnAndFree(sql), prms);
        }

        public SqlBatchTargetConnection GetAllObjectListIntf<T, TIntf>([NotNull] IList<TIntf> targetList, string additionalWhereClause, params object[] prms) where T : class, TIntf, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var sql = StringBuilderCache.Allocate();
            sql.AppendFormat("SELECT * FROM {0}", accessor.TableName);
            if (additionalWhereClause.IsNotNullOrWhitespace())
                sql.Append(" WHERE ").Append(additionalWhereClause);
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReaderIntf<T, TIntf>(targetList, reader), CommandType.Text, StringBuilderCache.ReturnAndFree(sql), prms);
        }

        public SqlBatchTargetConnection ObjectList<T>([NotNull] IList<T> targetList, CommandType cmdType, string sqlText, params object[] prms) where T : class, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReader(targetList, reader), cmdType, sqlText, prms);
        }

        public SqlBatchTargetConnection ObjectList<T>([NotNull] IList<T> targetList) where T : class, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReader(targetList, reader));
        }

        public SqlBatchTargetConnection ObjectListIntf<T, TIntf>([NotNull] IList<TIntf> targetList, CommandType cmdType,
            string sqlText, params object[] prms) where T : class, TIntf, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReaderIntf<T, TIntf>(targetList, reader), cmdType, sqlText,
                prms);
        }

        public SqlBatchTargetConnection ObjectListIntf<T, TIntf>([NotNull] IList<TIntf> targetList) where T : class, TIntf, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReaderIntf<T, TIntf>(targetList, reader));
        }

        public SqlBatchTargetConnection ObjectList<T>([NotNull] IList<T> targetList, CommandType cmdType, string sqlText,
            [NotNull] ObjectFactory<T> factory, params object[] prms) where T : class, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReader(targetList, reader, factory), cmdType,
                sqlText, prms);
        }

        public SqlBatchTargetConnection ObjectList<T>([NotNull] IList<T> targetList, [NotNull] ObjectFactory<T> factory) where T : class, new()
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            return ExecuteReader(reader => ObjectMapper.MapListFromDataReader(targetList, reader, factory));
        }

        public SqlBatchTargetConnection Object<T>([NotNull] Action<T> setterAction, CommandType cmdType, string sqlText, params object[] prms) where T : class, new()
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            return ExecuteReader(reader => setterAction(ObjectMapper.MapFromDataReader<T>(reader)), cmdType, sqlText,
                prms);
        }

        public SqlBatchTargetConnection Object<T>([NotNull] Action<T> setterAction) where T : class, new()
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            return ExecuteReader(reader => setterAction(ObjectMapper.MapFromDataReader<T>(reader)));
        }

        public SqlBatchTargetConnection Object<T>([NotNull] Action<T> setterAction, [NotNull] ObjectFactory<T> factory) where T : class, new()
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            return ExecuteReader(reader => setterAction(ObjectMapper.MapFromDataReader<T>(reader, factory)));
        }

        public SqlBatchTargetConnection ObjectVertical<T>([NotNull] Action<T> setterAction, string propertyNameField, string propertyValueField, bool mappableOnly, CommandType cmdType, string sqlText, params object[] prms) where T : class, new()
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            return ExecuteReader(reader => setterAction(ObjectMapper.MapVerticallyFrom<T>(reader, propertyNameField, propertyValueField, mappableOnly)), cmdType, sqlText,
                prms);
        }

        public SqlBatchTargetConnection ObjectVertical<T>([NotNull] Action<T> setterAction, string propertyNameField, string propertyValueField, bool mappableOnly) where T : class, new()
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            return ExecuteReader(reader => setterAction(ObjectMapper.MapVerticallyFrom<T>(reader, propertyNameField, propertyValueField, mappableOnly)));
        }

        public SqlBatchTargetConnection ExecuteScalar<T>([NotNull] Action<T> setterAction, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            var task = new ScalarValueSqlBatchContainerItem<T>(setterAction, cmdType, sqlText, prms);
            currentBatchContainer.AddItem(task);
            return this;
        }

        public SqlBatchTargetConnection ExecuteScalar<T>([NotNull] Action<T> setterAction)
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            var task = new ScalarValueSqlBatchContainerItem<T>(setterAction);
            currentBatchContainer.AddItem(task);
            return this;
        }

        public SqlBatchTargetConnection ExecuteScalar<T>([NotNull] Action<T> setterAction, T defaultValue, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            var task = new ScalarValueSqlBatchContainerItem<T>(setterAction, defaultValue, cmdType, sqlText, prms);
            currentBatchContainer.AddItem(task);
            return this;
        }

        public SqlBatchTargetConnection ExecuteScalar<T>([NotNull] Action<T> setterAction, T defaultValue)
        {
            if (setterAction == null) throw new ArgumentNullException(nameof(setterAction));
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            var task = new ScalarValueSqlBatchContainerItem<T>(setterAction, defaultValue);
            currentBatchContainer.AddItem(task);
            return this;
        }

        public SqlBatchTargetConnection ScalarList<T>([NotNull] IList<T> targetList, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => reader.ReadScalarList(targetList), cmdType, sqlText, prms);
        }

        public SqlBatchTargetConnection ScalarList<T>([NotNull] IList<T> targetList)
        {
            if (targetList == null) throw new ArgumentNullException(nameof(targetList));
            return ExecuteReader(reader => reader.ReadScalarList(targetList));
        }

        public SqlBatchTargetConnection ScalarDictionary<TKey, TValue>([NotNull] IDictionary<TKey, TValue> targetDictionary, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (targetDictionary == null) throw new ArgumentNullException(nameof(targetDictionary));
            return ExecuteReader(reader => reader.ReadScalarDictionary(targetDictionary), cmdType, sqlText, prms);
        }

        public SqlBatchTargetConnection ScalarDictionary<TKey, TValue>([NotNull] IDictionary<TKey, TValue> targetDictionary)
        {
            if (targetDictionary == null) throw new ArgumentNullException(nameof(targetDictionary));
            return ExecuteReader(reader => reader.ReadScalarDictionary(targetDictionary));
        }

        public SqlBatchTargetConnection FillDataTable([NotNull] DataTable targetTable, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (targetTable == null) throw new ArgumentNullException(nameof(targetTable));
            return ExecuteReader(targetTable.Load, cmdType, sqlText, prms);
        }

        public SqlBatchTargetConnection ExecuteDataTable([NotNull] Action<DataTable> setter, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (setter == null) throw new ArgumentNullException(nameof(setter));
            ExecuteReader(reader =>
            {
                var result = new DataTable();
                result.Load(reader);
                setter(result);
            }, cmdType, sqlText, prms);
            return this;
        }

        public SqlBatchTargetConnection ExecuteDataSet([NotNull] Action<DataSet> setter, CommandType cmdType, string sqlText, params object[] prms)
        {
            if (setter == null) throw new ArgumentNullException(nameof(setter));
            ExecuteReader(reader =>
            {
                var result = new DataSet();
                do
                {
                    var table = new DataTable();
                    table.Load(reader);
                    result.Tables.Add(table);
                } while (!reader.IsClosed);

                setter(result);
            }, cmdType, sqlText, prms);
            return this;
        }

        public SqlBatchTargetConnection ExecuteNonQuery(CommandType cmdType, string sqlText, params object[] prms)
        {
            EnsureSqlBatchContainerType<SqlNonQueryBatchContainer>();
            currentBatchContainer.AddItem(new SqlNonQueryBatchContainerItem(cmdType, sqlText, prms));
            return this;
        }

        public SqlBatchTargetConnection ExecuteReader(Action<DbDataReader> readerAction, CommandType cmdType, string sqlText, params object[] prms)
        {
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            currentBatchContainer.AddItem(new GenericDataReaderSqlBatchContainerItem(readerAction, cmdType, sqlText, prms));
            return this;
        }

        public SqlBatchTargetConnection ExecuteReader(Action<DbDataReader> readerAction)
        {
            EnsureSqlBatchContainerType<SqlDataReaderBatchContainer>();
            currentBatchContainer.AddItem(new GenericDataReaderSqlBatchContainerItem(readerAction));
            return this;
        }

        public SqlBatchTargetConnection Insert<T>(T instance, PropertyProvider<T> propertyProvider, bool identityInsert = false) where T : class
        {
            var sql = CnnName.SqlClientFactory.SqlDialect.GenerateInsertScript(CnnName, instance, propertyProvider,
                out var requiresReader, out var prms, identityInsert);
            if (requiresReader)
            {
                ExecuteReader(rdr =>
                {
                    if (rdr.Read())
                        propertyProvider.Accessor.IdentityProperty.Setter.SetValue(instance, rdr.GetValue(0));
                }, CommandType.Text, sql, prms);
            }
            else
                ExecuteNonQuery(CommandType.Text, sql, prms);

            return this;
        }

        public SqlBatchTargetConnection Update<T>(T instance, PropertyProvider<T> propertyProvider) where T : class
        {
            var sql = CnnName.SqlClientFactory.SqlDialect.GenerateUpdateScript(CnnName, instance, propertyProvider,
                out var prms);
            ExecuteNonQuery(CommandType.Text, sql, prms);
            return this;
        }

        public SqlBatchTargetConnection BulkInsertDataTable(DataTable table, SqlDialectBulkCopyOptions options)
        {
            EnsureSqlBatchContainerType<SqlBulkCopyBatchContainer>();
            currentBatchContainer.AddItem(new SqlBulkCopyBatchContainerItem(table, SqlDialectBulkCopyOptions.Default));
            return this;
        }

        public SqlBatchTargetConnection UpdateAll<T>(IEnumerable<T> instances, PropertyProvider<T> propertyProvider, Action<BatchUpdateBeforeExecuteArgs> beforeUpdate = null)
            where T : class
        {
            if (propertyProvider == null) throw new ArgumentNullException(nameof(propertyProvider));
            var willBeUpdated = instances as IList<T> ?? instances.ToList();

            if (willBeUpdated.Count == 0) return this;
            var accessor = TypeAccessor.CreateOrGet<T>();
            var tableMetadata = CnnName.Database.GetCachedTable(accessor.TableName);
            var allPks = accessor.PrimaryKeys;
            // güncellenecek alanlar arasında pk alanlar olmamalı
            propertyProvider.ExcludeProperties(allPks);

            var modifiedProperties = propertyProvider.GetProperties();
            var tmpTableName = DbHelpers.GetTempTableName();
            var allProperties = allPks.Concat(modifiedProperties).ToArray();

            var tblInfo = TableInfo.Create(willBeUpdated, accessor, tableMetadata, allProperties, false);

            ExecuteNonQuery(CommandType.Text, tblInfo.CreateTableScript(tmpTableName, false));

            var updateSql = new StringBuilder();
            updateSql.Append("UPDATE tgt SET ");
            var dt = new DataTable(tmpTableName);
            var i = 0;
            foreach (var prop in allProperties)
            {
                dt.Columns.Add(prop.ColumnName, prop.UnderlyingType);

                if (prop.InPrimaryKey) continue;

                if (i++ > 0)
                    updateSql.Append(", ");
                updateSql.AppendFormat("[{0}] = src.[{0}]", prop.ColumnName);
            }

            updateSql
                .AppendFormat(" FROM {0} tgt INNER JOIN [{1}] src ON ", accessor.TableName, tmpTableName);
            i = 0;
            foreach (var prop in allPks)
            {
                if (i++ > 0)
                    updateSql.Append(" AND ");
                updateSql.AppendFormat("( tgt.[{0}] = src.[{0}] )", prop.ColumnName);
            }

            foreach (var row in tblInfo.Rows)
                dt.Rows.Add(row);

            BulkInsertDataTable(dt, SqlDialectBulkCopyOptions.Default);
            var prms = new List<object>();
            beforeUpdate?.Invoke(new BatchUpdateBeforeExecuteArgs(this, accessor, updateSql, tmpTableName, prms));
            updateSql.AppendLine("DROP TABLE " + tmpTableName);
            ExecuteNonQuery(CommandType.Text, updateSql.ToString(), prms.ToArray());
            return this;
        }


        public SqlBatchTargetConnection InsertAll<T>(IEnumerable<T> instances, PropertyProvider<T> propertyProvider,
            Action<BatchInsertAfterExecuteArgs> afterInsert = null) where T : class
        {
            if (propertyProvider == null) throw new ArgumentNullException(nameof(propertyProvider));
            var willBeInserted = instances as IList<T> ?? instances.ToList();

            if (willBeInserted.Count == 0) return this;

            var accessor = propertyProvider.Accessor;
            var tableMetadata = CnnName.Database.GetCachedTable(accessor.TableName);
            var idProp = accessor.IdentityProperty;
            var useTempTable = idProp != null;
            var tmpTable = accessor.TableName;

            var tblInfo = TableInfo.Create(willBeInserted, accessor, tableMetadata, propertyProvider.GetProperties(),
                useTempTable);

            StringBuilder columnNames = null;
            StringBuilder valuesStatement = null;
            if (useTempTable)
            {
                // identity kolon var ise
                // ilk önce kayıtları geçici bir tabloya insert ediyoruz
                // sonra buradan dönen identity değerlerini model'e yazıyoruz
                // geçici tablo ismi
                tmpTable = DbHelpers.GetTempTableName();

                ExecuteNonQuery(CommandType.Text, tblInfo.CreateTableScript(tmpTable, true));

                // modelin gerçek propertylerinden kolon isimlerini toplamak için kullanılıyor
                columnNames = new StringBuilder();
                valuesStatement = new StringBuilder();
            }

            var dt = new DataTable(tmpTable);
            var i = 0;
            foreach (var col in tblInfo.Columns)
            {
                dt.Columns.Add(col.Property.ColumnName, col.Property.UnderlyingType);
                if (!useTempTable)
                    continue;
                if (i++ > 0)
                {
                    columnNames.Append(',');
                    valuesStatement.Append(',');
                }

                columnNames.Append('[').Append(col.Property.ColumnName).Append(']');
                valuesStatement.Append("temp.[").Append(col.Property.ColumnName).Append(']');
            }

            if (useTempTable)
            {
                // fazladan bir de indeks kolonu ekliyoruz
                // eklediğimiz sırada bize dönsün diye
                // bu sıralama ile örneklerin ID değerlerini güncelleyeceğiz
                dt.Columns.Add("IDX", typeof(int));
            }

            foreach (var row in tblInfo.Rows)
                dt.Rows.Add(row);

            BulkInsertDataTable(dt, SqlDialectBulkCopyOptions.Default);

            if (!useTempTable) return this;
            var insertSql = new StringBuilder();
            insertSql.Append("DECLARE @idTable TABLE( IDX int, ");
            var idColumn = tableMetadata.FindColumn(idProp.ColumnNameHash);
            insertSql.Append($"{idColumn.Name} {CnnName.Database.SqlDialect.GetColumnDefinition(idColumn)}");
            insertSql.AppendLine(" );");

            insertSql.Append($@"MERGE INTO {accessor.TableName} USING {tmpTable} AS temp ON 1 = 0
WHEN NOT MATCHED THEN 
    INSERT ({columnNames})
    VALUES ({valuesStatement}) OUTPUT temp.IDX, INSERTED.{idColumn.Name} INTO @idTable;");

            var prms = new List<object>();
            afterInsert?.Invoke(new BatchInsertAfterExecuteArgs(this, accessor, insertSql, "@idTable", prms));
            insertSql.AppendLine("SELECT * FROM @idTable;");
            insertSql.AppendLine("DROP TABLE " + tmpTable);
            ExecuteReader(reader =>
            {
                var idSetter = idProp.Setter;
                // gelen id leri modele yaz
                while (reader.Read())
                {
                    var item = willBeInserted[reader.GetInt32(0)];
                    idSetter.SetValue(item, reader.GetValue(1));
                }
            }, CommandType.Text, insertSql.ToString(), prms.ToArray());
            return this;
        }

        public HierarchicalInsertBuilder BeginHiearchicalInsert()
        {
            return new HierarchicalInsertBuilder(this);
        }

        public void Upsert<T>([NotNull] T item, long auditUserId, params string[] excludedProperties) where T : IAuditableEntity
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (auditUserId <= 0) throw new ArgumentOutOfRangeException(nameof(auditUserId));
            var accessor = TypeAccessor.CreateOrGet<T>();
            var prms = new List<object> { DbSwitches.UseNamedParams, "@auditUserId", auditUserId, "@id", item.IdValue };
            var sql = EntityUtils.CreateUpsertUniqueEntitySql(item, null, excludedProperties, accessor, prms);
            ExecuteScalar<long>(val =>
            {
                if (val == 0L)
                    throw new InvalidOperationException("already exists");
                item.IdValue = val;
            }, CommandType.Text, sql, prms.ToArray());
        }

        public void Delete<T>(T item, long auditUserId) where T : IAuditableEntity
        {
            var accessor = TypeAccessor.CreateOrGet<T>();
            ExecuteNonQuery(CommandType.Text,
                $"UPDATE {accessor.TableName} SET Deleted = 1, UpdatedBy=@p1, UpdatedDateTime=GETDATE() WHERE ID = @p2 AND Deleted = 0",
                auditUserId, item.IdValue);
        }

        void EnsureSqlBatchContainerType<T>() where T : SqlBatchContainer, new()
        {
            if (!(currentBatchContainer is T))
                batches.Add(currentBatchContainer = new T());
        }

        public async Task Execute()
        {
            try
            {
                foreach (var batch in batches)
                    await batch.Execute(ActiveConnection);
            }
            finally
            {
                batches.Clear();
            }
        }

        protected override void DisposeCore()
        {
            //Console.WriteLine("{0} disposing with dispose connection: {1}", debugId, disposeConnection);
            if (!disposeConnection) return;
            activeConnection?.Dispose();
        }
    }

    public sealed class BatchUpdateBeforeExecuteArgs
    {
        public BatchUpdateBeforeExecuteArgs(SqlBatchTargetConnection connection, TypeAccessor accessor, StringBuilder updateSql,
             string tempTableName, List<object> prms)
        {
            Connection = connection;
            Accessor = accessor;
            TempTableName = tempTableName;
            Prms = prms;
            UpdateSql = updateSql;
        }

        public SqlBatchTargetConnection Connection { get; }
        public TypeAccessor Accessor { get; }
        public string TempTableName { get; }
        public List<object> Prms { get; }
        public StringBuilder UpdateSql { get; }
    }

    public sealed class BatchInsertAfterExecuteArgs
    {
        public BatchInsertAfterExecuteArgs(SqlBatchTargetConnection connection, TypeAccessor accessor, StringBuilder insertSql,
            string tempTableName, List<object> prms)
        {
            Connection = connection;
            Accessor = accessor;
            InsertSql = insertSql;
            TempTableName = tempTableName;
            Prms = prms;
        }

        public SqlBatchTargetConnection Connection { get; }
        public TypeAccessor Accessor { get; }
        public StringBuilder InsertSql { get; }
        public string TempTableName { get; }
        public List<object> Prms { get; }
    }

    public sealed class HierarchicalInsertBuilder
    {
        bool generatorsOrdered;
        readonly SqlBatchTargetConnection connection;
        readonly List<IInsertAllGenerator> generators = new List<IInsertAllGenerator>();
        readonly HashSet<TypeRelationship> relationships = new HashSet<TypeRelationship>();
        public HierarchicalInsertBuilder(SqlBatchTargetConnection connection)
        {
            this.connection = connection;
        }

        public HierarchicalInsertBuilder InsertAll<T>(IList<T> sourceList, PropertyProvider<T> properties)
            where T : class, IAuditableEntity
        {
            var generator = new InsertAllGenerator<T>(this, sourceList, properties);
            generators.Add(generator);
            foreach (var relationship in generator.Accessor.Relationships)
                relationships.Add(relationship);
            return this;
        }

        void ReorderGenerators()
        {
            if (generatorsOrdered) return;
            foreach (var relationship in relationships)
            {
                var parentIndex = generators.FindIndex(_ => _.Accessor == relationship.ParentType);
                if (parentIndex == -1)
                    throw new InvalidOperationException(relationship.ParentType.TableName + " table must be in insert queue");
                var childIndex = generators.FindIndex(_ => _.Accessor == relationship.ChildType);
                if (childIndex == -1)
                    throw new InvalidOperationException(relationship.ParentType.TableName + " table must be in insert queue");
                if (childIndex >= parentIndex) continue;
                var tmp = generators[parentIndex];
                generators.RemoveAt(parentIndex);
                generators.Insert(childIndex, tmp);
            }

            generatorsOrdered = true;
        }

        internal IInsertAllGenerator FindGeneratorByAccessor(TypeAccessor accessor) =>
            generators.Find(_ => _.Accessor == accessor);

        public HierarchicalInsertBuilder ReIndex()
        {
            ReorderGenerators();
            foreach (var generator in generators)
                generator.ReIndex();
            return this;
        }

        public SqlBatchTargetConnection EndHierarchical()
        {
            ReorderGenerators();
            var sql = StringBuilderCache.Allocate();
            var prms = new SqlParamBuilder(true);
            foreach (var generator in generators)
                generator.GeneratePushSql(connection, sql);
            foreach (var generator in generators)
                generator.GeneratePullSql(sql);
            connection.ExecuteReader(reader =>
            {
                var i = 0;
                do
                {
                    generators[i++].ProcessResult(reader);
                    if (i >= generators.Count)
                        break;
                } while (reader.NextResult());
            }, CommandType.Text, StringBuilderCache.ReturnAndFree(sql), prms.Build());
            return connection;
        }
    }

    interface IInsertAllGenerator
    {
        TypeAccessor Accessor { get; }
        void ReIndex();
        int? FindRowIndexByOidValue(object oid);
        void GeneratePushSql(SqlBatchTargetConnection connection, StringBuilder sql);
        void GeneratePullSql(StringBuilder sql);
        void ProcessResult(DbDataReader reader);

    }

    sealed class InsertAllGenerator<T> : IInsertAllGenerator where T : class, IAuditableEntity
    {
        readonly HierarchicalInsertBuilder builder;
        readonly IList<T> sourceList;
        readonly PropertyProvider<T> propertyProvider;
        readonly string masterDetail;
        bool reIndexed;
        readonly List<string> parentColumns = new List<string>();
        string selfReferencingColumn;

        public InsertAllGenerator(HierarchicalInsertBuilder builder, IList<T> sourceList, PropertyProvider<T> propertyProvider, string masterDetail = "M")
        {
            this.builder = builder;
            this.sourceList = sourceList;
            this.propertyProvider = propertyProvider;
            this.masterDetail = masterDetail;
            Accessor = propertyProvider.Accessor;
        }


        public TypeAccessor Accessor { get; }

        public void ReIndex()
        {
            if (reIndexed) return;
            foreach (var relationship in Accessor.Relationships)
            {
                var parent = builder.FindGeneratorByAccessor(relationship.ParentType);
                var thisProperty = relationship.ChildProperty;
                foreach (var item in sourceList)
                {
                    var idx = parent.FindRowIndexByOidValue(thisProperty.Getter.GetValue(item));
                    thisProperty.Setter.SetValue(item, idx);
                }
            }
            reIndexed = true;
        }

        public int? FindRowIndexByOidValue(object oid)
        {
            if (oid == null) return null;
            var idProp = Accessor.IdentityProperty;
            var idx = 0;
            foreach (var item in sourceList)
            {
                if (Equals(oid, idProp.Getter.GetValue(item)))
                    return idx;
                idx++;
            }

            return null;
        }

        public void GeneratePushSql(SqlBatchTargetConnection connection, StringBuilder sql)
        {
            var tempTable = DbHelpers.GetTempTableName();
            var dialect = connection.ActiveConnection.SqlDialect;
            var tableMetadata = connection.ActiveConnection.DbConnectionName.Database.GetCachedTable(Accessor.TableName);
            var tblInfo = TableInfo.Create(sourceList, Accessor, tableMetadata, propertyProvider.GetProperties(), true);
            connection.ExecuteNonQuery(CommandType.Text, tblInfo.CreateTableScript(tempTable, true));
            var dt = new DataTable(tempTable);
            foreach (var col in tblInfo.Columns)
                dt.Columns.Add(col.Property.ColumnName, col.Property.UnderlyingType);
            // fazladan bir de indeks kolonu ekliyoruz
            // eklediğimiz sırada bize dönsün diye
            // bu sıralama ile örneklerin ID değerlerini güncelleyeceğiz
            dt.Columns.Add("IDX", typeof(int));
            foreach (var row in tblInfo.Rows)
                dt.Rows.Add(row);
            connection.BulkInsertDataTable(dt, SqlDialectBulkCopyOptions.Default);

            foreach (var rel in Accessor.Relationships)
            {
                // buradaki toplanan alanları eşleme tablosuna ekleyeceğiz modeli güncellemek için gerekiyor.
                parentColumns.Add(rel.ChildProperty.ColumnName);
                if (rel == Accessor.SelfReferencingTypeRelationship)
                {
                    selfReferencingColumn = rel.ChildProperty.ColumnName;
                    continue;
                }
                // yeni insert ettiğimiz temp tablonun alanlarını güncelle
                // Not: bir üst tablo @tmpTabloAdı şeklinde önceden bir geçici tablo 
                // oluşturmuş oluyor
                sql.Append($@"
-- Mantıksal indekslerin yerine {rel.ParentType.TableName} tablosuna eklenmiş Id değerlerine güncelle 

UPDATE tgt SET {rel.ChildProperty.ColumnName} = parTable.{rel.ParentKeyProperty.ColumnName} FROM {tempTable} tgt 
  INNER JOIN @tmp{rel.ParentType.TableName} parTable ON tgt.{rel.ChildProperty.ColumnName} = parTable.IDX;");
            }
            var idColumn = tableMetadata.FindColumn(Accessor.IdentityProperty.ColumnNameHash);
            sql.AppendFormat("\r\n-- {0} için bir eşleme tablosu oluştur\r\n\r\nDECLARE @tmp{0} TABLE ( IDX int, {1}",
                Accessor.TableName, dialect.GetColumnDefinition(idColumn));
            foreach (var column in parentColumns)
            {
                var pCol = tblInfo.FindColumnByName(column);
                if (pCol == null)
                    throw new InvalidOperationException($"Parent column '{column}' could not be found");
                sql.Append(", ").Append(dialect.GetColumnDefinition(pCol.Schema));
            }
            sql.Append(" );\r\n");
            var insertColumns = string.Join(",", tblInfo.Columns.Select(_ => string.Concat("[", _.Schema.Name, "]")));
            var valuesStmts = string.Join(",", tblInfo.Columns.Select(GetInsertValueStatement));
            var outputStmts = string.Join(",", parentColumns.Select(_ => string.Concat("temp.[", _, "]")));
            var outputColumns = string.Join(",", parentColumns.Select(_ => string.Concat("[", _, "]")));
            sql.AppendFormat(@"
-- Temp tablodaki verileri asıl {0} tablosuna aktar.

MERGE INTO {0} USING {1} AS temp ON 1 = 0
WHEN NOT MATCHED THEN
    INSERT ({2})
    VALUES ({3}) ", Accessor.TableName, tempTable, insertColumns, valuesStmts);
            sql.AppendFormat("OUTPUT temp.IDX, inserted.{1}{2} INTO @tmp{0} (IDX, {1}{3});",
                Accessor.TableName, idColumn.Name, outputStmts.PrependIfNotEmpty(", "), outputColumns.PrependIfNotEmpty(", "));
            if (Accessor.SelfReferencingTypeRelationship != null)
            {
                sql.AppendFormat(@"
-- Üst Id değerlerini eşleme tablosundan bakarak güncelle

UPDATE tgt SET {2} = src.{1} FROM {0} tgt
	INNER JOIN @tmp{0} lnk ON tgt.{1} = lnk.{1}
	INNER JOIN @tmp{0} src ON lnk.{2} = src.IDX;
", Accessor.TableName, idColumn.Name, selfReferencingColumn);
            }
            sql.AppendFormat("\r\nDROP TABLE [{0}];\r\n", tempTable);
        }

        public void GeneratePullSql(StringBuilder sql)
        {
            if (Accessor.SelfReferencingTypeRelationship != null)
            {
                var columns = string.Join(",", parentColumns.Select(_ => string.Concat("src.[", _, "]")));
                sql.AppendFormat(@"SELECT tgt.{0},{2} FROM @tmp{1} tgt
    INNER JOIN {1} src ON tgt.ID = src.ID ORDER BY IDX;",
                    Accessor.IdentityProperty.ColumnName, Accessor.TableName, columns);
            }
            else
            {
                var otherColumns = string.Join(",", parentColumns.Select(_ => string.Concat("[", _, "]")));
                sql.AppendFormat("SELECT {0}{2} FROM @tmp{1} ORDER BY IDX;\r\n",
                    Accessor.IdentityProperty.ColumnName, Accessor.TableName, otherColumns.PrependIfNotEmpty(","));
            }
        }

        string GetInsertValueStatement(ColumnInfo column)
        {
            var columnName = column.Schema.Name;
            // kendini referans eden kolon ise bu alanın değeri NULL olarak geçmeli ki foreign key hatasına neden olmasın.
            return string.Equals(columnName, selfReferencingColumn, StringComparison.OrdinalIgnoreCase)
                ? "NULL"
                : string.Concat("temp.[", columnName, "]");
        }

        public void ProcessResult(DbDataReader reader)
        {
            var i = 0;
            var idSetter = Accessor.IdentityProperty.Setter;
            var otherSetters = new List<IPropertySetter>(parentColumns.Count);
            Accessor.GetPropertySettersFromNames(otherSetters, parentColumns);

            while (reader.Read())
            {
                var item = sourceList[i++];
                idSetter.SetValue(item, reader.GetValue(0));
                for (var j = 1; j <= parentColumns.Count; j++)
                    otherSetters[j - 1].SetValue(item, reader.GetValue(j));
            }
        }
    }
}