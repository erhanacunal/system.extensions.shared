﻿#if !MOBILE
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Utils;
using JetBrains.Annotations;

namespace System.Custom.Services
{
    /// <summary>
    /// Başlatıldığında her 1 saniyede kendisinde kayıtlı olan süresi gelmiş görevleri çalıştırmayı dener.
    /// </summary>
    public static class MyTaskScheduler
    {

        static readonly ConcurrentDictionary<uint, TaskInfo> Queue = new ConcurrentDictionary<uint, TaskInfo>();
        static readonly object SyncRoot = new object();
        static bool started;
        static Thread schedulerThread;
        static bool running;
        static MyTaskScheduler()
        {


        }
        /// <summary>
        /// Görev zamanlayıcısını başlatır.
        /// </summary>
        public static void Start()
        {
            if (started) return;
            running = true;
            schedulerThread = new Thread(Run);
            schedulerThread.Start(null);
            started = true;
        }
        /// <summary>
        /// Görev zamanlayıcısını durdurur.
        /// </summary>
        public static void Stop()
        {
            if (!started) return;
            running = false;
            schedulerThread = null;
            started = false;
        }
        /// <summary>
        /// Her 250 saniye de bir çağrılan yöntem
        /// </summary>
        /// <param name="state"></param>
        static async void Run(object state)
        {
            while (running)
            {
                Thread.Sleep(250);
                TaskInfo[] snapshot;
                lock (SyncRoot)
                    snapshot = Queue.Values.ToArray();
                foreach (var taskInfo in snapshot)
                {
                    if (taskInfo.Status == TaskStatus.Suspended) continue;
                    taskInfo.CurrentTick++;
                    if (taskInfo.CurrentTick < taskInfo.MaxTicks) continue;
                    await taskInfo.Execute();
                    if (taskInfo.SingleShot && !taskInfo.Recovering)
                        Queue.TryRemove(taskInfo.Id, out var _);
                }

            }
        }

        /// <summary>
        /// Verilen görev bilgilerini kaydeder
        /// </summary>
        /// <param name="id">Uygulama içindeki benzersiz numara</param>
        /// <param name="act">Çalışacak eylem</param>
        /// <param name="arg">Çalışacak eylem için gerekli parametre</param>
        /// <param name="intervalInTicks">Kaç tick'de bir çalışacağını ayarlar. (1 Tick = 250ms)</param>
        /// <param name="options"></param>
        public static void Register(uint id, [NotNull] Func<object, Task> act, object arg, int intervalInTicks, MyTaskOptions options = null)
        {
            if (act == null) throw new ArgumentNullException(nameof(act));
            Queue[id] = new TaskInfo(id, act, arg, intervalInTicks, options);
        }

        public static void Register(string id, [NotNull] Func<object, Task> act, object arg, int intervalInTicks, MyTaskOptions options = null)
        {
            if (act == null) throw new ArgumentNullException(nameof(act));
            var hash = HashUtils.JenkinsHash(id);
            Register(hash, act, arg, intervalInTicks, options);
        }

        /// <summary>
        /// Verilen grup ismine uyan tüm görevleri durdurur
        /// </summary>
        /// <param name="groupName">Durdurulacak grup adı</param>
        public static void SuspendGroup(string groupName)
        {
            foreach (var taskInfo in Queue.Values)
                if (string.Equals(taskInfo.GroupName, groupName, StringComparison.OrdinalIgnoreCase))
                    taskInfo.Status = TaskStatus.Suspended;
        }

        /// <summary>
        /// Verilen grup ismine uyan tüm görevleri başlatır
        /// </summary>
        /// <param name="groupName">Başlatır grup adı</param>
        public static void ResumeGroup(string groupName)
        {
            foreach (var taskInfo in Queue.Values)
                if (string.Equals(taskInfo.GroupName, groupName, StringComparison.OrdinalIgnoreCase))
                    taskInfo.Status = TaskStatus.Running;
        }

        public static void Suspend(uint id)
        {
            if (Queue.TryGetValue(id, out var taskInfo))
                taskInfo.Status = TaskStatus.Suspended;
        }

        public static void Resume(uint id)
        {
            if (Queue.TryGetValue(id, out var taskInfo))
                taskInfo.Status = TaskStatus.Running;
        }

        public static bool Reset(uint id)
        {
            if (!Queue.TryGetValue(id, out var taskInfo)) return false;
            taskInfo.CurrentTick = 0;
            return true;
        }

        sealed class TaskInfo
        {
            public uint Id { get; }
            public string GroupName { get; }
            public int CurrentTick { get; set; }
            public int MaxTicks { get; }
            public TaskStatus Status { get; set; }
            public bool SingleShot { get; }
            readonly Func<object, Task> taskAction;
            readonly object argument;

            internal bool Recovering { get; private set; }
            readonly bool recoverIfFails;
            readonly int maxRecoveryCount;
            int recoverCount;

            public TaskInfo(uint id, [NotNull] Func<object, Task> act, object arg, int maxTicks, MyTaskOptions options)
            {
                Id = id;
                GroupName = options?.GroupName;
                taskAction = act ?? throw new ArgumentNullException(nameof(act));
                argument = arg;
                SingleShot = options?.SingleShot ?? false;
                if (options != null)
                    Status = options.Suspended ? TaskStatus.Suspended : TaskStatus.Running;
                else Status = TaskStatus.Running;
                CurrentTick = 0;
                MaxTicks = maxTicks;
                if (!SingleShot || options == null) return;
                recoverIfFails = options.RecoverIfFails;
                maxRecoveryCount = options.MaximumRecoveryCount;
            }

            internal async Task Execute()
            {
                CurrentTick = 0;
                try
                {
                    await taskAction(argument);
                }
                catch (Exception)
                {
                    Recover();
                }
            }

            void Recover()
            {
                if (!recoverIfFails) return;
                if (Recovering)
                {
                    recoverCount++;
                    // -1 infinity
                    if (maxRecoveryCount == -1) return;
                    if (recoverCount <= maxRecoveryCount) return;
                    // maximum recovery count is reached
                    // stop recovering
                    Recovering = false;
                    recoverCount = 0;
                }
                else
                    Recovering = true;
            }
        }

        enum TaskStatus
        {
            Running,
            Suspended
        }
    }

    public sealed class MyTaskOptions
    {
        public bool RecoverIfFails { get; set; }
        public int MaximumRecoveryCount { get; set; }
        public string GroupName { get; set; }
        public bool SingleShot { get; set; }
        public bool Suspended { get; set; }
        public static MyTaskOptions Create()
        {
            return new MyTaskOptions { MaximumRecoveryCount = 3, RecoverIfFails = false };
        }

        public MyTaskOptions SetRecoveryOptions(bool recover, int maxRecoveryCount)
        {
            RecoverIfFails = recover;
            MaximumRecoveryCount = maxRecoveryCount;
            return this;
        }

        public MyTaskOptions SetGroupName(string groupName)
        {
            GroupName = groupName;
            return this;
        }

        public MyTaskOptions SetSingleShot()
        {
            SingleShot = true;
            return this;
        }
        public MyTaskOptions SetSuspended()
        {
            Suspended = true;
            return this;
        }
    }
}

#endif