using System;
using System.Collections.Generic;
using System.Linq;

namespace System.Text
{
    sealed class ChangedSourceText : SourceText
    {
        public ChangedSourceText(SourceText oldText, SourceText newText, IEnumerable<TextChange> changes)
            : base(newText.Container)
        {
            if (changes == null)
                throw new ArgumentNullException(nameof(changes));

            OldText = oldText ?? throw new ArgumentNullException(nameof(oldText));
            NewText = newText ?? throw new ArgumentNullException(nameof(newText));
            Changes = changes.ToArray();
        }

        public SourceText OldText { get; }

        public SourceText NewText { get; }

        public TextChange[] Changes { get; }

        public override int GetLineNumberFromPosition(int position)
        {
            return NewText.GetLineNumberFromPosition(position);
        }

        public override string GetText(TextSpan textSpan)
        {
            return NewText.GetText(textSpan);
        }

        public override char this[int index] => NewText[index];

        public override int Length => NewText.Length;

        public override TextLineCollection Lines => NewText.Lines;
    }
}