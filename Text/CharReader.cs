﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.Parsers;
using System.Utils;

namespace System.Text
{
    public class CharReader
    {
        public const char INVALID_CHARACTER = char.MaxValue;

        Stack<int> stateStack;

        int position;
        string source;
        int lineStart;
        int line;

        public int Line => line;

        public int Column => (position - lineStart) + 1;

        public int Position => position;

        public bool Eof => position >= source.Length;

        public CharReader(string source, int startIndex = 0)
        {
            this.source = source;
            position = startIndex;
            line = 1;
        }

        public void ResetSource(string src, int startIndex = 0)
        {
            source = src;
            position = startIndex;
            line = 1;
            OnSourceChanged();
        }

        protected virtual void OnSourceChanged()
        {

        }

        public void Seek(int newPosition)
        {
            if (newPosition < 0 || newPosition >= source.Length)
                throw new ArgumentOutOfRangeException(nameof(newPosition));
            position = newPosition;
        }

        public char PeekChar()
        {
            if (source.Length > 0 && position < source.Length)
                return source[position];
            // hayır : Escape(27) karakterini döndür
            return INVALID_CHARACTER;
        }
        public char PeekChar(int delta)
        {
            var newPos = position + delta;
            if (newPos >= 0 && newPos < source.Length)
                return source[newPos];
            return INVALID_CHARACTER;
        }
        /// <summary>
        /// Returns current char and advances position
        /// </summary>
        /// <returns></returns>
        public char NextChar()
        {
            var ch = PeekChar();
            if (position < source.Length)
            {
                AdvanceChar();
                if (ch == '\r' && PeekChar() != '\n') ch = '\n';
                if (ch == '\n') { line++; lineStart = position; }
                return ch;
            }
            return INVALID_CHARACTER;
        }

        public string Take(int count)
        {
            var sb = new StringBuilder(count);
            for (var i = 0; i < count; i++)
            {
                var ch = NextChar();
                if (ch != INVALID_CHARACTER)
                    sb.Append(ch);
                else
                    break;
            }
            return sb.ToString();
        }

        public string TakeUntil(char ch, params char[] throwIfEncounter)
        {
            var remaining = source.Length - position;
            var sb = new StringBuilder(10);
            for (var i = 0; i < remaining; i++)
            {
                var cur = NextChar();
                if (Array.IndexOf(throwIfEncounter, cur) != -1)
                    throw new GenericLexerException("TakeUntil encountered invalid char -> " + ch);
                if (cur == ch)
                    break;
                sb.Append(cur);
            }
            return sb.ToString();
        }
        // 1234567890
        // 10 - 3 = 7
        public bool HasChar(char ch)
        {
            var remaining = source.Length - (position + 1);
            for (var i = 0; i < remaining; i++)
            {
                if (PeekChar(i) == ch)
                    return true;
            }
            return false;
        }

        public bool MatchString(string str)
        {
            if (string.IsNullOrEmpty(str)) throw new ArgumentException("Value cannot be null or empty.", nameof(str));
            for (var i = 0; i < str.Length; i++)
                if (PeekChar(i) != str[i])
                    return false;
            return true;
        }

        public bool MatchStringAndSkip(string str)
        {
            if (MatchString(str))
            {
                AdvanceChar(str.Length);
                return true;
            }
            return false;
        }

        public void AdvanceChar()
        {
            position++;
        }

        public void AdvanceChar(int n)
        {
            position += n;
        }

        public void SkipWhiteSpace()
        {
            while (char.IsWhiteSpace(PeekChar()))
                NextChar();
        }

        public void SkipToNextLine()
        {
            while (true)
            {
                var ch = PeekChar();
                if (ch == '\r')
                {
                    NextChar();
                    if (PeekChar() == '\n')
                        NextChar();
                    break;
                }
                if (ch == '\n' || ch == INVALID_CHARACTER)
                    break;
                NextChar();
            }
        }

        public bool TryFindChar(char ch, int maxAdvance, bool forward, out int index)
        {
            var direction = forward ? 1 : -1;
            for (var i = 1; i <= maxAdvance; i++)
            {
                var offset = i * direction;
                if (PeekChar(offset) != ch)
                    continue;
                index = position + (offset);
                return true;
            }
            index = -1;
            return false;
        }
        /// <summary>
        /// Finds given string in source and goes to found position, if not, does nothing
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool TrySeekTo(string s)
        {
            var idx = source.IndexOf(s, position, StringComparison.Ordinal);
            if (idx == -1)
                return false;
            position = idx;
            return true;
        }

        /// <summary>
        /// Searches given string in source from given position + offset, if encounters a char in <paramref name="failIfEncounter"/> parameter, returns -1
        /// </summary>
        /// <param name="s">String to search</param>
        /// <param name="offset">Offset from current position</param>
        /// <param name="idx"></param>
        /// <param name="failIfEncounter">Search stopper chars</param>
        /// <param name="found"></param>
        /// <returns>If founds returns start position of string, else returns -1</returns>
        public void IndexOf(string s, int offset, out bool found, out int idx, params char[] failIfEncounter)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentNullException(nameof(s));
            var chIdx = 0;
            found = false;
            idx = -1;
            for (var i = position + offset; i < source.Length; i++)
            {
                var dstCh = source[i];
                if (Array.IndexOf(failIfEncounter, dstCh) != -1)
                {
                    idx = i;
                    break;
                }
                var srcCh = s[chIdx];
                if (srcCh == dstCh)
                {
                    if (++chIdx >= s.Length)
                    {
                        found = true;
                        idx = i - (chIdx - 1);
                        break;
                    }
                    continue;
                }
                chIdx = 0;
            }
        }

        public string TakeWhile(Func<char, bool> predict)
        {
            var sb = new StringBuilder();
            while (predict(PeekChar()))
                sb.Append(NextChar());
            return sb.ToString();
        }

        public string GetUntilWhitespace()
        {
            var sb = new StringBuilder();
            while (!StringUtils.IsWhitespace(PeekChar()))
            {
                sb.Append(NextChar());
            }
            return sb.ToString();
        }

        public TokenLocation GetTextLocation()
        {
            return new TokenLocation(Line, Column, position);
        }

        public void PushState()
        {
            stateStack = stateStack ?? new Stack<int>();
            stateStack.Push(position);
            stateStack.Push(line);
            stateStack.Push(lineStart);
        }

        public void PopState()
        {
            if (stateStack == null || stateStack.Count == 0)
                return;
            lineStart = stateStack.Pop();
            line = stateStack.Pop();
            position = stateStack.Pop();
            if (stateStack.Count == 0)
                stateStack = null;

        }

        protected void DropState()
        {
            if (stateStack == null || stateStack.Count == 0)
                return;
            stateStack.Pop(); // line start 
            stateStack.Pop(); // line
            stateStack.Pop(); // position
            if (stateStack.Count == 0)
                stateStack = null;
        }
    }

    [DebuggerDisplay("{Line}:{Column}")]
    public struct TokenLocation : IComparable<TokenLocation>, IEquatable<TokenLocation>
    {
        /// <summary>
        /// Gets the line number.
        /// </summary>
        public int Line { get; }

        /// <summary>
        /// Gets the column number.
        /// </summary>
        public int Column { get; }

        public int Offset { get; }

        public bool IsEmpty => Column <= 0 && Line <= 0;

        public TokenLocation(int line, int column, int offset)
            : this()
        {
            Line = line;
            Column = column;
            Offset = offset;
        }

        public override string ToString()
        {
            return $"{Line}:{Column}";
        }


        /// <summary>
        /// Gets a hash code.
        /// </summary>
        public override int GetHashCode()
        {
            return unchecked(191 * Column.GetHashCode() ^ Line.GetHashCode());
        }

        /// <summary>
        /// Equality test.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is TokenLocation)) return false;
            return (TokenLocation)obj == this;
        }

        /// <summary>
        /// Equality test.
        /// </summary>
        public bool Equals(TokenLocation other)
        {
            return this == other;
        }

        /// <summary>
        /// Equality test.
        /// </summary>
        public static bool operator ==(TokenLocation left, TokenLocation right)
        {
            return left.Column == right.Column && left.Line == right.Line;
        }

        /// <summary>
        /// Inequality test.
        /// </summary>
        public static bool operator !=(TokenLocation left, TokenLocation right)
        {
            return left.Column != right.Column || left.Line != right.Line;
        }

        /// <summary>
        /// Compares two text locations.
        /// </summary>
        public static bool operator <(TokenLocation left, TokenLocation right)
        {
            if (left.Line < right.Line)
                return true;
            if (left.Line == right.Line)
                return left.Column < right.Column;
            return false;
        }

        /// <summary>
        /// Compares two text locations.
        /// </summary>
        public static bool operator >(TokenLocation left, TokenLocation right)
        {
            if (left.Line > right.Line)
                return true;
            if (left.Line == right.Line)
                return left.Column > right.Column;
            return false;
        }

        /// <summary>
        /// Compares two text locations.
        /// </summary>
        public static bool operator <=(TokenLocation left, TokenLocation right)
        {
            return !(left > right);
        }

        /// <summary>
        /// Compares two text locations.
        /// </summary>
        public static bool operator >=(TokenLocation left, TokenLocation right)
        {
            return !(left < right);
        }

        /// <summary>
        /// Compares two text locations.
        /// </summary>
        public int CompareTo(TokenLocation other)
        {
            if (this == other)
                return 0;
            if (this < other)
                return -1;
            return 1;
        }

        public TokenLocation Add(int line, int column, int offset)
        {
            return new TokenLocation(Line + line, Column + column, Offset + offset);
        }
    }
}
