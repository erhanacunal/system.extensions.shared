﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Text
{
    public class LineBreakCounter
    {
        readonly List<int> lineBreaks = new List<int>();
        readonly int length;

        public LineBreakCounter(string text)
        {
            length = text?.Length ?? throw new ArgumentNullException(nameof(text));
            for (var i = 0; i < text.Length; i++)
            {
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (text[i])
                {
                    case '\n':
                        lineBreaks.Add(i);
                        break;
                    case '\r' when i < text.Length - 1 && text[i + 1] == '\n':
                        lineBreaks.Add(++i);
                        break;
                }
            }
        }
        /// <summary>
        /// Verilen yerin bulunduğu satırı 0 tabanlı olarak döndürür
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public int GetLine(int offset)
        {
            if (offset < 0 || offset > length)
                throw new ArgumentOutOfRangeException(nameof(offset));
            var result = lineBreaks.BinarySearch(offset);
            return result < 0 ? ~result : result;
        }

        public int Lines => lineBreaks.Count + 1;
        /// <summary>
        /// Verilen satırın yer olarak başlangıcını verir
        /// </summary>
        /// <param name="line">0 tabanlı satır numarası</param>
        /// <returns></returns>
        public int GetOffset(int line)
        {
            if (line < 0 || line >= Lines)
                throw new ArgumentOutOfRangeException(nameof(line));

            if (line == 0)
                return 0;

            return lineBreaks[line - 1] + 1;
        }
    }
}
