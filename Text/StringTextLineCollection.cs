using System;
using System.Collections.Generic;

namespace System.Text
{
    sealed class StringTextLineCollection : TextLineCollection
    {
        readonly IReadOnlyList<TextLine> _lines;

        public StringTextLineCollection(IReadOnlyList<TextLine> lines)
        {
            _lines = lines ?? throw new ArgumentNullException(nameof(lines));
        }

        public override IEnumerator<TextLine> GetEnumerator()
        {
            return _lines.GetEnumerator();
        }

        public override int Count => _lines.Count;

        public override TextLine this[int index] => _lines[index];
    }
}