﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Parsers;
using System.Text.Parsers.Expressions;
using System.Utils;

namespace System.Text
{
    public static class Preprocessor
    {

        public static void Preprocess(Stream source, Stream destination, CtSimpleEvaluationContext ctx)
        {
            using (var reader = new StreamReader(source))
            using (var writer = new StreamWriter(destination))
                Preprocess(reader, writer, ctx);
        }

        public static string Preprocess(string source, CtSimpleEvaluationContext ctx)
        {
            using (var reader = new StringReader(source))
            using (var writer = new StringWriter())
            {
                Preprocess(reader, writer, ctx);
                return writer.ToString();
            }
        }

        public static string Preprocess(Stream source, CtSimpleEvaluationContext ctx)
        {
            using (var reader = new StreamReader(source))
            using (var writer = new StringWriter())
            {
                Preprocess(reader, writer, ctx);
                return writer.ToString();
            }
        }

#if !MOBILE
        public static void CompileToFile(string source, string fileName)
        {
            var nodes = new List<IPreprocessNode>();
            using (var reader = new StringReader(source))
            using (var fs = File.Create(fileName))
            using (var writer = new BinaryWriter(fs))
            {
                Parse(nodes, reader);
                writer.Write(nodes.Count);
                foreach (var node in nodes)
                {
                    node.Write(writer);
                }
            }
        } 

        public static void PreprocessFromCompiled(string fileName, StringBuilder targetBuilder, CtSimpleEvaluationContext ctx)
        {
            using (var textWriter = new StringWriter(targetBuilder))
            using (var fs = File.OpenRead(fileName))
            using (var reader = new BinaryReader(fs))
            {
                var count = reader.ReadInt32();
                for (var i = 0; i < count; i++)
                {
                    var node = PreprocessNode.Create(reader);
                    node.Process(textWriter, ctx);
                }
            }
        }
#endif

        public static void CompileToList(string source, IList<IPreprocessNode> nodes)
        {
            using (var reader = new StringReader(source))
            {
                Parse(nodes, reader);
            }
        }
        
        static void Parse(IList<IPreprocessNode> nodes, TextReader reader)
        {
            var ifStack = new Stack<IfNode>();
            var listStack = new Stack<IList<IPreprocessNode>>();
            listStack.Push(nodes);
            var currentList = nodes;
            while (true)
            {
                var line = reader.ReadLine();
                if (line == null)
                    break;
                if (!line.StartsWith("#"))
                {
                    AddToCurrentList(currentList, line);
                    continue;
                }
                var remaining = line.Substring(1);
                var directiveName = remaining.ReadIdentifier();
                if (directiveName.Length == 0)
                    continue;
                string trimmed;
                IfNode node;
                switch (directiveName)
                {
                    case "if":
                        node = ParseIfDirective(remaining, 2);
                        ifStack.Push(node);
                        currentList.Add(node);
                        currentList = node.IfTrueNodes;
                        listStack.Push(currentList);
                        break;
                    case "else":
                        if (ifStack.Count == 0)
                            throw new InvalidOperationException("There is no 'if' directive defined before 'endif'");
                        var currentIf = ifStack.Peek();
                        // else olabilmesi için yığındaki if'in IfTrueNodes property'sini göstermesi gerekiyor list yığındaki listenin
                        if (currentIf.IfTrueNodes.Equals(listStack.Pop()))
                        {
                            currentList = currentIf.IfFalseNodes;
                            listStack.Push(currentList);
                        }
                        else
                            throw new InvalidOperationException("'else' of current if is already used!");
                        break;
                    case "endif":
                        if (ifStack.Count == 0)
                            throw new InvalidOperationException("There is no 'if' directive defined before 'endif'");
                        listStack.Pop(); // önceki if'in listesini bırak 
                        currentList = listStack.Peek();
                        ifStack.Pop(); // önceki if'i de bırak                      
                        break;
                    case "elif":
                        // öncekini endif gibi algılayıp yeni bir if başlatılacak
                        if (ifStack.Count == 0)
                            throw new InvalidOperationException("There is no 'if' directive defined before 'endif'");
                        listStack.Pop(); // önceki if'in listesini bırak                        
                                         // bir önceki if'i bırak
                        ifStack.Pop();
                        // yeni bir if başlat

                        currentList = listStack.Peek();

                        node = ParseIfDirective(remaining, 4);
                        ifStack.Push(node);
                        // şu anki listeye bu 'if' ekle
                        currentList.Add(node);
                        currentList = node.IfTrueNodes;
                        listStack.Push(currentList);
                        break;
                    case "define":
                        trimmed = remaining.Substring(6).Trim();
                        currentList
                            .Add(StringUtils.Instance.TryGetNameValue(trimmed, out var name, out var val)
                                ? new DefineSymbolNode(name, TypeUtils.ParseValue(val))
                                : new DefineSymbolNode(name, true));
                        break;
                    case "undef":
                        // sembol tanımını sil
                        trimmed = remaining.Substring(5).Trim();
                        currentList.Add(new UndefineSymbolNode(trimmed));
                        break;
                    default:
                        AddToCurrentList(currentList, line);
                        break;
                }
            }
        }

        private static void AddToCurrentList(IList<IPreprocessNode> currentList, string line)
        {
            var lastContent = currentList.Count > 0
                ? currentList[currentList.Count - 1] as ContentNode
                : null;
            if (lastContent == null)
                currentList.Add(new ContentNode(line));
            else
            {
                lastContent.Content.AppendLine().Append(line);
            }
        }

        public static void Preprocess(TextReader reader, TextWriter writer, CtSimpleEvaluationContext ctx)
        {
            var nodes = new List<IPreprocessNode>();
            Parse(nodes, reader);
            foreach (var node in nodes)
            {
                node.Process(writer, ctx);
            }
        }

        static IfNode ParseIfDirective(string source, int startIndex) => new IfNode
        {
            Expression = source.Substring(startIndex),
            Condition = CtExpressionUtils.ParseLogicalExpression(source, startIndex)
        };

        const int NODE_TYPE_IF = 1;
        const int NODE_TYPE_CONTENT = 2;
        const int NODE_TYPE_DEFINE = 3;
        const int NODE_TYPE_UNDEF = 4;

        #region Nested Classes

        abstract class PreprocessNode : IPreprocessNode
        {

            public abstract void Process(TextWriter sb, CtSimpleEvaluationContext ctx);
            public abstract void Write(BinaryWriter writer);
            public abstract void Read(BinaryReader reader);

            protected void WriteNodes(BinaryWriter writer, ICollection<IPreprocessNode> nodes)
            {
                writer.Write(nodes.Count);
                foreach (var node in nodes)
                {
                    node.Write(writer);
                }
            }

            protected void ReadNodes(BinaryReader reader, ICollection<IPreprocessNode> nodes)
            {
                var count = reader.ReadInt32();
                for (var i = 0; i < count; i++)
                {
                    nodes.Add(Create(reader));
                }
            }

            internal static PreprocessNode Create(BinaryReader reader)
            {
                var type = reader.ReadInt32();
                PreprocessNode result = null;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (type)
                {
                    case NODE_TYPE_IF:
                        result = new IfNode();
                        result.Read(reader);
                        break;
                    case NODE_TYPE_CONTENT:
                        result = new ContentNode();
                        result.Read(reader);
                        break;
                    case NODE_TYPE_DEFINE:
                        result = new DefineSymbolNode();
                        result.Read(reader);
                        break;
                    case NODE_TYPE_UNDEF:
                        result = new UndefineSymbolNode();
                        result.Read(reader);
                        break;
                }
                return result;

            }

        }


        sealed class IfNode : PreprocessNode
        {
            internal string Expression;
            public CtExpressionSyntax Condition { private get; set; }
            public List<IPreprocessNode> IfTrueNodes { get; } = new List<IPreprocessNode>();
            public List<IPreprocessNode> IfFalseNodes { get; } = new List<IPreprocessNode>();

            public override void Process(TextWriter sb, CtSimpleEvaluationContext ctx)
            {
                var nodes = CtExpressionUtils.EvaluateBool(Condition, ctx) ? IfTrueNodes : IfFalseNodes;
                foreach (var node in nodes)
                {
                    node.Process(sb, ctx);
                }
            }

            public override void Write(BinaryWriter writer)
            {
                writer.Write(NODE_TYPE_IF);
                writer.Write(Expression);
                WriteNodes(writer, IfTrueNodes);
                WriteNodes(writer, IfFalseNodes);
            }
            public override void Read(BinaryReader reader)
            {
                Expression = reader.ReadString();
                Condition = CtExpressionUtils.ParseLogicalExpression(Expression);
                ReadNodes(reader, IfTrueNodes);
                ReadNodes(reader, IfFalseNodes);
            }

            public override string ToString()
            {
                return $"if {Condition}";
            }

        }

        sealed class ContentNode : PreprocessNode
        {
            internal StringBuilder Content { get; private set; }

            public ContentNode()
            {

            }

            public ContentNode(string content)
            {
                Content = new StringBuilder(content);
            }

            public override void Process(TextWriter sb, CtSimpleEvaluationContext ctx)
            {
                sb.WriteLine(Content);
            }

            public override string ToString()
            {
                return Content.ToString();
            }

            public override void Write(BinaryWriter writer)
            {
                writer.Write(NODE_TYPE_CONTENT);
                writer.Write(Content.ToString());
            }

            public override void Read(BinaryReader reader)
            {
                Content = new StringBuilder(reader.ReadString());
            }
        }

        sealed class DefineSymbolNode : PreprocessNode
        {
            string symbolName;
            object value;

            public DefineSymbolNode()
            {

            }
            public DefineSymbolNode(string symbol, object value = null)
            {
                symbolName = symbol;
                this.value = value;
            }

            public override void Process(TextWriter sb, CtSimpleEvaluationContext ctx)
            {
                ctx.Symbols[symbolName] = value ?? true;
            }

            public override string ToString()
            {
                return $"define {symbolName}";
            }

            public override void Write(BinaryWriter writer)
            {
                writer.Write(NODE_TYPE_DEFINE);
                writer.Write(symbolName);
                writer.WriteObjectEx(value);
            }

            public override void Read(BinaryReader reader)
            {
                symbolName = reader.ReadString();
                value = reader.ReadObjectEx();
            } 
        }

        sealed class UndefineSymbolNode : PreprocessNode
        {
            string symbolName;

            public UndefineSymbolNode()
            {

            }
            public UndefineSymbolNode(string symbol)
            {
                symbolName = symbol;
            }

            public override void Process(TextWriter sb, CtSimpleEvaluationContext ctx)
            {
                ctx.Symbols.Remove(symbolName);
            }

            public override string ToString()
            {
                return $"undef {symbolName}";
            }

            public override void Write(BinaryWriter writer)
            {
                writer.Write(NODE_TYPE_UNDEF);
                writer.Write(symbolName);
            }

            public override void Read(BinaryReader reader)
            {
                symbolName = reader.ReadString();
            }
        }


        #endregion
    }

    public interface IPreprocessNode
    {
        void Process(TextWriter sb, CtSimpleEvaluationContext ctx);
        void Write(BinaryWriter writer);
        void Read(BinaryReader reader);
    }
}
