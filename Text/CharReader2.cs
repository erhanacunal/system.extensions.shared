using System;

namespace System.Text
{
    public sealed class CharReader2
    {
        readonly SourceText _text;

        public CharReader2(SourceText text)
        {
            _text = text;
        }

        public void NextChar()
        {
            Position++;
        }

        public int Position { get; private set; }

        public char Current => Peek(0);

        public char Peek()
        {
            return Peek(1);
        }

        public char Peek(int offset)
        {
            var index = Position + offset;
            return index < _text.Length
                       ? _text[index]
                       : '\0';
        }

        public void Advance(int delta)
        {
            Position += delta;
        }
    }
}