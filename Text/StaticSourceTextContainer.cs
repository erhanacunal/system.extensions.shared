using System;

namespace System.Text
{
    class StaticSourceTextContainer : SourceTextContainer
    {
        public StaticSourceTextContainer(SourceText current)
        {
            Current = current ?? throw new ArgumentNullException(nameof(current));
        }

        public override SourceText Current { get; }

        public override event EventHandler<EventArgs> CurrentChanged
        {
            add {}
            remove {}
        }
    }
}