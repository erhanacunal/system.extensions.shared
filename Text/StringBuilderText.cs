﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Text
{
    public class StringBuilderText : SourceText
    {
        readonly StringBuilder _text;
        readonly TextLineCollection _lines;

        public StringBuilderText(StringBuilder text)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
            _lines = Parse(this, text);
        }

        static StringTextLineCollection Parse(SourceText sourceText, StringBuilder text)
        {
            var textLines = new List<TextLine>();
            var position = 0;
            var lineStart = 0;

            while (position < text.Length)
            {
                var lineBreakWidth = GetLineBreakWidth(text, position);

                if (lineBreakWidth == 0)
                {
                    position++;
                }
                else
                {
                    AddLine(sourceText, textLines, lineStart, position);

                    position += lineBreakWidth;
                    lineStart = position;
                }
            }

            if (lineStart <= position)
                AddLine(sourceText, textLines, lineStart, text.Length);

            return new StringTextLineCollection(textLines);
        }

        static void AddLine(SourceText sourceText, List<TextLine> textLines, int lineStart, int lineEnd)
        {
            var lineLength = lineEnd - lineStart;
            var textLine = new TextLine(sourceText, lineStart, lineLength);
            textLines.Add(textLine);
        }

        static int GetLineBreakWidth(StringBuilder text, int position)
        {
            const char EOF = '\0';
            const char CR = '\r';
            const char LF = '\n';

            var n = position + 1;
            var c = text[position];
            var l = n < text.Length ? text[n] : EOF;

            if (c == CR && l == LF)
                return 2;

            if (c == CR || c == LF)
                return 1;

            return 0;
        }

        public override int GetLineNumberFromPosition(int position)
        {
            if (position < 0 || position > Length)
                throw new ArgumentOutOfRangeException(nameof(position));

            var lower = 0;
            var upper = _lines.Count - 1;
            while (lower <= upper)
            {
                var index = lower + ((upper - lower) >> 1);
                var current = _lines[index];
                var start = current.Span.Start;
                if (start == position)
                    return index;

                if (start > position)
                    upper = index - 1;
                else
                    lower = index + 1;
            }

            return lower - 1;
        }

        public override string GetText(TextSpan textSpan)
        {
            return _text.ToString(textSpan.Start, textSpan.Length);
        }

        public override int Length => _text.Length;

        public override char this[int index] => _text[index];

        public override TextLineCollection Lines => _lines;
    }
}
