using System;

namespace System.Text
{
    public struct TextLine : IEquatable<TextLine>
    {
        readonly int _start;
        readonly int _length;

        public TextLine(SourceText text, int start, int length)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            _start = start;
            _length = length;
        }

        public SourceText Text { get; }

        public TextSpan Span => new TextSpan(_start, _length);

        public TextSpan SpanIncludingLineBreak
        {
            get
            {
                var nextLineIndex = LineNumber + 1;
                var nextLine = nextLineIndex < Text.Lines.Count
                                ? (TextLine?) Text.Lines[nextLineIndex]
                                : null;
                var start = Span.Start;
                var end = Span.End;
                var nextStart = nextLine?.Span.Start ?? end;
                return TextSpan.FromBounds(start, nextStart);
            }
        }

        public int LineBreakLength => SpanIncludingLineBreak.Length - Span.Length;

        public int LineNumber => Text.GetLineNumberFromPosition(_start);

        public string GetText()
        {
            return Text.GetText(_start, _length);
        }

        public bool Equals(TextLine other)
        {
            return Text == other.Text &&
                   _start == other._start &&
                   _length == other._length;
        }

        public override bool Equals(object obj)
        {
            return obj is TextLine other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Text != null ? Text.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ _start;
                hashCode = (hashCode*397) ^ _length;
                return hashCode;
            }
        }

        public static bool operator ==(TextLine left, TextLine right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TextLine left, TextLine right)
        {
            return !left.Equals(right);
        }
    }
}