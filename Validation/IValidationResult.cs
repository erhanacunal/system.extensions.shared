﻿#if !MOBILE
using System;
using System.Collections.Generic;

namespace System.Data.Validation
{
    public interface IValidationResult
    {

        object Entity { get; set; }
        ValidationResultType ResultType { get; }
        string PropertyName { get; }
        IValidationMessage Message { get; }
        string ExceptionDetails { get; }
        IValidationRule Rule { get; }

        object this[int id] { get; set; }
        /// <summary>
        /// Bu hata örneğine kullanıcı tanımlı bir veri ekler veya günceller.
        /// </summary>
        /// <param name="id">Veri tanımlayıcısı </param>
        /// <param name="value">Değer</param>
        /// <returns>Kendi örneğini döndürür</returns>
        IValidationResult AddData(int id, object value);
        bool TryGetData(int id, out object value);
        bool HasData(int id);
        IEnumerable<int> GetDataIds();

    }
} 
#endif