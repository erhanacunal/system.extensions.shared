﻿#if !MOBILE
using System;

namespace System.Data.Validation
{
    /// <summary>
    /// Koşul doğru ise hata verir ayrıca Condition'a parametre olarak IValidationContext örneği verir
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class FailIfTrueRule2<T> : ValidationRuleBase where T : class
    {

        public Func<IValidationContext, T, bool> Condition { get; set; }

        public FailIfTrueRule2(Func<IValidationContext, T, bool> cond, IValidationMessage msg)
            : base("Eğer Doğruysa Başarısız Ol Kuralı 2")
        {
            Message = msg;
            Condition = cond;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var obj = ctx.CurrentEntity as T;
            if (obj != null)
            {
                return AssertFalse(ctx, Condition(ctx, obj));
            }
            return true;
        }
    }
} 
#endif