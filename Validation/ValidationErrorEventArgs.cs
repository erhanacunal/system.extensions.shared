﻿#if !MOBILE
using System;

namespace System.Data.Validation
{
    public sealed class ValidationErrorEventArgs : EventArgs
    {

        public IValidationResult Error { get; private set; }

        public ValidationErrorEventArgs(IValidationResult err)
        {
            Error = err;
        }
    }
} 
#endif