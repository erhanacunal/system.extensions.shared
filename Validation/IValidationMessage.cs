﻿#if !MOBILE
namespace System.Data.Validation
{
    public interface IValidationMessage
    {
        string GetMessage();
    }
} 
#endif