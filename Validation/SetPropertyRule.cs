﻿#if !MOBILE
using System;
using System.Reflection;

namespace System.Data.Validation
{
    public class SetPropertyRule : ValidationRuleBase
    {
        readonly object value;

        public SetPropertyRule(string property, object value)
            : base("Property Ayarla Kuralı")
        {
            this.value = value;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            if (ctx.CurrentEntity == null)
                return true;
            var accessor = TypeAccessor.CreateOrGet(ctx.CurrentEntity.GetType());
            accessor[ctx.CurrentEntity, Property] = value;
            return true;
        }
    }
} 
#endif