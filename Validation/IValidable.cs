﻿#if !MOBILE
namespace System.Data.Validation
{
    public interface IValidable
    {
        bool IsValid(IValidationContext ctx);
    }
} 
#endif