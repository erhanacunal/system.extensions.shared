﻿#if !MOBILE
using System;
using System.Collections.Generic;

namespace System.Data.Validation
{
    public abstract class ValidationRuleBuilder
    {
        readonly Type type;
        protected List<IValidationRule> Rules;

        protected ValidationRuleBuilder(Type type)
        {
            this.type = type;
        }

        public static ValidationRuleBuilder<T> Create<T>() where T : class
        {
            return new ValidationRuleBuilder<T>(null);
        }

        public virtual void Build()
        {
            Validator.AddRulesFor(type, Rules);
        }

    }

    public class ValidationRuleBuilder<T> : ValidationRuleBuilder where T : class
    {
        readonly ValidationRuleBuilder<T> parent;
        public ValidationRuleBuilder(ValidationRuleBuilder<T> parent) : base(typeof(T))
        {
            this.parent = parent;
        }

        public virtual ValidationRuleBuilder<T> Add(IValidationRule rule)
        {
            if (Rules == null)
                Rules = new List<IValidationRule>();
            Rules.Add(rule);
            return this;
        }

        public ValidationRuleBuilder<T> Required(string property, IValidationMessage msg)
        {
            return Add(new RequiredRule(property, msg));
        }

        public ValidationRuleBuilder<T> MustBeLessThan(string property, double value, IValidationMessage msg)
        {
            return Add(new LessThanRule(value, msg)); ;
        }

        public ValidationRuleBuilder<T> MustBeLessThan<TValue>(string property, TValue value, IValidationMessage msg) where TValue : IComparable<TValue>
        {
            return Add(new LessThanRule<TValue>(property, value, msg)); ;
        }

        public ValidationRuleBuilder<T> MustBeGreaterThan(string property, double value, IValidationMessage msg)
        {
            return Add(new GreaterThanRule(property, value, msg)); ;
        }

        public ValidationRuleBuilder<T> MustBeGreaterThan<TValue>(string property, TValue value, IValidationMessage msg) where TValue : IComparable<TValue>
        {
            return Add(new GreaterThanRule<TValue>(property, value, msg));
        }

        public ValidationRuleBuilder<T> ThrowError(IValidationMessage msg)
        {
            return Add(new ThrowErrorRule(msg));
        }

        public ValidationRuleBuilder<T> ChildCheck(string property)
        {
            return Add(new ChildCheckRule(property));
        }

        public ValidationRuleBuilder<T> MaxLength(string property, int length, IValidationMessage msg)
        {
            return Add(new MaxLengthRule(property, length, msg));
        }

        public ValidationRuleBuilder<T> MinLength(string property, int length, IValidationMessage msg)
        {
            return Add(new MinLengthRule(property, length, msg));
        }

        public ValidationRuleBuilder<T> SetProperty(string property, object value)
        {
            return Add(new SetPropertyRule(property, value));
        }

#if JSON
        public ValidationRuleBuilder<T> JsonConvertAndValidate<TObject>(string property, bool required = false, IValidationMessage requiredMessage = null) where TObject : class
        {
            return Add(new JsonConvertAndValidateRule<TObject>(property, required, requiredMessage));
        }
#endif

        public ValidationRuleBuilder<T> ValidEmail(string property, IValidationMessage msg, bool isRequired)
        {
            return Add(new ValidEmailRule(property, isRequired, msg));
        }

        public ValidationRuleBuilder<T> ValidValues<TValue>(string property, IEnumerable<TValue> values, IValidationMessage msg)
        {
            return Add(new ValidValuesRule<TValue>(property, values, EqualityComparer<TValue>.Default, msg));
        }

        public ValidationRuleBuilder<T> ValidValues<TValue>(string property, IEnumerable<TValue> values, IEqualityComparer<TValue> comparer, IValidationMessage msg)
        {
            return Add(new ValidValuesRule<TValue>(property, values, comparer, msg));
        }

        public ValidationRuleBuilder<T> ValidValues<TValue>(string property, IValidationMessage msg, params TValue[] values)
        {
            return Add(new ValidValuesRule<TValue>(property, values, EqualityComparer<TValue>.Default, msg));
        }

        public ValidationRuleBuilder<T> ValidValues<TValue>(string property, IEqualityComparer<TValue> comparer, IValidationMessage msg, params TValue[] values)
        {
            return Add(new ValidValuesRule<TValue>(property, values, comparer, msg));
        }

        /**/

        public ValidationRuleBuilder<T> InvalidValues<TValue>(string property, IEnumerable<TValue> values, IValidationMessage msg)
        {
            return Add(new InvalidValuesRule<TValue>(property, values, EqualityComparer<TValue>.Default, msg));
        }

        public ValidationRuleBuilder<T> InvalidValues<TValue>(string property, IEnumerable<TValue> values, IEqualityComparer<TValue> comparer, IValidationMessage msg)
        {
            return Add(new InvalidValuesRule<TValue>(property, values, comparer, msg));
        }

        public ValidationRuleBuilder<T> InvalidValues<TValue>(string property, IValidationMessage msg, params TValue[] values)
        {
            return Add(new InvalidValuesRule<TValue>(property, values, EqualityComparer<TValue>.Default, msg));
        }

        public ValidationRuleBuilder<T> InvalidValues<TValue>(string property, IEqualityComparer<TValue> comparer, IValidationMessage msg, params TValue[] values)
        {
            return Add(new InvalidValuesRule<TValue>(property, values, comparer, msg));
        }

        public ValidationRuleBuilder<T> BeginIf(Func<T, bool> condition)
        {
            return new IfBuilder(this, condition);
        }

        public ValidationRuleBuilder<T> FailIfTrue(Func<T, bool> condition, IValidationMessage msg)
        {
            return Add(new FailIfTrueRule<T>(condition, msg));
        }

        public ValidationRuleBuilder<T> FailIfFalse(Func<T, bool> condition, IValidationMessage msg)
        {
            return Add(new FailIfFalseRule<T>(condition, msg));
        }

        public virtual ValidationRuleBuilder<T> EndBuilder()
        {
            if (parent == null)
                throw new InvalidOperationException("Cannot be ended because has no parent");
            return parent;
        }

        public virtual ValidationRuleBuilder<T> Else()
        {
            throw new NotSupportedException("Else is not supported");
        }

        sealed class IfBuilder : ValidationRuleBuilder<T>
        {
            readonly Func<T, bool> condition;
            readonly List<IValidationRule> ifTrueRules = new List<IValidationRule>();
            readonly List<IValidationRule> ifFalseRules = new List<IValidationRule>();
            bool trueRulesBuilding = true;

            public IfBuilder(ValidationRuleBuilder<T> parent, Func<T, bool> condition) : base(parent)
            {
                this.condition = condition;
            }

            public override ValidationRuleBuilder<T> EndBuilder()
            {
                var result = base.EndBuilder();
                result.Rules.Add(new IfRule<T>(condition, ifTrueRules, ifFalseRules));
                return result;
            }

            public override ValidationRuleBuilder<T> Else()
            {
                if (!trueRulesBuilding)
                    throw new InvalidOperationException("Already in else rule building");
                trueRulesBuilding = false;
                return this;
            }

            public override ValidationRuleBuilder<T> Add(IValidationRule rule)
            {
                if (trueRulesBuilding)
                    ifTrueRules.Add(rule);
                else
                    ifFalseRules.Add(rule);
                return this;
            }

            public override void Build()
            {
                throw new InvalidOperationException("Build is not allowed in if builder");
            }
        }

    }
}

#endif