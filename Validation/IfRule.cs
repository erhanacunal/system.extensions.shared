﻿#if !MOBILE
using System;
using System.Collections.Generic;

namespace System.Data.Validation
{
    /// <summary>
    /// Koşul doğru ise alt kuralları da kontrol eder.
    /// </summary>
    /// <remarks>
    /// CurrentEntity property'sini T ye dönüştürmeyi dener, Eğer başarılı olursa koşula vererek sonuca göre işlem yapar
    /// </remarks>
    /// <typeparam name="T">Koşulda kullanılacak entity tipi</typeparam>
    public class IfRule<T> : ValidationRuleBase where T : class
    {

        public List<IValidationRule> IfTrueRules { get; }
        public List<IValidationRule> IfFalseRules { get; }
        public Func<T, bool> Condition { get; set; }

        public IfRule(Func<T, bool> condition, params IValidationRule[] ifTrueRules)
            : base("Koşullu Kurallar Kuralı")
        {
            IfTrueRules = new List<IValidationRule>(ifTrueRules);
            IfFalseRules = new List<IValidationRule>();
            Condition = condition;
        }

        public IfRule(Func<T, bool> condition, IEnumerable<IValidationRule> ifTrueRules)
            : base("Koşullu Kurallar Kuralı")
        {
            IfTrueRules = new List<IValidationRule>(ifTrueRules);
            IfFalseRules = new List<IValidationRule>();
            Condition = condition;
        }

        public IfRule(Func<T, bool> condition, IEnumerable<IValidationRule> ifTrueRules, IEnumerable<IValidationRule> ifFalseRules)
            : base("Koşullu Kurallar Kuralı")
        {
            IfTrueRules = new List<IValidationRule>(ifTrueRules);
            IfFalseRules = new List<IValidationRule>(ifFalseRules);
            Condition = condition;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var result = true;
            var obj = ctx.CurrentEntity as T;
            if (obj != null)
            {
                if (Condition(obj))
                {
                    foreach (var rule in IfTrueRules)
                    {
                        if (!rule.IsValid(ctx))
                            result = false;
                    }
                }
                else
                {
                    foreach (var rule in IfFalseRules)
                    {
                        if (!rule.IsValid(ctx))
                            result = false;
                    }
                }
            }
            return result;
        }
    }
} 
#endif