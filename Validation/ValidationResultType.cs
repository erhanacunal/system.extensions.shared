﻿#if !MOBILE
using System.ComponentModel;
namespace System.Data.Validation
{
    public enum ValidationResultType
    {
        [Description("HATA")]
        Error,
        [Description("UYARI")]
        Warning,
        [Description("BİLGİ")]
        Info
    }
}
#endif