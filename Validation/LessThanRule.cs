﻿#if !MOBILE
using System;
using System.Utils;

namespace System.Data.Validation
{

    /// <summary>
    /// Property değeri verilen değerden küçük olmalı kuralı
    /// </summary>
    public class LessThanRule : ValidationRuleBase
    {

        public double Value { get; set; }

        public LessThanRule(double value, IValidationMessage message)
            : base("Küçüktür Kuralı")
        {
            Value = value;
            Message = message;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (!AssertTrue(ctx, val != null))
                return false;
            // ReSharper disable once PossibleNullReferenceException
            if (TypeUtils.IsNumericType(val.GetType()))
                return AssertTrue(ctx, TypeUtils.CoerceValue<double>(val) < Value);
            ctx.AddError(this, Property, new SimpleValidationMessage("Property sayısal değil!"));
            return false;
        }
    }
    public class LessThanRule<T> : ValidationRuleBase where T : IComparable<T>
    {
        public T Value { get; set; }

        public LessThanRule(string property, T value, IValidationMessage msg)
            : base("Genel Küçüktür Kuralı")
        {
            Message = msg;
            Value = value;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            if (val is T)
            {
                return AssertTrue(ctx, ((T)val).CompareTo(Value) < 0);
            }
            ctx.AddError(this, Property, new SimpleValidationMessage($"Property {typeof(T).Name} tipinde değil!"));
            return false;
        }
    }
} 
#endif