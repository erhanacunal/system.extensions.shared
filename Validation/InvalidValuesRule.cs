﻿#if !MOBILE
using System;
using System.Collections.Generic;

namespace System.Data.Validation
{
    public sealed class InvalidValuesRule<T> : ValidationRuleBase
    {
        readonly IEnumerable<T> invalidValues;
        readonly IEqualityComparer<T> comparer;

        public InvalidValuesRule(string property, IEnumerable<T> invalidValues, IEqualityComparer<T> comparer, IValidationMessage msg) : base("Geçersiz değerler kuralı")
        {
            if (invalidValues == null)
                throw new ArgumentNullException(nameof(invalidValues));
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));
            this.invalidValues = invalidValues;
            this.comparer = comparer;
            Message = msg;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var value = (T)GetValue(ctx, Property);
            foreach (var invalidValue in invalidValues)
            {
                if (!AssertTrue(ctx, !comparer.Equals(invalidValue, value)))
                    return false;
            }
            return true;
        }
    }
} 
#endif