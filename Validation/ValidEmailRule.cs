﻿#if !MOBILE
namespace System.Data.Validation
{
    public class ValidEmailRule : ValidationRuleBase
    {
        readonly bool isRequired;

        public ValidEmailRule(string property, bool isRequired, IValidationMessage msg) : base("Geçerli e-Posta Kuralı")
        {
            Message = msg;
            Property = property;
            this.isRequired = isRequired;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (!isRequired && val == null)
                return true;
            if (!AssertTrue(ctx, val != null))
                return false;
            var str = val as string;
            if (str != null)
            {
                if (str.IsNotNullOrWhitespace())
                    return AssertTrue(ctx, str.IsValidEmail(), new StringFormatValidationMessage(Message.GetMessage(), str));
                if (!isRequired)
                    return true;
                ctx.AddError(this, Property, new SimpleValidationMessage("Boş olamaz!"));
                return false;


            }
            ctx.AddError(this, Property, new SimpleValidationMessage("Property string değil"));
            return false;
        }
    }
} 
#endif