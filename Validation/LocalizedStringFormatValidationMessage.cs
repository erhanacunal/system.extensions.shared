﻿#if !MOBILE
using System.ComponentModel;
using System.Resources;

namespace System.Data.Validation
{
    public class LocalizedStringFormatValidationMessage : StringFormatValidationMessage
    {
        public LocalizedStringFormatValidationMessage(string message, params object[] args) : base(message, args)
        {
        }

        public override string GetMessage()
        {
            var rm = MyServiceProvider.Instance.GetService<ResourceManager>();
            var args = (object[])Args.Clone();
            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                var str = arg as string;
                if (str == null) continue;
                var translated = rm.GetString(str);
                if (translated.IsNotNullOrWhitespace())
                    args[i] = translated;
            }
            return string.Format(rm.GetString(Message) ?? string.Empty, args);
        }
    }
} 
#endif