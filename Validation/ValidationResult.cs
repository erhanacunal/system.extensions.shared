﻿#if !MOBILE
using System.Collections.Generic;

namespace System.Data.Validation
{
    public sealed class ValidationResult : IValidationResult
    {

        Dictionary<int, object> datas;

        public Dictionary<int, object> Datas =>
            datas ?? (datas = new Dictionary<int, object>());

        public object Entity { get; set; }
        public ValidationResultType ResultType { get; }
        public string PropertyName { get; }
        public IValidationMessage Message { get; }
        public string ExceptionDetails { get; }
        public IValidationRule Rule { get; }

        public object this[int id]
        {
            get
            {
                object val;
                return !Datas.TryGetValue(id, out val) ? null : val;
            }
            set
            {
                Datas[id] = value;
            }
        }


        public ValidationResult(IValidationRule rule, object entity, ValidationResultType type, string property, IValidationMessage message, string exception = "")
        {
            Rule = rule;
            Entity = entity;
            ResultType = type;
            PropertyName = property;
            Message = message;
            ExceptionDetails = exception ?? string.Empty;
        }

        public IValidationResult AddData(int id, object value)
        {
            this[id] = value;
            return this;
        }

        public override string ToString()
        {
            return string.Format("{4} {0}:{1}->{2} [{3}]",
                ResultType,
                PropertyName,
                Message,
                Entity?.GetType().Name ?? "",
                Rule?.GetType().Name ?? "");
        }

        public IEnumerable<int> GetDataIds()
        {
            return Datas.Keys;
        }


        public bool TryGetData(int id, out object value)
        {
            return Datas.TryGetValue(id, out value);
        }


        public bool HasData(int id)
        {
            return Datas.ContainsKey(id);
        }
    }
} 
#endif