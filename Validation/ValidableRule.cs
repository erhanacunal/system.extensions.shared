﻿#if !MOBILE
namespace System.Data.Validation
{
    public class ValidableRule : ValidationRuleBase
    {

        public ValidableRule(string property)
            : base("IValidable Kuralı")
        {
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var obj = GetValue(ctx, Property) as IValidable;
            if (obj == null) // null ise bulaşma
                return true;
            return obj.IsValid(ctx);
        }
    }
} 
#endif