﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Utils;

namespace System.Data.Validation
{
    public abstract class ValidationRuleBase : IValidationRule
    {
        public IValidationMessage Message { get; set; }
        public string Id { get; set; }
        public string Property { get; set; }
        /// <summary>
        /// Property'nin görünen adı
        /// </summary>
        public string DisplayName { get; set; }
        public string RuleName { get; private set; }

        protected ValidationRuleBase(string ruleName)
        {
            RuleName = ruleName;
        }

        public abstract bool IsValid(IValidationContext ctx);

        protected virtual bool AssertTrue(IValidationContext ctx, bool condition, IValidationMessage message = null)
        {
            if (condition) return true;
            var target = ctx.CurrentEntity;
            var desc = Message?.GetMessage();
            if (message != null)
                desc = message.GetMessage();
            if (desc != null && target != null)
            {
                var propNames = new List<EnclosedString>(); ;
                StringUtils.Instance.GetEnclosedStrings(desc, propNames, "&", ";");
                propNames.Reverse();
                desc = propNames.Aggregate(desc, (current, propName) => propName.Replace(current, Convert.ToString(GetValueCore(target, propName.Text))));
            }
            ctx.AddError(this, Property, new SimpleValidationMessage(desc));
            return false;
        }

        protected virtual bool AssertFalse(IValidationContext ctx, bool condition, IValidationMessage message = null)
        {
            return AssertTrue(ctx, !condition, message);
        }

        static object GetValueCore(object entity, string propName)
        {
            var accessor = TypeAccessor.CreateOrGet(entity.GetType());
            return accessor[entity, propName];
        }

        protected object GetValue(IValidationContext ctx, string propName)
        {
            var idm = ctx.CurrentEntity;
            if (idm == null)
                throw new InvalidOperationException("No active Entity!");
            return GetValueCore(idm, propName);
        }
    }
} 
#endif