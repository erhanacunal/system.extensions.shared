﻿#if !MOBILE
namespace System.Data.Validation
{
    public class SimpleValidationMessage : ValidationMessage
    {
        public string Message { get; }

        public SimpleValidationMessage(string message)
        {
            Message = message;
        }
        public override string GetMessage()
        {
            return Message;
        }
    }
} 
#endif