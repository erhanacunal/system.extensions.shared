﻿#if !MOBILE
using System;
using System.Collections.Generic;

namespace System.Data.Validation
{
    public sealed class ValidValuesRule<T> : ValidationRuleBase
    {
        readonly IEnumerable<T> validValues;
        readonly IEqualityComparer<T> comparer;

        public ValidValuesRule(string property, IEnumerable<T> validValues, IEqualityComparer<T> comparer, IValidationMessage msg) : base("Geçerli değerler kuralı")
        {
            if (validValues == null)
                throw new ArgumentNullException(nameof(validValues));
            if (comparer == null)
                throw new ArgumentNullException(nameof(comparer));
            this.validValues = validValues;
            this.comparer = comparer;
            Message = msg;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var value = (T)GetValue(ctx, Property);
            foreach (var validValue in validValues)
            {
                if (comparer.Equals(validValue, value))
                {
                    return true;
                }
            }
            AssertTrue(ctx, false);
            return false;
        }
    }
} 
#endif