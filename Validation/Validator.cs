﻿#if !MOBILE
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace System.Data.Validation
{
    public static class Validator
    {

        static readonly ConcurrentDictionary<Type, List<IValidationRule>> TypeRules =
            new ConcurrentDictionary<Type, List<IValidationRule>>();

        public static void AddRuleFor(Type model, IValidationRule rule)
        {
            if (TypeRules.ContainsKey(model))
                TypeRules[model].Add(rule);
            else
            {
                var list = TypeRules.GetOrAdd(model, new List<IValidationRule>());
                lock (list) { list.Add(rule); }
            }
        }

        public static void AddRulesFor(Type model, IEnumerable<IValidationRule> rules)
        {
            if (TypeRules.ContainsKey(model))
                TypeRules[model].AddRange(rules);
            else
            {
                var list = TypeRules.GetOrAdd(model, new List<IValidationRule>());
                lock (list) { list.AddRange(rules); }
            }
        }

        static readonly IValidationResult[] EmptyResults = new IValidationResult[0];

        public static IValidationResult[] Validate(Type model, IValidationContext ctx)
        {
            List<IValidationRule> rules;
            if (!TypeRules.TryGetValue(model, out rules)) return EmptyResults;
            foreach (var rule in rules)
            {
                rule.IsValid(ctx);
            }
            return ctx.Results.ToArray();
        }

        public static bool Validate2<T>(T model, IValidationContext ctx = null, IObserver<IValidationResult> observer = null)
        {
            if (ctx == null)
                ctx = new ValidationContext();
            var modelType = typeof(T);
            if (modelType == typeof(object))
                modelType = model.GetType();
            ctx.EnterChild(model);
            IDisposable unsubscriber = null;
            if (observer != null)
                unsubscriber = ctx.Subscribe(observer);
            try
            {
                List<IValidationRule> rules;
                if (!TypeRules.TryGetValue(modelType, out rules)) return true;
                var result = true;
                foreach (var rule in rules)
                    result &= rule.IsValid(ctx);
                return result;
            }
            finally
            {
                ctx.LeaveChild();
                unsubscriber?.Dispose();
            }
        }

        public static IValidationResult[] Validate<T>(T instance, IValidationContext ctx = null, IObserver<IValidationResult> observer = null)
        {
            if (ctx == null)
                ctx = new ValidationContext();
            ctx.EnterChild(instance);
            IDisposable unsubscriber = null;
            if (observer != null)
                unsubscriber = ctx.Subscribe(observer);
            try
            {
                return Validate(typeof(T), ctx);
            }
            finally
            {
                ctx.LeaveChild();
                unsubscriber?.Dispose();
            }
        }
    }
}

#endif