﻿#if !MOBILE
using System;

namespace System.Data.Validation
{
    /// <summary>
    /// Koşul doğru ise belirtilen hatayı verir
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class FailIfTrueRule<T> : ValidationRuleBase where T : class
    {

        public Func<T, bool> Condition { get; set; }

        public FailIfTrueRule(Func<T, bool> cond, IValidationMessage msg)
            : base("Eğer Doğruysa Başarısız Ol Kuralı")
        {
            Message = msg;
            Condition = cond;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var obj = ctx.CurrentEntity as T;
            return obj == null || AssertFalse(ctx, Condition(obj));
        }
    }
} 
#endif