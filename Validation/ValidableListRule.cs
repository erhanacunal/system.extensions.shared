﻿#if !MOBILE
using System.Collections;
using System.Linq;

namespace System.Data.Validation
{
    public class ValidableListRule : ValidationRuleBase
    {

        public ValidableListRule(string property)
            : base("Çocuk Liste Denetim Kuralı")
        {
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var obj = GetValue(ctx, Property);
            if (obj == null) // null ise bulaşma
                return true;
            var enm = obj as IEnumerable;
            if (enm == null)
            {
                ctx.AddError(this, Property, new SimpleValidationMessage("Property IEnumerable değil!"));
                return false;
            }
            var result = true;
            foreach (var item in enm.OfType<IValidable>())
            {
                if (!item.IsValid(ctx))
                    result = false;
            }
            return result;
        }
    }
} 
#endif