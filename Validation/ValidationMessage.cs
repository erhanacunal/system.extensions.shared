﻿#if !MOBILE
namespace System.Data.Validation
{
    public abstract class ValidationMessage : IValidationMessage
    {
        public abstract string GetMessage();

        public override string ToString()
        {
            return GetMessage();
        }
    }
} 
#endif