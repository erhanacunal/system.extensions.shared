﻿#if !MOBILE
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Custom.Models;

namespace System.Data.Validation
{
    public sealed class ValidationContext : Subject<IValidationResult>, IValidationContext
    {
        #region Private & Protected Members

        readonly List<IValidationResult> resultList = new List<IValidationResult>();
        readonly Stack<object> entityStack = new Stack<object>();
        HashSet<string> flags;
        Dictionary<string, object> arguments;

#if !MOBILE
        ServiceContainer serviceContainer; 
#endif


        Dictionary<string, object> Arguments =>
            arguments ?? (arguments = new Dictionary<string, object>());

        #endregion

        public IEnumerable<IValidationResult> Results => resultList;

        public object CurrentEntity
        {
            get
            {
                if (entityStack.Count > 0)
                    return entityStack.Peek();
                return null;
            }
        }
        public object this[string name]
        {
            get
            {
                object result;
                return Arguments.TryGetValue(name, out result) ? result : null;
            }
            set
            {
                Arguments[name] = value;
            }
        }

        public object this[string name, object defaultValue]
        {
            get
            {
                object result;
                if (!arguments.TryGetValue(name, out result))
                    result = defaultValue;
                return result;
            }
            set
            {
                Arguments[name] = value;
            }
        }

#if !MOBILE
        public IServiceContainer ServiceContainer =>
            serviceContainer ?? (serviceContainer = new ServiceContainer()); 
#endif

        public bool HasErrors { get { return resultList.Any(_ => _.ResultType == ValidationResultType.Error); } }
        public bool CheckValidationAllowed(string propertyName)
        {
            return true;
        }

        public void EnterChild(object child)
        {
            entityStack.Push(child);
        }

        public void LeaveChild()
        {
            if (entityStack.Count > 0)
                entityStack.Pop();
        }

        public IValidationResult AddError(IValidationRule rule, string property, IValidationMessage message)
        {
            var err = Create(rule, ValidationResultType.Error, property, message);
            Add(err);
            return err;
        }

        public IValidationResult AddError(IValidationRule rule, string property, IValidationMessage message, string exception)
        {
            var err = Create(rule, ValidationResultType.Error, property, message, exception ?? string.Empty);
            Add(err);
            return err;
        }

        public IValidationResult AddWarning(IValidationRule rule, string property, IValidationMessage message)
        {
            var err = Create(rule, ValidationResultType.Warning, property, message);
            Add(err);
            return err;
        }

        public IValidationResult AddInfo(IValidationRule rule, string property, IValidationMessage message)
        {
            var err = Create(rule, ValidationResultType.Info, property, message);
            Add(err);
            return err;
        }

        void RaiseResultAdded(IValidationResult err)
        {
            OnNext(err);
            var temp = ResultAdded;
            temp?.Invoke(this, new ValidationErrorEventArgs(err));
        }

        public event EventHandler<ValidationErrorEventArgs> ResultAdded;

        public bool IsFlagSet(string flag)
        {
            return flags != null && flags.Contains(flag);
        }

        public IDisposable SetFlag(string flag, bool on)
        {
            if (flags == null)
                flags = new HashSet<string>();

            if (on)
            {
                flags.Add(flag);
                return new UnsetFlag(this, flag);
            }
            flags.Remove(flag);

            return null;

        }

        public void ClearErrors()
        {
            resultList.Clear();
        }

        public string ErrorsAsString()
        {
            var sb = new StringBuilder();
            foreach (var error in resultList)
            {
                if (error.ResultType == ValidationResultType.Info || error.ResultType == ValidationResultType.Warning)
                    continue;
                sb.Append(error.ResultType.GetDescription()).Append(" ");
                if (error.Rule != null)
                    sb.Append(error.Rule.DisplayName).Append(" :");
                if (error.PropertyName.IsNotNullOrEmpty())
                    sb.AppendFormat("[{0}] ", error.PropertyName);
                sb.AppendFormat("{0}\r\n", error.Message);
            }
            return sb.ToString();
        }



        public IValidationResult Create(IValidationRule rule, ValidationResultType type, string property, IValidationMessage message, string exception = "")
        {
            return new ValidationResult(rule, CurrentEntity, type, property, message, exception);
        }

        public void Add(IValidationResult error)
        {
            resultList.Add(error);
            RaiseResultAdded(error);
        }


        public object GetService(Type serviceType)
        {
#if !SILVERLIGHT && !MOBILE
            return serviceContainer?.GetService(serviceType);
#else
            return null;
#endif
        }

        sealed class UnsetFlag : DisposableBase
        {
            readonly string flagName;
            readonly WeakReference<IValidationContext> context;

            public UnsetFlag(IValidationContext ctx, string flagName)
            {
                this.flagName = flagName;
                context = new WeakReference<IValidationContext>(ctx);
            }

            protected override void DisposeCore()
            {
                IValidationContext ctx;
                if (context.TryGetTarget(out ctx))
                {
                    ctx.SetFlag(flagName, false);
                }
            }
        }

    }
} 
#endif