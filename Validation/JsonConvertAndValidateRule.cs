﻿#if JSON && !MOBILE
using System;
using Newtonsoft.Json;

namespace System.Data.Validation
{
    public class JsonConvertAndValidateRule<T> : ValidationRuleBase where T : class
    {
        readonly bool required;
        readonly IValidationMessage requiredMessage;

        public JsonConvertAndValidateRule(string property, bool required = false, IValidationMessage requiredMessage = null) : base("Json'a Dönüştür ve Doğrula Kuralı")
        {
            this.required = required;
            if (required && requiredMessage == null)
                throw new ArgumentNullException(nameof(requiredMessage));
            this.requiredMessage = requiredMessage;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var value = GetValue(ctx, Property);
            var str = value as string;
            if (str != null)
            {
                if (str.IsNotNullOrWhitespace())
                {
                    var obj = JsonConvert.DeserializeObject<T>(str);
                    return Validator.Validate2(obj, ctx);
                }
            }
            if (!required)
                return true;
            ctx.AddError(this, Property, requiredMessage);
            return false;
        }
    }
}
#endif