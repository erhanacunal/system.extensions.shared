﻿#if !MOBILE
using System;
using System.Collections;

namespace System.Data.Validation
{
    public class ChildCheckRule : ValidationRuleBase
    {

        public ChildCheckRule(string property) : base("Çocuk Property Denetim Kuralı")
        {
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            var enumValue = val as IEnumerable;
            var result = true;
            if (enumValue == null)
                Validator.Validate2(val, ctx);
            else
                foreach (var obj in enumValue)
                {
                    if (obj == null) continue;
                    result &= Validator.Validate2(obj, ctx);
                }
            return result;
        }
    }
}

#endif