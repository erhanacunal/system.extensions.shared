﻿#if !MOBILE
using System.ComponentModel;
using System.Resources;

namespace System.Data.Validation
{
    public class LocalizedValidationMessage : SimpleValidationMessage
    {
        public LocalizedValidationMessage(string message) : base(message)
        {
        }

        public override string GetMessage()
        {
            return MyServiceProvider.Instance.GetService<ResourceManager>().GetString(Message);
        }
    }
} 
#endif