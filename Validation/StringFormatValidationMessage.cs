﻿#if !MOBILE
namespace System.Data.Validation
{
    public class StringFormatValidationMessage : ValidationMessage
    {
        public StringFormatValidationMessage(string message, params object[] args)
        {
            Message = message;
            Args = args;
        }

        public string Message { get; }
        public object[] Args { get; }
        public override string GetMessage()
        {
            return string.Format(Message, Args);
        }
    }
} 
#endif