﻿#if !MOBILE
using System;
using System.Utils;

namespace System.Data.Validation
{
    public class ValueRangeRule : ValidationRuleBase
    {

        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public ValueRangeRule(string property, int min, int max, IValidationMessage msg)
            : base("Değer Aralığı Kuralı")
        {
            Message = msg;
            Property = property;
            Minimum = min;
            Maximum = max;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            if (TypeUtils.IsNumericType(val.GetType()))
                return AssertTrue(ctx, TypeUtils.CoerceValue<int>(val).Between(Minimum, Maximum));
            ctx.AddError(this, Property, new SimpleValidationMessage("Property sayısal değil!"));
            return false;
        }
    }
} 
#endif