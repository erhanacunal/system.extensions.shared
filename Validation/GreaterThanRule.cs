﻿#if !MOBILE
using System;
using System.Utils;

namespace System.Data.Validation
{
    public class GreaterThanRule : ValidationRuleBase
    {

        public double Value { get; set; }

        public GreaterThanRule(string property, double value, IValidationMessage msg)
            : base("Büyüktür Kuralı")
        {
            Message = msg;
            Value = value;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            if (TypeUtils.IsNumericType(val.GetType()))
                return AssertTrue(ctx, TypeUtils.CoerceValue<double>(val) > Value);
            ctx.AddError(this, Property, new SimpleValidationMessage("Property sayısal değil!"));
            return false;
        }
    }

    /// <summary>
    /// Property değeri verilen değerden büyük olmalı kuralı
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GreaterThanRule<T> : ValidationRuleBase where T : IComparable<T>
    {
        public T Value { get; set; }

        public GreaterThanRule(string property, T value, IValidationMessage msg)
            : base("Genel Büyüktür Kuralı")
        {
            Message = msg;
            Value = value;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            if (val is T)
            {
                return AssertTrue(ctx, ((T)val).CompareTo(Value) > 0);
            }
            ctx.AddError(this, Property, new SimpleValidationMessage($"Property {typeof(T).Name} tipinde değil!"));
            return false;
        }
    }
} 
#endif