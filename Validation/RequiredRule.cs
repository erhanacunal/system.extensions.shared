﻿#if !MOBILE
using System;
using System.Collections;

namespace System.Data.Validation
{
    public class RequiredRule : ValidationRuleBase
    {

        public RequiredRule(string property, IValidationMessage msg)
            : base("Gereklilik Kuralı")
        {
            Message = msg;
            Property = property;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (AssertTrue(ctx, val != null))
            {
                var str = val as string;
                if (str != null)
                {
                    return AssertTrue(ctx, str.IsNotNullOrEmpty());
                }
                var list = val as IList;
                if (list != null)
                    return AssertTrue(ctx, list.Count > 0);
                var enumerable = val as IEnumerable;
                if (enumerable != null)
                    return AssertTrue(ctx, enumerable.Any());
                if (val is decimal)
                    return AssertTrue(ctx, (decimal)val > 0m);
                if (val is int)
                    return AssertTrue(ctx, (int)val > 0);
                if (val is bool)
                    return AssertTrue(ctx, (bool)val);
                if (val is byte)
                    return AssertTrue(ctx, (byte)val > 0);
                if (val is short)
                    return AssertTrue(ctx, (short)val > 0);
                if (val is long)
                    return AssertTrue(ctx, (long)val > 0L);
                if (val is float)
                    return AssertTrue(ctx, (float)val > 0);
            }
            else
                return false;
            return true;
        }
    }
} 
#endif