﻿#if !MOBILE
namespace System.Data.Validation
{
    public interface IValidationRule
    {

        string Property { get; }
        string Id { get; }
        string DisplayName { get; }

        bool IsValid(IValidationContext ctx);
    }
} 
#endif