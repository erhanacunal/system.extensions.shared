﻿#if !MOBILE
using System;
using System.Collections.Generic;
#if !MOBILE
using System.ComponentModel.Design; 
#endif

namespace System.Data.Validation
{
    public interface IValidationContext : IObservable<IValidationResult>, IServiceProvider
    {

        IEnumerable<IValidationResult> Results { get; }
        object CurrentEntity { get; }
        bool HasErrors { get; }

        event EventHandler<ValidationErrorEventArgs> ResultAdded;

        bool CheckValidationAllowed(string propertyName);
        bool IsFlagSet(string flag);
        IDisposable SetFlag(string flag, bool on);
        object this[string name] { get; set; }
        object this[string name, object defaultValue] { get; set; }
#if !MOBILE
        IServiceContainer ServiceContainer { get; } 
#endif
        void EnterChild(object child);
        void LeaveChild();
        IValidationResult AddError(IValidationRule rule, string property, IValidationMessage message);
        IValidationResult AddError(IValidationRule rule, string property, IValidationMessage message, string exception);
        IValidationResult AddWarning(IValidationRule rule, string property, IValidationMessage message);
        IValidationResult AddInfo(IValidationRule rule, string property, IValidationMessage message);

        /// <summary>
        /// Yeni bir doğrulama hatası oluşturur, fakat hata listesine eklemez
        /// </summary>
        /// <param name="rule">Hatayı bulan kural örneği</param>
        /// <param name="type">Hatanın tipi</param>
        /// <param name="property">Hatanın oluştuğu property</param>
        /// <param name="message">Hatanın açıklaması</param>
        /// <param name="exception">Hataya neden olan istisna</param>
        /// <returns>IValidationError örneği</returns>
        /// <remarks>
        /// Gerekli verileri IValidationError.AddData ile ekleyip sonra hata listesine kaydetmek isterseniz bu güvenli bir yöntem olabilir.
        /// </remarks>
        IValidationResult Create(IValidationRule rule, ValidationResultType type, string property, IValidationMessage message, string exception = "");
        void Add(IValidationResult error);

        void ClearErrors();

    }
} 
#endif