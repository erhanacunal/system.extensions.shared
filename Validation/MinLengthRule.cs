﻿#if !MOBILE
using System.Collections;
using System.Linq;

namespace System.Data.Validation
{
    public class MinLengthRule : ValidationRuleBase
    {

        public int Length { get; set; }

        public MinLengthRule(string property, int length, IValidationMessage msg)
            : base("Uzunluk Kuralı")
        {
            Message = msg;
            Property = property;
            Length = length;
        }
        public override bool IsValid(IValidationContext ctx)
        {
            var val = GetValue(ctx, Property);
            if (val == null)
                return true;
            var str = val as string;
            if (str != null)
                return AssertTrue(ctx, str.Length >= Length);
            if (!(val is IEnumerable))
            {
                ctx.AddError(this, Property, new SimpleValidationMessage("Property IEnumerable değil"));
                return false;
            }
            var en = val as IEnumerable;
            return AssertTrue(ctx, en.Cast<object>().Count() >= Length);
        }
    }
} 
#endif