﻿#if !MOBILE
namespace System.Data.Validation
{
    public sealed class ThrowErrorRule : ValidationRuleBase
    {

        public ThrowErrorRule(IValidationMessage msg)
            : base("Hata Fırlat Kuralı")
        {
            Message = msg;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            return AssertTrue(ctx, false);
        }
    }
} 
#endif