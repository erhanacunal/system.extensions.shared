﻿#if !MOBILE
using System;

namespace System.Data.Validation
{
    /// <summary>
    /// Koşul yanlış ise belirtilen hatayı verir
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class FailIfFalseRule<T> : ValidationRuleBase where T : class
    {

        public Func<T, bool> Condition { get; set; }

        public FailIfFalseRule(Func<T, bool> cond, IValidationMessage msg)
            : base("Eğer Yanlışsa Başarısız Ol Kuralı")
        {
            Message = msg;
            Condition = cond;
        }

        public override bool IsValid(IValidationContext ctx)
        {
            var obj = ctx.CurrentEntity as T;
            return obj == null || AssertTrue(ctx, Condition(obj));
        }
    }
} 
#endif