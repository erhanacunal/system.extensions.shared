﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace System.Utils
{
    public sealed class StringUtils
    {

        internal static UTF8Encoding SimpleTextEncoder = new UTF8Encoding(false, false);

        public static readonly StringUtils Instance = new StringUtils();

        static readonly string[] Ones = { "", "Bir", "İki", "Üç", "Dört", "Beş", "Altı", "Yedi", "Sekiz", "Dokuz" };

        static readonly string[] Tens = { "", "On", "Yirmi", "Otuz", "Kırk", "Elli", "Altmış", "Yetmiş", "Seksen", "Doksan" };

        static readonly string[] Thous = { "Yüz", "Bin", "Milyon", "Milyar", "Trilyon", "Katrilyon" };

        public void ReplaceValue(ref string[] nameValuePairs, string name, string newValue)
        {
            for (var i = 0; i < nameValuePairs.Length; i += 2)
            {
                if (nameValuePairs[i].Equals(name, StringComparison.OrdinalIgnoreCase))
                    nameValuePairs[i + 1] = newValue;
            }
        }

        public string JoinNameValuePairs(string[] nameValuePairs, string nameValueSeparator = "=", string delimiter = ";")
        {
            var sb = new StringBuilder();
            for (var i = 0; i < nameValuePairs.Length; i += 2)
            {
                if (i > 0)
                    sb.Append(delimiter);
                sb.Append(nameValuePairs[i]).Append(nameValueSeparator).Append(nameValuePairs[i + 1]);
            }
            return sb.ToString();
        }

        public string GetValue(string nameValuePair, string nameValueSeparator = "=")
        {
            var idx = nameValuePair.IndexOf(nameValueSeparator, StringComparison.Ordinal);
            return idx > -1 ? nameValuePair.Substring(idx + 1) : string.Empty;
        }

        public bool TryGetNameValue(string src, out string name, out string value, string nameValueSeparator = "=")
        {
            name = string.Empty;
            value = string.Empty;
            var idx = src.IndexOf(nameValueSeparator, StringComparison.Ordinal);
            if (idx == -1)
                return false;
            name = src.Substring(0, idx);
            value = src.Substring(idx + 1);
            return true;
        }

       

        public void GetEnclosedStrings(string src, IList<EnclosedString> targetList, string startTag, string endTag, int startIndex = 0, bool escapeChars = true)
        {
            if (src == null)
                throw new ArgumentNullException(nameof(src));
            if ((startIndex < 0) || (startIndex > src.Length))
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Indeks sınırların dışındaydı");
            do
            {
                if (TryGetEnclosedString(src, startTag, endTag, out var str, startIndex, escapeChars))
                {
                    targetList.Add(str);
                    startIndex = str.EndIndex;
                    if (startIndex == (src.Length - 1))
                        break;
                }
                else
                    break;
            } while (true);
        }

        public bool TryGetEnclosedString(string src, string startTag, string endTag, out EnclosedString str, int startIndex = 0, bool escapeChars = true)
        {
            if (src == null)
                throw new ArgumentNullException(nameof(src));
            if ((startIndex < 0) || (startIndex > src.Length))
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Indeks sınırların dışındaydı");

            str = EnclosedString.Empty;
            var startTagIndex = src.IndexOf(startTag, startIndex, StringComparison.Ordinal);
            if (startTagIndex == -1)
                return false;
            // başlangıç etiketinden sonra herhangi bir karakter var mı?
            if ((startTagIndex + startTag.Length) < src.Length)
            {
                // başlangıç etiketinin sonrasına git
                startTagIndex += startTag.Length;
                // bitiş etiketi başlangıcını belirle
                var endTagStartIndex = startTagIndex;
                checkEndTag:
                var endTagIndex = src.IndexOf(endTag, endTagStartIndex, StringComparison.Ordinal);
                if (endTagIndex > -1)
                {
                    // bitiş etiketinden kaçış var mı kontrol et
                    if (endTag.Length == 1 && src[endTagIndex - 1] == '\\')
                    {
                        // kaçış var o zaman bitiş etiketi başlangıcını ileri taşı
                        endTagStartIndex = endTagIndex + 1;
                        // hala kaynak string'in sınırları içinde miyiz?
                        if (endTagStartIndex < src.Length)
                            goto checkEndTag; // aramaya devam et
                        return false; // sınır dışındayız bulamadık
                    }
                    var part = src.Substring(startTagIndex, endTagIndex - startTagIndex);
                    if (escapeChars)
                        part = UnescapeStrings(part);
                    str = new EnclosedString(startTagIndex - startTag.Length, endTagIndex + endTag.Length, part);
                    return true;
                }
            }
            return false;
        }

        public string UnescapeStrings(string src)
        {
            var sb = new StringBuilder();
            var unescape = false;
            for (var i = 0; i < src.Length; i++)
            {
                var ch = src[i];
                if (unescape)
                {
                    sb.Append(ch);
                    unescape = false;
                    continue;
                }
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                if (ch == '\\' && !unescape)
                    unescape = true;
                else
                    sb.Append(ch);
            }
            return sb.ToString();
        }


        public string ToWords(decimal number, ToWordsOptions options)
        {
            if (number < 0)
                return "eksi " + ToWords(Math.Abs(number), options);
            number = Math.Round(number, 2);
            var fmt = options.InsertSpace ? "{0} {1} {2} {3}" : "{0}{1}{2}{3}";
            var intPortion = (int)number;
            var decPortion = (int)((number - intPortion) * 100m);

            return string.Format(fmt,
                ToWords(intPortion, options),
                options.IntPortionText,
                decPortion > 0
                    ? ToWords(decPortion, options)
                    : string.Empty,
                decPortion > 0
                    ? options.DecimalPortionText
                    : string.Empty);
        }

        static string ToWords(int number, ToWordsOptions options, string appendScale = "")
        {
            var fmt = options.InsertSpace ? "{0} {1}" : "{0}{1}";
            string numString;
            if (number < 100)
            {
                if (number < 10)
                {
                    if (appendScale.IsOneOfThese(StringComparison.OrdinalIgnoreCase, "Yüz", "Bin") && number == 1)
                        numString = string.Empty;
                    else
                        numString = Ones[number];
                }
                else
                {
                    numString = Tens[number / 10];
                    if ((number % 10) > 0)
                        numString += (options.InsertSpace ? " " : string.Empty) + Ones[number % 10];
                }
            }
            else
            {
                int pow;
                string powStr;

                if (number < 1000)
                { // number is between 100 and 1000
                    pow = 100;
                    powStr = Thous[0];
                }
                else
                { // find the scale of the number
                    var log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = Thous[log];
                }

                numString = string.Format(fmt, ToWords(number / pow, options, powStr), ToWords(number % pow, options)).Trim();
            }

            return string.Format(fmt, numString, appendScale).Trim();
        }

#if !MOBILE

        public bool TryGetNameValue<TK, TV>(string src, out KeyValuePair<TK, TV> pair, string nameValueSeparator = "=")
        {
            pair = new KeyValuePair<TK, TV>();
            var idx = src.IndexOf(nameValueSeparator, StringComparison.Ordinal);
            if (idx > -1)
            {
                pair = new KeyValuePair<TK, TV>(
                   TypeUtils.CoerceValue<TK>(src.Substring(0, idx)),
                   TypeUtils.CoerceValue<TV>(src.Substring(idx + 1)));
                return true;
            }
            return false;
        }

        public IDictionary<TK, TV> ToDictionary<TK, TV>(string source, string nameValueSeparator = "=", string delimiter = ";")
        {
            var result = new Dictionary<TK, TV>();
            var parts = source.Split(new[] {
                nameValueSeparator, delimiter }, StringSplitOptions.None);
            if (parts.Length < 2)
                return result;
            for (var i = 0; i < parts.Length; i += 2)
            {
                var key = TypeUtils.CoerceValue<TK>(parts[i]);
                var value = TypeUtils.GetDefaultOf<TV>();
                if ((i + 1) < parts.Length)
                    value = TypeUtils.CoerceValue<TV>(parts[i + 1]);
                result.Add(key, value);
            }
            return result;
        }

        public IList<Tuple<TK, TV>> ToTupleList<TK, TV>(string source, string nameValueSeparator = "=", string delimiter = ";")
        {
            var result = new List<Tuple<TK, TV>>();
            var parts = source.Split(new[] {
                nameValueSeparator, delimiter }, StringSplitOptions.None);
            if (parts.Length < 2)
                return result;
            for (var i = 0; i < parts.Length; i += 2)
            {
                var key = TypeUtils.CoerceValue<TK>(parts[i]);
                var value = TypeUtils.GetDefaultOf<TV>();
                if ((i + 1) < parts.Length)
                    value = TypeUtils.CoerceValue<TV>(parts[i + 1]);
                result.Add(Tuple.Create(key, value));
            }
            return result;
        } 

        public void StrArrayToNameValue(string[] args, NameValueCollection target)
        {
            if ((args.Length % 2) != 0)
                throw new ArgumentOutOfRangeException(nameof(args));
            for (var i = 0; i < args.Length; i += 2)
            {
                target.Add(args[i], args[i + 1]);
            }
        }

        public Tuple<T1, T2> ToTuple<T1, T2>(string str)
        {
            var parts = str.Split(',');
            var v1 = default(T1);
            var v2 = default(T2);
            if (parts.Length > 0)
                v1 = TypeUtils.CoerceValue<T1>(parts[0]);
            if (parts.Length > 1)
                v2 = TypeUtils.CoerceValue<T2>(parts[1]);
            return Tuple.Create(v1, v2);
        }

        public Tuple<T1, T2, T3> ToTuple<T1, T2, T3>(string str)
        {
            var parts = str.Split(',');
            var v1 = default(T1);
            var v2 = default(T2);
            var v3 = default(T3);
            if (parts.Length > 0)
                v1 = TypeUtils.CoerceValue<T1>(parts[0]);
            if (parts.Length > 1)
                v2 = TypeUtils.CoerceValue<T2>(parts[1]);
            if (parts.Length > 2)
                v3 = TypeUtils.CoerceValue<T3>(parts[2]);
            return Tuple.Create(v1, v2, v3);
        } 
#endif

        public string DictionaryToString<TK, TV>(IDictionary<TK, TV> source, string nameValueSeparator = "=", string delimiter = ";")
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var kv in source)
            {
                if (i++ > 0)
                    sb.Append(delimiter);
                sb.Append(kv.Key).Append(nameValueSeparator).Append(kv.Value);
            }
            return sb.ToString();
        }

        public string BytesToString(long byteCount)
        {
            string[] suf = { " B", " KB", " MB", " GB", " TB", " PB", " EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            var bytes = Math.Abs(byteCount);
            var place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            var num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString(CultureInfo.InvariantCulture) + suf[place];
        }

        public void StrArrayToDictionary(string[] args, IDictionary<string, string> targetDictionary)
        {
            if ((args.Length % 2) != 0)
                throw new ArgumentOutOfRangeException(nameof(args));
            for (var i = 0; i < args.Length; i += 2)
            {
                targetDictionary.Add(args[i], args[i + 1]);
            }
        }

        public bool AnyOneIsNullOrEmpty(string s1, string s2)
        {
            return string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2);
        }

        public bool AnyOneIsNullOrEmpty(string s1, string s2, string s3)
        {
            return string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2) || string.IsNullOrEmpty(s3);
        }

        public bool AnyOneIsNullOrEmpty(string s1, string s2, string s3, string s4)
        {
            return string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2) || string.IsNullOrEmpty(s3) || string.IsNullOrEmpty(s4);
        }

        public bool AllIsNotNullOrEmpty(string s1, string s2)
        {
            return !string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2);
        }

        public bool AllIsNotNullOrEmpty(string s1, string s2, string s3)
        {
            return !string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2) && !string.IsNullOrEmpty(s3);
        }

        public bool AllIsNotNullOrEmpty(string s1, string s2, string s3,
            string s4)
        {
            return !string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2) && !string.IsNullOrEmpty(s3) && !string.IsNullOrEmpty(s4);
        }

        public bool AllIsNotNullOrEmpty(string s1, string s2, string s3,
            string s4, string s5)
        {
            return !string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2) && !string.IsNullOrEmpty(s3) && !string.IsNullOrEmpty(s4) && !string.IsNullOrEmpty(s5);
        }

        public string ConcatNonEmptyWithDelim(string delimiter, params string[] strings)
        {
            var i = 0;
            var sb = new StringBuilder();
            foreach (var str in strings)
            {
                if (string.IsNullOrWhiteSpace(str))
                    continue;
                if (i++ > 0)
                    sb.Append(delimiter);
                sb.Append(str);
            }
            return sb.ToString();
        }

        public string FirstNotEmpty(params string[] strings)
        {
            foreach (var str in strings)
            {
                if (!string.IsNullOrWhiteSpace(str))
                    return str;
            }
            return string.Empty;
        }

        public string WrapWithQuoteThenJoin(IEnumerable<string> src, char quote = '\'', string delimiter = ",")
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var str in src)
            {
                if (i++ > 0) sb.Append(delimiter);
                sb.Append(quote).Append(str).Append(quote);
            }
            return sb.ToString();
        }

        public string AddPrefixThenJoin(IEnumerable<string> src, string prefix, string delimiter = ",")
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var str in src)
            {
                if (i++ > 0) sb.Append(delimiter);
                sb.Append(prefix).Append(str);
            }
            return sb.ToString();
        }

        public string NormalizeLineEndings(string src) => Regex.Replace(src, @"\r\n|\n\r|\n|\r", "\r\n");

        public List<CommentRange> GetComments(string src, params CommentStyle[] commentStyles)
        {
            if (src == null) throw new ArgumentNullException(nameof(src));
            if (commentStyles == null) throw new ArgumentNullException(nameof(commentStyles));
            var lbc = new LineBreakCounter(src);
            var result = new List<CommentRange>();
            var nav = new CharReader(src);
            CommentStyle currentComment = null;
            var startIndex = 0;
            while (!nav.Eof)
            {
                restart:
                if (currentComment != null)
                {
                    if (nav.MatchStringAndSkip(currentComment.EndChars))
                        result.Add(new CommentRange(startIndex, nav.Position - 1, true));
                    else nav.NextChar();
                    continue;
                }
                foreach (var commentStyle in commentStyles)
                {
                    if (commentStyle.MultiLine && nav.MatchStringAndSkip(commentStyle.StartChars))
                    {
                        currentComment = commentStyle;
                        startIndex = nav.Position - 2;
                    }
                    else if (!commentStyle.MultiLine && nav.MatchStringAndSkip(commentStyle.StartChars))
                    {
                        var line = lbc.GetLine(nav.Position);
                        var lineStart = lbc.GetOffset(line);
                        var lineEnd = (line + 1) < lbc.Lines ? lbc.GetOffset(line + 1) - 2 : src.Length - 1;
                        result.Add(new CommentRange(nav.Position - 2, lineEnd, false));
                        nav.AdvanceChar(lineEnd - lineStart);
                        goto restart;
                    }
                }
                nav.NextChar();
            }
            return result;
        }

        public static bool IsWhitespace(char ch)
        {
            // whitespace:
            //   Any character with Unicode class Zs
            //   Horizontal tab character (U+0009)
            //   Vertical tab character (U+000B)
            //   Form feed character (U+000C)

            // Space and no-break space are the only space separators (Zs) in ASCII range

            return ch == ' '
                   || ch == '\t'
                   || ch == '\v'
                   || ch == '\f'
                   || ch == '\u00A0' // NO-BREAK SPACE
                                     // The native compiler, in ScanToken, recognized both the byte-order
                                     // marker '\uFEFF' as well as ^Z '\u001A' as whitespace, although
                                     // this is not to spec since neither of these are in Zs. For the
                                     // sake of compatibility, we recognize them both here. Note: '\uFEFF'
                                     // also happens to be a formatting character (class Cf), which means
                                     // that it is a legal non-initial identifier character. So it's
                                     // especially funny, because it will be whitespace UNLESS we happen
                                     // to be scanning an identifier or keyword, in which case it winds
                                     // up in the identifier or keyword.
                   || ch == '\uFEFF'
                   || ch == '\u001A'
                   || (ch > 255 && CharUnicodeInfo.GetUnicodeCategory(ch) == UnicodeCategory.SpaceSeparator);
        }

        public static bool IsNewLine(char ch)
        {
            // new-line-character:
            //   Carriage return character (U+000D)
            //   Line feed character (U+000A)
            //   Next line character (U+0085)
            //   Line separator character (U+2028)
            //   Paragraph separator character (U+2029)

            return ch == '\r'
                   || ch == '\n'
                   || ch == '\u0085'
                   || ch == '\u2028'
                   || ch == '\u2029';
        }

    }

    public struct EnclosedString
    {
        readonly bool m_IsEmpty;

        public static readonly EnclosedString Empty = new EnclosedString(true);

        public string Text;
        public int StartIndex;
        public int EndIndex;
        public int Length;

        public bool IsEmpty => m_IsEmpty;


        EnclosedString(bool isEmpty)
        {
            StartIndex = -1;
            EndIndex = -1;
            Length = 0;
            Text = string.Empty;
            m_IsEmpty = isEmpty;
        }

        public EnclosedString(int start, int end, string text)
        {
            StartIndex = start;
            EndIndex = end;
            Text = text;
            Length = end - start;
            m_IsEmpty = false;
        }

        public string Remove(string source)
        {
            return source.Remove(StartIndex, Length);
        }

        public string Replace(string targetString, string newValue)
        {
            return targetString
                .Remove(StartIndex, Length)
                .Insert(StartIndex, newValue);
        }
    }

    public struct ToWordsOptions
    {
        public static readonly ToWordsOptions Default = new ToWordsOptions(true, "Lira", "Kuruş");
        public bool InsertSpace;
        public string IntPortionText;
        public string DecimalPortionText;
        public ToWordsOptions(bool insertSpace, string intPortionText, string decPortionText)
        {
            InsertSpace = insertSpace;
            IntPortionText = intPortionText;
            DecimalPortionText = decPortionText;
        }
    }

    public sealed class CommentStyle
    {
        public string StartChars { get; }
        public string EndChars { get; }
        public bool MultiLine { get; }

        public CommentStyle(string lineCommentChars)
        {
            if (string.IsNullOrWhiteSpace(lineCommentChars)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(lineCommentChars));
            StartChars = lineCommentChars;
            EndChars = null;
            MultiLine = false;
        }

        public CommentStyle(string start, string end)
        {
            if (string.IsNullOrWhiteSpace(start)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(start));
            if (string.IsNullOrWhiteSpace(end)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(end));
            StartChars = start;
            EndChars = end;
            MultiLine = true;
        }
    }

    public sealed class CommentRange
    {
        public int Start { get; }
        public int End { get; }
        public bool Multiline { get; }

        public CommentRange(int start, int end, bool multiline)
        {
            Start = start;
            End = end;
            Multiline = multiline;
        }

        public bool InRange(int offset) => offset.Between(Start, End);
    }
}
