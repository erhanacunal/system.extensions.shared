﻿using System;
#if !MOBILE
using System.Security.Cryptography; 
#endif
using System.Text;

namespace System.Utils
{
    public static class HashUtils
    {
#if !MOBILE
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            var byteValue = Encoding.UTF8.GetBytes(input);

            var byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        } 
#endif

        public static uint JenkinsHash(string str)
        {
            uint result = 0;
            if (str == null) return result;
            foreach (var chr in str)
            {
                result += chr;
                result += (result << 10);
                result ^= result >> 6;
            }
            result += result << 3;
            result ^= result >> 11;
            result += result << 15;
            return result;
        }

        public static uint[] JenkinsHashes(params string[] strings)
        {
            var result = new uint[strings.Length];
            for (var i = 0; i < strings.Length; i++)
            {
                result[i] = JenkinsHash(strings[i]);
            }
            return result;
        }
    }
}
