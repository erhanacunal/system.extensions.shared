﻿#if REGISTRY
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace System.Utils
{
    public static class RegistryUtils
    {

        public static RegistryKey GetKeyWithValue(string path, bool writable, out string valueName)
        {
            var slashIndex = path.IndexOf('\\');
            valueName = string.Empty;
            if (slashIndex == -1)
                return null;
            var tmp = path.Substring(0, slashIndex); // hive'i al
            path = path.Remove(0, slashIndex + 1); // slash ile beraber sil
            var hive = StringToHive(tmp);
            var lastSlashIndex = path.LastIndexOf('\\');
            if (lastSlashIndex == -1 || lastSlashIndex == slashIndex)
                return null;
            valueName = path.Substring(lastSlashIndex + 1); // değer adı
            path = path.Remove(lastSlashIndex); // değer adını sil
            var root = GetBaseKey(hive);
            var subKey = root.OpenSubKey(path, writable);
            if (subKey == null && writable)
                subKey = root.CreateSubKey(path);
            return subKey;
        }

        public static RegistryKey GetKeyPath(string path, bool writable)
        {
            var parts = path.Split('\\');
            var root = GetBaseKey(StringToHive(parts[0]));
            var subKey = root.OpenSubKey(string.Join("\\", parts.Skip(1)), writable);
            if (subKey == null && writable)
                subKey = root.CreateSubKey(path);
            return subKey;
        }

        public static object ReadValuePath(string path, object defaultValue)
        {
            using (var subKey = GetKeyWithValue(path, false, out var valueName))
                return (subKey?.GetValue(valueName, defaultValue)) ?? defaultValue;
        }

        public static object ReadValue(string subKeyName, string valueName, object defaultValue, RegistryHive hive = RegistryHive.CurrentUser)
        {
            var root = GetBaseKey(hive);
            var subKey = root.OpenSubKey(subKeyName);
            if (subKey != null)
                using (subKey)
                    return subKey.GetValue(valueName, defaultValue);
            return defaultValue;
        }

        public static T ReadValue<T>(string subKeyName, string valueName, T defaultValue, RegistryHive hive = RegistryHive.CurrentUser)
        {
            var root = GetBaseKey(hive);
            using (var subKey = root.OpenSubKey(subKeyName))
            {
                if (subKey != null)
                    return TypeUtils.CoerceValue<T>(subKey.GetValue(valueName, defaultValue));
                return defaultValue;
            }
        }

        public static void WriteValue(string subKeyName, string valueName, object value, RegistryHive hive = RegistryHive.CurrentUser)
        {
            var root = GetBaseKey(hive);
            using (var subKey = root.CreateSubKey(subKeyName))
            {
                var vk = RegistryValueKind.String;
                if (value is long || value is ulong)
                    vk = RegistryValueKind.QWord;
                else if (value is int || value is uint)
                    vk = RegistryValueKind.DWord;
                subKey?.SetValue(valueName, value, vk);
            }
        }

        static void ToXmlInternal(XElement root, RegistryKey subKey)
        {
            foreach (var valueName in subKey.GetValueNames())
            {
                var kind = subKey.GetValueKind(valueName);
                var valueElement = new XElement("Value", new XAttribute("Name", valueName),
                    new XAttribute("Kind", kind.ToString()));
                root.Add(valueElement);
                valueElement.Value = ValueToString(subKey, valueName);
            }
            foreach (var keyName in subKey.GetSubKeyNames())
            {
                var subKeyElement = new XElement("Key",
                    new XAttribute("Name", keyName));
                root.Add(subKeyElement);
                ToXmlInternal(subKeyElement, subKey.OpenSubKey(keyName));
            }
        }

        public static XElement ToXml([NotNull] string subKeyName, RegistryHive hive = RegistryHive.CurrentUser)
        {
            if (string.IsNullOrWhiteSpace(subKeyName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(subKeyName));
            var result = new XElement("Key",
                new XAttribute("Name", System.IO.Path.GetFileName(subKeyName)),
                new XAttribute("Path", System.IO.Path.GetDirectoryName(subKeyName)),
                new XAttribute("Hive", hive));
            var root = RegistryKey.OpenBaseKey(hive, RegistryView.Default);
            using (var subKey = root.OpenSubKey(subKeyName))
            {
                if (subKey != null)
                {
                    ToXmlInternal(result, subKey);
                }
            }
            return result;
        }

        static void FromXmlInternal(XElement root, RegistryKey parent)
        {
            using (var subKey = parent.CreateSubKey(root.AttrValue("Name")))
            {
                foreach (var valueXml in root.Elements("Value"))
                {
                    var valueName = valueXml.AttrValue("Name");
                    var kind = valueXml.AttrValue("Kind").ToEnum<RegistryValueKind>();
                    switch (kind)
                    {
                        case RegistryValueKind.Binary:
                            subKey?.SetValue(valueName, valueXml.Value.ToByteArray(), kind);
                            break;
                        case RegistryValueKind.DWord:
                            subKey?.SetValue(valueName, Convert.ToInt32(valueXml.Value));
                            break;
                        case RegistryValueKind.ExpandString:
                        case RegistryValueKind.String:
                        case RegistryValueKind.MultiString:
                            subKey?.SetValue(valueName, valueXml.Value);
                            break;
                        case RegistryValueKind.QWord:
                            subKey?.SetValue(valueName, Convert.ToInt64(valueXml.Value));
                            break;
                        case RegistryValueKind.Unknown:
                        case RegistryValueKind.None:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                foreach (var subKeyXml in root.Elements("Key"))
                {
                    FromXmlInternal(subKeyXml, subKey);
                }
            }
        }

        public static void FromXml(XElement root)
        {
            var hive = root.AttrValue("Hive", "CurrentUser").ToEnum<RegistryHive>();
            var rootKey = RegistryKey.OpenBaseKey(hive, RegistryView.Default);
            using (var subKey = rootKey.OpenSubKey(root.AttrValue("Path"), true))
            {
                if (subKey != null)
                {
                    FromXmlInternal(root, subKey);
                }
            }
        }

        public static void FromXml(string xml)
        {
            FromXml(XElement.Parse(xml));
        }

        public static RegistryHive StringToHive(string shortName)
        {
            switch (shortName)
            {
                case "HKLM":
                    return RegistryHive.LocalMachine;
                case "HKCR":
                    return RegistryHive.ClassesRoot;
                case "HKU":
                    return RegistryHive.Users;
                case "HKCC":
                    return RegistryHive.CurrentConfig;
                default:
                    return RegistryHive.CurrentUser;
            }
        }

        public static string HiveToString(RegistryHive hive)
        {
            switch (hive)
            {
                case RegistryHive.ClassesRoot:
                    return "HKCR";
                case RegistryHive.CurrentConfig:
                    return "HKCC";
                case RegistryHive.CurrentUser:
                    return "HKCU";
                case RegistryHive.LocalMachine:
                    return "HKLM";
                case RegistryHive.Users:
                    return "HKU";
                case RegistryHive.PerformanceData:
                    return "PD";
                case RegistryHive.DynData:
                    return "DD";
                default:
                    throw new NotSupportedException();
            }
        }

        public static RegistryKey GetBaseKey(RegistryHive hive)
        {
            switch (hive)
            {
                case RegistryHive.ClassesRoot:
                    return Registry.ClassesRoot;
                case RegistryHive.CurrentUser:
                    return Registry.CurrentUser;
                case RegistryHive.LocalMachine:
                    return Registry.LocalMachine;
                case RegistryHive.Users:
                    return Registry.Users;
                case RegistryHive.PerformanceData:
                    return Registry.PerformanceData;
                case RegistryHive.CurrentConfig:
                    return Registry.CurrentConfig;
                case RegistryHive.DynData:
                    return Registry.PerformanceData;
                default:
                    throw new NotSupportedException(hive + "");
            }
        }

        public static string ValueToString(this RegistryKey key, string name)
        {
            var result = string.Empty;
            var value = key.GetValue(name, DBNull.Value);
            if (value == DBNull.Value) return result;
            var kind = key.GetValueKind(name);
            result = kind == RegistryValueKind.Binary ? ((byte[])value).ToHex() : value.ToString();
            return result;
        }

        public static object StringToValue(string value, RegistryValueKind kind)
        {
            switch (kind)
            {
                case RegistryValueKind.Binary:
                    return value.ToByteArray();
                case RegistryValueKind.DWord:
                    return Convert.ToInt32(value);
                case RegistryValueKind.QWord:
                    return Convert.ToInt64(value);
                case RegistryValueKind.String:
                case RegistryValueKind.ExpandString:
                case RegistryValueKind.MultiString:
                    return value;
                case RegistryValueKind.Unknown:
                case RegistryValueKind.None:
                    return null;
                default:
                    return value;
            }
        }

        public static bool HasValue(this RegistryKey key, string name)
        {
            return key.GetValueNames().Contains(name);
        }

        public static bool HasKey(this RegistryKey key, string name)
        {
            return key.GetSubKeyNames().Contains(name);
        }

        public static void GetNamesValues<T>(string subKeyName, IDictionary<string, T> targetDictionary, RegistryHive hive = RegistryHive.CurrentUser)
        {
            var root = GetBaseKey(hive);
            using (var subKey = root.OpenSubKey(subKeyName))
            {
                if (subKey != null)
                {
                    foreach (var name in subKey.GetValueNames())
                    {
                        targetDictionary.Add(name, TypeUtils.CoerceValue<T>(subKey.GetValue(name, default(T))));
                    }
                }
            }
        }

        public static void SetNamesValues<T>(string subKeyName, IDictionary<string, T> sourceDictionary, bool deleteBeforeAdding = true, RegistryHive hive = RegistryHive.CurrentUser)
        {
            var root = GetBaseKey(hive);
            using (var subKey = root.CreateSubKey(subKeyName))
            {
                if (deleteBeforeAdding)
                {
                    var valueNames = subKey?.GetValueNames();
                    if (valueNames != null)
                    {
                        foreach (var name in valueNames)
                            subKey.DeleteValue(name);
                    }
                }

                foreach (var kv in sourceDictionary)
                    subKey?.SetValue(kv.Key, kv.Value);
            }
        }

    }
}

#endif