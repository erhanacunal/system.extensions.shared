﻿#if COREFULL || COREDATA
using System.Data;
using Microsoft.Win32;

namespace System.Utils
{
    public static class ConnStrUtils
    {

        public static int MinPoolSizeOption = 3;

        public static string ReplaceValuesFromRegistry(CtSqlConnectionStringBuilder sb, string key)
        {
            using (var reg = Registry.CurrentUser.OpenSubKey(key))
            {
                sb.MinPoolSize = 3;
                if (reg == null) return sb.ConnectionString;
                var val = (string)reg.GetValue("OverrideConnectionString", "");
                var parts = val.Split('=', ';');
                if (parts.Length % 2 == 0)
                {
                    for (int i = 0; i < parts.Length; i += 2)
                    {
                        switch (parts[i].ToLowerInvariant())
                        {
                            case "initial catalog":
                                sb.InitialCatalog = parts[i + 1];
                                break;
                            case "data source":
                                sb.DataSource = parts[i + 1];
                                break;
                            case "integrated security":
                                sb.IntegratedSecurity = true;
                                break;
                            case "user id":
                                sb.IntegratedSecurity = false;
                                sb.UserID = parts[i + 1];
                                break;
                            case "password":
                                sb.Password = parts[i + 1];
                                break;
                        }
                    }
                }
                return sb.ConnectionString;
            }
        }

        public static CtSqlConnectionStringBuilder Parse(string connectionString, string catalog = "", string appName = "")
        {
            var sb = new CtSqlConnectionStringBuilder(connectionString, catalog, appName);
            sb.MinPoolSize = MinPoolSizeOption;
            return sb;
        }
    }
}

#endif