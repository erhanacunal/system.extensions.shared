﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Xml.Linq;
using System.Reflection;
#if JSON
using Newtonsoft.Json.Linq;
#endif

namespace System.Utils
{
    public static class TypeUtils
    {

        /// <summary>
        /// Varsayılan en küçük tarihi verir, 01-01-1900
        /// </summary>
        public static readonly DateTime DefaultDateTime = new DateTime(1900, 1, 1);
        public static readonly object DefaultInt16Box = (short)0;
        public static readonly object DefaultInt32Box = 0;
        public static readonly object DefaultInt64Box = (long)0;
        public static readonly object DefaultUInt16Box = (ushort)0;
        public static readonly object DefaultUInt32Box = (uint)0;
        public static readonly object DefaultUInt64Box = (ulong)0;
        public static readonly object DefaultDecimalBox = 0m;
        public static readonly object DefaultDoubleBox = 0d;
        public static readonly object DefaultFloatBox = 0f;
        public static readonly object DefaultBooleanBox = false;
        public static readonly object DefaultCharBox = char.MinValue;
        public static readonly object DefaultByteBox = (byte)0;

        public static readonly object EmptyString;

        static Tuple<Type, string>[] aliases;

        internal static Tuple<Type, string>[] Aliases => aliases ?? (aliases = new[] {
            new Tuple<Type, string>( typeof (byte), "byte" ),
            new Tuple<Type, string>( typeof (sbyte), "sbyte" ),
            new Tuple<Type, string>( typeof (short), "short" ),
            new Tuple<Type, string>( typeof (ushort), "ushort" ),
            new Tuple<Type, string>( typeof (int), "int" ),
            new Tuple<Type, string>( typeof (uint), "uint" ),
            new Tuple<Type, string>( typeof (long), "long" ),
            new Tuple<Type, string>( typeof (ulong), "ulong" ),
            new Tuple<Type, string>( typeof (float), "float" ),
            new Tuple<Type, string>( typeof (double), "double" ),
            new Tuple<Type, string>( typeof (decimal), "decimal" ),
            new Tuple<Type, string>( typeof (object), "object" ),
            new Tuple<Type, string>( typeof (bool), "bool" ),
            new Tuple<Type, string>( typeof (char), "char" ),
            new Tuple<Type, string>( typeof (string), "string" ),
            new Tuple<Type, string>( typeof (void), "void" )
        });

        static SerializeOptions defaultOptions;

        internal static SerializeOptions DefaultOptions => defaultOptions ?? (defaultOptions = new SerializeOptions());

        static TypeUtils()
        {

            EmptyString = string.Empty;

        }

        public static string GetTypeAlias(Type type)
        {
            foreach (var tuple in Aliases)
            {
                if (tuple.Item1 == type)
                    return tuple.Item2;
            }
            return type.FullName;
        }

        public static Type GetTypeFromAlias(string alias)
        {
            foreach (var tuple in Aliases)
            {
                if (tuple.Item2.Equals(alias))
                    return tuple.Item1;
            }
            return Type.GetType(alias);
        }

        public static Type MakeNullable(Type type)
        {
            if (type == typeof(string)) return type;
            var nullable = typeof(Nullable<>);
            return nullable.MakeGenericType(type);
        }

        public static T GetDefaultOf<T>()
        {
            var df = GetDefaultOfType(typeof(T));
            return (T)df;
        }

        public static object GetDefaultOfType(Type type)
        {
            if (type == typeof(string))
                return EmptyString;
            if (type == typeof(DateTime))
                return DefaultDateTime;
            if (type == typeof(short))
                return DefaultInt16Box;
            if (type == typeof(int))
                return DefaultInt32Box;
            if (type == typeof(long))
                return DefaultInt64Box;
            if (type == typeof(ushort))
                return DefaultUInt16Box;
            if (type == typeof(uint))
                return DefaultUInt32Box;
            if (type == typeof(ulong))
                return DefaultUInt64Box;
            if (type == typeof(decimal))
                return DefaultDecimalBox;
            if (type == typeof(double))
                return DefaultDoubleBox;
            if (type == typeof(float))
                return DefaultFloatBox;
            if (type == typeof(bool))
                return DefaultBooleanBox;
            if (type == typeof(char))
                return DefaultCharBox;
            return type == typeof(byte) ? DefaultByteBox : null;
        }

        public static bool IsNumericValue(object value)
        {
            if (value == null || value == DBNull.Value) return false;
            return value is byte ||
                   value is sbyte ||
                   value is ushort ||
                   value is short ||
                    value is uint ||
                   value is int ||
                   value is ulong ||
                   value is long ||
                   value is double ||
                   value is float ||
                   value is decimal;
        }

        public static bool IsIntegerNumberValue(object value)
        {
            if (value == null || value == DBNull.Value) return false;
            return value is byte ||
                   value is sbyte ||
                   value is ushort ||
                   value is short ||
                   value is uint ||
                   value is int ||
                   value is ulong ||
                   value is long;
        }

        public static bool IsFloatingNumberValue(object value)
        {
            if (value == null || value == DBNull.Value) return false;
            return value is double ||
                   value is float ||
                   value is decimal;
        }

        public static bool IsNaN(object value)
        {
            if (value == null || value == DBNull.Value) return false;
            switch (value)
            {
                case double d:
                    return double.IsNaN(d);
                case float f:
                    return float.IsNaN(f);
            }
            return false;
        }

        public static bool IsNumericType(Type type)
        {
            return type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(decimal);
        }

        public static bool IsFloatingNumberType(Type type)
        {
            return type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(decimal);
        }

        public static bool IsIntegerNumberType(Type type)
        {
            return type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong);
        }

        public static bool IsSizable(Type type)
        {
            return type.IsArray || type == typeof(string);
        }

        public static object ParseValue(string val)
        {
            if (int.TryParse(val, out var ival))
                return ival;
            if (long.TryParse(val, out var lval))
                return lval;
            if (double.TryParse(val, out var dval))
                return dval;
            if (bool.TryParse(val, out var bval))
                return bval;
            return val;
        }

        public static object ConvertTo(Type targetType, object value, int maxLength = -1)
        {
            if ((value == null) || (ReferenceEquals(value, DBNull.Value)))
                return GetDefaultOfType(targetType);
            if (targetType == typeof(object))
                return value;
            var valueType = value.GetType();
            object converted = null;
            if (targetType == valueType)
                return value;
            if (targetType.IsEnum)
            {
                var underlyingType = Enum.GetUnderlyingType(targetType);
                if (valueType == underlyingType)
                    return value;
                if (valueType == typeof(string))
#if SILVERLIGHT
                    return Enum.Parse( targetType, (string)value, true );
#else
                    return Enum.Parse(targetType, (string)value, true);
#endif
                var enumTc = Type.GetTypeCode(underlyingType);
                switch (enumTc)
                {
                    case TypeCode.Byte:
                        converted = CoerceValue<byte>(value);
                        break;
                    case TypeCode.Int16:
                        converted = CoerceValue<short>(value);
                        break;
                    case TypeCode.Int32:
                        converted = CoerceValue<int>(value);
                        break;
                    case TypeCode.Int64:
                        converted = CoerceValue<long>(value);
                        break;
                    case TypeCode.SByte:
                        converted = CoerceValue<sbyte>(value);
                        break;
                    case TypeCode.UInt16:
                        converted = CoerceValue<ushort>(value);
                        break;
                    case TypeCode.UInt32:
                        converted = CoerceValue<uint>(value);
                        break;
                    case TypeCode.UInt64:
                        converted = CoerceValue<ulong>(value);
                        break;
                    default:
                        throw new InvalidCastException(targetType.Name + " cannot be converted from " +
                                                        valueType.Name);
                }
                return converted;
            }
            else
            {
                var underlyingType = Nullable.GetUnderlyingType(targetType);
                if (underlyingType != null)
                {
                    converted = valueType == underlyingType ? value : ConvertTo(underlyingType, value, maxLength);
                }
                else
                {
                    var targetTypeCode = Type.GetTypeCode(targetType);
                    var sourceTypeCode = Type.GetTypeCode(valueType);
                    byte[] byteVal;
                    var str = string.Empty;
                    switch (targetTypeCode)
                    {
#region String
                        case TypeCode.String:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    str = ((bool)value).ToString();
                                    break;
                                case TypeCode.Byte:
                                    str = ((byte)value).ToString();
                                    break;
                                case TypeCode.Char:
                                    str = ((char)value).ToString();
                                    break;
                                case TypeCode.DBNull:
                                    str = null;
                                    break;
                                case TypeCode.DateTime:
                                    str = ((DateTime)value).ToString(CultureInfo.InvariantCulture);
                                    break;
                                case TypeCode.Decimal:
                                    str = ((decimal)value).ToString(CultureInfo.InvariantCulture);
                                    break;
                                case TypeCode.Double:
                                    str = ((double)value).ToString(CultureInfo.InvariantCulture);
                                    break;
                                case TypeCode.Empty:
                                    str = value.ToString();
                                    break;
                                case TypeCode.Int16:
                                    str = ((short)value).ToString();
                                    break;
                                case TypeCode.Int32:
                                    str = ((int)value).ToString();
                                    break;
                                case TypeCode.Int64:
                                    str = ((long)value).ToString();
                                    break;
                                case TypeCode.Object:
                                    str = Convert.ToString(value);
                                    break;
                                case TypeCode.SByte:
                                    str = ((sbyte)value).ToString();
                                    break;
                                case TypeCode.Single:
                                    str = ((float)value).ToString(CultureInfo.InvariantCulture);
                                    break;
                                case TypeCode.String:
                                    str = (string)value;
                                    break;
                                case TypeCode.UInt16:
                                    str = ((ushort)value).ToString();
                                    break;
                                case TypeCode.UInt32:
                                    str = ((uint)value).ToString();
                                    break;
                                case TypeCode.UInt64:
                                    str = ((ulong)value).ToString();
                                    break;
                            }
                            if (maxLength > 0 && !string.IsNullOrEmpty(str))
                                return str.Length > maxLength ? str.Substring(0, maxLength) : str;
                            return str;
#endregion
#region Int16
                        case TypeCode.Int16:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? (short)1 : (short)0;
                                    break;
                                case TypeCode.Byte:
                                    converted = ((byte)value);
                                    break;
                                case TypeCode.Char:
                                    converted = (short)((char)value);
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = 0;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (short)((decimal)value);
                                    break;
                                case TypeCode.Double:
                                    converted = (short)((double)value);
                                    break;
                                case TypeCode.Empty:
                                    converted = 0;
                                    break;
                                case TypeCode.Int16:
                                    converted = ((short)value);
                                    break;
                                case TypeCode.Int32:
                                    converted = (short)((int)value);
                                    break;
                                case TypeCode.Int64:
                                    converted = (short)((long)value);
                                    break;
                                case TypeCode.SByte:
                                    converted = ((sbyte)value);
                                    break;
                                case TypeCode.Single:
                                    converted = (short)((float)value);
                                    break;
                                case TypeCode.String:
                                    converted = short.TryParse((string)value, out var parsed) ? parsed : (short)0;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (short)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (short)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (short)(ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 2)
                                            converted = BitConverter.ToInt16(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region UInt16
                        case TypeCode.UInt16:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? (ushort)1 : (ushort)0;
                                    break;
                                case TypeCode.Byte:
                                    converted = (ushort)((byte)value);
                                    break;
                                case TypeCode.Char:
                                    converted = (ushort)((char)value);
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = 0;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (ushort)((decimal)value);
                                    break;
                                case TypeCode.Double:
                                    converted = (ushort)((double)value);
                                    break;
                                case TypeCode.Empty:
                                    converted = 0;
                                    break;
                                case TypeCode.Int16:
                                    converted = ((ushort)value);
                                    break;
                                case TypeCode.Int32:
                                    converted = (ushort)((int)value);
                                    break;
                                case TypeCode.Int64:
                                    converted = (ushort)((long)value);
                                    break;
                                case TypeCode.SByte:
                                    converted = (ushort)((sbyte)value);
                                    break;
                                case TypeCode.Single:
                                    converted = (ushort)((Single)value);
                                    break;
                                case TypeCode.String:
                                    converted = ushort.TryParse((string)value, out var parsed) ? parsed : (ushort)0;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (ushort)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (ushort)(ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 2)
                                            converted = BitConverter.ToUInt16(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region Int32
                        case TypeCode.Int32:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1 : 0;
                                    break;
                                case TypeCode.Byte:
                                    converted = (int)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (int)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = 0;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (int)(decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (int)(double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0;
                                    break;
                                case TypeCode.Int16:
                                    converted = (int)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (int)(long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (int)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (int)(float)value;
                                    break;
                                case TypeCode.String:
                                    converted = int.TryParse((string)value, out var parsed) ? parsed : 0;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (int)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (int)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (int)(ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 4)
                                            converted = BitConverter.ToInt32(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region UInt32
                        case TypeCode.UInt32:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1U : 0U;
                                    break;
                                case TypeCode.Byte:
                                    converted = (uint)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (uint)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0U;
                                    break;
                                case TypeCode.DateTime:
                                    converted = 0U;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (uint)(decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (uint)(double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0U;
                                    break;
                                case TypeCode.Int16:
                                    converted = (uint)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (uint)(int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (uint)(long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (uint)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (uint)(Single)value;
                                    break;
                                case TypeCode.String:
                                    converted = uint.TryParse((string)value, out var parsed) ? parsed : (uint)0;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (uint)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (uint)(ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 4)
                                            converted = BitConverter.ToUInt32(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region Int64
                        case TypeCode.Int64:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1L : 0L;
                                    break;
                                case TypeCode.Byte:
                                    converted = (long)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (long)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0L;
                                    break;
                                case TypeCode.DateTime:
                                    converted = ((DateTime)value).Ticks;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (long)(decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (long)(double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0L;
                                    break;
                                case TypeCode.Int16:
                                    converted = (long)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (long)(int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (long)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (long)(Single)value;
                                    break;
                                case TypeCode.String:
                                    converted = long.TryParse((string)value, out var parsed) ? parsed : 0L;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (long)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (long)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (long)(ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 8)
                                            converted = BitConverter.ToInt64(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region UInt64
                        case TypeCode.UInt64:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1UL : 0UL;
                                    break;
                                case TypeCode.Byte:
                                    converted = (ulong)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (ulong)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0UL;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (ulong)((DateTime)value).Ticks;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (ulong)(decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (ulong)(double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0UL;
                                    break;
                                case TypeCode.Int16:
                                    converted = (ulong)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (ulong)(int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (ulong)(long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (ulong)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (ulong)(Single)value;
                                    break;
                                case TypeCode.String:
                                    converted = ulong.TryParse((string)value, out var parsed) ? parsed : (ulong)0;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (ulong)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (ulong)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (ulong)value;
                                    break;
                                case TypeCode.Object:
                                    if (value is byte[])
                                    {
                                        byteVal = (byte[])value;
                                        if (byteVal.Length >= 8)
                                            converted = BitConverter.ToUInt64(byteVal, 0);
                                    }
                                    break;
                            }
                            return converted;
#endregion
#region Double
                        case TypeCode.Double:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1d : 0d;
                                    break;
                                case TypeCode.Byte:
                                    converted = (double)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (double)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0.0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (double)((DateTime)value).Ticks;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (double)(decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0d;
                                    break;
                                case TypeCode.Int16:
                                    converted = (double)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (double)(int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (double)(long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (double)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (double)(Single)value;
                                    break;
                                case TypeCode.String:
                                    converted = double.TryParse((string)value, out var parsed) ? parsed : 0d;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (double)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (double)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (double)(ulong)value;
                                    break;
                            }
                            return converted;
#endregion
#region Boolean
                        case TypeCode.Boolean:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = (bool)value;
                                    break;
                                case TypeCode.Byte:
                                    converted = ((byte)value) != 0;
                                    break;
                                case TypeCode.Char:
                                    converted = ((char)value) != 0;
                                    break;
                                case TypeCode.DBNull:
                                    converted = false;
                                    break;
                                case TypeCode.DateTime:
                                    converted = !((DateTime)value).Equals(DateTime.MinValue);
                                    break;
                                case TypeCode.Decimal:
                                    converted = ((decimal)value) != 0m;
                                    break;
                                case TypeCode.Double:
                                    converted = Math.Abs((double)value) > 0.001;
                                    break;
                                case TypeCode.Empty:
                                    converted = false;
                                    break;
                                case TypeCode.Int16:
                                    converted = ((short)value) != 0;
                                    break;
                                case TypeCode.Int32:
                                    converted = ((int)value) != 0;
                                    break;
                                case TypeCode.Int64:
                                    converted = ((long)value) != 0L;
                                    break;
                                case TypeCode.SByte:
                                    converted = ((sbyte)value) != 0;
                                    break;
                                case TypeCode.Single:
                                    converted = Math.Abs(((float)value)) > 0.01;
                                    break;
                                case TypeCode.String:
                                    str = (string)value;
                                    bool bl;
                                    converted = bool.TryParse(str, out bl) ? bl : str.Equals("1");
                                    break;
                                case TypeCode.UInt16:
                                    converted = ((ushort)value) != 0;
                                    break;
                                case TypeCode.UInt32:
                                    converted = ((uint)value) != 0;
                                    break;
                                case TypeCode.UInt64:
                                    converted = ((ulong)value) != 0;
                                    break;
                            }
                            return converted;
#endregion
#region DateTime
                        case TypeCode.DateTime:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Byte:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Char:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.DBNull:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (DateTime)value;
                                    break;
                                case TypeCode.Decimal:
                                    converted = new DateTime(((long)((decimal)value)));
                                    break;
                                case TypeCode.Double:
                                    converted = new DateTime(((long)((double)value)));
                                    break;
                                case TypeCode.Empty:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Int16:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Int32:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Int64:
                                    converted = new DateTime(((long)value));
                                    break;
                                case TypeCode.Object:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.SByte:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.Single:
                                    converted = new DateTime(((long)((Single)value)));
                                    break;
                                case TypeCode.String:
                                    converted = Convert.ToDateTime((string)value);
                                    break;
                                case TypeCode.UInt16:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.UInt32:
                                    converted = DateTime.MinValue;
                                    break;
                                case TypeCode.UInt64:
                                    converted = new DateTime(((long)((ulong)value)));
                                    break;
                            }
                            return converted;
#endregion
#region Byte
                        case TypeCode.Byte:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? (byte)1 : (byte)0;
                                    break;
                                case TypeCode.Byte:
                                    converted = ((byte)value);
                                    break;
                                case TypeCode.Char:
                                    converted = (byte)((char)value);
                                    break;
                                case TypeCode.DBNull:
                                    converted = (byte)0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (byte)0;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (byte)((decimal)value);
                                    break;
                                case TypeCode.Double:
                                    converted = (byte)((double)value);
                                    break;
                                case TypeCode.Empty:
                                    converted = (byte)0;
                                    break;
                                case TypeCode.Int16:
                                    converted = (byte)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (byte)((int)value);
                                    break;
                                case TypeCode.Int64:
                                    converted = (byte)((long)value);
                                    break;
                                case TypeCode.SByte:
                                    converted = ((sbyte)value);
                                    break;
                                case TypeCode.Single:
                                    converted = (byte)((Single)value);
                                    break;
                                case TypeCode.String:
                                    if (byte.TryParse((string)value, out var parsed))
                                        converted = parsed;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (byte)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (byte)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (byte)(ulong)value;
                                    break;
                            }
                            return converted;
#endregion
#region Char
                        case TypeCode.Char:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? (char)1 : (char)0;
                                    break;
                                case TypeCode.Byte:
                                    converted = (char)((byte)value);
                                    break;
                                case TypeCode.Char:
                                    converted = ((char)value);
                                    break;
                                case TypeCode.DBNull:
                                    converted = (char)0;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (char)0;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (char)((decimal)value);
                                    break;
                                case TypeCode.Double:
                                    converted = (char)((double)value);
                                    break;
                                case TypeCode.Empty:
                                    converted = (char)0;
                                    break;
                                case TypeCode.Int16:
                                    converted = (char)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (char)((int)value);
                                    break;
                                case TypeCode.Int64:
                                    converted = (char)((long)value);
                                    break;
                                case TypeCode.SByte:
                                    converted = (char)((sbyte)value);
                                    break;
                                case TypeCode.Single:
                                    converted = (char)((Single)value);
                                    break;
                                case TypeCode.String:
                                    converted = Convert.ToChar((string)value);
                                    break;
                                case TypeCode.UInt16:
                                    converted = (char)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (char)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (char)(ulong)value;
                                    break;
                            }
                            return converted;
#endregion
#region Decimal
                        case TypeCode.Decimal:
                            switch (sourceTypeCode)
                            {
                                case TypeCode.Boolean:
                                    converted = ((bool)value) ? 1m : 0m;
                                    break;
                                case TypeCode.Byte:
                                    converted = (decimal)(byte)value;
                                    break;
                                case TypeCode.Char:
                                    converted = (decimal)(char)value;
                                    break;
                                case TypeCode.DBNull:
                                    converted = 0m;
                                    break;
                                case TypeCode.DateTime:
                                    converted = (decimal)((DateTime)value).Ticks;
                                    break;
                                case TypeCode.Decimal:
                                    converted = (decimal)value;
                                    break;
                                case TypeCode.Double:
                                    converted = (decimal)(double)value;
                                    break;
                                case TypeCode.Empty:
                                    converted = 0m;
                                    break;
                                case TypeCode.Int16:
                                    converted = (decimal)(short)value;
                                    break;
                                case TypeCode.Int32:
                                    converted = (decimal)(int)value;
                                    break;
                                case TypeCode.Int64:
                                    converted = (decimal)(long)value;
                                    break;
                                case TypeCode.SByte:
                                    converted = (decimal)(sbyte)value;
                                    break;
                                case TypeCode.Single:
                                    converted = (decimal)(Single)value;
                                    break;
                                case TypeCode.String:
                                    converted = decimal.TryParse((string)value, out var parsed) ? parsed : 0m;
                                    break;
                                case TypeCode.UInt16:
                                    converted = (decimal)(ushort)value;
                                    break;
                                case TypeCode.UInt32:
                                    converted = (decimal)(uint)value;
                                    break;
                                case TypeCode.UInt64:
                                    converted = (decimal)(ulong)value;
                                    break;
                            }
                            return converted;
#endregion
#region Object
                        case TypeCode.Object:
                            if (sourceTypeCode == TypeCode.String)
                            {
                                if (targetType == typeof(Version))
                                    converted = Version.Parse((string)value);
                                else if (targetType == typeof(XElement))
                                    converted = XElement.Parse((string)value);
                                else if (typeof(ObjectValueProvider).IsAssignableFrom(targetType))
                                    converted = Activator.CreateInstance(targetType, value);
#if JSON
                                else if (targetType == typeof(JObject))
                                    converted = JObject.Parse((string)value);
                                else if (targetType == typeof(JArray))
                                    converted = JArray.Parse((string)value);
#endif
                            }
                            break;
#endregion
                    }
                }
            }
            if (converted == null)
                return GetDefaultOfType(targetType);
            return converted;
        }

        public static TTarget CoerceValue<TTarget>(object value, int maxLength = -1)
        {
            if ((value == null) || (ReferenceEquals(value, DBNull.Value)))
                return default(TTarget);
            var converted = ConvertTo(typeof(TTarget), value, maxLength);
            if (converted == null)
                return default(TTarget);
            return (TTarget)converted;
        }

        public static DbType TypeCodeToDbType(TypeCodeEx ex)
        {
            switch (ex)
            {
                case TypeCodeEx.Boolean:
                    return DbType.Boolean;
                case TypeCodeEx.Byte:
                    return DbType.Byte;
                case TypeCodeEx.ByteArray:
                    return DbType.Binary;
                case TypeCodeEx.Char:
                    return DbType.StringFixedLength;
                case TypeCodeEx.DateTime:
                    return DbType.DateTime;
                case TypeCodeEx.Decimal:
                    return DbType.Decimal;
                case TypeCodeEx.Double:
                    return DbType.Double;
                case TypeCodeEx.Int16:
                    return DbType.Int16;
                case TypeCodeEx.Int32:
                    return DbType.Int32;
                case TypeCodeEx.Int64:
                    return DbType.Int64;
                case TypeCodeEx.SByte:
                    return DbType.SByte;
                case TypeCodeEx.Single:
                    return DbType.Single;
                case TypeCodeEx.String:
                    return DbType.String;
                case TypeCodeEx.TimeSpan:
                    return DbType.Time;
                case TypeCodeEx.UInt16:
                    return DbType.UInt16;
                case TypeCodeEx.UInt32:
                    return DbType.UInt32;
                case TypeCodeEx.UInt64:
                    return DbType.UInt64;
                case TypeCodeEx.Type:
                    return DbType.Object;
                default:
                    throw new NotImplementedException();
            }
        }
        public static bool IsAssignableToGenericType(Type givenType, Type genericType)
        {
            var interfaceTypes = givenType.GetInterfaces();

            foreach (var it in interfaceTypes)
                if (it.IsGenericType)
                    if (it.GetGenericTypeDefinition() == genericType) return true;

            var baseType = givenType.BaseType;
            if (baseType == null) return false;

            return baseType.IsGenericType &&
                baseType.GetGenericTypeDefinition() == genericType ||
                IsAssignableToGenericType(baseType, genericType);
        }

        public static List<T> GetList<T>(T obj, bool addToList = false)
        {
            var result = new List<T>();
            if (addToList)
                result.Add(obj);
            return result;
        }

        public static IList<T> GetIList<T>(T obj)
        {
            return null;
        }

        public static Type GetType<T>(T obj)
        {
            return typeof(T);
        }

        public static IDictionary<TKey, TValue> CreateDictionary<TKey, TValue>(TValue anonymousObject)
        {
            return new Dictionary<TKey, TValue>();
        }

        public static string DataReaderMethodNameOfType(Type type)
        {
            var tc = Type.GetTypeCode(type);
            switch (tc)
            {
                case TypeCode.Boolean:
                    return "GetBoolean";
                case TypeCode.Byte:
                    return "GetByte";
                case TypeCode.Char:
                    return "GetChar";
                case TypeCode.DBNull:
                    return "IsDBNull";
                case TypeCode.DateTime:
                    return "GetDateTime";
                case TypeCode.Decimal:
                    return "GetDecimal";
                case TypeCode.Double:
                    return "GetDouble";
                case TypeCode.Empty:
                    return "";
                case TypeCode.Int16:
                    return "GetInt16";
                case TypeCode.Int32:
                    return "GetInt32";
                case TypeCode.Int64:
                    return "GetInt64";
                case TypeCode.Object:
                    return "GetValue";
                case TypeCode.SByte:
                    return "GetInt16";
                case TypeCode.Single:
                    return "GetDouble";
                case TypeCode.String:
                    return "GetString";
                case TypeCode.UInt16:
                    return "GetInt16";
                case TypeCode.UInt32:
                    return "GetInt32";
                case TypeCode.UInt64:
                    return "GetInt64";
                default:
                    throw new NotImplementedException("Type code not implemented -> " + tc);
            }
        }

        public static string GetConvertMethodNameOfType(Type type)
        {
            var tc = Type.GetTypeCode(type);
            switch (tc)
            {
                case TypeCode.Boolean:
                    return "ToBoolean";
                case TypeCode.Byte:
                    return "ToByte";
                case TypeCode.Char:
                    return "ToChar";
                case TypeCode.DateTime:
                    return "ToDateTime";
                case TypeCode.Decimal:
                    return "ToDecimal";
                case TypeCode.Double:
                    return "ToDouble";
                case TypeCode.Int16:
                    return "ToInt16";
                case TypeCode.Int32:
                    return "ToInt32";
                case TypeCode.Int64:
                    return "ToInt64";
                case TypeCode.Object:
                    return "ToValue";
                case TypeCode.SByte:
                    return "ToInt16";
                case TypeCode.Single:
                    return "ToDouble";
                case TypeCode.String:
                    return "ToString";
                case TypeCode.UInt16:
                    return "ToInt16";
                case TypeCode.UInt32:
                    return "ToInt32";
                case TypeCode.UInt64:
                    return "ToInt64";
                default:
                    throw new NotImplementedException("Type code not implemented -> " + tc);
            }
        }

        public static Type GetUnderlyingType(Type type)
        {
            Type result = null;
            if (type.IsEnum)
                result = Enum.GetUnderlyingType(type);
            else if ((type.IsGenericType && !type.IsGenericTypeDefinition) &&
                (type.GetGenericTypeDefinition() == typeof(Nullable<>)))
                result = type.GetGenericArguments()[0];
            return result;
        }

        public static TypeCodeEx GetTypeCodeEx(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            restart:
            var tc = Type.GetTypeCode(type);
            if (tc != TypeCode.Object) return (TypeCodeEx)(int)tc;
            if (type == typeof(byte[]))
                return TypeCodeEx.ByteArray;
            if (type == typeof(string[]))
                return TypeCodeEx.StringArray;
            if (type == typeof(char[]))
                return TypeCodeEx.CharArray;
            if (type == typeof(Guid))
                return TypeCodeEx.Guid;
            if (type == typeof(TimeSpan))
                return TypeCodeEx.TimeSpan;
            if (type.FullName.Equals("System.Type") || type.FullName.Equals("System.RuntimeType"))
                return TypeCodeEx.Type;
            if (type == typeof(object))
                return TypeCodeEx.SystemObject;
            var underlyingType = GetUnderlyingType(type);
            if (underlyingType == null)
            {
                return TypeCodeEx.Object;
            }
            type = underlyingType;
            goto restart;
        }

        public static bool IsPrimitiveType(Type type)
        {
            var code = GetTypeCodeEx(type);
            switch (code)
            {
                case TypeCodeEx.Object:
                case TypeCodeEx.Empty:
                case TypeCodeEx.DBNull:
                case TypeCodeEx.CharArray:
                case TypeCodeEx.StringArray:
                case TypeCodeEx.TimeSpan:
                case TypeCodeEx.ObjectArray:
                case TypeCodeEx.Type:
                    return false;
                case TypeCodeEx.Boolean:
                case TypeCodeEx.Char:
                case TypeCodeEx.SByte:
                case TypeCodeEx.Byte:
                case TypeCodeEx.Int16:
                case TypeCodeEx.UInt16:
                case TypeCodeEx.Int32:
                case TypeCodeEx.UInt32:
                case TypeCodeEx.Int64:
                case TypeCodeEx.UInt64:
                case TypeCodeEx.Single:
                case TypeCodeEx.Double:
                case TypeCodeEx.Decimal:
                case TypeCodeEx.DateTime:
                case TypeCodeEx.String:
                case TypeCodeEx.Guid:
                case TypeCodeEx.ByteArray:
                case TypeCodeEx.SystemObject:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static Type GetTypeFromTypeCodeEx(TypeCodeEx tc)
        {
            switch (tc)
            {
                case TypeCodeEx.Empty:
                    return null;
                case TypeCodeEx.SystemObject:
                case TypeCodeEx.Object:
                    return typeof(object);
                case TypeCodeEx.DBNull:
                    return typeof(DBNull);
                case TypeCodeEx.Boolean:
                    return typeof(bool);
                case TypeCodeEx.Char:
                    return typeof(char);
                case TypeCodeEx.SByte:
                    return typeof(sbyte);
                case TypeCodeEx.Byte:
                    return typeof(byte);
                case TypeCodeEx.Int16:
                    return typeof(short);
                case TypeCodeEx.UInt16:
                    return typeof(ushort);
                case TypeCodeEx.Int32:
                    return typeof(int);
                case TypeCodeEx.UInt32:
                    return typeof(uint);
                case TypeCodeEx.Int64:
                    return typeof(long);
                case TypeCodeEx.UInt64:
                    return typeof(ulong);
                case TypeCodeEx.Single:
                    return typeof(Single);
                case TypeCodeEx.Double:
                    return typeof(double);
                case TypeCodeEx.Decimal:
                    return typeof(decimal);
                case TypeCodeEx.DateTime:
                    return typeof(DateTime);
                case TypeCodeEx.String:
                    return typeof(string);
                case TypeCodeEx.ByteArray:
                    return typeof(byte[]);
                case TypeCodeEx.CharArray:
                    return typeof(char[]);
                case TypeCodeEx.StringArray:
                    return typeof(string[]);
                case TypeCodeEx.Guid:
                    return typeof(Guid);
                case TypeCodeEx.Type:
                    return typeof(Type);
                default:
                    return null;
            }
        }

        public static bool IsDbTypeString(this DbType type)
        {
            switch (type)
            {
                case DbType.String:
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.StringFixedLength:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeInteger(this DbType type)
        {
            switch (type)
            {
                case DbType.Int16:
                case DbType.Int32:
                case DbType.Int64:
                case DbType.UInt16:
                case DbType.UInt32:
                case DbType.UInt64:
                case DbType.Byte:
                case DbType.SByte:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeNumber(this DbType type)
        {
            switch (type)
            {
                case DbType.Int16:
                case DbType.Int32:
                case DbType.Int64:
                case DbType.UInt16:
                case DbType.UInt32:
                case DbType.UInt64:
                case DbType.Byte:
                case DbType.SByte:

                case DbType.Currency:
                case DbType.Decimal:
                case DbType.Double:
                case DbType.Single:
                case DbType.VarNumeric:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeNumeric(this DbType type)
        {
            switch (type)
            {
                case DbType.Currency:
                case DbType.Decimal:
                case DbType.Double:
                case DbType.Single:
                case DbType.VarNumeric:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeUnicodeString(this DbType type)
        {
            switch (type)
            {
                case DbType.StringFixedLength:
                case DbType.String:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeAnsiString(this DbType type)
        {
            switch (type)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsDbTypeBinary(this DbType type)
        {
            return type == DbType.Binary;
        }

        public sealed class SerializeOptions
        {

            public bool DateTimeAsDouble { get; }

#if !PocketPC
            public SerializeOptions(bool datetimeAsDouble = true)
            {
#else
            public SerializeOptions( bool datetimeAsDouble ) {
#endif
                DateTimeAsDouble = datetimeAsDouble;
            }

        }
    }

    public enum TypeCodeEx
    {
        Empty = TypeCode.Empty,
        Object = TypeCode.Object,
        // ReSharper disable once InconsistentNaming
        DBNull = TypeCode.DBNull,
        Boolean = TypeCode.Boolean,
        Char = TypeCode.Char,
        SByte = TypeCode.SByte,
        Byte = TypeCode.Byte,
        Int16 = TypeCode.Int16,
        UInt16 = TypeCode.UInt16,
        Int32 = TypeCode.Int32,
        UInt32 = TypeCode.UInt32,
        Int64 = TypeCode.Int64,
        UInt64 = TypeCode.UInt64,
        Single = TypeCode.Single,
        Double = TypeCode.Double,
        Decimal = TypeCode.Decimal,
        DateTime = TypeCode.DateTime,
        String = TypeCode.String,

        ByteArray = 0x40,
        CharArray = 0x41,
        StringArray = 0x42,
        Guid = 0x43,
        TimeSpan = 0x44,
        ObjectArray = 0x45,
        Type = 0x46,
        SystemObject = 0x47
    }
}

