﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Custom.Models;

namespace System.Utils
{
    public static class EnumUtils
    {
        public static void GetEnumFriendlyNames<T>(ICollection<NameValuePair<string, string>> targetList)
        {
            var type = typeof(T);
            if (!type.IsEnum) return;
            foreach (var field in type.GetFields())
            {
                if (field.IsSpecialName) continue;
                var displayAttribute = field.GetCustomAttributes(false).OfType<DisplayAttribute>().FirstOrDefault();
                var name = field.Name;
                if (displayAttribute != null)
                    name = displayAttribute.GetName();
                targetList.Add(new NameValuePair<string, string>(name, field.Name));
            }
        }
    }
}
