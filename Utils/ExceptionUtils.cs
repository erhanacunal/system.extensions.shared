﻿using System;
using System.Net;
#if !MOBILE && !MSSQLTVF
using System.Net.Http; 
#endif
using System.Reflection;
using JetBrains.Annotations;

namespace System.Utils {
    public static class ExceptionUtils {
#if !MOBILE
        /// <summary>
        /// Sets a flag on an <see cref="T:System.Exception"/> so that all the stack trace information is preserved 
        /// when the exception is re-thrown.
        /// </summary>
        /// <remarks>This is useful because "throw" removes information, such as the original stack frame.</remarks>
        /// <see href="http://weblogs.asp.net/fmarguerie/archive/2008/01/02/rethrowing-exceptions-and-preserving-the-full-call-stack-trace.aspx"/>
        public static void PreserveStackTrace(Exception ex)
        {
            MethodInfo preserveStackTrace = typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);
            preserveStackTrace.Invoke(ex, null);
        }

#if !MSSQLTVF
        public static bool IsTransientHttpError([NotNull] HttpRequestException ex)
        {
            if (ex == null) throw new ArgumentNullException(nameof(ex));
            if (!(ex.InnerException is WebException wex)) return false;
            return IsTransientHttpError(wex);
        }

        public static bool IsTransientHttpError([NotNull] WebException wex)
        {
            if (wex == null) throw new ArgumentNullException(nameof(wex));
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (wex.Status)
            {
                case WebExceptionStatus.ConnectFailure:
                case WebExceptionStatus.Timeout:
                case WebExceptionStatus.ConnectionClosed:
                case WebExceptionStatus.PipelineFailure:
                case WebExceptionStatus.KeepAliveFailure:
                case WebExceptionStatus.ReceiveFailure:
                case WebExceptionStatus.RequestCanceled:
                case WebExceptionStatus.SecureChannelFailure:
                case WebExceptionStatus.SendFailure:
                case WebExceptionStatus.Pending:
                case WebExceptionStatus.NameResolutionFailure:
                case WebExceptionStatus.MessageLengthLimitExceeded:
                    return true;
                default:
                    return false;
            }

        } 
#endif
#endif
    }
}
