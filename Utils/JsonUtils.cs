﻿#if JSON
using System;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;

namespace System.Utils
{
    public static class JsonUtils
    {

        [NotNull]
        public static JToken GetPropertyValue([NotNull] JObject obj, string path)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            var parts = path.Split('.');
            JToken propValue = null;
            switch (parts.Length)
            {
                case 0:
                    return JValue.CreateUndefined();
                case 1:
                    if (!obj.TryGetValue(parts[0], out propValue))
                        return JValue.CreateUndefined();
                    return propValue;
                default:
                    JToken token = obj;
                    
                    var i = 0;
                    while (i < parts.Length)
                    {
                        if (token == null || token.Type == JTokenType.Null)
                            return token ?? JValue.CreateNull();
                        if (token.Type == JTokenType.Undefined)
                            return token;
                        var part = parts[i++];
                        if (token is JObject jobj)
                        {
                            if (StringUtils.Instance.TryGetEnclosedString(part, "[", "]", out var es))
                            {
                                part = part.Substring(0, es.StartIndex);
                                if (!jobj.TryGetValue(part, out propValue))
                                {
                                    token = JValue.CreateUndefined();
                                    continue;
                                }
                                token = propValue;
                                if (!(token is JArray jarr))
                                {
                                    token = JValue.CreateUndefined();
                                    continue;
                                }
                                var arrayIndex = es.Text.ToInt32(-1);
                                if (arrayIndex == -1)
                                    throw new ArgumentException("Invalid array index -> " + es.Text);
                                if (arrayIndex >= 0 && arrayIndex < jarr.Count)
                                    token = jarr[arrayIndex];
                                else
                                    token = JValue.CreateUndefined();
                                continue;
                            }
                            if (!jobj.TryGetValue(part, out propValue))
                                return JValue.CreateUndefined();
                            token = propValue;
                        }
                        else if (token is JArray jarr)
                        {
                            if (!StringUtils.Instance.TryGetEnclosedString(part, "[", "]", out var es))
                                throw new ArgumentException("[] (brackets) are expected");
                            var arrayIndex = es.Text.ToInt32(-1);
                            if (arrayIndex == -1)
                                throw new ArgumentException("Invalid array index -> " + es.Text);
                            if (arrayIndex >= 0 && arrayIndex < jarr.Count)
                                token = jarr[arrayIndex];
                            else
                                token = JValue.CreateUndefined();
                        }
                        else
                            throw new InvalidOperationException("JObject or JArray are expected");
                    }
                    return token;
            }
        }

        public static bool IsNumber(JTokenType type)
        {
            switch (type)
            {
                case JTokenType.Integer:
                case JTokenType.Float:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsPrimitive(JTokenType type)
        {
            switch (type)
            {
                case JTokenType.String:
                case JTokenType.Integer:
                case JTokenType.Float:
                case JTokenType.Boolean:
                case JTokenType.Date:
                case JTokenType.TimeSpan:
                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.Bytes:
                    return true;
                default:
                    return false;
            }
        }

        public static Type GetClrTypeFrom(JTokenType type)
        {
            switch (type)
            {
                case JTokenType.String:
                    return typeof(string);
                case JTokenType.Integer:
                    return typeof(long);
                case JTokenType.Float:
                    return typeof(decimal);
                case JTokenType.Boolean:
                    return typeof(bool);
                case JTokenType.Date:
                    return typeof(DateTime);
                case JTokenType.TimeSpan:
                    return typeof(TimeSpan);
                case JTokenType.Guid:
                    return typeof(Guid);
                case JTokenType.Uri:
                    return typeof(Uri);
                case JTokenType.Bytes:
                    return typeof(byte[]);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }
    }
}
#endif