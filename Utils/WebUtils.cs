﻿using System.Text;
#if JSON
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization; 
#endif

namespace System.Utils
{
    public static class WebUtils
    {
        public static readonly UTF8Encoding DefaultEncoding = new UTF8Encoding(false, false);
#if JSON
        static JsonSerializerSettings camelCasePropertyNamesContractResolver;

        public static JsonSerializerSettings CamelCasePropertyNamesContractResolver => camelCasePropertyNamesContractResolver ?? (camelCasePropertyNamesContractResolver = new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() }); 
#endif
    }
}
