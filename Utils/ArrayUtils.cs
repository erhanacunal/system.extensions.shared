﻿using System;
using System.Collections.Generic;

namespace System.Utils
{
    public static class ArrayUtils
    {
        public static void CheckArray<T>(ref T[] source, int count)
        {
            if (source == null)
                source = new T[4];
            else if (source.Length == count)
            {
                Array.Resize(ref source, checked(count * 2));
            }
        }

        public static T[] Concat<T>(T[] src1, T[] src2)
        {
            if (src1 == null && src2 != null)
                return src2;
            if (src2 == null && src1 != null)
                return src1;
            if (src1 == null && src2 == null)
                throw new InvalidOperationException("src1 or src2 must be provided");
            if (src1.Length == 0)
                return src2;
            if (src2.Length == 0)
                return src1;
            var result = new T[src1.Length + src2.Length];
            if (src1.Length > 0)
                Array.Copy(src1, 0, result, 0, src1.Length);
            if (src2.Length > 0)
                Array.Copy(src2, 0, result, src1.Length, src2.Length);
            return result;
        }

        public static void ArrayToDictionary<TK, TV>(object[] args, IDictionary<TK, TV> targetDictionary)
        {
            if ((args.Length % 2) != 0)
                throw new ArgumentOutOfRangeException(nameof(args));
            for (var i = 0; i < args.Length; i += 2)
            {
                targetDictionary.Add(TypeUtils.CoerceValue<TK>(args[i]), TypeUtils.CoerceValue<TV>(args[i + 1]));
            }
        }

        public static IDictionary<TK,TV> ArrayToDictionary<TK, TV>(params object[] args)
        {
            if ((args.Length % 2) != 0)
                throw new ArgumentOutOfRangeException(nameof(args));
            var result = new Dictionary<TK, TV>();
            ArrayToDictionary(args, result);
            return result;
            
        }
    }
}
