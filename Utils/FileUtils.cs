﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using JetBrains.Annotations;
#if !USETEMPFOLDER
#if !NETCOREAPP2_0
using System.Web;
using System.Web.Hosting;
#endif
#endif

namespace System.Utils
{
    public static class FileUtils
    {

        static string workingDirectory;

        public static string WorkingDirectory
        {
            get
            {
#if !NETCOREAPP2_0
                if (workingDirectory.Length == 0)
                {
#if !USETEMPFOLDER
                    workingDirectory = HostingEnvironment.IsHosted
                                                ? HttpContext.Current.Server.MapPath("~")
                                                : AppDomain.CurrentDomain.BaseDirectory;
#else
                    workingDirectory = Path.GetTempPath();
#endif
                }
#endif
                return workingDirectory;
            }
#if NETCOREAPP2_0
            set { workingDirectory = value; }
#endif
        }
        static FileUtils()
        {
            workingDirectory = string.Empty;
        }

#if !NETCOREAPP2_0
        public static bool UserHasDirectoryAccessRights(string path, FileSystemRights accessRights)
        {
            var isInRoleWithAccess = false;
            try
            {
                var di = new DirectoryInfo(path);
                var acl = di.GetAccessControl();
                var rules = acl.GetAccessRules(true, true, typeof(NTAccount));

                var currentUser = WindowsIdentity.GetCurrent();
                var principal = new WindowsPrincipal(currentUser);
                foreach (AuthorizationRule rule in rules)
                {
                    var fsAccessRule = rule as FileSystemAccessRule;
                    if (fsAccessRule == null || (fsAccessRule.FileSystemRights & accessRights) == 0)
                        continue;
                    var ntAccount = rule.IdentityReference as NTAccount;
                    if (ntAccount == null)
                        continue;

                    if (principal.IsInRole(ntAccount.Value))
                    {
                        if (fsAccessRule.AccessControlType == AccessControlType.Deny)
                            return false;
                        isInRoleWithAccess = true;
                    }

                }
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            return isInRoleWithAccess;
        }

        public static bool SetDirectoryRightsFor(string accountName, string path, FileSystemRights accessRights, AccessControlType act)
        {
            try
            {
                var dirInfo = new DirectoryInfo(path);
                var acl = dirInfo.GetAccessControl();
                var rules = acl.GetAccessRules(true, true, typeof(NTAccount));
                var found = false;
                var changed = false;
                foreach (AuthorizationRule rule in rules)
                {
                    if ((rule.PropagationFlags & PropagationFlags.InheritOnly) == PropagationFlags.InheritOnly) continue;
                    if (!rule.IdentityReference.Value.Equals(accountName, StringComparison.CurrentCultureIgnoreCase))
                        continue;
                    found = true;
                    if (!(rule is FileSystemAccessRule fsar) || (fsar.FileSystemRights & accessRights) == accessRights)
                        continue;
                    acl.SetAccessRule(new FileSystemAccessRule(
                        accountName,
                        accessRights,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                        PropagationFlags.None,
                        act));
                    changed = true;

                }
                if (!found)
                {
                    acl.SetAccessRule(new FileSystemAccessRule(
                        accountName,
                        accessRights,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                        PropagationFlags.None, act));
                    changed = true;
                }
                if (changed)
                    dirInfo.SetAccessControl(acl);
                return true;
            }
            catch
            {
                return false;
            }
        }
#endif

        static void GetFilesCore(string currentDirectory, string path, List<GetFilesItem> targetList)
        {
            var dirInfo = new DirectoryInfo(path);
            foreach (var entry in dirInfo.EnumerateFileSystemInfos())
            {
                var relativePath = currentDirectory.Length == 0 ? entry.Name : string.Concat(currentDirectory, "\\", entry.Name);
                if ((entry.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    GetFilesCore(relativePath, string.Concat(path, "\\", entry.Name), targetList);
                }
                else
                    targetList.Add(new GetFilesItem(
                        relativePath,
                        entry.FullName, 0, entry.LastWriteTime));
            }
        }

        public static IList<GetFilesItem> GetFiles(string path)
        {
            var result = new List<GetFilesItem>();
            GetFilesCore(string.Empty, path, result);
            return result;
        }
        /// <summary>
        /// Dizin içerisindekileri alt nesneler olarak verir
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IList<FileSystemEntry> GetEntries(string path)
        {
            var result = new List<FileSystemEntry>();
            GetEntriesCore(null, path);
            return result;

            void GetEntriesCore(FileSystemEntry parent, string subPath)
            {
                var dirInfo = new DirectoryInfo(subPath);
                foreach (var entry in dirInfo.EnumerateFileSystemInfos())
                {
                    var isDirectory = (entry.Attributes & FileAttributes.Directory) == FileAttributes.Directory;
                    var newEntry = new FileSystemEntry(parent, entry.Name,
                        entry.FullName, isDirectory, isDirectory ? 0 : ((FileInfo)entry).Length, entry.LastWriteTime);
                    if (parent != null)
                        parent.SubEntries.Add(newEntry);
                    else
                        result.Add(newEntry);
                    if (isDirectory)
                        GetEntriesCore(newEntry, newEntry.FullPath);
                }
            }
        }

        public static string EncodeBase64(string fileName)
        {
            return Convert.ToBase64String(File.ReadAllBytes(fileName));
        }

        public static string CombinePath(string other)
        {
            return Path.Combine(WorkingDirectory, other);
        }

        public static string CombinePath(params string[] other)
        {
            var paths = new string[other.Length + 1];
            paths[0] = WorkingDirectory;
            Array.Copy(other, 0, paths, 1, other.Length);
            return Path.Combine(paths);
        }

        public static bool IsFileExists(string fileName)
        {
            return File.Exists(CombinePath(fileName));
        }

        public static bool TryResolveFile(string fileName, out string fullFileName)
        {
            fullFileName = CombinePath(fileName);
            if (!File.Exists(fullFileName))
                fullFileName = null;
            return fullFileName != null;
        }

        public static bool IsDirectoryExists(string dirName)
        {
            return Directory.Exists(CombinePath(dirName));
        }

        public static void CreateDirectory(string dirName)
        {
            var fullPath = CombinePath(dirName);
            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);
        }

        public static IList<PathPartInfo> AnalyzePath([NotNull] string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (!Path.IsPathRooted(path))
                throw new ArgumentException("Path must be rooted");
            var result = new List<PathPartInfo>();
            var parts = path.Split(Path.DirectorySeparatorChar);
            var sb = new StringBuilder();
            var i = 0;
            foreach (var part in parts)
            {
                if (i++ > 0)
                    sb.Append(Path.DirectorySeparatorChar);
                sb.Append(part);
                if (part.EndsWithChar(Path.VolumeSeparatorChar))
                    continue;
                var full = sb.ToString();
                result.Add(new PathPartInfo(part, full, Directory.Exists(full)));
            }
            return result;
        }

    }

    public struct GetFilesItem
    {
        public string RelativePath { get; }
        public string FullPath { get; }
        public long Size { get; }
        public DateTime LastModified { get; }

        public GetFilesItem(string relativePath, string fullPath, long size, DateTime lastModified)
        {
            RelativePath = relativePath;
            FullPath = fullPath;
            Size = size;
            LastModified = lastModified;
        }
    }

    public sealed class FileSystemEntry
    {
        public FileSystemEntry Parent { get; }
        public string Name { get; }
        public string FullPath { get; }
        public bool IsDirectory { get; }
        public List<FileSystemEntry> SubEntries { get; } = new List<FileSystemEntry>();
        public long Size { get; }
        public DateTime LastModified { get; }

        public FileSystemEntry(FileSystemEntry parent, string name, string fullPath, bool isDirectory, long size, DateTime lastModified)
        {
            Parent = parent;
            Name = name;
            FullPath = fullPath;
            IsDirectory = isDirectory;
            Size = size;
            LastModified = lastModified;
        }
    }

    public struct PathPartInfo
    {
        public bool Exists { get; }
        public string Name { get; }
        public string FullName { get; }

        public PathPartInfo(string name, string fullName, bool exists)
        {
            Name = name;
            FullName = fullName;
            Exists = exists;
        }
    }
}

#endif