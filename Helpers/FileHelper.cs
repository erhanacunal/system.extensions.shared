﻿#if !MOBILE
using System;
using System.IO;
#if !NETCOREAPP2_0
using System.Security.AccessControl;
using System.Security.Principal;
#endif

namespace System.IO.Helpers
{
    public sealed class FileHelper
    {
        public string WorkingDirectory { get; private set; }

        public FileHelper(string workingDirectory)
        {
            WorkingDirectory = workingDirectory;
        }

        public void ChangeDirectory(string subDirectory)
        {
            if (!IsDirectoryExists(subDirectory))
                CreateDirectory(subDirectory);
            WorkingDirectory = CombinePath(subDirectory);
        }

#if !NETCOREAPP2_0
        public bool UserHasDirectoryAccessRights(string directory, FileSystemRights accessRights)
        {
            var isInRoleWithAccess = false;
            try
            {
                var di = new DirectoryInfo(CombinePath(directory));
                var acl = di.GetAccessControl();
                var rules = acl.GetAccessRules(true, true, typeof(NTAccount));

                var currentUser = WindowsIdentity.GetCurrent();
                var principal = new WindowsPrincipal(currentUser);
                foreach (AuthorizationRule rule in rules)
                {
                    var fsAccessRule = rule as FileSystemAccessRule;
                    if (fsAccessRule == null || (fsAccessRule.FileSystemRights & accessRights) == 0)
                        continue;
                    var ntAccount = rule.IdentityReference as NTAccount;
                    if (ntAccount == null)
                        continue;

                    if (principal.IsInRole(ntAccount.Value))
                    {
                        if (fsAccessRule.AccessControlType == AccessControlType.Deny)
                            return false;
                        isInRoleWithAccess = true;
                    }

                }
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            return isInRoleWithAccess;
        }

        public bool TrySetDirectoryRightsFor(string accountName, string directory, FileSystemRights accessRights, AccessControlType act)
        {
            try
            {
                var dirInfo = new DirectoryInfo(CombinePath(directory));
                var acl = dirInfo.GetAccessControl();
                var rules = acl.GetAccessRules(true, true, typeof(NTAccount));
                var found = false;
                var changed = false;
                foreach (AuthorizationRule rule in rules)
                {
                    if ((rule.PropagationFlags & PropagationFlags.InheritOnly) == PropagationFlags.InheritOnly) continue;
                    if (!rule.IdentityReference.Value.Equals(accountName, StringComparison.CurrentCultureIgnoreCase))
                        continue;
                    found = true;
                    if (!(rule is FileSystemAccessRule fsar) || (fsar.FileSystemRights & accessRights) == accessRights)
                        continue;
                    acl.SetAccessRule(new FileSystemAccessRule(
                        accountName,
                        accessRights,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                        PropagationFlags.None,
                        act));
                    changed = true;

                }
                if (!found)
                {
                    acl.SetAccessRule(new FileSystemAccessRule(
                        accountName,
                        accessRights,
                        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                        PropagationFlags.None, act));
                    changed = true;
                }
                if (changed)
                    dirInfo.SetAccessControl(acl);
                return true;
            }
            catch
            {
                return false;
            }
        }
#endif

        public string CombinePath(string other)
        {
            if (string.IsNullOrWhiteSpace(other)) return WorkingDirectory;
            return Path.Combine(WorkingDirectory, other);
        }

        public string CombinePath(params string[] other)
        {
            if (other.Length == 0) return WorkingDirectory;
            var paths = new string[other.Length + 1];
            paths[0] = WorkingDirectory;
            Array.Copy(other, 0, paths, 1, other.Length);
            return Path.Combine(paths);
        }

        public DirectoryInfo GetDirectoryInfo(string directory = null)
        {
            return new DirectoryInfo(CombinePath(directory));
        }

        public bool IsFileExists(string fileName)
        {
            return File.Exists(CombinePath(fileName));
        }

        public bool TryResolveFile(string fileName, out string fullFileName)
        {
            fullFileName = CombinePath(fileName);
            if (!File.Exists(fullFileName))
                fullFileName = null;
            return fullFileName != null;
        }

        public bool TryResolveFolder(string folderName, out string fullPath)
        {
            fullPath = CombinePath(folderName);
            if (!Directory.Exists(fullPath))
                fullPath = null;
            return fullPath != null;
        }

        public bool IsDirectoryExists(string dirName)
        {
            return Directory.Exists(CombinePath(dirName));
        }

        public void CreateDirectory(string dirName)
        {
            var fullPath = CombinePath(dirName);
            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);
        }

        public string GetFileContentAsString(string fileName)
        {
            if (TryResolveFile(fileName, out string fullFileName))
                return File.ReadAllText(fullFileName);
            throw new InvalidOperationException("File not found -> " + fileName);
        }

        public FileHelper GetSubFileHelper(string subDirectory)
        {
            return new FileHelper(CombinePath(subDirectory));
        }
    }
}

#endif