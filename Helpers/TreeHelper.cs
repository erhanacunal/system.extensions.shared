﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Helpers
{
    public class TreeHelper
    {
        
        


        public abstract class SubNodeBuilder
        {
            

            protected internal abstract void Build();
        }

        public class SubNodeBuilder<TParent, TNode, TKey> : SubNodeBuilder where TNode : class
        {
            readonly List<SubNodeBuilder> childBuilders = new List<SubNodeBuilder>();

            internal SubNodeBuilder(TParent helper)
            {

            }

            protected internal override void Build()
            {
                
            }
        }
    }
}
