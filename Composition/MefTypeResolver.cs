﻿#if COREFULL || MEF
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
    public static class MefTypeResolver
    {
        static bool isInitialized;

        public const string EXPORT_TYPE_IDENTITY = "ExportTypeIdentity";

        public static CompositionContainer CompositionContainer { get; private set; }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Initialize(MefOptions opt)
        {
            if (isInitialized) return;
            isInitialized = true;
            var catalog = opt.AggregateCatalog ?? new AggregateCatalog();

            if (opt.UseDirectoryCatalog)
            {
                var dirCatalog = new DirectoryCatalog(opt.DirectoryCatalogFolder, opt.DirectoryCatalogExtension);
                dirCatalog.Refresh();
                catalog.Catalogs.Add(dirCatalog);
            }

            foreach (var asm in opt.OtherAssemblies)
            {
                catalog.Catalogs.Add(new AssemblyCatalog(asm));
            }

            CompositionContainer = new CompositionContainer(catalog, true);
            var compositionBatch = new CompositionBatch();
            if (opt.AddCompositionContainerToExports)
                compositionBatch.AddExportedValue(CompositionContainer);
            if (opt.AddAggregateCatalogToExports)
                compositionBatch.AddExportedValue(catalog);

            foreach (var part in opt.OtherPartsToExport)
            {
                compositionBatch.AddPart(part);
            }

            CompositionContainer.Compose(compositionBatch);
        }

        public static T Resolve<T>()
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValue<T>();
        }

        public static T ResolveOrDefault<T>()
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValueOrDefault<T>();
        }

        static void EnsureInitialized()
        {
            if (!isInitialized)
                throw new InvalidOperationException("Not initialized");
        }

        public static T Resolve<T>(string contractName)
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValue<T>(contractName);
        }

        public static T ResolveOrDefault<T>(string contractName)
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValueOrDefault<T>(contractName);
        }

        public static IEnumerable<T> GetExportedValues<T>()
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValues<T>();
        }

        public static IEnumerable<T> GetExportedValues<T>(string contractName)
        {
            EnsureInitialized();
            return CompositionContainer.GetExportedValues<T>(contractName);
        }

        public static IEnumerable<ExportDefinition> GetExportDefinitions<T>()
        {
            EnsureInitialized();
            var t = typeof(T);
            foreach (var part in CompositionContainer.Catalog.Parts)
            {
                foreach (var exportDefinition in part.ExportDefinitions)
                {
                    if (exportDefinition.Metadata.ContainsKey("ExportTypeIdentity") &&
                        exportDefinition.Metadata["ExportTypeIdentity"].Equals(t.FullName))
                        yield return exportDefinition;
                }
            }
        }

        public static IEnumerable<Lazy<TType, TMetadataView>> GetExports<TType, TMetadataView>()
        {
            EnsureInitialized();
            return CompositionContainer.GetExports<TType, TMetadataView>();
        }

        public static IEnumerable<Lazy<TType, TMetadataView>> GetExports<TType, TMetadataView>(string contractName)
        {
            EnsureInitialized();
            return CompositionContainer.GetExports<TType, TMetadataView>(contractName);
        }
    }

    public class MefOptions
    {
        public bool UseDirectoryCatalog { get; set; }
        public string DirectoryCatalogFolder { get; set; }
        public string DirectoryCatalogExtension { get; set; }
        public bool AddCompositionContainerToExports { get; set; }
        public List<object> OtherPartsToExport { get; } = new List<object>();
        public List<Assembly> OtherAssemblies { get; } = new List<Assembly>();
        public bool AddAggregateCatalogToExports { get; set; }
        public AggregateCatalog AggregateCatalog { get; set; }

        public MefOptions()
        {
            AddCompositionContainerToExports = true;
            AddAggregateCatalogToExports = false;
        }

        public static MefOptions Create() => new MefOptions();

        public MefOptions SetUseDirectoryCatalog(string path, string ext)
        {
            UseDirectoryCatalog = true;
            DirectoryCatalogFolder = path;
            DirectoryCatalogExtension = ext;
            return this;
        }

        public MefOptions SetAddCompositionContainerToExports(bool value = true)
        {
            AddCompositionContainerToExports = value;
            return this;
        }

        public MefOptions AddOtherParts(params object[] parts)
        {
            OtherPartsToExport.AddRange(parts);
            return this;
        }

        public MefOptions AddAssemblies(params Assembly[] assemblies)
        {
            OtherAssemblies.AddRange(assemblies);
            return this;
        }

        public MefOptions AddAssembliesFromTypes(params Type[] types)
        {
            foreach (var type in types)
                OtherAssemblies.Add(type.Assembly);
            return this;
        }

    }
}

#endif