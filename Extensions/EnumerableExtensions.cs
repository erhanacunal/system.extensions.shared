﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Utils;

namespace System.Extensions
{
    public static class EnumerableExtensions
    {
#if !MOBILE
        /// <summary>
        /// Verilen nesne sıralamasından verilen property isimlerini içeren bir datatable üretir. 
        /// </summary>
        /// <typeparam name="T">Sıralamanın eleman tipi</typeparam>
        /// <param name="instance">Sıralamanın örneği</param>
        /// <param name="tableName">DataTable'la verilecek isim</param>
        /// <param name="propertyNames">Direk property ismi veya property ismi = kolon adı şeklinde olmalıdır. örn: ID veya ID=Value gibi</param>
        /// <returns>Verilen property bilgilerine göre oluşturulmuş datatable</returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> instance, string tableName, params string[] propertyNames) where T : class
        {
            return ToDataTable(instance, TypeAccessor.CreateOrGet<T>(), tableName, propertyNames);
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> instance, TypeAccessor accessor, string tableName, IEnumerable<string> propertyNames) where T : class
        {
            var propertyArray = propertyNames as string[] ?? propertyNames.ToArray();
            if (propertyArray.Length == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(propertyNames));
            var result = new DataTable(tableName);
            var propertyAccessors = new PropertyAccessor[propertyArray.Length];
            for (var i = 0; i < propertyArray.Length; i++)
            {
                var name = propertyArray[i];
                if (!StringUtils.Instance.TryGetNameValue(name, out var propertyName, out var columnName))
                    propertyName = columnName = name;
                var propertyAccessor = propertyAccessors[i] = accessor.FindProperty(propertyName);
                result.Columns.Add(columnName, propertyAccessor.Property.PropertyType);
            }
            foreach (var item in instance)
            {
                var row = new object[propertyArray.Length];
                for (var i = 0; i < propertyAccessors.Length; i++)
                {
                    var propertyAccessor = propertyAccessors[i];
                    row[i] = propertyAccessor.Getter.GetValue(item);
                }
                result.Rows.Add(row);
            }
            return result;
        }
        /// <summary>
        /// Verilen nesne sıralamasından verilen property'nin değerlerinden bir <see cref="DataTable"/> üretir.
        /// </summary>
        /// <typeparam name="T">Sıralamanın eleman tipi</typeparam>
        /// <param name="instance">Sırlamanın örneği</param>
        /// <param name="tableName">DataTable'la verilecek isim</param>
        /// <param name="propertyName">Direk property ismi veya property ismi = kolon adı şeklinde olmalıdır. örn: ID veya ID=Value gibi</param>
        /// <param name="distinct">Aynı değerleri içerip içermeyeceğini ayarlar</param>
        /// <returns>Verilen property'ye göre oluşturulmuş <see cref="DataTable"/></returns>
        public static DataTable ToDataTableSingle<T>(this IEnumerable<T> instance, string tableName, string propertyName, bool distinct = false) where T : class
        {
            var result = new DataTable(tableName);
            var accessor = TypeAccessor.CreateOrGet(typeof(T));
            if (!StringUtils.Instance.TryGetNameValue(propertyName, out var actualName, out var columnName))
                actualName = columnName = propertyName;
            var propertyAccessor = accessor.FindProperty(actualName);
            result.Columns.Add(columnName, propertyAccessor.Property.PropertyType);
            if (distinct)
            {
                var distinctSet = new HashSet<object>();
                foreach (var item in instance)
                {
                    var value = propertyAccessor.Getter.GetValue(item);
                    if (distinctSet.Contains(value)) continue;
                    result.Rows.Add(value);
                    distinctSet.Add(value);
                }
            }
            else foreach (var item in instance)
                    result.Rows.Add(propertyAccessor.Getter.GetValue(item));
            return result;
        }

        public static DataTable ToSingleColumnDataTable<T>(this IEnumerable<T> instance, string tableName,
            string columnName)
        {
            var result = new DataTable(tableName);
            result.Columns.Add(columnName, typeof(T));
            // örnek null ise boş tablo döndür
            if (instance == null) return result;
            foreach (var item in instance)
                result.Rows.Add(item);
            return result;
        } 
#endif

        public static IEnumerable<T> Descendants<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> childSelector)
        {
            var stack = new Stack<T>();
            foreach (var item in source)
            {
                stack.Push(item);
                while (stack.Count > 0)
                {
                    T current = stack.Pop();
                    yield return current;
                    foreach (var subItem in childSelector(current))
                    {
                        stack.Push(subItem);
                    }
                }
            }
        }

        public static IEnumerable<IList<T>> Partition<T>(this IEnumerable<T> src, int num)
        {
            using (var enu = src.GetEnumerator())
            {
                while (true)
                {
                    var result = new List<T>(num);
                    for (var i = 0; i < num; i++)
                    {
                        if (!enu.MoveNext())
                        {
                            if (i > 0) yield return result;
                            yield break;
                        }
                        result.Add(enu.Current);
                    }
                    yield return result;
                }
            }
        }

        public static List<T> ToTree<T, TKey>(this IEnumerable<T> instance, Func<T, TKey> keySelector, Func<T, TKey> parentSelector, Func<T, IList<T>> itemsSelector,
            TKey defaultKey = default(TKey))
        {
            if (instance == null) throw new ArgumentNullException(nameof(instance));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
            if (itemsSelector == null) throw new ArgumentNullException(nameof(itemsSelector));
            var lookup = instance.ToDictionary(keySelector, _ => _);
            var result = new List<T>();
            foreach (var value in lookup.Values)
            {
                var parentKey = parentSelector(value);
                if (Equals(parentKey, defaultKey))
                    result.Add(value);
                else if (lookup.TryGetValue(parentKey, out var item))
                    itemsSelector(item).Add(value);
                else
                    result.Add(value);
            }
            return result;
        }
        /// <summary>
        /// Temel bir sınıftan türemiş liste öğelerini ağaç şekline çevirir
        /// </summary>
        /// <typeparam name="T">Türemiş sınıf</typeparam>
        /// <typeparam name="TKey">Anahtar property tipi</typeparam>
        /// <typeparam name="TBase">Temel sınıf</typeparam>
        /// <param name="instance">Örnek</param>
        /// <param name="keySelector">Anahtar değere erişici</param>
        /// <param name="parentSelector">Üst anahtar değere erişici</param>
        /// <param name="itemsSelector">Eşleşen öğelerin atılacağı liste erişici</param>
        /// <param name="defaultKey">İlk anahtar değeri</param>
        /// <returns>Ağaç şekline getirilmiş liste</returns>
        public static List<T> ToTree2<T, TKey, TBase>(this IEnumerable<T> instance, Func<T, TKey> keySelector, Func<T, TKey> parentSelector, Func<T, IList<TBase>> itemsSelector,
            TKey defaultKey = default(TKey)) where T : TBase
        {
            if (instance == null) throw new ArgumentNullException(nameof(instance));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
            if (itemsSelector == null) throw new ArgumentNullException(nameof(itemsSelector));
            var lookup = instance.ToDictionary(keySelector, _ => _);
            var result = new List<T>();
            foreach (var value in lookup.Values)
            {
                var parentKey = parentSelector(value);
                if (Equals(parentKey, defaultKey))
                    result.Add(value);
                else if (lookup.TryGetValue(parentKey, out T item))
                    itemsSelector(item).Add(value);
                else
                    result.Add(value);
            }
            return result;
        }
    }
}
