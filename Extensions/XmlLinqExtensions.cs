﻿
using System.Collections.Generic;
// ReSharper disable once CheckNamespace
namespace System.Xml.Linq {
    public static class XmlLinqExtensions {

        public static string AttrValue( this XElement element, XName name, string defaultValue = null ) {
            var attr = element.Attribute( name );
            if ( attr != null )
                return attr.Value;
            return defaultValue ?? string.Empty;
        }
                
        public static T? AttrEnum<T>( this XElement element, XName name, T? defaultValue = null ) where T : struct {
            var attr = element.Attribute( name );
            if ( attr != null )
                return attr.Value.ToEnum<T>();
            return defaultValue;
        }

        public static string ElementValue( this XElement element, XName name, string defaultValue = null ) {
            var el = element.Element( name );
            if ( el != null )
                return el.Value;
            return defaultValue ?? string.Empty;
        }

        public static XElement NewElement( this XElement element, XName name, params object[] content ) {
            var _new = new XElement( name, content );
            element.Add( _new );
            return _new;
        }

        public static bool TryGetElement( this XElement element, XName name, out XElement value ) {
            value = element.Element( name );
            return value != null;
        }

        public static bool TryGetAttribute( this XElement element, XName name, out XAttribute value ) {
            value = element.Attribute( name );
            return value != null;
        }
        
        public static void RemoveElement( this XElement root, XName name ) {
            var el = root.Element( name );
            el?.Remove();
        }

        public static void RemoveAttribute( this XElement root, XName name ) {
            var el = root.Attribute( name );
            el?.Remove();
        }
        /// <summary>
        /// Verilen yoldaki en son elementin değerini döndürür örn: cac:PartyScheme/cbc:Name
        /// </summary>
        /// <param name="root"></param>
        /// <param name="path">Yol</param>
        /// <param name="namespaceTable">Namespace tablosu</param>
        /// <param name="value">Bulamadığında döndürülecek varsayılan değer</param>
        /// <returns></returns>
        public static bool ElementValuePath( this XElement root, string path,
            IDictionary<string, XNamespace> namespaceTable, out string value ) {
            if ( ElementPath( root, path, namespaceTable, out var found ) ) {
                value = found.Value;
                return true;
            }
            value = string.Empty;
            return false;
        }

        /// <summary>
        /// Verilen yoldaki en son elementi döndürür örn: cac:PartyScheme/cbc:Name
        /// </summary>
        /// <param name="root"></param>
        /// <param name="path">Yol</param>
        /// <param name="namespaceTable">Namespace tablosu</param>
        /// <param name="element">Bulamadığında döndürülecek varsayılan değer</param>
        /// <returns></returns>
        public static bool ElementPath( this XElement root, string path,
            IDictionary<string, XNamespace> namespaceTable,
            out XElement element ) {
            var parts = path.Split( '/' );
            var current = root;
            foreach ( var part in parts ) {
                var colonIndex = part.IndexOf( ':' );
                if ( colonIndex == -1 ) {
                    current = current.Element( part );
                }
                else {
                    var nsAlias = part.Substring( 0, colonIndex );
                    var ns = namespaceTable[nsAlias];
                    current = current.Element( ns + part.Substring( colonIndex + 1 ) );
                }
                if ( current == null ) {
                    element = null;
                    return false;
                }
            }
            element = current;
            return true;
        }

        public static XElement GenerateElementsFrom<T>( this XElement root, IEnumerable<T> source, Func<T, XElement> generator ) {
            foreach ( var item in source ) {
                var element = generator( item );
                if ( element != null )
                    root.Add( element );
            }
            return root;
        }
    }
}
