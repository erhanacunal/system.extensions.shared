﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Extensions;
using JetBrains.Annotations;

public static class StringExtensions
{
    /// <summary>
    /// Allows case insensitive checks
    /// </summary>
    public static bool Contains(this string source, string toCheck, StringComparison comp)
    {
        return source.IndexOf(toCheck, comp) >= 0;
    }

    public static void ThrowIfEmpty(this string value, string name)
    {
        if (string.IsNullOrEmpty(value?.Trim()))
            throw new Exception($"{name} alanını boş geçemezsiniz.");
    }

#if !MAYA
    /// <summary>
    /// MD5 hash string value.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string MD5Hash(this string source)
    {
        return CryptographyExtension.MD5Sifrele(source);
    }
#endif

    /// <summary>
    /// Eğer gönderilen string değer int, float veya 
    /// decimal tipinde ise number() formatlı geri dönderilecektir.
    /// </summary>
    /// <example>
    /// value.ToNumberFormat(System.Globalization.CultureInfo.CurrentCulture) şeklinde kullanabilirsiniz.
    /// </example>
    /// <param name="source">Kaynak string</param>
    /// <param name="culture">
    /// Formatlamanın yapılacağı kültürü girmelisiniz eğer systemdeki 
    /// o anki kültürü almak isteseniz System.Globalization.CultureInfo.CurrentCulture 
    /// veya Thread.CurrentThread.CultureInfo şeklinde erişebilirsiniz.
    /// </param>
    /// <param name="format">
    /// Formatlamanın yapılacağı sayısal format girilir varsayılanı {0:N} formatıdır.
    /// </param>
    /// <returns>
    /// Geriye formatlama yapılır ise formatlanmış string eğer yapılamaz
    /// ise girilmiş olan değer olduğu gibi geri döndürülür.
    /// </returns>
    public static string ToNumberFormat(this string source, CultureInfo culture, string format = "{0:N}")
    {
        return decimal.TryParse(source, out var value) ? value.ToString("n", culture) : source;
    }

    /// <summary>
    /// &#304; şeklinde olan html kodunu normal karaktere dönüştürür.
    /// </summary>
    /// <param name="source"></param>
    /// <returns>Geriye html kodlarından arındırılmış string döner.</returns>
    /// <example>
    /// &#304; karakterini İ, &#305; ı v.b html karakterlerini normal karaktere çevirir.
    /// </example>
    public static string HtmlCodeToString(this string source)
    {
        return source.Replace("&#304;", "İ")
                     .Replace("&#305;", "ı")
                     .Replace("&#214;", "Ö")
                     .Replace("&#246;", "ö")
                     .Replace("&#220;", "Ü")
                     .Replace("&#252;", "ü")
                     .Replace("&#199;", "Ç")
                     .Replace("&#231;", "ç")
                     .Replace("&#286;", "Ğ")
                     .Replace("&#287;", "ğ")
                     .Replace("&#350;", "Ş")
                     .Replace("&#351;", "ş");
    }

    /// <summary>
    /// Türkçe karakterleri html koduna dönüştürür.
    /// </summary>
    /// <param name="source"></param>
    /// <returns>Geriye türkçe karakterlerden arındırılmış html kodu döner.</returns>
    /// <example>
    /// İ karakterini &#304;, ı karakterini &#305; v.b türkçe karakterlerini html koda çevirir.
    /// </example>
    public static string TrCharToHtmlCode(this string source)
    {
        return source.Replace("İ", "&#304;")
                     .Replace("ı", "&#305;")
                     .Replace("Ö", "&#214;")
                     .Replace("ö", "&#246;")
                     .Replace("Ü", "&#220;")
                     .Replace("ü", "&#252;")
                     .Replace("Ç", "&#199;")
                     .Replace("ç", "&#231;")
                     .Replace("Ğ", "&#286;")
                     .Replace("ğ", "&#287;")
                     .Replace("Ş", "&#350;")
                     .Replace("ş", "&#351;");
    }

    /// <summary>
    /// Türkçe karakterleri temizler.
    /// </summary>
    /// <param name="source"></param>
    /// <returns>Geriye türkçe karakterlerden arındırılmış string döner.</returns>
    /// <example>
    /// 
    /// </example>
    public static string TrCharClean(this string source)
    {
        return source.Replace("İ", "I")
                     .Replace("ı", "i")
                     .Replace("Ö", "O")
                     .Replace("ö", "o")
                     .Replace("Ü", "U")
                     .Replace("ü", "u")
                     .Replace("Ç", "C")
                     .Replace("ç", "c")
                     .Replace("Ğ", "G")
                     .Replace("ğ", "g")
                     .Replace("Ş", "S")
                     .Replace("ş", "s");
    }

    public static string ToEnglishCharset(this string source)
    {
        var builder = new StringBuilder(source.Length);
        foreach (var ch in source)
        {
            if (!TryConvertChar(ch, builder))
                builder.Append(ch); // çeviremediklerini de ekle
        }
        return builder.ToString();
    }

    static bool TryConvertChar(char ch, StringBuilder builder)
    {
        switch (ch)
        {
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
                builder.Append(ch);
                return true;
            case 'Ş':
                builder.Append('S');
                return true;
            case 'Ç':
                builder.Append('C');
                return true;
            case 'Ü':
                builder.Append('U');
                return true;
            case 'Ğ':
                builder.Append('G');
                return true;
            case 'Ö':
                builder.Append('O');
                return true;
            case 'İ':
                builder.Append('I');
                return true;
            case 'ş':
                builder.Append('s');
                return true;
            case 'ç':
                builder.Append('c');
                return true;
            case 'ü':
                builder.Append('u');
                return true;
            case 'ğ':
                builder.Append('g');
                return true;
            case 'ö':
                builder.Append('o');
                return true;
            case 'ı':
                builder.Append('i');
                return true;
            default:
                return false;
        }
    }

    /// <summary>
    /// string.Format yazmak yerine 'test'.Format() yazabilmek için hazırlanmıştır
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    /// <example>
    /// 
    /// </example>
    [StringFormatMethod("args")]
    public static string FormatEx(this string source, params object[] args)
    {
        return string.Format(source, args);
    }

    public static string ReplaceAll(this string source, string newValue, params string[] oldValues)
    {
        foreach (var item in oldValues)
            source = source.Replace(item, newValue);
        return source;
    }

    /// <summary>
    /// Verilen başlangıç indeksinden bir tanımlayıcı bulmaya çalışır.
    /// </summary>
    /// <param name="source">Tanımlayıcının aranacağı kaynak dize</param>
    /// <param name="startIndex">Başlangıç indeksi</param>
    /// <returns>Bulunursa tanımlayıcıyı içeren dize, değilse <c>string.Empty</c></returns>
    public static string ReadIdentifier(this string source, int startIndex = 0)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        if (startIndex < 0 || startIndex >= source.Length)
            throw new ArgumentOutOfRangeException(nameof(startIndex));
        if (string.IsNullOrEmpty(source))
            return string.Empty;
        var ch = source[startIndex];
        if (ch != '_' && !char.IsLetter(ch))
            return string.Empty;
        var sb = new StringBuilder(10);
        sb.Append(ch);
        for (var i = startIndex + 1; i < source.Length; i++)
        {
            ch = source[i];
            if (char.IsLetterOrDigit(ch))
                sb.Append(ch);
            else
                break;
        }
        return sb.ToString();
    }

    public static string ToIdentifier(this string source, int startIndex = 0, bool invariant = false)
    {
        if (string.IsNullOrWhiteSpace(source)) return source;
        var sb = new StringBuilder();
        for (var i = 0; i < source.Length; i++)
        {
            var ch = source[i];
            if (char.IsLetter(ch) || (i > 0 && char.IsDigit(ch)))
            {
                if (!invariant || !TryConvertChar(ch, sb))
                    sb.Append(ch);
            }
        }
        return sb.ToString();
    }

    public static string ClearNonLettersAndDigits(this string source)
    {
        if (string.IsNullOrWhiteSpace(source)) return source;
        var sb = new StringBuilder();
        foreach (var ch in source)
        {
            if (!char.IsLetter(ch) && !char.IsDigit(ch) && ch != ' ') continue;
            sb.Append(ch);
        }
        return sb.ToString();
    }

    public static string GetOnlyDigits(this string source)
        => new Regex("[^\\d]").Replace(source, "");

    public static string IfStartThenRemove(this string source, string start)
    {
        var res = (source ?? "").Trim();
        if (res.StartsWith(start))
        {
            res = res.Substring(res.IndexOf(start, StringComparison.Ordinal) + start.Length);
        }
        return res;
    }

    public static bool IsOneOfThese(this string source, params string[] strings)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        foreach (var str in strings)
        {
            if (string.Equals(source, str))
                return true;
        }
        return false;
    }

    public static bool IsOneOfThese(this string source, StringComparison comparisonType, params string[] strings)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        foreach (var str in strings)
        {
            if (string.Equals(str, source, comparisonType))
                return true;
        }
        return false;
    }

    public static bool ContainsOneOfThese(this string source, params string[] strings)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        foreach (var str in strings)
        {
            if (source.IndexOf(str, StringComparison.Ordinal) > -1)
                return true;
        }
        return false;
    }

    public static bool ContainsOneOfThese(this string source, StringComparison comparisonType,
        params string[] strings)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        foreach (var str in strings)
        {
            if (source.IndexOf(str, comparisonType) > -1)
                return true;
        }
        return false;
    }

    public static bool IsNumber(this string src)
    {
        if (string.IsNullOrWhiteSpace(src))
            return false;
        bool valid = false, hasDot = false, hasNumbers = false;
        var startIndex = 0;
        if (src.StartsWith("-"))
            startIndex++;
        for (var i = startIndex; i < src.Length; i++)
        {
            var ch = src[i];
            switch (ch)
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    valid = true;
                    hasNumbers = true;
                    break;
                case '.':
                    if (valid && !hasDot || !valid)
                    {
                        hasDot = true;
                        if (!hasNumbers) valid = true;
                    }
                    else valid = false;
                    break;
                default:
                    valid = false;
                    break;
            }
            if (!valid)
                break;
        }
        return valid;
    }
    [ContractAnnotation("source:null => true")]
    public static bool IsNullOrEmpty(this string source)
    {
        return string.IsNullOrEmpty(source);
    }
    [ContractAnnotation("source:null => true")]
    public static bool IsNullOrWhitespace(this string source)
    {
        return string.IsNullOrWhiteSpace(source);
    }

    public static string IfNullOrEmpty(this string source, string val)
    {
        return string.IsNullOrEmpty(source) ? val : source;
    }

    public static string IfNullOrWhiteSpace(this string source, string val)
    {
        return string.IsNullOrWhiteSpace(source) ? val : source;
    }
    [ContractAnnotation("source:null => false", true)]
    public static bool IsNotNullOrEmpty(this string source)
    {
        return !string.IsNullOrEmpty(source);
    }
    [ContractAnnotation("source:null => false", true)]
    public static bool IsNotNullOrWhitespace(this string source)
    {
        return !string.IsNullOrWhiteSpace(source);
    }

    public static bool StartsWithLetter(this string source)
    {
        if (source.IsNullOrWhitespace()) return false;
        return char.IsLetter(source[0]);
    }

    public static bool StartsWithDigit(this string source)
    {
        if (source.IsNullOrWhitespace()) return false;
        return char.IsDigit(source[0]);
    }

    public static bool StartsWithLetterOrDigit(this string source)
    {
        if (source.IsNullOrWhitespace()) return false;
        return char.IsLetterOrDigit(source[0]);
    }

    public static bool StartsWithChar(this string source, char ch)
        => !string.IsNullOrEmpty(source) && source[0] == ch;

    public static bool EndsWithChar(this string source, char ch)
        => !string.IsNullOrEmpty(source) && source[source.Length - 1] == ch;

    public static bool CheckChar(this string source, [NotNull] Func<char, bool> checker)
    {
        if (checker == null) throw new ArgumentNullException(nameof(checker));
        foreach (var ch in source)
        {
            if (checker(ch))
                return true;
        }

        return false;
    }

    static Regex splitIntoLines;

    static Regex GetSplitIntoLinesRegEx() =>
        LazyInitializer.EnsureInitialized(ref splitIntoLines, () => new Regex("\r\n|\r|\n", RegexOptions.Compiled));

    public static string[] SplitIntoLines(this string source) => GetSplitIntoLinesRegEx().Split(source);

    ///<summary>
    /// Şu an ki string değeri int tipine dönüştürür.
    ///</summary>
    public static int ToInt32(this string source)
    {
        return int.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri int tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static int ToInt32(this string source, int defaultValue)
    {
        return !int.TryParse(source, out var result) ? defaultValue : result;
    }

    ///<summary>
    /// Şu an ki string değeri int tipine dönüştürür.
    ///</summary>
    public static uint ToUInt32(this string source)
    {
        return uint.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri int tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static uint ToUInt32(this string source, uint defaultValue)
    {
        return !uint.TryParse(source, out var result) ? defaultValue : result;
    }


    ///<summary>
    /// Şu an ki string değeri double tipine dönüştürür.
    ///</summary>
    public static double ToDouble(this string source)
    {
        return double.Parse(source, CultureInfo.InvariantCulture);
    }

    ///<summary>
    /// Şu an ki string değeri Double tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static double ToDouble(this string source, double defaultValue)
    {
        return !double.TryParse(source, NumberStyles.Float, CultureInfo.InvariantCulture, out var result)
            ? defaultValue
            : result;
    }

    ///<summary>
    /// Şu an ki string değeri decimal tipine dönüştürür.
    ///</summary>
    public static decimal ToDecimal(this string source)
    {
        return decimal.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri decimal tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static decimal ToDecimal(this string source, decimal defaultValue)
    {
        return !decimal.TryParse(source, out var result) ? defaultValue : result;
    }

    ///<summary>
    /// Şu an ki string değeri decimal tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static decimal ToDecimal(this string source, decimal defaultValue, CultureInfo culture)
    {
        return !decimal.TryParse(source, NumberStyles.Number, culture, out var result) ? defaultValue : result;
    }


    ///<summary>
    /// Şu an ki string değeri bool tipine dönüştürür.
    ///</summary>
    public static bool ToBoolean(this string source)
    {
        return bool.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri Boolean tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static bool ToBoolean(this string source, bool defaultValue)
    {
        return !bool.TryParse(source, out var result) ? defaultValue : result;
    }

    ///<summary>
    /// Şu an ki string değeri bool tipine dönüştürür.
    ///</summary>
    public static byte ToByte(this string source)
    {
        return byte.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri Boolean tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static byte ToByte(this string source, byte defaultValue)
    {
        return !byte.TryParse(source, out var result) ? defaultValue : result;
    }

    ///<summary>
    /// Şu an ki string değeri DateTime tipine dönüştürür.
    ///</summary>
    public static DateTime ToDateTime(this string source)
    {
        return DateTime.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri DateTime tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static DateTime ToDateTime(this string source, DateTime defaultValue)
    {
        return !DateTime.TryParse(source, out var result) ? defaultValue : result;
    }

    ///<summary>
    /// Şu an ki string değeri short tipine dönüştürür.
    ///</summary>
    public static short ToInt16(this string source)
    {
        return short.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri Int16 tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static short ToInt16(this string source, short defaultValue)
    {
        return !short.TryParse(source, out var result) ? defaultValue : result;
    }


    ///<summary>
    /// Şu an ki string değeri long tipine dönüştürür.
    ///</summary>
    public static long ToInt64(this string source)
    {
        return long.Parse(source);
    }

    ///<summary>
    /// Şu an ki string değeri Int64 tipine dönüştürür.Dönüştürme başarılı olmaz ise <paramref name="defaultValue"/> parametresindeki değeri döndürür.
    ///</summary>
    public static long ToInt64(this string source, long defaultValue)
    {
        return !long.TryParse(source, out var result) ? defaultValue : result;
    }

    public static T ToEnum<T>(this string source)
    {
        return (T)Enum.Parse(typeof(T), source, true);
    }

    public static T ToEnum<T>(this string source, T defaultValue) where T : struct
    {
        return !Enum.TryParse(source, true, out T result) ? defaultValue : result;
    }
    /// <summary>
    /// StringComparison enumunu destekleyen dize değiştirme işlevi
    /// </summary>
    /// <param name="source">Kaynak dize</param>
    /// <param name="old">Aranacak dize</param>
    /// <param name="new">Yerine geçecek dize</param>
    /// <param name="comparison">Dize karşılaştırma tipi</param>
    /// <returns>Bulunmuş ise değiştirilmiş, değilse orijinal hali döner</returns>
    public static string Replace2(this string source, string old, string @new, StringComparison comparison)
    {
        var start = 0;
        while (true)
        {
            var idx = source.IndexOf(old, start, comparison);
            if (idx == -1)
                return source;
            source = source.Remove(idx, old.Length).Insert(idx, @new);
            start = idx;
        }
    }

    public static string Slice(this string source, int start, int end)
    {
        if (end < 0)
        {
            // Keep this for negative end support
            end = source.Length + end;
        }
        var len = end - start; // Calculate length
        return source.Substring(start, len); // Return Substring of length
    }

    public static string ToBase64(this string source)
    {
        if (source.IsNullOrEmpty())
            return string.Empty;
        var bytes = Encoding.UTF8.GetBytes(source);
        return Convert.ToBase64String(bytes);
    }

    public static string FromBase64(this string source)
    {
        var bytes = Convert.FromBase64String(source);
        return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
    }

    public static StringBuilder AppendIf(this StringBuilder sb, bool condition, string value)
    {
        if (!condition) return sb;
        sb.Append(value);
        return sb;
    }

    public static StringBuilder AppendLineIf(this StringBuilder sb, bool condition, string value)
    {
        if (!condition) return sb;
        sb.AppendLine(value);
        return sb;
    }

    public static StringBuilder AppendFormatIf(this StringBuilder sb, bool condition, string value, params object[] args)
    {
        if (!condition) return sb;
        sb.AppendFormat(value, args);
        return sb;
    }

    const string EMAIL_CHECK_REGEX = @"^(([^<>()[\]\\.,;:\s@\""]+"
                                         + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                                         + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                                         + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                                         + "[a-zA-Z]{2,}))$";

    static Regex emailChecker;
    public static bool IsValidEmail(this string inputEmail)
    {
        if (emailChecker == null)
            emailChecker = new Regex(EMAIL_CHECK_REGEX, RegexOptions.Compiled);
        return emailChecker.IsMatch(inputEmail ?? string.Empty);
    }

    /// <summary>
    /// Ayrıcı ile ayrılmış dizenin istenilen parçasını verir.
    /// </summary>
    /// <param name="source">Kaynak dize</param>
    /// <param name="partNumber">Hangi parçanın değeri alınacağı( 1 tabanlıdır )</param>
    /// <param name="delimiter">Veri ayırıcı</param>
    /// <returns>İlgili parçada bulunan veri</returns>
    public static string GetPart(this string source, int partNumber, string delimiter)
    {
        var partCount = 0;
        var pos = 0;
        do
        {
            var idx = source.IndexOf(delimiter, pos, StringComparison.Ordinal);
            if (idx == -1)
                return source.Substring(pos);
            partCount++;
            var part = source.Substring(pos, idx);
            pos += idx + delimiter.Length;
            if (partCount == partNumber)
                return part;

        } while (true);
    }

    public static string GetNextPart(this string source, string delimiter, out int foundIndex, out string remaining)
    {
        foundIndex = source.IndexOf(delimiter, StringComparison.Ordinal);
        if (foundIndex > -1)
        {
            remaining = (foundIndex + 1 < source.Length) ? source.Substring(foundIndex + 1) : "";
            return source.Substring(0, foundIndex);
        }
        remaining = source;
        return source;
    }

    public static string GetLastPart(this string source, char delimiter)
    {
        var idx = source.LastIndexOf(delimiter);
        if (idx > -1)
        {
            return source.Substring(idx + 1);
        }
        return null;
    }

    public static string GetLastPart(this string source, char delimiter, out int idx)
    {
        idx = source.LastIndexOf(delimiter);
        if (idx > -1)
        {
            return source.Substring(idx + 1);
        }
        return null;
    }

    public static string ToCamelCase(this string src)
    {
        if (string.IsNullOrEmpty(src))
            return src;
        var len = src.Length;
        if (!char.IsUpper(src[0]))
            return src;
        var chars = src.ToCharArray();
        for (var i = 0; i < len; i++)
        {
            var ch = chars[i];
            var hasNext = (i + 1 < len);
            if (i > 0 && hasNext && !char.IsUpper(chars[i + 1]))
                break;
            chars[i] = char.ToLowerInvariant(ch);
        }
        return new string(chars);
    }

    public static string ToKebabCase(this string src, CultureInfo culture = null)
    {
        if (string.IsNullOrEmpty(src))
            return src;
        if (culture == null)
            culture = CultureInfo.InvariantCulture;
        var sb = new StringBuilder();
        for (var i = 0; i < src.Length; i++)
        {
            var ch = src[i];
            if (i == 0)
                sb.Append(char.ToLower(ch, culture));
            else if (char.IsUpper(ch))
                sb.Append('-').Append(char.ToLower(ch, culture));
            else
                sb.Append(ch);
        }

        return sb.ToString();
    }

    public static string HumanizeCamelCase(this string src)
    {
        var builder = new StringBuilder();
        foreach (var c in src)
        {
            if (char.IsUpper(c) && builder.Length > 0) builder.Append(' ');
            builder.Append(c);
        }
        return builder.ToString();
    }

    public static string PickNotEmpty(this string src, params string[] others)
    {
        if (string.IsNullOrEmpty(src))
        {
            foreach (var item in others)
            {
                if (!string.IsNullOrEmpty(item))
                    return item;
            }
        }
        return src;
    }

    public static string SkipUntil(this string src, char ch)
    {
        var idx = src.IndexOf(ch);
        return idx == -1 ? src : src.Substring(idx + 1);
    }

    public static string TakeUntil(this string src, char ch)
    {
        var idx = src.IndexOf(ch);
        return idx == -1 ? src : src.Substring(0, idx);
    }

    public static string[] FixedLengthSplit(this string src, int length)
    {
        var parts = new List<string>();
        var position = 0;
        while (true)
        {
            if ((position + length) < src.Length)
                parts.Add(src.Substring(position, length));
            else
            {
                parts.Add(src.Substring(position));
                break;
            }
            position += length;
        }
        return parts.ToArray();
    }

    public static string[] FixedLengthsSplit(this string src, params int[] lengths)
    {
        if (lengths.Length == 0)
            throw new ArgumentException("Lengths must be larger than 0", nameof(lengths));
        var parts = new List<string>();
        var position = 0;
        var lengthIndex = -1;
        while ((++lengthIndex) < lengths.Length)
        {
            var length = lengths[lengthIndex];
            if ((position + length) < src.Length)
                parts.Add(src.Substring(position, length));
            else
            {
                parts.Add(src.Substring(position));
                break;
            }
            position += length;
        }
        return parts.ToArray();
    }
    public static int[] ToInt32Array(this string source, char[] seperator, StringSplitOptions options)
    {
        var parts = source.Split(seperator, options);
        var idx = 0;
        var result = new int[parts.Length];
        foreach (var part in parts)
        {
            if (int.TryParse(part, out int val))
                result[idx++] = val;
        }
        if (idx < result.Length)
            Array.Resize(ref result, idx);
        return result;
    }

    public static long[] ToInt64Array(this string source, char[] seperator, StringSplitOptions options)
    {
        var parts = source.Split(seperator, options);
        var idx = 0;
        var result = new long[parts.Length];
        foreach (var part in parts)
        {
            if (long.TryParse(part, out long val))
                result[idx++] = val;
        }
        if (idx < result.Length)
            Array.Resize(ref result, idx);
        return result;
    }

    public static string AppendIfNotEmpty(this string source, string s)
    {
        return string.IsNullOrWhiteSpace(source) ? source : string.Concat(source, s);
    }

    public static string PrependIfNotEmpty(this string source, string s)
    {
        return string.IsNullOrWhiteSpace(source) ? source : string.Concat(s, source);
    }

    public static StringBuilder CheckedAppend(this StringBuilder sb, string s, ref bool added)
    {
        if (!added)
        {
            sb.Append(s);
            added = true;
        }
        return sb;
    }

    public static StringBuilder AppendIfTrue(this StringBuilder sb, bool condition, string s)
    {
        if (condition)
            sb.Append(s);
        return sb;
    }


}
