﻿#if !MOBILE
namespace System.Data
{
    public static class DataRowExtensions
    {
        public static T Value<T>(this DataRow row, string columnName)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[columnName]);
        }

        public static T Value<T>(this DataRow row, string columnName, T defaultValue)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[columnName], defaultValue);
        }

        public static T Value<T>(this DataRow row, DataColumn column)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[column]);
        }

        public static T Value<T>(this DataRow row, DataColumn column, T defaultValue)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[column], defaultValue);
        }

        public static T Value<T>(this DataRow row, int columnIndex)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[columnIndex]);
        }

        public static T Value<T>(this DataRow row, int columnIndex, T defaultValue)
        {
            if (row == null)
                throw new ArgumentNullException(nameof(row));
            return Unboxer<T>(row[columnIndex], defaultValue);
        }

        static T Unboxer<T>(object value)
        {
            if (value == null || value == DBNull.Value)
                return default(T);
            return (T)value;
        }

        static T Unboxer<T>(object value, T defaultValue)
        {
            if (value == null || value == DBNull.Value)
                return defaultValue;
            return (T)value;
        }
    }
}

#endif