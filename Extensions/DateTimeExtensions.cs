﻿using System.Collections.Generic;
using System.MyConverters;
using System.MyResources;

// ReSharper disable once CheckNamespace
namespace System
{
    public static class DateTimeExtensions
    {
        static DateTime unixDelta = new DateTime(1970, 1, 1, 0, 0, 0);
        public static List<DateTime> GetDatesBetween(this DateTime startDate, DateTime finishDate)
        {
            var dates = new List<DateTime>();
            var startDateDate = startDate.Date;
            var diff = finishDate.Date - startDateDate;
            var totalDays = Math.Round(Conversion.Fix(diff.TotalDays));
            for (var i = 0; i <= totalDays; i++)
                dates.Add(startDateDate.AddDays(i));
            return dates;
        }

        public static int GetDaysCountBetween(this DateTime startDate, DateTime finishDate)
        {
            var startDateDate = startDate.Date;
            var diff = finishDate.Date - startDateDate;
            return (int)Math.Round(Conversion.Fix(diff.TotalDays));
        }

        public static bool IsBetween(this DateTime instance, DateTime start, DateTime end) =>
            instance >= start && instance <= end;

        public static DateTime StartOfMonth(this DateTime instance) => new DateTime(instance.Year, instance.Month, 1);

        public static DateTime StartOfDay(this DateTime instance) => instance.Date;

        public static DateTime EndOfDay(this DateTime instance) => instance.Date.AddDays(1).AddSeconds(-1);

        public static DateTime EndOfMonth(this DateTime instance) => instance.StartOfMonth().AddMonths(1).AddSeconds(-1);

        public static string ToTextualDistance(this DateTime dt, bool appendSuffix = true)
        {
            TimeSpan distance = DateTime.Now - dt;
            string suffix = string.Empty;
            if (appendSuffix)
                suffix = distance.TotalMilliseconds < 0 ? SR.TextualDistanceFromNow : SR.TextualDistanceAgo;
            if (Math.Abs(distance.TotalDays / 365) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalDays / 365)), SR.TextualDistanceYear, suffix);
            if (Math.Abs(distance.TotalDays / 30) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalDays / 30)), SR.TextualDistanceMonth, suffix);
            if (Math.Abs(distance.TotalDays / 7) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalDays / 7)), SR.TextualDistanceWeek, suffix);
            if (Math.Abs(distance.TotalDays) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalDays)), SR.TextualDistanceDay, suffix);
            if (Math.Abs(distance.TotalHours) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalHours)), SR.TextualDistanceHour, suffix);
            if (Math.Abs(distance.TotalMinutes) >= 1)
                return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalMinutes)), SR.TextualDistanceMinute, suffix);
            return string.Format(SR.TextualDistanceFormat, Math.Abs(Math.Round(distance.TotalSeconds)), SR.TextualDistanceSecond, suffix);
        }

        public static string ToTextualString(this TimeSpan ts)
        {
            if (ts.Days > 0)
                return $"{ts.Days} gün";
            if (ts.Hours > 0)
                return $"{ts.Hours} sa";
            if (ts.Minutes > 0)
                return $"{ts.Minutes} dk";
            if (ts.Seconds > 0)
                return $"{ts.Seconds} sn";
            return $"{ts.Milliseconds} ms";
        }

        public static bool MonthsAreSame(this DateTime instance, DateTime other)
            => instance.Year == other.Year && instance.Month == other.Month;
        public static long ToUnixTimestamp(this DateTime d)
        {
            var duration = d - unixDelta;
            return (long)duration.TotalSeconds;
        }

        public static DateTime FromUnixTimestampToDateTime(this long unixTimestamp)
        {
            return unixDelta.AddSeconds(unixTimestamp);
        }
    }
}
