﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Utils;
#if JSON
using Newtonsoft.Json; 
#endif

namespace System
{
    public static class ObjectExtensions
    {

        static uint[] knownExcludedProperties;

        const ObjectMapperOptions OBJECT_MAPPER_OPTIONS = ObjectMapperOptions.InvariantSearch | ObjectMapperOptions.SkipVirtualProperties;

        static uint[] KnownExcludedProperties
            =>
            knownExcludedProperties ??
            (knownExcludedProperties =
                HashUtils.JenkinsHashes("deleted", "enteredby", "entrydatetime", "updatedby", "updatedatetime"));

        public static TTarget ToObject<TTarget>(this object source) where TTarget : class, new()
        {
            return CopyToObject(source, new TTarget());
        }

        public static List<TTarget> ToObjectList<TSource, TTarget>(this IEnumerable<TSource> sources) where TTarget : class, new() where TSource : class
        {
            var targetList = new List<TTarget>();
            ObjectMapper.MapToList(sources, targetList, OBJECT_MAPPER_OPTIONS, KnownExcludedProperties);
            return targetList;
        }

        public static TTarget CopyToObject<TTarget>(this object source, TTarget target) where TTarget : class
        {
            return ObjectMapper.Map(source.GetType(), source, target, OBJECT_MAPPER_OPTIONS, KnownExcludedProperties);
        } 

        public static T GetValue<T>(this Hashtable ht, object key, T defaultValue)
        {
            var value = ht[key];
            if (value == null)
                return defaultValue;
            return TypeUtils.CoerceValue<T>(value);

        }

        public static TAttr GetAttribute<TAttr, T>(this T value, bool inherit = false)
            where T : struct
            where TAttr : Attribute
        {
            var type = typeof(T);
            MemberInfo mi = type;
            if (type.IsEnum)
                mi = type.GetField(value.ToString());
            if (mi == null)
                return null;
            return mi.GetCustomAttributes(typeof(TAttr), inherit).FirstOrDefault() as TAttr;
        }

        /// <summary>
        /// Object değerin null olup olmadığı ve parametre olarak 
        /// geçilen boolean değeri ile karşılaştırma yapılıyor.
        /// </summary>
        /// <param name="Object_"></param>
        /// <param name="booleanVal">null kontrolü ile beraber kontrol edilecek boolean değer girinizi True/False</param>
        /// <returns>
        /// Null ve boolean kontrolü ile beraber çıkan boolean sonucu geriye döndürülür.
        /// </returns>
        public static bool NotNullAndBooleanControl(this object Object_, bool booleanVal)
        {
            return Object_ != null && (bool)Object_ == booleanVal;
        }
        
        public static bool Any(this IEnumerable enm)
        {
            return enm.GetEnumerator().MoveNext();
        }

#if JSON
        public static string ToJsonString(this object obj, Formatting formatting) => JsonConvert.SerializeObject(obj, formatting);

        public static string ToJsonString(this object obj, JsonSerializerSettings settings) => JsonConvert.SerializeObject(obj, settings); 
#endif
    }
}
