﻿public static class BoleanExtension
{
    public static string ToJavascript(this bool value)
    {
        return value ? "true" : "false";
    }

    public static byte ToNumber(this bool value) 
        => (byte)(value ? 1 : 0);

    public static bool IsFalse(this bool value)
    {
        return value == false;
    }

    public static bool IsTrue(this bool? value)
    {
        return value == true;
    }

    public static bool IsFalse(this bool? value)
    {
        return value == false;
    }

    public static bool IsFalseOrNull(this bool? value)
    {
        return value == false || value == null;
    }
}
