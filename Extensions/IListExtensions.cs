﻿// ReSharper disable once CheckNamespace

using System.Linq;
using JetBrains.Annotations;

namespace System.Collections.Generic
{
    public static class ListExtensions
    {
        public static void AddChecked<T>(this IList<T> instance, T item)
        {
            if (instance.IndexOf(item) == -1)
                instance.Add(item);
        }

        public static IList<T> AddIf<T>(this IList<T> instance, bool condition, T item)
        {
            if (!condition) return instance;
            instance.Add(item);
            return instance;

        }

        public static IList<T> AddRange2<T>(this IList<T> instance, params T[] items)
        {
            foreach (var item in items)
                instance.Add(item);
            return instance;

        }

        public static IList<T> AddRange3<T>(this IList<T> instance, IEnumerable<T> items)
        {
            foreach (var item in items)
                instance.Add(item);
            return instance;

        }

        public static IList<TIntf> AddRangeIntf<T, TIntf>(this IList<TIntf> instance, IEnumerable<T> items) where T : TIntf
        {
            foreach (var item in items)
                instance.Add(item);
            return instance;

        }

        public static IList<T> AddRangeChecked<T>(this IList<T> instance, IEnumerable<T> items)
        {
            foreach (var item in items)
                if (instance.IndexOf(item) == -1)
                    instance.Add(item);
            return instance;
        }

        public static bool Compare(this IList<string> instance, [NotNull] IList<string> other, StringComparer comparer = null)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            comparer = comparer ?? StringComparer.Ordinal;
            var firstNotSecond = instance.Except(other, comparer).ToArray();
            var secondNotFirst = other.Except(instance, comparer).ToArray();
            return firstNotSecond.Length == 0 && secondNotFirst.Length == 0;
        }

        public static bool Compare<T>(this IList<T> instance, [NotNull] IList<T> other, [NotNull] IEqualityComparer<T> comparer)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (comparer == null) throw new ArgumentNullException(nameof(comparer));
            var firstNotSecond = instance.Except(other, comparer).ToArray();
            var secondNotFirst = other.Except(instance, comparer).ToArray();
            return firstNotSecond.Length == 0 && secondNotFirst.Length == 0;
        }

        public static bool Compare<T>(this IList<T> instance, [NotNull] IList<T> other)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            var firstNotSecond = instance.Except(other).ToArray();
            var secondNotFirst = other.Except(instance).ToArray();
            return firstNotSecond.Length == 0 && secondNotFirst.Length == 0;
        }
    }
}
