﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.Text;
using System.Utils;

// ReSharper disable once CheckNamespace
namespace System.Reflection
{
    public static class TypeAccessorExtensions
    {
        /// <summary>
        /// Seçilebilir kolonların adlarını virgülle ayrılmış olarak verir, Identity kolon dahil 
        /// </summary>
        /// <returns></returns>
        public static string GetSelectableColumns(this TypeAccessor accessor, params uint[] excludeProperties)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var item in accessor.MappableProperties)
            {
                if (Array.IndexOf(excludeProperties, item.ColumnNameHash) != -1 || Array.IndexOf(excludeProperties, item.NameHash) != -1)
                    continue;
                if (i++ > 0)
                    sb.Append(", ");
                sb.Append("[").Append(item.ColumnName).Append("]");
            }
            return sb.ToString();
        }

        public static string GetInsertColumns(this TypeAccessor accessor, params string[] excludeProperties)
            => GetInsertColumns(accessor, HashUtils.JenkinsHashes(excludeProperties));

        public static string GetInsertColumns(this TypeAccessor accessor, params uint[] excludeProperties)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var item in accessor.AllUpdatableProperties)
            {
                if (Array.IndexOf(excludeProperties, item.ColumnNameHash) != -1 || Array.IndexOf(excludeProperties, item.NameHash) != -1)
                    continue;
                if (i++ > 0)
                    sb.Append(", ");
                sb.Append("[").Append(item.ColumnName).Append("]");
            }
            return sb.ToString();
        }

        public static string GetUpdateSetExpressionsWithParams(this TypeAccessor accessor, object instance, IList<object> prms,
            params string[] excludeProperties)
            => GetUpdateSetExpressionsWithParams(accessor, instance, prms, HashUtils.JenkinsHashes(excludeProperties));

        public static string GetUpdateSetExpressionsWithParams(this TypeAccessor accessor, object instance, IList<object> prms, params uint[] excludeProperties)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var item in accessor.AllUpdatableProperties)
            {
                if (Array.IndexOf(excludeProperties, item.ColumnNameHash) != -1 || Array.IndexOf(excludeProperties, item.NameHash) != -1)
                    continue;
                if (i++ > 0)
                    sb.Append(", ");
                var prmName = item.ColumnName.ToCamelCase();
                sb.AppendFormat("[{0}] = @{1}", item.ColumnName, prmName);
                prms?.AddRange2(string.Concat("@", prmName), item.GetPropertyData(instance).Value);
            }
            return sb.ToString();
        }

        public static string GetInsertValuesWithAlias(this TypeAccessor accessor, string aliasWithDot, params string[] excludeProperties)
            => GetInsertValuesWithAlias(accessor, aliasWithDot, HashUtils.JenkinsHashes(excludeProperties));

        public static string GetInsertValuesWithAlias(this TypeAccessor accessor, string aliasWithDot, params uint[] excludeProperties)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var property in accessor.AllUpdatableProperties)
            {
                if (Array.IndexOf(excludeProperties, property.ColumnNameHash) != -1 || Array.IndexOf(excludeProperties, property.NameHash) != -1)
                    continue;
                if (i++ > 0)
                    sb.Append(", ");
                sb.Append(aliasWithDot).Append(property.ColumnName);
            }
            return sb.ToString();
        }

        public static string GetInsertValuesWithParams(this TypeAccessor accessor, object instance, IList<object> prms, params string[] excludeProperties)
            => GetInsertValuesWithParams(accessor, instance, prms, HashUtils.JenkinsHashes(excludeProperties));

        public static string GetInsertValuesWithParams(this TypeAccessor accessor, object instance, IList<object> prms, params uint[] excludeProperties)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var item in accessor.AllUpdatableProperties)
            {
                if (Array.IndexOf(excludeProperties, item.ColumnNameHash) != -1 || Array.IndexOf(excludeProperties, item.NameHash) != -1)
                    continue;
                if (i++ > 0)
                    sb.Append(", ");
                var prmName = string.Concat("@", item.ColumnName.ToCamelCase());
                sb.Append(prmName);
                prms.AddRange2(prmName, item.GetPropertyData(instance).Value);
            }
            return sb.ToString();
        }

        public static int MaesureMaximumSizeOf<T>(this PropertyAccessor propertyAccessor, IEnumerable<T> list) where T : class
        {
            if (!propertyAccessor.IsSizable) return 0;
            var size = 0;
            foreach (var item in list)
            {
                var data = propertyAccessor.GetPropertyData(item);
                size = Math.Max(size, data.Size);
            }

            return size;
        }
    }
}

#endif