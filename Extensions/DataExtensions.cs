﻿#if COREFULL || COREDATA
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Data;
using System.Utils;

// ReSharper disable once CheckNamespace
namespace System.Data
{
    public static class DataExtensions
    {
        /// <summary>
        /// Komut nesnesinden parametrelerdeki değerleri kullanarak çalıştırılabilir bir sql oluşturur.
        /// </summary>
        /// <param name="cmd">Gerekli komut</param>
        /// <returns>Çalıştırılabilir sql ifadesi</returns>
        public static string GetRunnableSql(this DbCommand cmd)
        {
            var sb = new StringBuilder();
            foreach (DbParameter prm in cmd.Parameters)
            {
                sb.Append("DECLARE ");
                sb.Append(prm.ParameterName).Append(' ');
                var val = prm.Value;
                if (val == null || ReferenceEquals(val, DBNull.Value))
                    sb.AppendLine("varchar(50) = NULL");
                else if (val is DataTable dt)
                {
                    sb.AppendFormat("{0};\r\n", dt.TableName);
                    foreach (DataRow row in dt.Rows)
                        sb.AppendFormat("INSERT INTO {0} VALUES({1});", prm.ParameterName, string.Join(",", row.ItemArray.Select(_ => TSql.ConvertSqlValue(_))));
                }
                else
                {
                    var type = val.GetType();
                    var sti = SqlServerTypeInfo.GetByType(type);
                    if (sti.IsSizable)
                    {
                        if (sti.IsBinary)
                        {
                            sb.Append("varbinary(");
                            if (val is byte[] bytes)
                            {
                                var barr = bytes;
                                sb.Append(barr.Length);
                                sb.Append(") = ");
                                sb.Append(TSql.ConvertSqlValue(barr));
                            }
                        }
                        else
                        {
                            sb.Append("varchar(");
                            if (val is char[] c)
                            {
                                var carr = c;
                                sb.Append(carr.Length);
                                sb.Append(") = ");
                                sb.Append(TSql.ConvertSqlValue(new string(carr)));
                            }
                            else
                            {
                                var str = (string)val;
                                sb.Append(str.Length);
                                sb.Append(") = ");
                                sb.Append(string.Concat(sti.IsUnicode ? "N" : string.Empty, TSql.ConvertSqlValue(str)));
                            }
                        }
                    }
                    else if (sti.HasPrecisionScale)
                    {
                        sb.Append("numeric(19,2) = ").Append(TSql.ConvertSqlValue(val));
                    }
                    else
                    {
                        sb.Append(sti.Name).Append(" = ").Append(TSql.ConvertSqlValue(val));
                    }
                    sb.AppendLine();
                }

            }
            sb.AppendLine().Append(cmd.CommandText);
            return sb.ToString();
        }

        static Dictionary<Type, AnonymousTypeInfo> anonymousTypeInfos;
        static Dictionary<Type, AnonymousTypeInfo> AnonymousConstructors => LazyInitializer.EnsureInitialized(ref anonymousTypeInfos, () => new Dictionary<Type, AnonymousTypeInfo>());

        public static T GetRcValue<T>(this DataTable tbl, int row, int col)
        {
            if (row.Between(0, tbl.Rows.Count - 1) && col.Between(0, tbl.Columns.Count - 1))
                return TypeUtils.CoerceValue<T>(tbl.Rows[row][col]);
            return default(T);
        }

        public static DataRow FindRowByColumnValue(this DataTable tbl, DataColumn col, object value)
        {
            foreach (DataRow row in tbl.Rows)
            {
                var actual = row[col];
                if (Equals(value, actual))
                    return row;
            }
            return null;
        }

        public static DbParameter AddWithValue(this DbCommand cmd, string name, object value)
        {
            var prm = cmd.CreateParameter();
            prm.ParameterName = name;
            prm.Value = value ?? DBNull.Value;
            cmd.Parameters.Add(prm);
            return prm;
        }

        public static DbParameter AddWithValue(this DbCommand cmd, string name, object value, DbType type)
        {
            var prm = AddWithValue(cmd, name, value);
            prm.DbType = type;
            return prm;
        }

        public static DbParameter AddWithValue(this DbCommand cmd, string name, object value, DbType type, int size)
        {
            var prm = AddWithValue(cmd, name, value);
            prm.DbType = type;
            prm.Size = size;
            return prm;
        }

        public static DbParameter GetParameter(this DbCommand cmd, int idx)
        {
            return cmd.Parameters[idx];
        }

        public static DbParameter GetParameter(this DbCommand cmd, string name)
        {
            return cmd.Parameters[name];
        }

        static void AddParameterInternal(DbCommand cmd, string name, object pr)
        {
            var parameter = cmd.CreateParameter();
            parameter.ParameterName = name;
            var val = pr ?? DBNull.Value;
            if (val is DateTime time && time == DateTime.MinValue)
                val = DBNull.Value;
            parameter.Value = val;
            cmd.Parameters.Add(parameter);
            if (!(pr is DataTable dataTable)) return;
            if (dataTable.TableName.IsNullOrWhitespace())
                throw new ArgumentException("Table name must be specified if the parameter value is DataTable");
            var sqlPrm = (SqlParameter)parameter;
            sqlPrm.SqlDbType = SqlDbType.Structured;
            sqlPrm.TypeName = dataTable.TableName;
        }

        public static void SetCommandParameters(this DbCommand cmd, params object[] parameters)
        {
            if (parameters.Length == 0) return;

            var startIndex = 0;
            var nameIndex = 1;

            var useNamedParams = cmd.CommandType == CommandType.StoredProcedure;

            if (useNamedParams)
                nameIndex--;

            if (!useNamedParams && parameters[startIndex] == DbSwitches.UseNamedParams)
            {
                startIndex++;
                nameIndex--;
                useNamedParams = true;
            }

            for (var prmIndex = startIndex; prmIndex < parameters.Length; prmIndex++)
            {

                if (!useNamedParams)
                {
                    var paramValue = parameters[prmIndex];
                    if (paramValue is DbParameter prmInstance)
                        cmd.Parameters.Add(prmInstance);
                    else
                        AddParameterInternal(cmd, "@p" + nameIndex, parameters[prmIndex]);
                }
                else if (nameIndex % 2 != 0)
                    AddParameterInternal(cmd, (string)parameters[prmIndex - 1], parameters[prmIndex]);

                nameIndex++;
            }
        }

        public static int IndexOfName(this DbDataReader reader, string name, int startIndex = 0)
        {
            for (var i = startIndex; i < reader.FieldCount; i++)
            {
                var field = reader.GetName(i);
                if (field.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return i;
            }
            return -1;
        }

        public static int FindFieldIndexByType<T>(this DbDataReader reader)
        {
            var type = typeof(T);
            for (var i = 0; i < reader.FieldCount; i++)
            {
                if (type == reader.GetFieldType(i))
                    return i;
            }
            return -1;
        }

        public static int FindFieldIndexByType(this DbDataReader reader, Predicate<Type> predicate)
        {
            for (var i = 0; i < reader.FieldCount; i++)
            {
                if (predicate(reader.GetFieldType(i)))
                    return i;
            }
            return -1;
        }

        public static T[] ReadArray<T>(this DbDataReader reader, int index = 0)
        {
            T[] result = null;
            var count = 0;
            while (reader.Read())
            {
                ArrayUtils.CheckArray(ref result, count);
                result[count] = TypeUtils.CoerceValue<T>(reader.GetValue(index));
                count++;
            }
            if (count == 0)
                return new T[0];
            if (result != null && result.Length != count)
                Array.Resize(ref result, count);
            return result;
        }

        public static Tuple<T, TK>[] ReadTupleArray<T, TK>(this DbDataReader reader)
        {
            var count = 0;
            Tuple<T, TK>[] result = null;
            while (reader.Read())
            {
                ArrayUtils.CheckArray(ref result, count);
                result[count] = Tuple.Create(
                    TypeUtils.CoerceValue<T>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<TK>(reader.GetValue(1)));
                count++;
            }
            if (count == 0)
                return new Tuple<T, TK>[0];
            if (result != null && result.Length != count)
                Array.Resize(ref result, count);
            return result;
        }

        public static Tuple<T, TK, TM>[] ReadTupleArray<T, TK, TM>(this DbDataReader reader)
        {
            var count = 0;
            Tuple<T, TK, TM>[] result = null;
            while (reader.Read())
            {
                ArrayUtils.CheckArray(ref result, count);
                result[count] = Tuple.Create(TypeUtils.CoerceValue<T>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<TK>(reader.GetValue(1)), TypeUtils.CoerceValue<TM>(reader.GetValue(2)));
                count++;
            }
            if (count == 0)
                return new Tuple<T, TK, TM>[0];
            if (result != null && result.Length != count)
                Array.Resize(ref result, count);
            return result;
        }

        public static Tuple<T, TK, TM, TL>[] ReadTupleArray<T, TK, TM, TL>(this DbDataReader reader)
        {
            var count = 0;
            Tuple<T, TK, TM, TL>[] result = null;
            while (reader.Read())
            {
                ArrayUtils.CheckArray(ref result, count);
                result[count] = Tuple.Create(
                    TypeUtils.CoerceValue<T>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<TK>(reader.GetValue(1)),
                    TypeUtils.CoerceValue<TM>(reader.GetValue(2)),
                    TypeUtils.CoerceValue<TL>(reader.GetValue(3)));
                count++;
            }
            if (count == 0)
                return new Tuple<T, TK, TM, TL>[0];
            if (result != null && result.Length != count)
                Array.Resize(ref result, count);
            return result;
        }

        public static void ReadScalarDictionary<TK, TV>(this DbDataReader reader, IDictionary<TK, TV> targetDict)
        {
            while (reader.Read())
            {
                var key = TypeUtils.CoerceValue<TK>(reader.GetValue(0));
                if (key != null && !targetDict.ContainsKey(key))
                    targetDict.Add(key, TypeUtils.CoerceValue<TV>(reader.GetValue(1)));
            }
        }

        public static void ReadScalarList<T>(this DbDataReader reader, IList<T> targetList)
        {
            while (reader.Read())
                targetList.Add(TypeUtils.CoerceValue<T>(reader.GetValue(0)));
        }

        public static void ReadScalarListUnionAll<T>(this DbDataReader reader, IList<T> targetList)
        {
            do
            {
                while (reader.Read())
                    targetList.Add(TypeUtils.CoerceValue<T>(reader.GetValue(0)));
            } while (reader.NextResult());
        }

        public static Tuple<T1, T2> ReadTuple<T1, T2>(this DbDataReader reader)
        {
            if (reader.Read())
            {
                return Tuple.Create(
                    TypeUtils.CoerceValue<T1>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<T2>(reader.GetValue(1)));
            }
            return null;
        }

        public static Tuple<T1, T2, T3> ReadTuple<T1, T2, T3>(this DbDataReader reader)
        {
            if (reader.Read())
            {
                return Tuple.Create(
                    TypeUtils.CoerceValue<T1>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<T2>(reader.GetValue(1)),
                    TypeUtils.CoerceValue<T3>(reader.GetValue(2)));
            }
            return null;
        }

        public static Tuple<T1, T2, T3, T4> ReadTuple<T1, T2, T3, T4>(this DbDataReader reader)
        {
            if (reader.Read())
            {
                return Tuple.Create(
                    TypeUtils.CoerceValue<T1>(reader.GetValue(0)),
                    TypeUtils.CoerceValue<T2>(reader.GetValue(1)),
                    TypeUtils.CoerceValue<T3>(reader.GetValue(2)),
                    TypeUtils.CoerceValue<T4>(reader.GetValue(3)));
            }
            return null;
        }

        public static Tuple<R1, R2> ReadScalarResults<R1, R2>(this DbDataReader reader)
        {
            var first = default(R1);
            var second = default(R2);
            if (reader.Read())
                first = TypeUtils.CoerceValue<R1>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                second = TypeUtils.CoerceValue<R2>(reader.GetValue(0));
            return Tuple.Create(first, second);
        }

        public static bool LoadVariables<V1, V2>(this DbDataReader reader, ref V1 v1, ref V2 v2)
        {
            if (!reader.Read()) return false;
            v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(1));
            return true;
        }

        public static bool LoadVariables<V1, V2, V3>(this DbDataReader reader, ref V1 v1, ref V2 v2, ref V3 v3)
        {
            if (!reader.Read()) return false;
            v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(1));
            v3 = TypeUtils.CoerceValue<V3>(reader.GetValue(2));
            return true;
        }

        public static bool LoadVariables<V1, V2, V3, V4>(this DbDataReader reader, ref V1 v1, ref V2 v2, ref V3 v3, ref V4 v4)
        {
            if (!reader.Read()) return false;
            v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(1));
            v3 = TypeUtils.CoerceValue<V3>(reader.GetValue(2));
            v4 = TypeUtils.CoerceValue<V4>(reader.GetValue(3));
            return true;
        }

        public static void LoadScalarResults<V1, V2>(this DbDataReader reader, ref V1 v1, ref V2 v2)
        {
            if (reader.Read())
                v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(0));
        }

        public static void LoadScalarResults<V1, V2, V3>(this DbDataReader reader, ref V1 v1, ref V2 v2, ref V3 v3)
        {
            if (reader.Read())
                v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v3 = TypeUtils.CoerceValue<V3>(reader.GetValue(0));
        }

        public static void LoadScalarResults<V1, V2, V3, V4>(this DbDataReader reader, ref V1 v1, ref V2 v2, ref V3 v3, ref V4 v4)
        {
            if (reader.Read())
                v1 = TypeUtils.CoerceValue<V1>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v2 = TypeUtils.CoerceValue<V2>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v3 = TypeUtils.CoerceValue<V3>(reader.GetValue(0));
            if (reader.NextResult() && reader.Read())
                v4 = TypeUtils.CoerceValue<V4>(reader.GetValue(0));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        static AnonymousTypeInfo GetConstructor<T>()
        {
            var targetType = typeof(T);
            if (AnonymousConstructors.ContainsKey(targetType))
                return AnonymousConstructors[targetType];
            var constructors = targetType.GetConstructors();
            if (constructors.Length != 1)
                throw new InvalidOperationException(targetType.Name + " is not anonymous!");
            var defctor = new AnonymousTypeInfo(constructors[0]);
            AnonymousConstructors.Add(targetType, defctor);
            return defctor;
        }

        public static List<T> ReadListAnonymous<T>(this DbDataReader reader, T obj)
        {
            var targetList = new List<T>();
            var ctor = GetConstructor<T>();
            while (reader.Read())
            {
                targetList.Add((T)ctor.Read(reader));
            }
            return targetList;
        }

        public static void ReadList<T>(this DbDataReader reader, List<T> targetList)
        {
            var ctor = GetConstructor<T>();
            while (reader.Read())
            {
                targetList.Add((T)ctor.Read(reader));
            }
        }

        public static T ReadAnonymousObject<T>(this DbDataReader reader, T obj)
        {
            var ctor = GetConstructor<T>();
            if (reader.Read())
            {
                return (T)ctor.Read(reader);
            }
            return obj;
        }

        public static void CalculateMaxLength(this DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (!TypeUtils.IsSizable(column.DataType)) continue;
                var length = 1;
                foreach (DataRow row in table.Rows)
                {
                    var value = row[column];
                    if (value == null || value == DBNull.Value) continue;
                    if (column.DataType == typeof(string))
                        length = Math.Max(length, ((string) value).Length);
                    else if (column.DataType == typeof(byte[]))
                        length = Math.Max(length, ((byte[])value).Length);
                    else if (column.DataType == typeof(char[]))
                        length = Math.Max(length, ((char[])value).Length);
                }

                column.MaxLength = length;
            }
        }

        #region Nested Classes

        sealed class AnonymousTypeInfo
        {
            readonly ConstructorInfo constructor;
            readonly Type[] parameters;

            public AnonymousTypeInfo(ConstructorInfo ci)
            {
                constructor = ci;
                var pi = ci.GetParameters();
                var length = pi.Length;
                parameters = new Type[length];
                for (var i = 0; i < length; i++)
                {
                    parameters[i] = pi[i].ParameterType;
                }
            }

            public object Read(DbDataReader reader)
            {
                var length = parameters.Length;
                var fieldCount = reader.FieldCount;
                var args = new object[length];
                for (var i = 0; i < length; i++)
                {
                    object val = DBNull.Value;
                    var prmType = parameters[i];
                    if (i < fieldCount)
                        val = reader.GetValue(i);
                    val = ReferenceEquals(val, DBNull.Value) ? TypeUtils.GetDefaultOfType(prmType) : TypeUtils.ConvertTo(prmType, val);
                    args[i] = val;
                }
                return constructor.Invoke(args);
            }
        }

        #endregion
    }

    public static class DbSwitches
    {
        public static readonly object UseNamedParams = new object();
    }
}

#endif