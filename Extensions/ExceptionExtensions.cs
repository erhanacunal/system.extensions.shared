﻿

// ReSharper disable once CheckNamespace
namespace System
{
    public static class ExceptionExtensions
    {
        public static string GetErrorMessage(this Exception exception, bool addStackTrace)
        {
            var result = exception.Message + "\r\n--------------------------------------------------------------------------\r\n" + (addStackTrace ? exception.StackTrace : "");
            while (exception.InnerException != null)
            {
                result = "\r\n--------------------------------------------------------------------------\r\n" + exception.InnerException.Message + "\r\n--------------------------------------------------------------------------\r\n" + (addStackTrace ? exception.InnerException.StackTrace : "");
                exception = exception.InnerException;
            }
            return result;
        }

        
    }
}
