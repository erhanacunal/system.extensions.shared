﻿using System.ComponentModel;
#if !MOBILE
using System.ComponentModel.DataAnnotations; 
#endif

namespace System
{
    public static class EnumExtensions
    {
        public static string GetDescription<T>(this T value) where T : struct
        {
            var desc = value.GetAttribute<DescriptionAttribute, T>();
            if (desc != null)
                return desc.Description;
            return value.ToString();
        }

#if !MOBILE
        public static string GetDisplayName<T>(this T value) where T : struct
        {
            var desc = value.GetAttribute<DisplayAttribute, T>();
            if (desc != null)
                return desc.Name;
            return value.ToString();
        } 
#endif


    }
}
