﻿using System.Linq;
using System.Text;

namespace System
{
    public static class IntExtensions
    {
        public static string RandomChar(this int count, bool uppercase = true, bool lowercase = true, bool number = true, bool userLatinAlphabet = true)
        {
            var upperCase = userLatinAlphabet ? 
                new[] { 'A', 'B', 'C', 'Ç', 'D', 'E', 'F', 'G', 'Ğ', 'H', 'I', 'İ', 'J', 'K', 'L', 'M', 'N', 'O', 'Ö', 'P', 'Q', 'R', 'S', 'Ş', 'T', 'U', 'Ü', 'V', 'W', 'X', 'Y', 'Z' } 
                : new[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

            var lowerCase = userLatinAlphabet 
                ? new[] { 'a', 'b', 'c', 'ç', 'd', 'e', 'f', 'g', 'ğ', 'h', 'ı', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'ö', 'p', 'q', 'r', 's', 'ş', 't', 'u', 'ü', 'v', 'w', 'x', 'y', 'z' } 
                : new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            var numbers = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            var rnd = new Random();

            var total = (uppercase ? upperCase : new char[] { })
                .Concat(lowercase ? lowerCase : new char[] { })
                .Concat(number ? numbers : new char[] { })
                .ToArray();

            var chars = Enumerable
                .Repeat(0, count)
                .Select(i => total[rnd.Next(total.Length)])
                .ToArray();

            return new string(chars);
        }

        public static bool In(this int value, params int[] inList)
        {
            for (var i = 0; i < inList.Length; i++)
            {
                if (inList[i] == value)
                    return true;
            }
            return false;
        }

        public static bool Between(this int value, int lowest, int highest)
        {
            return value >= lowest && value <= highest;
        }

        const string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public static string RandomString(this int size)
        {
            var result = new char[size];
            var length = ALPHABET.Length;
            for (var i = 0; i < size; i++)
            {
                result[i] = ALPHABET[RandomGen.Next(length)];
            }
            return new string(result);
        }

        static readonly Random RandomGen = new Random();

        public static bool EqualZero(this int value)
        {
            return value == 0;
        }

        public static bool NotEqualZero(this int value)
        {
            return value != 0;
        }

        public static bool GreaterThanZero(this int value)
        {
            return value > 0;
        }

        public static bool LessThanZero(this int value)
        {
            return value > 0;
        }

        public static bool GreaterAndEqualZero(this int value)
        {
            return value >= 0;
        }

        const string ALPHABET2 = "8GZFQ4TJEHSBR795D03VWCPL1MYXOUK6AI2N";
        public static string GenerateCode(this decimal value)
        {
            var sb = new StringBuilder();
            decimal alphabetLength = ALPHABET2.Length;
            while (true)
            {
                var letter = Math.Floor(value / alphabetLength) - 1;
                sb.Insert(0, ALPHABET2[(int)(value % alphabetLength)]);
                if (letter < 0)
                    break;
                value = letter;
            }
            return sb.ToString();
        }
    }
}
