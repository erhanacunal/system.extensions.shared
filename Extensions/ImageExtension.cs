﻿#if COREFULL || DRAWING
using System.Drawing;

namespace System.Extensions
{
    public static class ImageExtension
    {
        public static Image CropImage(this Image img, Rectangle cropArea)
        {
            var bmpImage = new Bitmap(img);
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }
    }
}

#endif