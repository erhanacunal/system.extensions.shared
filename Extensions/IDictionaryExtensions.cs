﻿using System.Collections.Specialized;
using System.Data;
using System.Utils;

namespace System.Collections.Generic
{
    public static class DictionaryExtensions
    {

        public static V GetValue<K, V>(this IDictionary<K, V> dict, K key, V defaultValue)
        {
            if (!dict.TryGetValue(key, out var result))
                result = defaultValue;
            return result;
        }

        public static K GetKey<K, V>(this IDictionary<K, V> dict, V value)
        {
            foreach (var kv in dict)
            {
                if (kv.Value.Equals(value))
                    return kv.Key;
            }
            return default(K);
        }

        public static void PopulateFromEnum<T>(this IDictionary<string, T> target) where T : struct
        {
            foreach (var val in Enum.GetValues(typeof(T)))
            {
                target.Add(val.ToString(), (T)val);
            }
        }

        /// <summary>
        /// Sözlüğün Value tipi List&lt;<typeparamref name="TK"/>&gt; olan değerine verilen <paramref name="value"/> değerini ekler
        /// Eğer daha önce sözlükte yoksa List&lt;<typeparamref name="TK"/>&gt; sınıfı başlatır ve o listeye verilen <paramref name="value"/> değerini ekler
        /// </summary>
        /// <typeparam name="TK">Sözlüğün anahtar tipi</typeparam>
        /// <typeparam name="TV">Sözlüğün Liste sınıfının eleman tipi</typeparam>
        /// <param name="instance">Sözlüğün örneği</param>
        /// <param name="key">Eklenecek anahtar değeri</param>
        /// <param name="value">Eklenecek eleman değeri</param>
        /// <param name="distinct">Eğer bu parametre <c>true</c> ise <paramref name="value"/> değeri daha önce eklenmişse bu değer listede yer almaz</param>
        public static void AddToListValue<TK, TV>(this IDictionary<TK, List<TV>> instance, TK key, TV value, bool distinct = false)
        {
            List<TV> lst;
            if (!instance.TryGetValue(key, out lst))
            {
                lst = new List<TV>();
                instance.Add(key, lst);
            }
            if (distinct)
                lst.AddChecked(value);
            else
                lst.Add(value);
        }

        

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> instance, TKey key, TValue value)
        {
            if (instance.TryGetValue(key, out var result))
                return result;
            instance.Add(key, value);
            return value;
        }

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> instance, TKey key, Func<TKey, TValue> valueFactory)
        {
            if (instance.TryGetValue(key, out var result))
                return result;
            result = valueFactory(key);
            instance.Add(key, result);
            return result;
        }

        public static object[] PopulateAsArray<TKey, TValue>(this IDictionary<TKey, TValue> instance)
        {
            var result = new object[instance.Count * 2];
            var i = 0;
            foreach (var kv in instance)
            {
                result[i * 2] = kv.Key;
                result[(i * 2) + 1] = kv.Value;
                i++;
            }

            return result;
        }

        public static object[] PopulateAsArray(this NameValueCollection instance)
        {
            var result = new object[instance.Count * 2];
            var i = 0;
            foreach (var key in instance.AllKeys)
            {
                result[i * 2] = key;
                result[(i * 2) + 1] = instance.Get(key);
                i++;
            }

            return result;
        } 

        public static DataTable ToDataTable<TK, TV>(this IDictionary<TK, List<TV>> instance, string tableName, string keyColumnName,
            string valueColumnName)
        {
            var result = new DataTable(tableName);
            result.Columns.Add(keyColumnName, typeof(TK));
            result.Columns.Add(valueColumnName, typeof(TV));
            foreach (var kv in instance)
                foreach (var value in kv.Value)
                    result.Rows.Add(kv.Key, value);
            return result;
        }

        public static void PopulateFromObjects<K, V>(this IDictionary<K, V> source, params object[] items)
        {
            for (var i = 0; i < items.Length; i += 2)
            {
                source.Add(
                    TypeUtils.CoerceValue<K>(items[i]),
                    TypeUtils.CoerceValue<V>(items[i + 1]));
            }
        }
    }
}
