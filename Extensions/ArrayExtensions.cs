﻿using System.Text;

namespace System {
    public static class ArrayExtensions {
        static readonly char[] Lookup = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        public static string ToHex( this byte[] arr ) {
            var sb = new StringBuilder( ( arr.Length * 2 ) + 2 );
            sb.Append( "0x" );
            for ( int i = 0; i < arr.Length; i++ ) {
                byte val = arr[i];
                sb.Append( Lookup[val >> 4] );
                sb.Append( Lookup[val & 0xF] );
            }
            return sb.ToString();
        }

        public static byte[] ToByteArray( this string s ) {                        
            byte[] result = new byte[( s.Length - 2 ) / 2];
            int idx = 0;
            for ( int i = 2; i < s.Length; i += 2 ) {
                int high = (byte)s[i];
                int low = (byte)s[i + 1];
                if ( high >= 97 && high <= 102 )
                    high -= 87;
                else if ( high >= 65 && high <= 70 )
                    high -= 55;
                else
                    high -= 48;
                if ( low >= 97 && low <= 102 )
                    low -= 87;
                else if ( low >= 65 && low <= 70 )
                    low -= 55;
                else
                    low -= 48;
                result[idx++] = (byte)( ( high << 4 ) | low );
            }
            return result;
        }

        public static T[] Fill<T>( this T[] destinationArray, params T[] value ) {
            if ( destinationArray == null ) {
                throw new ArgumentNullException( nameof(destinationArray) );
            }

            if ( value.Length > destinationArray.Length ) {
                throw new ArgumentException( "Length of value array must not be more than length of destination" );
            }

            // set the initial array value
            Array.Copy( value, destinationArray, value.Length );

            int copyLength, nextCopyLength;

            for ( copyLength = value.Length; ( nextCopyLength = copyLength << 1 ) < destinationArray.Length; copyLength = nextCopyLength ) {
                Array.Copy( destinationArray, 0, destinationArray, copyLength, copyLength );
            }

            Array.Copy( destinationArray, 0, destinationArray, copyLength, destinationArray.Length - copyLength );
            return destinationArray;
        }

    }
}
