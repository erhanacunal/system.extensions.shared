﻿
using System.Collections.Generic;
using System.Text;
using System.Utils;

namespace System.IO
{
    public static class BinaryReaderWriterExtensions
    {

        public static DateTime ReadDateTime(this BinaryReader reader)
        {
            return new DateTime(reader.ReadInt64());
        }

        public static Guid ReadGuid(this BinaryReader reader)
        {
            return new Guid(reader.ReadBytes(16));
        }

        public static void WriteDateTime(this BinaryWriter writer, DateTime value)
        {
            writer.Write(value.Ticks);
        }

        public static void WriteInt32Encoded(this BinaryWriter writer, int value)
        {
            // Write out an int 7 bits at a time.  The high bit of the byte,
            // when on, tells reader to continue reading more bytes.
            var v = (uint)value;   // support negative numbers
            while (v >= 0x80)
            {
                writer.Write((byte)(v | 0x80));
                v >>= 7;
            }
            writer.Write((byte)v);
        }

        public static void WriteInt64Encoded(this BinaryWriter writer, long value)
        {
            // Write out an int 7 bits at a time.  The high bit of the byte,
            // when on, tells reader to continue reading more bytes.
            var v = (ulong)value;   // support negative numbers
            while (v >= 0x80)
            {
                writer.Write((byte)(v | 0x80));
                v >>= 7;
            }
            writer.Write((byte)v);
        }

        public static int ReadInt32Encoded(this BinaryReader reader)
        {
            // Read out an Int32 7 bits at a time.  The high bit
            // of the byte when on means to continue reading more bytes.
            var count = 0;
            var shift = 0;
            byte b;
            do
            {
                // Check for a corrupted stream.  Read a max of 5 bytes.
                // In a future version, add a DataFormatException.
                if (shift == 5 * 7)  // 5 bytes max per Int32, shift += 7
                    throw new FormatException("Invalid 7 bit encoded int32 value");

                // ReadByte handles end of stream cases for us.
                b = reader.ReadByte();
                count |= (b & 0x7F) << shift;
                shift += 7;
            } while ((b & 0x80) != 0);
            return count;
        }

        public static long ReadInt64Encoded(this BinaryReader reader)
        {
            // Read out an Int64 7 bits at a time.  The high bit
            // of the byte when on means to continue reading more bytes.
            ulong count = 0;
            var shift = 0;
            byte b;
            do
            {
                // Check for a corrupted stream.  Read a max of 9 bytes.
                // In a future version, add a DataFormatException.
                if (shift == 9 * 7)  // 9 bytes max per Int64, shift += 7
                    throw new FormatException("Invalid 7 bit encoded int64 value");

                // ReadByte handles end of stream cases for us.
                b = reader.ReadByte();
                count |= (ulong)(b & 0x7F) << shift;
                shift += 7;
            } while ((b & 0x80) != 0);
            return (long)count;
        }

        public static void WriteObjectEx(this BinaryWriter writer, object val, ReadWriteObjectExOptions opts = null)
        {
            if (opts == null)
                opts = new ReadWriteObjectExOptions();
            if (val == null)
            {
                if (opts.WriteWithTypeCode)
                    writer.Write((int)TypeCodeEx.Empty);
            }
            else
            {
                var tc = TypeUtils.GetTypeCodeEx(val.GetType());
                if (opts.WriteWithTypeCode)
                    writer.Write((int)tc);
                switch (tc)
                {
                    case TypeCodeEx.Boolean:
                        writer.Write((bool)val);
                        break;
                    case TypeCodeEx.Byte:
                        writer.Write((byte)val);
                        break;
                    case TypeCodeEx.Char:
                        writer.Write((char)val);
                        break;
                    case TypeCodeEx.Empty:
                    case TypeCodeEx.DBNull:
                        break;
                    case TypeCodeEx.DateTime:
                        opts.WriteDateTime(writer, (DateTime)val);
                        break;
                    case TypeCodeEx.Decimal:
                        writer.WriteDecimal((decimal)val);
                        break;
                    case TypeCodeEx.Double:
                        writer.Write((double)val);
                        break;
                    case TypeCodeEx.Int16:
                        writer.Write((short)val);
                        break;
                    case TypeCodeEx.Int32:
                        writer.Write((int)val);
                        break;
                    case TypeCodeEx.Int64:
                        writer.Write((long)val);
                        break;
                    case TypeCodeEx.ByteArray:
                        var barr = (byte[])val;
                        writer.Write(barr.Length);
                        writer.Write(barr);
                        break;
                    case TypeCodeEx.Guid:
                        var guid = (Guid)val;
                        writer.Write(guid.ToByteArray());
                        break;
                    case TypeCodeEx.CharArray:
                        var chrs = (char[])val;
                        var carr = Encoding.UTF8.GetBytes(chrs);
                        writer.Write(carr.Length);
                        writer.Write(carr);
                        break;
                    case TypeCodeEx.StringArray:
                        var arr = (string[])val;
                        writer.Write(arr.Length);
                        for (var i = 0; i < arr.Length; i++)
                        {
                            writer.Write(arr[i].ToBase64());
                        }
                        break;
                    case TypeCodeEx.SByte:
                        writer.Write((sbyte)val);
                        break;
                    case TypeCodeEx.Single:
                        writer.Write((Single)val);
                        break;
                    case TypeCodeEx.String:
                        opts.WriteString(writer, (string)val);
                        break;
                    case TypeCodeEx.UInt16:
                        writer.Write((ushort)val);
                        break;
                    case TypeCodeEx.UInt32:
                        writer.Write((uint)val);
                        break;
                    case TypeCodeEx.UInt64:
                        writer.Write((ulong)val);
                        break;
                    case TypeCodeEx.TimeSpan:
                        writer.Write(((TimeSpan)val).Ticks);
                        break;
                    case TypeCodeEx.ObjectArray:
                        var objArr = (object[])val;
                        writer.Write(objArr.Length);
                        foreach (var obj in objArr)
                        {
                            writer.WriteObjectEx(obj, opts);
                        }
                        break;
                    case TypeCodeEx.Type:
                        writer.Write(((Type)val).FullName);
                        break;
                    default:
                        throw new InvalidOperationException(tc + " cannot be written!");

                }
            }
        }

        public static object ReadObjectEx(this BinaryReader reader, TypeCodeEx? tc = null, ReadWriteObjectExOptions opts = null)
        {
            if (opts == null)
                opts = new ReadWriteObjectExOptions();
            if (opts.WriteWithTypeCode)
                tc = (TypeCodeEx)reader.ReadInt32();
            else if (!tc.HasValue)
                throw new InvalidOperationException("TypeCode must be specified!");
            switch (tc.Value)
            {
                case TypeCodeEx.Boolean:
                    return reader.ReadBoolean();
                case TypeCodeEx.Byte:
                    return reader.ReadByte();
                case TypeCodeEx.Char:
                    return reader.ReadChar();
                case TypeCodeEx.DBNull:
                    return DBNull.Value;
                case TypeCodeEx.DateTime:
                    return opts.ReadDateTime(reader);
                case TypeCodeEx.Decimal:
                    return reader.ReadDecimalEx();
                case TypeCodeEx.Double:
                    return reader.ReadDouble();
                case TypeCodeEx.Empty:
                    return null;
                case TypeCodeEx.Int16:
                    return reader.ReadInt16();
                case TypeCodeEx.Int32:
                    return reader.ReadInt32();
                case TypeCodeEx.Int64:
                    return reader.ReadInt64();
                case TypeCodeEx.SByte:
                    return reader.ReadSByte();
                case TypeCodeEx.Single:
                    return reader.ReadSingle();
                case TypeCodeEx.String:
                    return opts.ReadString(reader);
                case TypeCodeEx.UInt16:
                    return reader.ReadUInt16();
                case TypeCodeEx.UInt32:
                    return reader.ReadUInt32();
                case TypeCodeEx.UInt64:
                    return reader.ReadUInt64();
                case TypeCodeEx.ByteArray:
                    return reader.ReadBytes(reader.ReadInt32());
                case TypeCodeEx.CharArray:
                    return reader.ReadChars(reader.ReadInt32());
                case TypeCodeEx.StringArray:
                    var sarr = new string[reader.ReadInt32()];
                    for (var i = 0; i < sarr.Length; i++)
                    {
                        sarr[i] = reader.ReadString().FromBase64();
                    }
                    return sarr;
                case TypeCodeEx.ObjectArray:
                    var oarr = new object[reader.ReadInt32()];
                    for (var i = 0; i < oarr.Length; i++)
                    {
                        oarr[i] = reader.ReadObjectEx(opts: opts);
                    }
                    return oarr;
                case TypeCodeEx.Guid:
                    return new Guid(reader.ReadBytes(16));
                case TypeCodeEx.TimeSpan:
                    return new TimeSpan(reader.ReadInt64());
                case TypeCodeEx.Type:
                    return Type.GetType(reader.ReadString());
                default:
                    throw new NotImplementedException(tc.Value + " cannot be read!");
            }
        }

        public static void WriteDecimal(this BinaryWriter bw, decimal val)
        {
            var bits = decimal.GetBits(val);
            bw.Write((byte)bits.Length);
            foreach (var bit in bits)
            {
                bw.Write(bit);
            }
        }

        public static decimal ReadDecimalEx(this BinaryReader reader)
        {
            var length = reader.ReadByte();
            var bits = new int[length];
            for (var i = 0; i < length; i++)
            {
                bits[i] = reader.ReadInt32();
            }
            return new decimal(bits);
        }

        public static void WriteObjects(this BinaryWriter writer, bool encodeString, bool withTypeCode, IEnumerable<object> args)
        {
            var opts = new ReadWriteObjectExOptions() { EncodeString = encodeString, WriteWithTypeCode = withTypeCode };
            foreach (var arg in args)
            {
                writer.WriteObjectEx(arg, opts);
            }
        }

        public static string ReadLengthPrefixedString(this BinaryReader reader, Encoding enc)
        {
            var len = reader.ReadInt32();
            if (len == 0)
                return string.Empty;
            return enc.GetString(reader.ReadBytes(len), 0, len - (enc.IsSingleByte ? 1 : 2));
        }

        public static string ReadString(this BinaryReader reader, Encoding enc, int length)
        {
            return enc.GetString(reader.ReadBytes(length), 0, length - 1);
        }

    }

    public sealed class ReadWriteObjectExOptions
    {

        public DateTimeConversionMethod DateTimeConversion { get; set; }
        public bool WriteWithTypeCode { get; set; }
        public bool EncodeString { get; set; }

        public ReadWriteObjectExOptions()
        {
            DateTimeConversion = DateTimeConversionMethod.Ticks;
            WriteWithTypeCode = true;
            EncodeString = true;
        }

        internal void WriteDateTime(BinaryWriter writer, DateTime val)
        {
            switch (DateTimeConversion)
            {
                case DateTimeConversionMethod.Ticks:
                    writer.Write(val.Ticks);
                    break;
                case DateTimeConversionMethod.UnixTimeStamp:
                    writer.Write(val.ToUnixTimestamp());
                    break;
                case DateTimeConversionMethod.OleDate:
                    writer.Write(val.ToOADate());
                    break;
            }
        }

        internal DateTime ReadDateTime(BinaryReader reader)
        {
            switch (DateTimeConversion)
            {
                case DateTimeConversionMethod.Ticks:
                    return new DateTime(reader.ReadInt64());
                case DateTimeConversionMethod.UnixTimeStamp:
                    return reader.ReadInt64().FromUnixTimestampToDateTime();
                case DateTimeConversionMethod.OleDate:
                    return DateTime.FromOADate(reader.ReadDouble());
                default:
                    return new DateTime();
            }
        }

        internal void WriteString(BinaryWriter writer, string val)
        {
            if (EncodeString)
                writer.Write(val.ToBase64());
            else
                writer.Write(val);
        }

        internal string ReadString(BinaryReader reader)
        {
            var str = reader.ReadString();
            return EncodeString ? str.FromBase64() : str;
        }


    }

    public enum DateTimeConversionMethod
    {
        Ticks,
        UnixTimeStamp,
        OleDate
    }

    public static class PathEx
    {

        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException(nameof(fromPath));
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException(nameof(toPath));

            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            return relativePath.Replace('/', Path.DirectorySeparatorChar);
        }

    }
}