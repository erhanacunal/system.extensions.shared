﻿using System;

namespace System.Diagnostics.Telemetry.Models
{
    public class ExceptionDto
    {
        public string ProductCode { get; set; }
        public string Details { get; set; }
        public DateTime ThrowDate { get; set; }
    }

    public class LogEntryDto
    {
        public string ProductCode { get; set; }
        public string LogLevel { get; set; }
        public string LoggerName { get; set; }
        public string Message { get; set; }
        public ExceptionDto Exception { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class FeatureUsageDto
    {
        public string FeatureCode { get; set; }
        public string UsageDetails { get; set; }
    }
}
