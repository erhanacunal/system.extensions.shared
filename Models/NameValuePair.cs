﻿namespace System.Custom.Models
{
    public class NameValuePair<T1, T2>
    {
        public T1 Name { get; set; }
        public T2 Value { get; set; }

        public NameValuePair()
        {

        }

        public NameValuePair(T1 name, T2 value)
        {
            Name = name;
            Value = value;
        }
    }
}
