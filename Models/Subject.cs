﻿using System;
using System.Collections.Generic;

namespace System.Custom.Models
{
    public abstract class Subject<T> : IObservable<T>, IObserver<T>
    {

        List<IObserver<T>> subscriptions;
        readonly bool clearOnCompleted;

        protected Subject(bool clearOnCompleted)
        {
            this.clearOnCompleted = clearOnCompleted;
        }

        protected Subject()
            : this(true)
        {

        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (subscriptions == null)
                subscriptions = new List<IObserver<T>>();
            subscriptions.Add(observer);
            return new UnsubscribeObserver(this, observer);
        }

        public IDisposable Subscribe(Action<T> onNext, Action onCompleted = null, Action<Exception> onError = null)
        {
            return Subscribe(new DelegateObserver<T>(onNext, onCompleted, onError));
        }

        public virtual void OnNext(T value)
        {
            if (subscriptions == null) return;
            foreach (var observer in subscriptions)
            {
                observer.OnNext(value);
            }
        }

        public virtual void OnError(Exception ex)
        {
            if (subscriptions == null) return;
            foreach (var observer in subscriptions)
            {
                observer.OnError(ex);
            }
        }

        public virtual void OnCompleted()
        {
            if (subscriptions == null) return;
            foreach (var observer in subscriptions)
            {
                observer.OnCompleted();
            }
            if (!clearOnCompleted) return;
            subscriptions.Clear();
            subscriptions = null;
        }

        void Unsubscribe(IObserver<T> observer)
        {
            if (observer == null || subscriptions == null) return;
            subscriptions.Remove(observer);
            if (subscriptions.Count == 0)
                subscriptions = null;
        }

        sealed class UnsubscribeObserver : IDisposable
        {
            Subject<T> subject;
            IObserver<T> observer;

            public UnsubscribeObserver(Subject<T> subject, IObserver<T> observer)
            {
                this.subject = subject;
                this.observer = observer;
            }

            public void Dispose()
            {
                subject?.Unsubscribe(observer);
                subject = null;
                observer = null;
            }
        }

    }

    public sealed class DelegateObserver<T> : IObserver<T>
    {
        readonly Action onCompletedHandler;
        readonly Action<T> onNextHandler;
        readonly Action<Exception> onErrorHandler;

        public DelegateObserver(Action<T> onNext, Action onCompleted = null, Action<Exception> onError = null)
        {
            onNextHandler = onNext;
            onCompletedHandler = onCompleted;
            onErrorHandler = onError;
        }

        public void OnCompleted()
        {
            onCompletedHandler?.Invoke();
        }

        public void OnError(Exception error)
        {
            onErrorHandler?.Invoke(error);
        }

        public void OnNext(T value)
        {
            onNextHandler?.Invoke(value);
        }
    }

}
