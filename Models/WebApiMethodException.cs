﻿using System;
using JetBrains.Annotations;

namespace System.Custom.Models
{
    public sealed class WebApiMethodException : Exception
    {
        readonly FakeException exception;

        public WebApiMethodException([NotNull] FakeException exception, string controllerName, string action) : base(exception.ExceptionMessage.IfNullOrWhiteSpace(exception.Message))
        {
            this.exception = exception ?? throw new ArgumentNullException(nameof(exception));
            Source = $"{controllerName}/{action}";
        }

        public override string StackTrace => exception.StackTrace;

        public override string ToString() => exception.ToString();

    }
}