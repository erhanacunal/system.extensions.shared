﻿using System;

namespace System.Custom.Models
{
    public sealed class FakeException
    {
        public string Message { get; set; }
        public FakeException InnerException { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionMessage { get; set; }
        public string StackTrace { get; set; }

        public override string ToString()
        {
            var message = Message;
            string result;

            if (message == null || message.Length <= 0)
            {
                result = ExceptionType;
            }
            else
            {
                result = ExceptionType + ": " + message;
            }

            if (InnerException != null)
            {
                result = result + " ---> " + InnerException + Environment.NewLine +
                    "   --- End of inner exception stack trace ---";
            }

            if (StackTrace.IsNotNullOrWhitespace())
                result += Environment.NewLine + StackTrace;
            return result;
        }
    }
}
