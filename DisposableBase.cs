﻿namespace System {
    public abstract class DisposableBase : IDisposable {

        bool isDisposed;

        protected bool IsDisposed => isDisposed;

        void Dispose( bool disposing ) {
            if ( isDisposed )
                return;
            DisposeCore();
            isDisposed = true;
        }

        protected abstract void DisposeCore();

        public void Dispose() {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        ~DisposableBase() {
            Dispose( false );
        }
    }

    public sealed class UsingAction : DisposableBase {

        readonly Action endAction;
        public UsingAction( Action endAction ) {
            this.endAction = endAction;
        }


        protected override void DisposeCore() {
            endAction?.Invoke();
        }
    }
}
