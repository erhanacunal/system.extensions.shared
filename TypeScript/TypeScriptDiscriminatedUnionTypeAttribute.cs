﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class TypeScriptDiscriminatedUnionTypeAttribute : Attribute
    {
        public string DiscriminantPropertyName { get; }
        public TypeScriptDiscriminatedUnionTypeAttribute(string discriminantPropertyName)
        {
            DiscriminantPropertyName = discriminantPropertyName;
        }
    }
}

#endif