﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class TypeScriptClientSidePropertyAttribute : Attribute
    {
        public string Name { get; }
        public string PropertyTypeName { get; }
        public Type PropertyType { get; }

        public TypeScriptClientSidePropertyAttribute(string name, Type propertyType)
        {
            Name = name;
            PropertyType = propertyType;
        }

        public TypeScriptClientSidePropertyAttribute(string name, string propertyTypeName)
        {
            Name = name;
            PropertyTypeName = propertyTypeName;
        }
    }
}

#endif