﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Assembly, AllowMultiple = true)]
    public sealed class TypeScriptEmitTypeAttribute : Attribute
    {
        public Type Type { get; }

        public TypeScriptEmitTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}

#endif