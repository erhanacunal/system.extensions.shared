﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    public class TypeScriptIgnore : Attribute
    {
        public TypeScriptIgnore()
        {

        }
    }
}

#endif