﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    /// <summary>
    /// Web api controller sınıflarına eklendiğinde aynı dosya isminde olanları bir dosyada oluşturur
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class TypeScriptFileGroupAttribute : Attribute
    {
        public string FileName { get; }

        public TypeScriptFileGroupAttribute(string fileName)
        {
            FileName = fileName;
        }
    }
}

#endif