﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.ReturnValue)]
    public sealed class TypeScriptTypeAttribute : Attribute
    {
        public Type Type { get; }
        public string TypeName { get; }

        public TypeScriptTypeAttribute(string typeName)
        {
            TypeName = typeName;
        }

        public TypeScriptTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}

#endif