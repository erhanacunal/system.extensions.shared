﻿#if COREFULL || TYPESCRIPT
using System;
using System.Diagnostics;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Property)]
    public sealed class TypeScriptFlagAttribute : Attribute
    {
        public TypeScriptFlagAttribute(string flags)
        {
            Flags = flags;
        }

        public string Flags { get; }

    }
}

#endif