﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.TypeScript
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TypeScriptCancellableAttribute: Attribute
    {
    }
}
