﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class TypeScriptEnableBatchOperationsAttribute : Attribute
    {
    }
}

#endif