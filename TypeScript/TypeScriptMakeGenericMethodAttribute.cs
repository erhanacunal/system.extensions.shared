﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TypeScriptMakeGenericMethodAttribute : Attribute
    {
        public TypeScriptMakeGenericMethodAttribute(params Type[] types)
        {
            Types = types;
        }

        public Type[] Types { get; }
    }
} 
#endif