#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TypeScriptUtilityFunctionAttribute : Attribute
    {
        public string Namespace { get; }
        public string FileName { get; }

        public TypeScriptUtilityFunctionAttribute(string @namespace, string fileName)
        {
            Namespace = @namespace;
            FileName = fileName;
        }
    }
} 
#endif