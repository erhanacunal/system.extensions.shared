﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TypeScriptAjaxOptionAttribute : Attribute
    {
        public string Property { get; }
        public string RawValue { get; }
        public AjaxOptionsProperty OptionsProperty { get; }

        public TypeScriptAjaxOptionAttribute(string property, string rawValue, AjaxOptionsProperty optionsProperty)
        {
            Property = property;
            RawValue = rawValue;
            OptionsProperty = optionsProperty;
        }
    }

    public enum AjaxOptionsProperty
    {
        Root,
        Header
    }
}

#endif