﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class TypeScriptClientSideBaseTypeAttribute : Attribute
    {
        public Type BaseType { get; }
        public string BaseTypeName { get; }
        /// <summary>
        /// Yeni bir <see cref="TypeScriptClientSideBaseTypeAttribute"/> örneği başlatır
        /// </summary>
        /// <param name="baseTypeName">Client tarafında temel tiplere eklenecek tipin tam adı (namespace dahil). Generic tipler için {0} argümanı bulunduğu tipin adını gösterir.</param>
        /// <example>
        /// [TypeScriptClientSideBaseType("KendoNode&lt;0&gt;")] -> interface XXX extends KendoNode&lt;XXX&gt; şeklinde aktarılır.
        /// </example>
        public TypeScriptClientSideBaseTypeAttribute(string baseTypeName)
        {
            BaseTypeName = baseTypeName;
        }

        public TypeScriptClientSideBaseTypeAttribute(Type baseType)
        {
            BaseType = baseType;
        }
    }
}

#endif