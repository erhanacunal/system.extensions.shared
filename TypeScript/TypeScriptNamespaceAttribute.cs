﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface)]
    public sealed class TypeScriptNamespaceAttribute : Attribute
    {
        public string Namespace { get; set; }

        public TypeScriptNamespaceAttribute(string @namespace)
        {
            Namespace = @namespace;
        }
    }
}

#endif