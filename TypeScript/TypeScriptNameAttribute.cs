﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Method)]
    public sealed class TypeScriptNameAttribute : Attribute
    {
        public string NewName { get; }

        public TypeScriptNameAttribute(string newName)
        {
            NewName = newName;
        }
    }
}

#endif