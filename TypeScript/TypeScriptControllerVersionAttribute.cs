﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class TypeScriptControllerVersionAttribute : Attribute
    {
        public int Version { get; }

        public TypeScriptControllerVersionAttribute(int version)
        {
            Version = version;
        }
    }
}

#endif