﻿#if COREFULL || TYPESCRIPT
using System;

namespace System.Extensions.TypeScript
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TypeScriptBusyContentAttribute : Attribute
    {
        public string Message { get; }

        public TypeScriptBusyContentAttribute(string message)
        {
            Message = message;
        }
    }
}

#endif