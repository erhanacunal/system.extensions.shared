﻿#if IISUTILS
using System.DirectoryServices;
using System.Security.Principal;
using Microsoft.Win32;

namespace System.Utils
{
    public static class SystemInfo
    {

        static bool? isRunningAsAdministrator;

        public static bool IsRunningAsAdministrator
        {
            get
            {
                if (isRunningAsAdministrator == null)
                {
                    WindowsIdentity identity = WindowsIdentity.GetCurrent();
                    var pricipal = new WindowsPrincipal(identity);
                    isRunningAsAdministrator = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
                }
                return isRunningAsAdministrator.Value;
            }
        }

        static Version iisVersion = null;

        public static Version IisVersion
        {
            get
            {
                if (iisVersion == null)
                {
                    iisVersion = new Version();
                    using (var ms = Registry.LocalMachine.OpenSubKey("Software\\Microsoft", false))
                    {
                        if (ms != null)
                        {
                            using (var inetsetup = ms.OpenSubKey("Inetstp"))
                            {
                                if (inetsetup != null)
                                {
                                    iisVersion = new Version(
                                        Convert.ToInt32(inetsetup.GetValue("MajorVersion", 0)),
                                        Convert.ToInt32(inetsetup.GetValue("MinorVersion", 0)));
                                }
                            }
                        }
                    }
                }
                return iisVersion;
            }
        }

        static string iisWwwRoot = null;

        public static string IisWwwRoot
        {
            get
            {
                if (iisWwwRoot != null) return iisWwwRoot;
                iisWwwRoot = "";
                using (var ms = Registry.LocalMachine.OpenSubKey("Software\\Microsoft", false))
                {
                    if (ms != null)
                    {
                        using (var inetsetup = ms.OpenSubKey("Inetstp"))
                        {
                            if (inetsetup != null)
                            {
                                iisWwwRoot = Convert.ToString(inetsetup.GetValue("PathWWWRoot", string.Empty));
                            }
                        }
                    }
                }
                return iisWwwRoot;
            }
        }

        public static string IisApplicationPoolIdentity
        {
            get
            {
                try
                {
                    if (IisVersion.Major > 0)
                    {
                        DirectoryEntry root = new DirectoryEntry("IIS://localhost/W3SVC");
                        if (root.Properties.Contains("AnonymousUserName"))
                        {
                            var userName = (string)root.Properties["AnonymousUserName"].Value;
                            return "IUSR".Equals(userName) ? "IIS_IUSRS" : userName;
                        }
                    }
                }
                catch { /**/ }
                return string.Empty;
            }
        }
    }
}

#endif