#if COREFULL || JSON
using System;

namespace System.MyConverters
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class EnumTargetTypeAttribute : Attribute
    {
        public Type Type { get; }

        public EnumTargetTypeAttribute(Type type)
        {
            Type = type;
        }
    }
} 
#endif