﻿#if COREFULL || JSON
using System;
using System.Reflection;
using System.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace System.MyConverters
{
    public class EnumJsonConverter<TTargetType, TEnum> : CustomCreationConverter<TTargetType> where TTargetType : class where TEnum : struct
    {
        readonly string propertyName;

        public EnumJsonConverter()
        {
            var attr = (EnumPropertyNameAttribute)typeof(TEnum).GetCustomAttribute(typeof(EnumPropertyNameAttribute), true);
            if (attr == null)
                throw new InvalidOperationException("Enum tipte EnumPropertyName niteliği bulunamadı!");
            propertyName = attr.PropertyName;
        }

        public override TTargetType Create(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var obj = JObject.Load(reader);
            var type = TypeUtils.CoerceValue<TEnum>(obj.Value<int>(propertyName));
            var attr = type.GetAttribute<EnumTargetTypeAttribute, TEnum>();
            if (attr == null)
                throw new InvalidOperationException($"'{propertyName}' property value is not defined -> " + type);
            var result = Activator.CreateInstance(attr.Type);
            serializer.Populate(obj.CreateReader(), result);
            return result;
        }
    }
}

#endif