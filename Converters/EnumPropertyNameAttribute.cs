﻿#if COREFULL || JSON
using System;

namespace System.MyConverters
{
    /// <summary>
    /// <see cref="EnumJsonConverter{TTargetType,TEnum}"/> sınıfının çalışması için gerekli property adını belirlemeye yarar.
    /// Burada belirtilen property'nin adına bakarak hangi tipin başlatılacağına karar vermesine yardımcı olur.
    /// </summary>
    /// <remarks>
    /// Json dökümanındaki property ismi olmalıdır
    /// </remarks>
    [AttributeUsage(AttributeTargets.Enum)]
    public sealed class EnumPropertyNameAttribute : Attribute
    {
        public string PropertyName { get; }

        public EnumPropertyNameAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }
    }
}

#endif