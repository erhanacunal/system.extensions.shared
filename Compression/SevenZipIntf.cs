﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SevenZip;

namespace System.IO.Compression
{
    public sealed class SevenZipIntf
    {

        public static void Compress(Stream inStream, Stream outStream, SevenZipOptions options)
        {
            CoderPropID[] propIDs =
            {
                CoderPropID.DictionarySize,
                CoderPropID.PosStateBits,
                CoderPropID.LitContextBits,
                CoderPropID.LitPosBits,
                CoderPropID.Algorithm,
                CoderPropID.NumFastBytes,
                CoderPropID.MatchFinder,
                CoderPropID.EndMarker
            };
            object[] properties =
            {
                options.DictionarySize,
                options.PosStateBits,
                options.LitContextBits,
                options.LitPosBits,
                options.Algorithm,
                options.NumberOfFastBytes,
                options.MatchFinder,
                options.WriteEndOfStreamMarker
            };
            var encoder = new SevenZip.Compression.LZMA.Encoder();
            encoder.SetCoderProperties(propIDs, properties);
            encoder.WriteCoderProperties(outStream);
            long fileSize;
            if (options.WriteEndOfStreamMarker)
                fileSize = -1;
            else
                fileSize = inStream.Length;
            for (var i = 0; i < 8; i++)
                outStream.WriteByte((byte)(fileSize >> (8 * i)));
            encoder.Code(inStream, outStream, -1, -1, null);
        }

        public static void Decompress(Stream inStream, Stream outStream)
        {
            var properties = new byte[5];
            if (inStream.Read(properties, 0, 5) != 5)
                throw new Exception("input .lzma is too short");
            var decoder = new SevenZip.Compression.LZMA.Decoder();
            decoder.SetDecoderProperties(properties);
            var outSize = 0L;
            for (var i = 0; i < 8; i++)
            {
                var v = inStream.ReadByte();
                if (v < 0)
                    throw (new Exception("Can't Read 1"));
                outSize |= (long)(byte)v << (8 * i);
            }
            var compressedSize = inStream.Length - inStream.Position;
            decoder.Code(inStream, outStream, compressedSize, outSize, null);
        }
    }

    public sealed class SevenZipOptions
    {
        public int DictionarySize { get; set; } = 1 << 23;
        public int PosStateBits { get; set; } = 2;
        public int LitContextBits { get; set; } = 3;
        public int LitPosBits { get; set; } = 0;
        public int Algorithm { get; set; } = 2;
        public int NumberOfFastBytes { get; set; } = 128;
        public string MatchFinder { get; set; } = "bt4";
        public bool WriteEndOfStreamMarker { get; set; }

    }
}

#endif