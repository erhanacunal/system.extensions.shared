﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Collections {
    /// <summary>
    /// Represents an ordered sequence of weak references.
    /// </summary>
    public sealed class WeakList<T> : IEnumerable<T>
        where T : class {
        WeakReference<T>[] _items;
        static readonly WeakReference<T>[] Empty = new WeakReference<T>[0];
        int _size;

        public WeakList() {
            _items = Empty;
        }

        void Resize() {
            Debug.Assert( _size == _items.Length );
            Debug.Assert( _items.Length == 0 || _items.Length >= MINIMAL_NON_EMPTY_SIZE );

            int alive = _items.Length;
            int firstDead = -1;
            for (int i = 0; i < _items.Length; i++) {
                if (!_items[i].TryGetTarget( out _ )) {
                    if (firstDead == -1) {
                        firstDead = i;
                    }

                    alive--;
                }
            }

            if (alive < _items.Length / 4) {
                // If we have just a few items left we shrink the array.
                // We avoid expanding the array until the number of new items added exceeds half of its capacity.
                Shrink( firstDead, alive );
            }
            else if (alive >= 3 * _items.Length / 4) {
                // If we have a lot of items alive we expand the array since just compacting them 
                // wouldn't free up much space (we would end up calling Resize again after adding a few more items).
                var newItems = new WeakReference<T>[GetExpandedSize( _items.Length )];

                if (firstDead >= 0) {
                    Compact( firstDead, newItems );
                }
                else {
                    Array.Copy( _items, 0, newItems, 0, _items.Length );
                    Debug.Assert( _size == _items.Length );
                }

                _items = newItems;
            }
            else {
                // Compact in-place to make space for new items at the end.
                // We will free up to length/4 slots in the array.
                Compact( firstDead, _items );
            }

            Debug.Assert( _items.Length > 0 && _size < 3 * _items.Length / 4, "length: " + _items.Length + " size: " + _size );
        }

        void Shrink( int firstDead, int alive ) {
            int newSize = GetExpandedSize( alive );
            var newItems = ( newSize == _items.Length ) ? _items : new WeakReference<T>[newSize];
            Compact( firstDead, newItems );
            _items = newItems;
        }

        const int MINIMAL_NON_EMPTY_SIZE = 4;

        static int GetExpandedSize( int baseSize ) {
            return Math.Max( ( baseSize * 2 ) + 1, MINIMAL_NON_EMPTY_SIZE );
        }

        /// <summary>
        /// Copies all live references from <see cref="_items"/> to <paramref name="result"/>.
        /// Assumes that all references prior <paramref name="firstDead"/> are alive.
        /// </summary>
        void Compact( int firstDead, WeakReference<T>[] result ) {
            Debug.Assert( _items[firstDead] == null );

            if (!ReferenceEquals( _items, result )) {
                Array.Copy( _items, 0, result, 0, firstDead );
            }

            int oldSize = _size;
            int j = firstDead;
            for (int i = firstDead + 1; i < oldSize; i++) {
                var item = _items[i];

                if (item.TryGetTarget( out _ )) {
                    result[j++] = item;
                }
            }

            _size = j;

            // free WeakReferences
            if (ReferenceEquals( _items, result )) {
                while (j < oldSize) {
                    _items[j++] = null;
                }
            }
        }

        /// <summary>
        /// Returns the number of weak references in this list. 
        /// Note that some of them might not point to live objects anymore.
        /// </summary>
        public int WeakCount => _size;

        public WeakReference<T> GetWeakReference( int index ) {
            if ( index < 0 || index >= _size ) {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }

            return _items[index];
        }

        public void Add( T item ) {
            if ( _size == _items.Length ) {
                Resize();
            }

            Debug.Assert( _size < _items.Length );
            _items[_size++] = new WeakReference<T>( item );
        }

        public IEnumerator<T> GetEnumerator() {
            int count = _size;
            int alive = _size;
            int firstDead = -1;

            for ( int i = 0; i < count; i++ ) {
                if ( _items[i].TryGetTarget( out var item ) ) {
                    yield return item;
                }
                else {
                    // object has been collected 

                    if ( firstDead < 0 ) {
                        firstDead = i;
                    }

                    alive--;
                }
            }

            if ( alive == 0 ) {
                _items = Empty;
                _size = 0;
            }
            else if ( alive < _items.Length / 4 ) {
                // If we have just a few items left we shrink the array.
                Shrink( firstDead, alive );
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        internal WeakReference<T>[] TestOnly_UnderlyingArray => _items;
    }
}
