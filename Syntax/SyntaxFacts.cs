﻿using System;

namespace Sytem.Text.Syntax
{
    public static class SyntaxFacts
    {
        public static bool IsLiteral(this SyntaxKind kind)
        {
            return kind == SyntaxKind.NumericLiteralToken ||
                   kind == SyntaxKind.StringLiteralToken;
        }

        public static bool IsComment(this SyntaxKind kind)
        {
            return kind == SyntaxKind.SingleLineCommentTrivia ||
                   kind == SyntaxKind.MultiLineCommentTrivia;
        }

        public static string GetText(this SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.BitwiseNotToken:
                    return "~";
                case SyntaxKind.AmpersandToken:
                    return "&";
                case SyntaxKind.BarToken:
                    return "|";
                case SyntaxKind.CaretToken:
                    return "^";
                case SyntaxKind.AtToken:
                    return "@";
                case SyntaxKind.LeftParenthesisToken:
                    return "(";
                case SyntaxKind.RightParenthesisToken:
                    return ")";
                case SyntaxKind.PlusToken:
                    return "+";
                case SyntaxKind.MinusToken:
                    return "-";
                case SyntaxKind.AsteriskToken:
                    return "*";
                case SyntaxKind.SlashToken:
                    return "/";
                case SyntaxKind.PercentToken:
                    return "%";
                case SyntaxKind.BackSlashToken:
                    return "\\";
                case SyntaxKind.CommaToken:
                    return ",";
                case SyntaxKind.SemicolonToken:
                    return ";";
                case SyntaxKind.ColonToken:
                    return ":";
                case SyntaxKind.BarBarToken:
                    return "||";
                case SyntaxKind.PlusPlusToken:
                    return "++";
                case SyntaxKind.MinusMinusToken:
                    return "--";
                case SyntaxKind.LeftBracketToken:
                    return "[";
                case SyntaxKind.LeftCurlyBraceToken:
                    return "{";
                case SyntaxKind.RightBracketToken:
                    return "]";
                case SyntaxKind.RightCurlyBraceToken:
                    return "}";
                case SyntaxKind.HashToken:
                    return "#";
                case SyntaxKind.DotToken:
                    return ".";
                case SyntaxKind.EqualsToken:
                    return "=";
                case SyntaxKind.QuestionToken:
                    return "?";
                case SyntaxKind.QuestionQuestionToken:
                    return "??";
                case SyntaxKind.EqualsEqualsToken:
                    return "==";
                case SyntaxKind.ExclamationEqualsToken:
                    return "!=";
                case SyntaxKind.LessGreaterToken:
                    return "<>";
                case SyntaxKind.LessToken:
                    return "<";
                case SyntaxKind.LessEqualToken:
                    return "<=";
                case SyntaxKind.GreaterToken:
                    return ">";
                case SyntaxKind.GreaterEqualToken:
                    return ">=";
                case SyntaxKind.AmpersandAmpersandToken:
                    return "&&";
                case SyntaxKind.LessLessToken:
                    return "<<";
                case SyntaxKind.GreaterGreaterToken:
                    return ">>";
                case SyntaxKind.TrueKeyword:
                    return "TRUE";
                case SyntaxKind.FalseKeyword:
                    return "FALSE";
                case SyntaxKind.NullKeyword:
                    return "NULL";
                default:
                    return string.Empty;
            }
        }

        public static bool Matches(this SyntaxToken token, string text)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            if (text == null)
                throw new ArgumentNullException(nameof(text));

            var comparison = token.IsQuotedIdentifier()
                ? StringComparison.Ordinal
                : StringComparison.OrdinalIgnoreCase;
            return string.Equals(token.ValueText, text, comparison);
        }

        public static bool IsQuotedIdentifier(this SyntaxToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            return token.Kind == SyntaxKind.IdentifierToken &&
                   token.Text.Length > 0 &&
                   token.Text[0] == '"';
        }

        public static bool IsParenthesizedIdentifier(this SyntaxToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            return token.Kind == SyntaxKind.IdentifierToken &&
                   token.Text.Length > 0 &&
                   token.Text[0] == '[';
        }

        public static bool IsTerminated(this SyntaxToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            switch (token.Kind)
            {
                case SyntaxKind.IdentifierToken:
                    if (token.IsQuotedIdentifier())
                        return EndsWithUnescapedChar(token.Text, '"');
                    if (token.IsParenthesizedIdentifier())
                        return EndsWithUnescapedChar(token.Text, ']');
                    return true;
                case SyntaxKind.StringLiteralToken:
                    return EndsWithUnescapedChar(token.Text, '\'');
                default:
                    return true;
            }
        }

        static bool EndsWithUnescapedChar(string text, char c)
        {
            var numberOfChars = 0;
            var i = text.Length - 1;
            while (i > 0 && text[i] == c)
            {
                numberOfChars++;
                i--;
            }
            return numberOfChars % 2 != 0;
        }

        public static bool IsIdentifierOrKeyword(this SyntaxKind kind)
        {
            return kind == SyntaxKind.IdentifierToken || kind.IsKeyword();
        }

        public static bool IsKeyword(this SyntaxKind kind)
        {
            switch (kind)
            {   
                case SyntaxKind.TrueKeyword:
                case SyntaxKind.FalseKeyword:
                case SyntaxKind.NullKeyword:
                    return true;
                default:
                    return false;
            }
        }

        public static SyntaxKind GetKeywordKind(string text)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text));

            switch (text.ToUpper())
            {
                case "FALSE":
                    return SyntaxKind.FalseKeyword;
                case "TRUE":
                    return SyntaxKind.TrueKeyword;
                case "NULL":
                    return SyntaxKind.NullKeyword;
                default:
                    return SyntaxKind.IdentifierToken;
            }
        }

        public static string GetDisplayText(this SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.EndOfFileToken:
                    return "<End-of-file>";

                case SyntaxKind.IdentifierToken:
                    return "<Identifier>";

                case SyntaxKind.NumericLiteralToken:
                    return "<Numeric Literal>";

                case SyntaxKind.StringLiteralToken:
                    return "<String Literal>";
                    
                default:
                    return GetText(kind);
            }
        }

        public static string GetDisplayText(this SyntaxToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            var result = token.Text;
            return !string.IsNullOrEmpty(result) ? result : token.Kind.GetDisplayText();
        }
    }
}
