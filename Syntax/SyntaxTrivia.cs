using System;
using System.Collections.Generic;
using System.IO;

using System.Text;

namespace Sytem.Text.Syntax
{
    public sealed class SyntaxTrivia
    {

        internal SyntaxTrivia(SyntaxKind kind, string text, TextSpan span, Diagnostic[] diagnostics)
        {
            Kind = kind;
            Text = text;
            Span = span;
            Diagnostics = diagnostics;
        }
        
        public SyntaxKind Kind { get; }

        public string Text { get; }

        public TextSpan Span { get; }
        
        public Diagnostic[] Diagnostics { get; }

        public void WriteTo(TextWriter writer)
        {
            if (writer == null)
                throw new ArgumentNullException(nameof(writer));

            writer.Write(Text);
        }

        public override string ToString()
        {
            using (var writer = new StringWriter())
            {
                WriteTo(writer);
                return writer.ToString();
            }
        }
    }
}