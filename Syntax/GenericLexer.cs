﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.Parsers;

namespace Sytem.Text.Syntax
{
    public sealed class GenericLexer
    {
        readonly SourceText text;
        readonly GenericLexerOptions options;
        readonly CharReader2 charReader;
        readonly List<SyntaxTrivia> leadingTrivia = new List<SyntaxTrivia>();
        readonly List<SyntaxTrivia> trailingTrivia = new List<SyntaxTrivia>();
        readonly List<Diagnostic> diagnostics = new List<Diagnostic>();

        SyntaxKind kind;
        object value;
        int start;

        public GenericLexer(SourceText text, GenericLexerOptions options)
        {
            this.text = text;
            this.options = options;
            charReader = new CharReader2(text);
        }

        public SyntaxToken Lex()
        {
            leadingTrivia.Clear();
            diagnostics.Clear();
            start = charReader.Position;
            ReadTrivia(leadingTrivia, false);
            var saveLeadingTrivia = leadingTrivia.ToArray();

            kind = SyntaxKind.BadToken;
            value = null;
            diagnostics.Clear();
            start = charReader.Position;
            ReadToken();
            var end = charReader.Position;
            var saveKind = kind;
            var span = TextSpan.FromBounds(start, end);
            var saveText = text.GetText(span);
            var saveDiagnostics = diagnostics.ToArray();

            trailingTrivia.Clear();
            diagnostics.Clear();
            start = charReader.Position;
            ReadTrivia(trailingTrivia, true);
            var saveTrailingTrivia = trailingTrivia.ToArray();

            return new SyntaxToken(saveKind, saveKind, false, span, saveText, value, saveLeadingTrivia, saveTrailingTrivia, saveDiagnostics);
        }

        TextSpan CurrentSpan => TextSpan.FromBounds(start, charReader.Position);

        TextSpan CurrentSpanStart => TextSpan.FromBounds(start, Math.Min(start + 2, text.Length));

        void ReadTrivia(List<SyntaxTrivia> target, bool isTrailing)
        {
            while (true)
            {
                switch (charReader.Current)
                {
                    case '\n':
                    case '\r':
                        {
                            ReadEndOfLine();
                            AddTrivia(target, SyntaxKind.EndOfLineTrivia);
                            if (isTrailing)
                                return;
                        }
                        break;
                    case '-':
                        if (!options.ParseComments)
                            return;
                        if (charReader.Peek() == '-')
                        {
                            ReadSinglelineComment();
                            AddTrivia(target, SyntaxKind.SingleLineCommentTrivia);
                        }
                        else
                        {
                            return;
                        }
                        break;
                    case '/':
                        if (!options.ParseComments)
                            return;
                        if (charReader.Peek() == '/')
                        {
                            ReadSinglelineComment();
                            AddTrivia(target, SyntaxKind.SingleLineCommentTrivia);
                        }
                        else if (charReader.Peek() == '*')
                        {
                            ReadMultilineComment();
                            AddTrivia(target, SyntaxKind.MultiLineCommentTrivia);
                        }
                        else
                        {
                            return;
                        }
                        break;
                    default:
                        if (char.IsWhiteSpace(charReader.Current))
                        {
                            ReadWhitespace();
                            AddTrivia(target, SyntaxKind.WhitespaceTrivia);
                        }
                        else
                        {
                            return;
                        }
                        break;
                }
            }
        }

        void ReadEndOfLine()
        {
            if (charReader.Current == '\r')
            {
                charReader.NextChar();

                if (charReader.Current == '\n')
                    charReader.NextChar();
            }
            else
            {
                charReader.NextChar();
            }
        }

        void ReadSinglelineComment()
        {
            kind = SyntaxKind.SingleLineCommentTrivia;
            while (true)
            {
                switch (charReader.Current)
                {
                    case '\0':
                        return;

                    case '\r':
                    case '\n':
                        return;

                    default:
                        charReader.NextChar();
                        break;
                }
            }
        }

        void ReadMultilineComment()
        {
            charReader.NextChar(); // Skip /
            charReader.NextChar(); // Skip *

            kind = SyntaxKind.MultiLineCommentTrivia;

            while (true)
            {
                switch (charReader.Current)
                {
                    case '\0':
                        diagnostics.ReportUnterminatedComment(CurrentSpanStart);
                        return;

                    case '*':
                        charReader.NextChar();
                        if (charReader.Current == '/')
                        {
                            charReader.NextChar();
                            return;
                        }
                        break;

                    default:
                        charReader.NextChar();
                        break;
                }
            }
        }

        void ReadWhitespace()
        {
            while (char.IsWhiteSpace(charReader.Current) &&
                   charReader.Current != '\r' &&
                   charReader.Current != '\n')
            {
                charReader.NextChar();
            }
        }

        void AddTrivia(List<SyntaxTrivia> target, SyntaxKind triviaKind)
        {
            var saveStart = start;
            var end = charReader.Position;
            var span = TextSpan.FromBounds(saveStart, end);
            var saveText = text.GetText(span);
            var saveDiagnostics = diagnostics.ToArray();
            var trivia = new SyntaxTrivia(triviaKind, saveText, span, saveDiagnostics);
            target.Add(trivia);

            diagnostics.Clear();
            start = charReader.Position;
        }

        void ReadToken()
        {
            switch (charReader.Current)
            {
                case '\0':
                    kind = SyntaxKind.EndOfFileToken;
                    break;

                case '~':
                    kind = SyntaxKind.BitwiseNotToken;
                    charReader.NextChar();
                    break;

                case '&':
                    kind = SyntaxKind.AmpersandToken;
                    charReader.NextChar();
                    if (charReader.Current == '&')
                    {
                        kind = SyntaxKind.AmpersandAmpersandToken;
                        charReader.NextChar();
                    }
                    break;

                case '|':
                    kind = SyntaxKind.BarToken;
                    charReader.NextChar();
                    if (charReader.Current == '|')
                    {
                        kind = SyntaxKind.BarBarToken;
                        charReader.NextChar();
                    }
                    break;

                case '^':
                    kind = SyntaxKind.CaretToken;
                    charReader.NextChar();
                    break;

                case '(':
                    kind = SyntaxKind.LeftParenthesisToken;
                    charReader.NextChar();
                    break;

                case ')':
                    kind = SyntaxKind.RightParenthesisToken;
                    charReader.NextChar();
                    break;

                case '{':
                    kind = SyntaxKind.LeftCurlyBraceToken;
                    charReader.NextChar();
                    break;

                case '}':
                    kind = SyntaxKind.RightCurlyBraceToken;
                    charReader.NextChar();
                    break;

                case '.':
                    if (char.IsDigit(charReader.Peek()))
                        ReadNumber();
                    else
                    {
                        kind = SyntaxKind.DotToken;
                        charReader.NextChar();
                    }
                    break;

                case '@':
                    kind = SyntaxKind.AtToken;
                    charReader.NextChar();
                    break;

                case '+':
                    kind = SyntaxKind.PlusToken;
                    charReader.NextChar();
                    if (charReader.Current == '+')
                    {
                        kind = SyntaxKind.PlusPlusToken;
                        charReader.NextChar();
                    }
                    break;

                case '-':
                    kind = SyntaxKind.MinusToken;
                    charReader.NextChar();
                    if (charReader.Current == '-')
                    {
                        kind = SyntaxKind.MinusMinusToken;
                        charReader.NextChar();
                    }
                    break;

                case '*':
                    charReader.NextChar();
                    kind = SyntaxKind.AsteriskToken;
                    break;

                case '/':
                    kind = SyntaxKind.SlashToken;
                    charReader.NextChar();
                    break;

                case '\\':
                    kind = SyntaxKind.BackSlashToken;
                    charReader.NextChar();
                    break;

                case '%':
                    kind = SyntaxKind.PercentToken;
                    charReader.NextChar();
                    break;

                case ',':
                    kind = SyntaxKind.CommaToken;
                    charReader.NextChar();
                    break;

                case '=':
                    kind = SyntaxKind.EqualsToken;
                    charReader.NextChar();
                    if (charReader.Current == '=')
                    {
                        kind = SyntaxKind.EqualsEqualsToken;
                        charReader.NextChar();
                    }
                    break;

                case '!':
                    charReader.NextChar();
                    kind = SyntaxKind.ExclamationToken;
                    if (charReader.Current == '=')
                    {
                        kind = SyntaxKind.ExclamationEqualsToken;
                        charReader.NextChar();
                    }
                    break;

                case '<':
                    charReader.NextChar();
                    kind = SyntaxKind.LessToken;
                    if (charReader.Current == '<')
                    {
                        kind = SyntaxKind.LessLessToken;
                        charReader.NextChar();
                    }
                    else if (charReader.Current == '>')
                    {
                        kind = SyntaxKind.LessGreaterToken;
                        charReader.NextChar();
                    }
                    else if (charReader.Current == '=')
                    {
                        kind = SyntaxKind.LessEqualToken;
                        charReader.NextChar();
                    }
                    break;

                case '>':
                    charReader.NextChar();
                    kind = SyntaxKind.GreaterToken;
                    if (charReader.Current == '>')
                    {
                        kind = SyntaxKind.GreaterGreaterToken;
                        charReader.NextChar();
                    }
                    else if (charReader.Current == '=')
                    {
                        kind = SyntaxKind.GreaterEqualToken;
                        charReader.NextChar();
                    }
                    break;

                case '\'':
                case '"':
                    ReadString(charReader.Current, options.ParseEscapeChars, options.AllowCrLfInStrings);
                    break;

                case '[':
                    if (options.ParseBracketIdentifiers)
                        ReadParenthesizedIdentifier();
                    else
                    {
                        kind = SyntaxKind.LeftBracketToken;
                        charReader.NextChar();
                    }
                    break;

                case ']':
                    kind = SyntaxKind.RightBracketToken;
                    charReader.NextChar();
                    break;

                case ':':
                    kind = SyntaxKind.ColonToken;
                    charReader.NextChar();
                    break;

                case ';':
                    kind = SyntaxKind.SemicolonToken;
                    charReader.NextChar();
                    break;

                case '?':
                    kind = SyntaxKind.QuestionToken;
                    charReader.NextChar();
                    if (charReader.Current == '?')
                    {
                        kind = SyntaxKind.QuestionQuestionToken;
                        charReader.NextChar();
                    }
                    break;
                case '#':
                    if (!options.AllowHashedIdentifiers)
                    {
                        kind = SyntaxKind.HashToken;
                        charReader.NextChar();
                    }
                    else
                        ReadHashedIdentifier();
                    break;

                default:
                    if (char.IsLetter(charReader.Current) || charReader.Current == '_' || (options.AllowDollarSignAsIdentifier && charReader.Current == '$'))
                    {
                        ReadIdentifierOrKeyword();
                    }
                    else if (char.IsDigit(charReader.Current))
                    {
                        ReadNumber();
                    }
                    else
                    {
                        ReadInvalidCharacter();
                    }

                    break;
            }
        }

        void ReadInvalidCharacter()
        {
            var c = charReader.Current;
            charReader.NextChar();
            diagnostics.ReportIllegalInputCharacter(CurrentSpan, c);
        }

        void ReadString(char quoteChar, bool parseEscapeChars, bool allowCrLfInStrings)
        {
            kind = SyntaxKind.StringLiteralToken;

            // Skip first single quote
            charReader.NextChar();

            var sb = new StringBuilder();

            while (true)
            {
                var ch = charReader.Current;
                switch (ch)
                {
                    case '\0':

                    case '\\':
                        sb.Append(parseEscapeChars ? ReadEscapeSequence() : ch);
                        if (!parseEscapeChars)
                            charReader.NextChar();
                        break;
                    case '\'':
                    case '"':
                        if (quoteChar == ch)
                        {
                            charReader.NextChar();
                            ch = charReader.Current;
                            if (!allowCrLfInStrings || ch != quoteChar)
                                goto ExitLoop;
                        }
                        sb.Append(ch);
                        charReader.NextChar();
                        break;
                    case '\r':
                    case '\n':
                        if (!allowCrLfInStrings)
                        {
                            diagnostics.ReportCrLfNotAllowedInStrings(CurrentSpan);
                            goto ExitLoop;
                        }
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        break;
                    default:
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        break;
                }
            }

            ExitLoop:
            value = sb.ToString();
        }

        char ReadEscapeSequence()
        {
            charReader.NextChar(); // skip \
            var ch = charReader.Current;
            charReader.NextChar();
            switch (ch)
            {
                case '\'':
                case '"':
                case '\\':
                    break;
                // translate escapes as per C# spec 2.4.4.4
                case '0':
                    ch = '\u0000';
                    break;
                case 'a':
                    ch = '\u0007';
                    break;
                case 'b':
                    ch = '\u0008';
                    break;
                case 'f':
                    ch = '\u000c';
                    break;
                case 'n':
                    ch = '\u000a';
                    break;
                case 'r':
                    ch = '\u000d';
                    break;
                case 't':
                    ch = '\u0009';
                    break;
                case 'v':
                    ch = '\u000b';
                    break;
                default:
                    diagnostics.ReportInvalidEscapeSequence(CurrentSpan);
                    break;
            }
            return ch;
        }

        void ReadNumber()
        {
            kind = SyntaxKind.NumericLiteralToken;

            // Just read everything that looks like it could be a number -- we will
            // verify it afterwards by proper number parsing.

            var sb = new StringBuilder();
            var hasExponentialModifier = false;
            var hasDotModifier = false;

            while (true)
            {
                switch (charReader.Current)
                {
                    // dot
                    case '.':

                        // "10.Equals" should not be recognized as a number.

                        var peek1 = charReader.Peek(1);
                        var peek2 = charReader.Peek(2);
                        var startsFloatingPoint = char.IsDigit(peek1) ||
                                                  ((peek1 == 'e' || peek1 == 'E') && (peek2 == '+' || peek2 == '-' || char.IsDigit(peek2)));
                        if (!startsFloatingPoint)
                            goto ExitLoop;

                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        hasDotModifier = true;
                        break;

                    // special handling for e, it could be the exponent indicator
                    // followed by an optional sign

                    case 'E':
                    case 'e':
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        hasExponentialModifier = true;
                        if (charReader.Current == '-' || charReader.Current == '+')
                        {
                            sb.Append(charReader.Current);
                            charReader.NextChar();
                        }
                        break;

                    default:
                        if (!char.IsLetterOrDigit(charReader.Current))
                            goto ExitLoop;
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        break;
                }
            }

            ExitLoop:

            var saveText = sb.ToString();
            value = hasDotModifier || hasExponentialModifier
                         ? ReadDouble(saveText)
                         : ReadInt32OrInt64(saveText);
        }

        double ReadDouble(string numtext)
        {
            try
            {
                return double.Parse(numtext, NumberStyles.AllowDecimalPoint | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);
            }
            catch (OverflowException)
            {
                diagnostics.ReportNumberTooLarge(CurrentSpan, numtext);
            }
            catch (FormatException)
            {
                diagnostics.ReportInvalidReal(CurrentSpan, numtext);
            }
            return 0.0;
        }

        object ReadInt32OrInt64(string numtext)
        {
            var int64 = ReadInt64(numtext);

            // If the integer can be represented as Int32 we return
            // an Int32 literal. Otherwise we return an Int64.

            try
            {
                checked
                {
                    return (int)int64;
                }
            }
            catch (OverflowException)
            {
                return int64;
            }
        }

        long ReadInt64(string numtext)
        {
            // Get indicator

            var indicator = numtext[numtext.Length - 1];

            // Remove trailing indicator (h, b, or o)

            var textWithoutIndicator = numtext.Substring(0, numtext.Length - 1);

            switch (indicator)
            {
                case 'H':
                case 'h':
                    try
                    {
                        return long.Parse(textWithoutIndicator, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                    }
                    catch (OverflowException)
                    {
                        diagnostics.ReportNumberTooLarge(CurrentSpan, textWithoutIndicator);
                    }
                    catch (FormatException)
                    {
                        diagnostics.ReportInvalidHex(CurrentSpan, textWithoutIndicator);
                    }

                    return 0;

                case 'B':
                case 'b':
                    try
                    {
                        return ReadBinaryValue(textWithoutIndicator);
                    }
                    catch (OverflowException)
                    {
                        diagnostics.ReportNumberTooLarge(CurrentSpan, textWithoutIndicator);
                    }

                    return 0;

                case 'O':
                case 'o':
                    try
                    {
                        return ReadOctalValue(textWithoutIndicator);
                    }
                    catch (OverflowException)
                    {
                        diagnostics.ReportNumberTooLarge(CurrentSpan, textWithoutIndicator);
                    }

                    return 0;

                default:
                    try
                    {
                        return long.Parse(numtext, CultureInfo.InvariantCulture);
                    }
                    catch (OverflowException)
                    {
                        diagnostics.ReportNumberTooLarge(CurrentSpan, numtext);
                    }
                    catch (FormatException)
                    {
                        diagnostics.ReportInvalidInteger(CurrentSpan, numtext);
                    }

                    return 0;
            }
        }

        long ReadBinaryValue(string binary)
        {
            long val = 0;

            for (int i = binary.Length - 1, j = 0; i >= 0; i--, j++)
            {
                if (binary[i] == '0')
                {
                    // Nothing to add
                }
                else if (binary[i] == '1')
                {
                    checked
                    {
                        // Don't use >> because this implicitly casts the operator to Int32.
                        // Also this operation will never detect an overflow.
                        val += (long)Math.Pow(2, j);
                    }
                }
                else
                {
                    diagnostics.ReportInvalidBinary(CurrentSpan, binary);
                    return 0;
                }
            }

            return val;
        }

        long ReadOctalValue(string octal)
        {
            long val = 0;

            for (int i = octal.Length - 1, j = 0; i >= 0; i--, j++)
            {
                int c;

                try
                {
                    c = int.Parse(new string(octal[i], 1), CultureInfo.InvariantCulture);

                    if (c > 7)
                    {
                        diagnostics.ReportInvalidOctal(CurrentSpan, octal);
                        return 0;
                    }
                }
                catch (FormatException)
                {
                    diagnostics.ReportInvalidOctal(CurrentSpan, octal);
                    return 0;
                }

                checked
                {
                    val += (long)(c * Math.Pow(8, j));
                }
            }

            return val;
        }

        void ReadIdentifierOrKeyword(int? exactStart = null)
        {
            var saveStart = exactStart ?? charReader.Position;

            // Skip first letter
            charReader.NextChar();

            // The following characters can be letters, digits the underscore and the dollar sign.

            while (char.IsLetterOrDigit(charReader.Current) ||
                   charReader.Current == '_' || (options.ParseDashedIdentifiers && charReader.Current == '-'))
            {
                charReader.NextChar();
            }

            var end = charReader.Position;
            var span = TextSpan.FromBounds(saveStart, end);
            var saveText = text.GetText(span);

            kind = SyntaxFacts.GetKeywordKind(saveText);
            switch (kind)
            {
                case SyntaxKind.NullKeyword:
                    value = null;
                    break;
                case SyntaxKind.TrueKeyword:
                    value = true;
                    break;
                case SyntaxKind.FalseKeyword:
                    value = false;
                    break;
                default:
                    value = saveText;
                    break;
            }
        }

        void ReadHashedIdentifier()
        {
            var saveStart = charReader.Position;
            // skip initial #
            charReader.NextChar();
            var pos = 0;
            // look ahead other # chars
            while (charReader.Peek(pos) == '#')
                pos++;
            // is identifier start ?
            if (char.IsLetter(charReader.Peek(pos)) || charReader.Peek(pos) == '_')
            {
                charReader.Advance(pos);
                ReadIdentifierOrKeyword(saveStart);
            }
            else
                kind = SyntaxKind.HashToken;
        }

        void ReadParenthesizedIdentifier()
        {
            kind = SyntaxKind.IdentifierToken;

            // Skip initial [
            charReader.NextChar();

            var sb = new StringBuilder();

            while (true)
            {
                switch (charReader.Current)
                {
                    case '\0':
                    case '\r':
                    case '\n':
                        diagnostics.ReportUnterminatedParenthesizedIdentifier(CurrentSpanStart);
                        goto ExitLoop;

                    case ']':
                        if (charReader.Peek() != ']')
                        {
                            charReader.NextChar();
                            goto ExitLoop;
                        }
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        charReader.NextChar();
                        break;


                    default:
                        sb.Append(charReader.Current);
                        charReader.NextChar();
                        break;
                }
            }

            ExitLoop:
            value = sb.ToString();
        }
    }
}