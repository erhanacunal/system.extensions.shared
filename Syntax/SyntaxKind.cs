namespace Sytem.Text.Syntax
{
    public enum SyntaxKind
    {
        // Tokens

        EndOfFileToken,
        BadToken,

        IdentifierToken,
        NumericLiteralToken,
        Int16LiteralToken,
        Int32LiteralToken,
        Int64LiteralToken,
        SingleLiteralToken,
        DoubleLiteralToken,
        DecimalLiteralToken,
        BooleanLiteralToken,
        StringLiteralToken,
        AtToken,

        BitwiseNotToken,
        AmpersandToken,
        AmpersandAmpersandToken,
        BarToken,
        BarBarToken,
        CaretToken,
        LeftParenthesisToken,
        RightParenthesisToken,
        PlusToken,
        PlusPlusToken,
        MinusToken,
        MinusMinusToken,
        AsteriskToken,
        SlashToken,
        BackSlashToken,
        PercentToken,
        CommaToken,
        DotToken,
        EqualsToken,
        EqualsEqualsToken,
        ExclamationToken,
        ExclamationEqualsToken,
        LessGreaterToken,
        LessToken,
        LessEqualToken,
        GreaterToken,
        GreaterEqualToken,
        GreaterGreaterToken,
        LessLessToken,
        LeftCurlyBraceToken,
        RightCurlyBraceToken,
        LeftBracketToken,
        RightBracketToken,
        QuestionToken,
        QuestionQuestionToken,
        ColonToken,
        SemicolonToken,
        HashToken,

        // Keywords
        TrueKeyword,
        FalseKeyword,
        NullKeyword,
        
        // Trivia

        WhitespaceTrivia,
        EndOfLineTrivia,
        MultiLineCommentTrivia,
        SingleLineCommentTrivia,
        SkippedTokensTrivia
    }
}