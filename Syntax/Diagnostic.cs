using System;
using System.Globalization;

using System.Text;

namespace Sytem.Text.Syntax
{
    public sealed class Diagnostic
    {
        public Diagnostic(TextSpan textSpan, DiagnosticId diagnosticId, string message)
        {
            Span = textSpan;
            DiagnosticId = diagnosticId;
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }

        public static Diagnostic Format(TextSpan textSpan, DiagnosticId diagnosticId, params object[] args)
        {
            var message = diagnosticId.ToString();
            var formattedMessage = string.Format(CultureInfo.CurrentCulture, message, args);
            return new Diagnostic(textSpan, diagnosticId, formattedMessage);
        }

        public TextSpan Span { get; }

        public DiagnosticId DiagnosticId { get; }

        public string Message { get; }

        public override string ToString()
        {
            return Message;
        }
    }
}