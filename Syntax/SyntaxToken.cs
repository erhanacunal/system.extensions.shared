using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Sytem.Text.Syntax
{
    public sealed class SyntaxToken
    {
        readonly string _text;

        internal SyntaxToken(SyntaxKind kind, SyntaxKind contextualKind, bool isMissing, TextSpan span, string text, object value, SyntaxTrivia[] leadingTrivia, SyntaxTrivia[] trailingTrivia, IEnumerable<Diagnostic> diagnostics)
        {
            Kind = kind;
            ContextualKind = contextualKind;
            IsMissing = isMissing;
            Span = span;
            _text = text;
            Value = value;
            LeadingTrivia = leadingTrivia;
            TrailingTrivia = trailingTrivia;
            Diagnostics = diagnostics.ToArray();
        }
        
        public SyntaxKind Kind { get; }

        public SyntaxKind ContextualKind { get; }

        public bool IsMissing { get; }

        public string Text => _text ?? Kind.GetText();

        public object Value { get; }

        public string ValueText => Value as string ?? Text;

        public TextSpan Span { get; }

        public TextSpan FullSpan
        {
            get
            {
                var start = LeadingTrivia.Length == 0
                                ? Span.Start
                                : LeadingTrivia[0].Span.Start;
                var end = TrailingTrivia.Length == 0
                              ? Span.End
                              : TrailingTrivia[TrailingTrivia.Length - 1].Span.End;
                return TextSpan.FromBounds(start, end);
            }
        }

        public SyntaxTrivia[] LeadingTrivia { get; }

        public SyntaxTrivia[] TrailingTrivia { get; }

        public Diagnostic[] Diagnostics { get; }
        
        public void WriteTo(TextWriter writer)
        {
            if (writer == null)
                throw new ArgumentNullException(nameof(writer));

            foreach (var syntaxTrivia in LeadingTrivia)
                syntaxTrivia.WriteTo(writer);

            writer.Write(_text);

            foreach (var syntaxTrivia in TrailingTrivia)
                syntaxTrivia.WriteTo(writer);
        }

        public bool IsEquivalentTo(SyntaxToken other)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            return SyntaxTokenEquivalence.AreEquivalent(this, other);
        }

        public SyntaxToken WithDiagnotics(params Diagnostic[] diagnostics)
        {
            if (diagnostics == null)
                throw new ArgumentNullException(nameof(diagnostics));

            return new SyntaxToken( Kind,ContextualKind, IsMissing, Span, _text, Value, LeadingTrivia, TrailingTrivia, diagnostics);
        }

        public SyntaxToken WithKind(SyntaxKind kind)
        {
            return new SyntaxToken(kind, ContextualKind, IsMissing, Span, _text, Value, LeadingTrivia, TrailingTrivia, Diagnostics);
        }

        public SyntaxToken WithLeadingTrivia(params SyntaxTrivia[] trivia)
        {
            if (trivia == null)
                throw new ArgumentNullException(nameof(trivia));

            return new SyntaxToken( Kind, ContextualKind, IsMissing, Span, _text, Value, trivia, TrailingTrivia, Diagnostics);
        }

        public SyntaxToken WithTrailingTrivia(params SyntaxTrivia[] trivia)
        {
            if (trivia == null)
                throw new ArgumentNullException(nameof(trivia));

            return new SyntaxToken( Kind, ContextualKind, IsMissing, Span, _text, Value, LeadingTrivia, trivia, Diagnostics);
        }

        public override string ToString()
        {
            using (var writer = new StringWriter())
            {
                WriteTo(writer);
                return writer.ToString();
            }
        }
    }

    public class SyntaxTokenEquivalence
    {
        public static bool AreEquivalent(SyntaxToken left, SyntaxToken right)
        {
            if (left == null && right == null)
                return true;

            if (left == null || right == null)
                return false;

            if (left.Kind != right.Kind)
                return false;

            if (left.IsMissing != right.IsMissing)
                return false;

            if (left.Kind == SyntaxKind.IdentifierToken)
            {
                // Let's say left is [test] and right is "Test"
                // In this case, we don't want to match.
                return left.Matches(right.ValueText) &&
                       right.Matches(left.ValueText);
            }

            if (left.Kind.IsLiteral())
                return Equals(left.Value, right.Value);

            return true;
        }
    }
}