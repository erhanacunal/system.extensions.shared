using System;

namespace Sytem.Text.Syntax
{
    public enum DiagnosticId
    {
        IllegalInputCharacter,
        UnterminatedComment,
        UnterminatedString,
        UnterminatedQuotedIdentifier,
        UnterminatedParenthesizedIdentifier,
        UnterminatedDate,
        
        InvalidInteger,
        InvalidReal,
        InvalidBinary,
        InvalidOctal,
        InvalidHex,
        NumberTooLarge,
        TokenExpected,
        InvalidOperatorForAllAny,
        CrLfNotAllowedInStrings,
        InvalidEscapeSequence
        
    }
}