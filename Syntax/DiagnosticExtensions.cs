﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sytem.Text.Syntax
{
    static class DiagnosticExtensions
    {
        public static string GetMessage(this DiagnosticId diagnosticId)
        {
            switch (diagnosticId)
            {
                case DiagnosticId.IllegalInputCharacter:
                    return "Invalid character in input '{0}'.";
                case DiagnosticId.UnterminatedComment:
                    return "Comment is not properly terminated.";
                case DiagnosticId.UnterminatedString:
                    return "String is not properly terminated.";
                case DiagnosticId.UnterminatedQuotedIdentifier:
                    return "Quoted identifier is not properly terminated.";
                case DiagnosticId.UnterminatedParenthesizedIdentifier:
                    return "Parenthesized identifier is not properly terminated.";
                case DiagnosticId.UnterminatedDate:
                    return "Date is not properly terminated.";
                case DiagnosticId.InvalidInteger:
                    return "'{0}' is not a valid integer.";
                case DiagnosticId.InvalidReal:
                    return "'{0}' is not a valid decimal number.";
                case DiagnosticId.InvalidBinary:
                    return "'{0}' is not a valid binary number.";
                case DiagnosticId.InvalidOctal:
                    return "'{0}' is not a valid octal number.";
                case DiagnosticId.InvalidHex:
                    return "'{0}' is not a valid hex number.";
                case DiagnosticId.NumberTooLarge:
                    return "The number '{0}' is too large.";
                case DiagnosticId.TokenExpected:
                    return "Found '{0}' but expected '{1}'.";
                case DiagnosticId.InvalidEscapeSequence:
                    return "Invalid escape sequence.";
                case DiagnosticId.CrLfNotAllowedInStrings:
                    return "Cr/Lf is not allowed in strings";
                default:
                    throw UnexpectedValue(diagnosticId);
            }
        }

        public static Exception UnexpectedValue(object value)
        {
            var message = value == null
                ? @"A null value was unexpected"
                : $"The value '{value}' of type {value.GetType().Name} was unexpected";

            return new InvalidOperationException(message);
        }

        public static void Report(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, DiagnosticId diagnosticId, params object[] args)
        {
            var diagnostic = Diagnostic.Format(textSpan, diagnosticId, args);
            diagnostics.Add(diagnostic);
        }

        public static void ReportIllegalInputCharacter(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, char character)
        {
            diagnostics.Report(textSpan, DiagnosticId.IllegalInputCharacter, character);
        }

        public static void ReportUnterminatedComment(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.UnterminatedComment);
        }

        public static void ReportUnterminatedString(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.UnterminatedString);
        }

        public static void ReportUnterminatedQuotedIdentifier(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.UnterminatedQuotedIdentifier);
        }

        public static void ReportUnterminatedParenthesizedIdentifier(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.UnterminatedParenthesizedIdentifier);
        }

        public static void ReportUnterminatedDate(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.UnterminatedDate);
        }

        public static void ReportCrLfNotAllowedInStrings(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.CrLfNotAllowedInStrings);
        }

        public static void ReportInvalidEscapeSequence(this ICollection<Diagnostic> diagnostics, TextSpan textSpan)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidEscapeSequence);
        }

        public static void ReportInvalidInteger(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidInteger, tokenText);
        }

        public static void ReportInvalidReal(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidReal, tokenText);
        }

        public static void ReportInvalidBinary(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidBinary, tokenText);
        }

        public static void ReportInvalidOctal(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidOctal, tokenText);
        }

        public static void ReportInvalidHex(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.InvalidHex, tokenText);
        }

        public static void ReportNumberTooLarge(this ICollection<Diagnostic> diagnostics, TextSpan textSpan, string tokenText)
        {
            diagnostics.Report(textSpan, DiagnosticId.NumberTooLarge, tokenText);
        }

        public static void ReportTokenExpected(this ICollection<Diagnostic> diagnostics, TextSpan span, SyntaxToken actual, SyntaxKind expected)
        {
            var actualText = actual.GetDisplayText();
            var expectedText = expected.GetDisplayText();
            diagnostics.Report(span, DiagnosticId.TokenExpected, actualText, expectedText);
        }
    }
}
