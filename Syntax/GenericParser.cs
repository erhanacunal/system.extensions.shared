﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Utils;

namespace Sytem.Text.Syntax
{
    public class GenericParser
    {
        readonly SourceText text;
        protected readonly List<SyntaxToken> Tokens = new List<SyntaxToken>();
        int tokenIndex;

        protected SyntaxToken Current => Peek(0);

        protected SyntaxToken Lookahead => Peek(1);

        public GenericParser(SourceText text, System.Text.Parsers.GenericLexerOptions options)
        {
            this.text = text;
            GetAllTokens(options);
        }

        protected void GetAllTokens(System.Text.Parsers.GenericLexerOptions options)
        {
            var lexer = new GenericLexer(text, options);
            var badTokens = new List<SyntaxToken>();

            SyntaxToken token;
            do
            {
                token = lexer.Lex();

                // Skip any bad tokens.

                badTokens.Clear();
                while (token.Kind == SyntaxKind.BadToken)
                {
                    badTokens.Add(token);
                    token = lexer.Lex();
                }

                if (badTokens.Count > 0)
                {
                    token = token.WithLeadingTrivia(ArrayUtils.Concat(new[] { CreateSkippedTokensTrivia(badTokens) }, token.LeadingTrivia));
                }

                Tokens.Add(token);
            } while (token.Kind != SyntaxKind.EndOfFileToken);
        }

        protected void SkipTokens(Func<SyntaxToken, bool> stopPredicate)
        {
            if (stopPredicate(Current))
                return;

            var tokens = new List<SyntaxToken>();
            do
            {
                tokens.Add(NextToken());
            } while (!stopPredicate(Current));

            var current = Tokens[tokenIndex];
            var skippedTokensTrivia = CreateSkippedTokensTrivia(tokens);

            var leadingTrivia = new List<SyntaxTrivia>(current.LeadingTrivia.Length + 1) { skippedTokensTrivia };
            leadingTrivia.AddRange(current.LeadingTrivia);

            Tokens[tokenIndex] = current.WithLeadingTrivia(leadingTrivia.ToArray());
        }

        protected SyntaxToken Peek(int offset)
        {
            var i = Math.Min(tokenIndex + offset, Tokens.Count - 1);
            return Tokens[i];
        }

        protected SyntaxToken GetPreviousToken()
        {
            var previousIndex = tokenIndex - 1;
            return previousIndex < 0
                ? null
                : Tokens[previousIndex];
        }

        protected SyntaxToken NextToken()
        {
            var result = Current;
            tokenIndex = Math.Min(tokenIndex + 1, Tokens.Count - 1);
            return result;
        }

        protected SyntaxToken NextTokenIf(SyntaxKind kind)
        {
            return Current.Kind == kind
                ? NextToken()
                : null;
        }

        protected SyntaxToken Match(SyntaxKind kind)
        {
            if (Current.Kind == kind)
                return NextToken();
            return InsertMissingToken(kind);
        }

        protected SyntaxToken Expect(SyntaxKind kind, bool advance = true)
        {
            if (Current.Kind != kind)
                throw new InvalidOperationException(kind.GetDisplayText() + " expected");
            if (!advance) return Current;
            return NextToken();
        }

        protected bool TryGetToken(SyntaxKind kind, out SyntaxToken token, bool advance = true)
        {
            token = null;
            if (Current.Kind == kind)
            {
                token = advance ? NextToken() : Current;
                return true;
            }
            return false;
        }

        SyntaxToken InsertMissingToken(SyntaxKind kind)
        {
            var missingTokenSpan = new TextSpan(Current.FullSpan.Start, 0);
            var diagnosticSpan = GetDiagnosticSpanForMissingToken();
            var diagnostics = new List<Diagnostic>(1);
            diagnostics.ReportTokenExpected(diagnosticSpan, Current, kind);
            return new SyntaxToken(kind, SyntaxKind.BadToken, true, missingTokenSpan, string.Empty, null, new SyntaxTrivia[0], new SyntaxTrivia[0], diagnostics);
        }

        //SyntaxToken SkipAndInsertMissingToken(SyntaxKind kind)
        //{
        //    var skippedToken = Current;
        //    var skippedTokensTrivia = new[] { CreateSkippedTokensTrivia(new[] { skippedToken }) };
        //    NextToken();

        //    var missingTokenSpan = new TextSpan(Current.FullSpan.Start, 0);
        //    var diagnosticSpan = GetDiagnosticSpanForMissingToken();
        //    var diagnostics = new List<Diagnostic>(1);
        //    diagnostics.ReportTokenExpected(diagnosticSpan, skippedToken, kind);
        //    return new SyntaxToken(kind, SyntaxKind.BadToken, true, missingTokenSpan, string.Empty, null, skippedTokensTrivia, new SyntaxTrivia[0], diagnostics);
        //}

        TextSpan GetDiagnosticSpanForMissingToken()
        {
            if (tokenIndex > 0)
            {
                var previousToken = Tokens[tokenIndex - 1];
                var previousTokenLine = text.GetLineFromPosition(previousToken.Span.End);
                var currentTokenLine = text.GetLineFromPosition(Current.Span.Start);
                if (currentTokenLine != previousTokenLine)
                    return new TextSpan(previousToken.Span.End, 2);
            }

            return Current.Span;
        }

        static SyntaxTrivia CreateSkippedTokensTrivia(IReadOnlyCollection<SyntaxToken> tokens)
        {
            var start = tokens.First().FullSpan.Start;
            var end = tokens.Last().FullSpan.End;
            var span = TextSpan.FromBounds(start, end);
            //var structure = new SkippedTokensTriviaSyntax(_syntaxTree, tokens);
            return new SyntaxTrivia(SyntaxKind.SkippedTokensTrivia, null, span, new Diagnostic[0]);
        }
    }
}
