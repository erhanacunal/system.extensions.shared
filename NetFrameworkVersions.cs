﻿#if !MOBILE
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace System
{
    public static class NetFramework
    {

        static readonly SortedSet<NetFrameworkVersion> Versions = new SortedSet<NetFrameworkVersion>(new NetFrameworkVersion.Comparer());

        public static bool IsNet46Installed { get; }
        public static bool IsNet47Installed { get; }

        public static bool IsNet45Installed { get; }

        public static bool IsNet40Installed { get; }

        public static bool IsNet35Installed { get; }

        static NetFrameworkVersion? installedFramework;

        public static NetFrameworkVersion InstalledFramework
        {
            get
            {
                if (installedFramework.HasValue) return installedFramework.Value;
                if (Versions.Count > 0)
                {
                    installedFramework = Versions.First();
                }
                else
                    throw new NotImplementedException("Cannot determine framework version!");
                return installedFramework.Value;
            }
        }

        static NetFramework()
        {
            using (var baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, Environment.Is64BitProcess ? RegistryView.Registry64 : RegistryView.Registry32))
            using (var root = baseKey.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP", false))
            {
                if (root != null)
                {
                    foreach (var subKey in root.GetSubKeyNames())
                    {
                        if (!subKey.StartsWithChar('v')) continue;
                        using (var frameworkKey = root.OpenSubKey(subKey))
                        {
                            if (frameworkKey == null) continue;
                            if (string.Equals((string)frameworkKey.GetValue("", ""), "deprecated", StringComparison.OrdinalIgnoreCase)) continue;
                            using (var fullKey = frameworkKey?.OpenSubKey("Full"))
                            {
                                if (fullKey != null)
                                {
                                    var v = new NetFrameworkVersion
                                    {
                                        Installed = (int)fullKey.GetValue("Install", 0) == 1,
                                        InstallPath = (string)fullKey.GetValue("InstallPath", ""),
                                        ServicePack = (int)fullKey.GetValue("Servicing", 1),
                                        Version = Version.Parse((string)fullKey.GetValue("Version"))
                                    };
                                    v.TargetVersion = Version.Parse((string)fullKey.GetValue("TargetVersion", v.Version.ToString()));
                                    Versions.Add(v);
                                }
                                else
                                {
                                    var valueNames = frameworkKey.GetValueNames();
                                    var v = new NetFrameworkVersion
                                    {
                                        Installed = (int)frameworkKey.GetValue("Install", 0) == 1,
                                        ServicePack = (int)frameworkKey.GetValue("SP"),
                                        Version = valueNames.Contains("Version")
                                            ? Version.Parse((string)frameworkKey.GetValue("Version"))
                                            : Version.Parse(subKey.Substring(1))
                                    };
                                    v.TargetVersion = v.Version;
                                    v.InstallPath = (string)frameworkKey.GetValue("InstallPath", "");
                                    Versions.Add(v);
                                }
                            }
                        }
                    }
                }
            }
            IsNet47Installed = IsInstalled("4.7");
            IsNet46Installed = IsInstalled("4.6");
            IsNet45Installed = IsInstalled("4.5");
            IsNet40Installed = IsInstalled("4.0");
            IsNet35Installed = IsInstalled("3.5");
        }

        public static bool IsInstalled(string version)
        {
            if (!Version.TryParse(version, out var v)) return false;
            foreach (var ver in Versions.Where(_ => _.Version.Major == v.Major))
            {
                if (ver.Version.Minor == v.Minor)
                    return true;
            }
            foreach (var major in Versions.Where(_ => _.TargetVersion.Major == v.Major))
            {
                if (major.TargetVersion.Minor == v.Minor)
                    return true;
            }
            return false;
        }

    }

    public struct NetFrameworkVersion
    {

        public Version Version { get; internal set; }
        public Version TargetVersion { get; internal set; }
        public int ServicePack { get; internal set; }
        public bool Installed { get; internal set; }
        public string InstallPath { get; set; }
        public override string ToString()
        {
            return string.Format("Installed:{0}, Version: {1}{2}", Installed ? "Yes" : "No", Version, ServicePack > 0 ? " SP" + ServicePack : "");
        }

        internal class Comparer : Comparer<NetFrameworkVersion>
        {
            public override int Compare(NetFrameworkVersion x, NetFrameworkVersion y)
            {
                return y.Version.CompareTo(x.Version);
            }
        }
    }
}

#endif