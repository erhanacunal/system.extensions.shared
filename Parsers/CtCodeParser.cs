﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Parsers.Expressions;

namespace System.Text.Parsers
{

    public class CtCodeParser
    {
        readonly GenericLexer tokenizer;

        public CtCodeParser(string expression, GenericLexerOptions options)
        {
            tokenizer = new GenericLexer(options, expression);
        }

        public CtExpressionSyntax ParseExpression()
        {
            return ParseExprLevel0(true);
        }

        CtExpressionSyntax ParseExprLevel0(bool skip)
        {
            var result = ParseExprLevel1(skip);
            var run = true;
            while (run)
            {
                switch (tokenizer.CurrentToken.Kind)
                {
                    case TokenKind.EqualEqual:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.Equal);
                        break;
                    case TokenKind.LessThan:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.LessThan);
                        break;
                    case TokenKind.LessThanOrEqual:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.LessThanOrEqual);
                        break;
                    case TokenKind.ExclamationEqual:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.NotEqual);
                        break;
                    case TokenKind.GreaterThan:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.GreaterThan);
                        break;
                    case TokenKind.GreaterThanOrEqual:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel1(true), CtBinaryOperator.GreaterThanOrEqual);
                        break;
                    case TokenKind.Question:
                        var ifTrue = ParseExprLevel1(true);
                        tokenizer.Expect(TokenKind.Colon);
                        var ifFalse = ParseExprLevel1(false);
                        result = new CtTernaryExpressionSyntax(result, ifTrue, ifFalse);
                        break;
                    default:
                        run = false;
                        break;
                }
            }
            return result;
        }

        CtExpressionSyntax ParseExprLevel1(bool skip)
        {
            var result = ParseExprLevel2(skip);
            var run = true;
            while (run)
            {
                var token = tokenizer.CurrentToken;
                switch (token.Kind)
                {
                    case TokenKind.Plus:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel2(true), CtBinaryOperator.Add);
                        break;
                    case TokenKind.Minus:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel2(true), CtBinaryOperator.Subtract);
                        break;
                    case TokenKind.Pipe:
                    case TokenKind.PipePipe:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel2(true), CtBinaryOperator.Or);
                        break;
                    case TokenKind.Caret:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel2(true), CtBinaryOperator.Xor);
                        break;
                    default:
                        run = false;
                        break;

                }
            }
            return result;
        }

        CtExpressionSyntax ParseExprLevel2(bool skip)
        {
            var result = ParseExprLevel3(skip);
            var run = true;
            while (run)
            {
                var token = tokenizer.CurrentToken;
                switch (token.Kind)
                {
                    case TokenKind.Multiply:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.Multiply);
                        break;
                    case TokenKind.Divide:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.Divide);
                        break;
                    case TokenKind.LessThanLessThan:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.Shl);
                        break;
                    case TokenKind.GreaterThanGreaterThan:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.Shr);
                        break;
                    case TokenKind.Ampersand:
                    case TokenKind.AmpersandAmpersand:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.And);
                        break;
                    case TokenKind.PercentSign:
                        result = new CtBinaryExpressionSyntax(result, ParseExprLevel3(true), CtBinaryOperator.Modulo);
                        break;
                    default:
                        run = false;
                        break;
                }
            }
            return result;
        }

        CtExpressionSyntax ParseExprLevel3(bool skip)
        {
            CtExpressionSyntax result;
            if (skip)
                tokenizer.NextToken();
            var token = tokenizer.CurrentToken;
            switch (token.Kind)
            {
                case TokenKind.Plus:
                    result = ParseExprLevel3(true);
                    break;
                case TokenKind.Minus:
                    result = new CtUnaryExpressionSyntax(ParseExprLevel3(true), CtUnaryOperator.Minus);
                    break;
                case TokenKind.Exclamation:
                    result = new CtUnaryExpressionSyntax(ParseExprLevel3(true), CtUnaryOperator.Not);
                    break;
                default:
                    result = ParseExprFactor();
                    break;

            }
            return result;

        }

        CtExpressionSyntax ParseExprFactor()
        {
            CtExpressionSyntax result;
            var token = tokenizer.CurrentToken;
            switch (token.Kind)
            {
                case TokenKind.Identifier:
                    if ("null".Equals(token.TokenString))
                    {
                        result = new CtPrimitiveExpressionSyntax(null);
                        tokenizer.NextToken();
                    }
                    else
                        result = ParseIdentifier(token as IdentifierToken);
                    break;
                case TokenKind.LParen:
                    result = ParseExprLevel0(true);
                    tokenizer.Expect(TokenKind.RParen);
                    break;
                case TokenKind.String:
                case TokenKind.Int16:
                case TokenKind.Int32:
                case TokenKind.Int64:
                case TokenKind.Single:
                case TokenKind.Double:
                case TokenKind.Decimal:
                case TokenKind.Boolean:
                    result = new CtPrimitiveExpressionSyntax(tokenizer.ParseLiteralValue());
                    break;
                default:
                    throw new InvalidOperationException("Factor expected!");
            }

            return result;
        }

        CtExpressionSyntax ParseIdentifier(IdentifierToken token)
        {
            tokenizer.NextToken();

            if (tokenizer.CurrentToken.Kind != TokenKind.LParen)
                return ParseIdentifierOverride(token);
            var method = token.IsQualified
                ? new CtInvocationExpressionSyntax(Type.GetType(token.Target.ToString()), token.TokenString)
                : new CtInvocationExpressionSyntax(token.TokenString);
            tokenizer.NextToken();
            if (tokenizer.CurrentToken.Kind != TokenKind.RParen)
            {
                while (true)
                {
                    var expression = ParseExprLevel0(false);
                    expression.Parent = method;
                    method.Parameters.Add(expression);
                    if (tokenizer.IsToken(TokenKind.RParen))
                        break;
                    tokenizer.Expect(TokenKind.Comma);
                }
                tokenizer.Expect(TokenKind.RParen);
            }
            else
                tokenizer.NextToken();
            return method;
        }

        protected virtual CtExpressionSyntax ParseIdentifierOverride(IdentifierToken token)
        {
            return new CtIdentifierExpressionSyntax(token.TokenString);
        }

    }

    public static class CtExpressionUtils
    {

        public static CtExpressionSyntax ParseLogicalExpression(string source, int startIndex = 0)
        {
            var st = new GenericLexer(new GenericLexerOptions(), source, startIndex);
            return ParseExpression(st);
        }

        #region Evaluation
        public static bool EvaluateBool(CtExpressionSyntax expr, CtSimpleEvaluationContext ctx)
        {
            var result = Evaluate(expr, ctx);
            return result is bool b && b;
        }

        public static bool EvaluateBool(string expression, CtSimpleEvaluationContext ctx)
        {
            var result = Evaluate(ParseLogicalExpression(expression), ctx);
            return result is bool b && b;
        }

        public static bool EvaluateBool(string expression, params object[] symbols)
        {
            var ctx = new CtSimpleEvaluationContext();
            ctx.Symbols.PopulateFromObjects(symbols);
            var expr = ParseLogicalExpression(expression);
            return EvaluateBool(expr, ctx);
        }

        public static object Evaluate(string expression, params object[] symbols)
        {
            var ctx = new CtSimpleEvaluationContext();
            ctx.Symbols.PopulateFromObjects(symbols);
            var expr = ParseLogicalExpression(expression);
            return Evaluate(expr, ctx);
        }

        public static object Evaluate(CtExpressionSyntax expr, CtSimpleEvaluationContext ctx)
        {
            switch (expr.Kind)
            {
                case CtSyntaxKind.ParenthesizedExpression:
                    return Evaluate(((CtParenthesizedExpressionSyntax)expr).Expression, ctx);
                case CtSyntaxKind.BinaryExpression:
                    var binExpr = (CtBinaryExpressionSyntax)expr;
                    switch (binExpr.Operator)
                    {
                        case CtBinaryOperator.And:
                            return EvaluateBool(binExpr.Left, ctx) && EvaluateBool(binExpr.Right, ctx);
                        case CtBinaryOperator.Or:
                            return EvaluateBool(binExpr.Left, ctx) || EvaluateBool(binExpr.Right, ctx);
                        case CtBinaryOperator.Equal:
                            return Equals(Evaluate(binExpr.Left, ctx), Evaluate(binExpr.Right, ctx));
                        case CtBinaryOperator.NotEqual:
                            return !Equals(Evaluate(binExpr.Left, ctx), Evaluate(binExpr.Right, ctx));
                        case CtBinaryOperator.GreaterThan:
                            return Convert.ToDecimal(Evaluate(binExpr.Left, ctx)) > Convert.ToDecimal(Evaluate(binExpr.Right, ctx));
                        case CtBinaryOperator.LessThan:
                            return Convert.ToInt32(Evaluate(binExpr.Left, ctx)) < Convert.ToInt32(Evaluate(binExpr.Right, ctx));
                        case CtBinaryOperator.GreaterThanOrEqual:
                            return Convert.ToInt32(Evaluate(binExpr.Left, ctx)) >= Convert.ToInt32(Evaluate(binExpr.Right, ctx));
                        case CtBinaryOperator.LessThanOrEqual:
                            return Convert.ToInt32(Evaluate(binExpr.Left, ctx)) <= Convert.ToInt32(Evaluate(binExpr.Right, ctx));
                        default:
                            return false;
                    }
                case CtSyntaxKind.PrimitiveExpression:
                    return ((CtPrimitiveExpressionSyntax)expr).Value;
                case CtSyntaxKind.UnaryExpression:
                    return !EvaluateBool(((CtUnaryExpressionSyntax)expr).TargetExpression, ctx);
                case CtSyntaxKind.IdentifierExpression:
                    var idName = ((CtIdentifierExpressionSyntax) expr).Name;
                    bool val;
                    if (bool.TryParse(idName, out val))
                        return val;
                    object varValue;
                    if (ctx.Symbols.TryGetValue(idName, out varValue))
                        return varValue;
                    break;
            }
            return false;
        }
        #endregion

        #region Parsing

        static CtExpressionSyntax ParseExpression(GenericLexer st)
        {
            return ParseLogicalOr(st);
        }

        static CtExpressionSyntax ParseLogicalOr(GenericLexer st)
        {
            var left = ParseLogicalAnd(st);
            while (st.CurrentToken.Kind == TokenKind.PipePipe)
            {
                st.NextToken();
                var right = ParseLogicalAnd(st);
                left = new CtBinaryExpressionSyntax(left, right, CtBinaryOperator.Or);
            }
            return left;
        }

        static CtExpressionSyntax ParseLogicalAnd(GenericLexer st)
        {
            var left = ParseEquality(st);
            while (st.CurrentToken.Kind == TokenKind.AmpersandAmpersand)
            {
                st.NextToken();
                var right = ParseEquality(st);
                left = new CtBinaryExpressionSyntax(left, right, CtBinaryOperator.And);
            }
            return left;
        }

        static CtExpressionSyntax ParseEquality(GenericLexer st)
        {
            var left = ParseGreaterOrEqual(st);
            while (st.CurrentToken.Kind == TokenKind.EqualEqual || st.CurrentToken.Kind == TokenKind.ExclamationEqual)
            {
                var op = st.CurrentToken.Kind == TokenKind.EqualEqual ? CtBinaryOperator.Equal : CtBinaryOperator.NotEqual;
                st.NextToken();
                var right = ParseEquality(st);
                left = new CtBinaryExpressionSyntax(left, right, op);
            }
            return left;
        }

        static CtExpressionSyntax ParseGreaterOrEqual(GenericLexer st)
        {
            var left = ParseLessOrEqual(st);
            while (st.CurrentToken.Kind == TokenKind.GreaterThan || st.CurrentToken.Kind == TokenKind.GreaterThanOrEqual)
            {
                var op = st.CurrentToken.Kind == TokenKind.GreaterThan ? CtBinaryOperator.GreaterThan : CtBinaryOperator.GreaterThanOrEqual;
                st.NextToken();
                var right = ParseGreaterOrEqual(st);
                left = new CtBinaryExpressionSyntax(left, right, op);
            }
            return left;
        }

        static CtExpressionSyntax ParseLessOrEqual(GenericLexer st)
        {
            var left = ParseLogicalNot(st);
            while (st.CurrentToken.Kind == TokenKind.LessThan || st.CurrentToken.Kind == TokenKind.LessThanOrEqual)
            {
                var op = st.CurrentToken.Kind == TokenKind.LessThan ? CtBinaryOperator.LessThan : CtBinaryOperator.LessThanOrEqual;
                st.NextToken();
                var right = ParseLessOrEqual(st);
                left = new CtBinaryExpressionSyntax(left, right, op);
            }
            return left;
        }

        static CtExpressionSyntax ParseLogicalNot(GenericLexer st)
        {
            if (st.CurrentToken.Kind == TokenKind.Exclamation)
            {
                st.NextToken();
                return new CtUnaryExpressionSyntax(ParseLogicalNot(st), CtUnaryOperator.Not);
            }
            return ParsePrimary(st);
        }

        static CtExpressionSyntax ParsePrimary(GenericLexer st)
        {
            switch (st.CurrentToken.Kind)
            {
                case TokenKind.LParen:
                    st.NextToken();
                    var expr = ParseExpression(st);
                    st.Expect(TokenKind.RParen);
                    return new CtParenthesizedExpressionSyntax(expr);
                case TokenKind.Identifier:
                    var idName = st.CurrentToken.TokenString;
                    st.NextToken();
                    return new CtIdentifierExpressionSyntax(idName);
                default:
                    if (GenericLexer.IsLiteralValue(st.CurrentToken.Kind))
                    {
                        var val = st.ParseLiteralValue();
                        return new CtPrimitiveExpressionSyntax(val);
                    }
                    throw new InvalidOperationException("Unknown token");
            }
        }
        #endregion
    }

    public class CtSimpleEvaluationContext : IEnumerable<KeyValuePair<string, object>>
    {
        public IDictionary<string, object> Symbols { get; }

        public CtSimpleEvaluationContext()
        {
            Symbols = new Dictionary<string, object>();
        }

        public CtSimpleEvaluationContext(IDictionary<string, object> symbols)
        {
            Symbols = symbols;
        }

        public void Add(string key, object value)
        {
            Symbols.Add(key, value);
        }


        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return Symbols.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }


}
