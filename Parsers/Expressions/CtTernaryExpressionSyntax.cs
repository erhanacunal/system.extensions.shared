﻿namespace System.Text.Parsers.Expressions
{
    public class CtTernaryExpressionSyntax : CtExpressionSyntax
    {
        public CtExpressionSyntax TestCondition { get; set; }

        public CtExpressionSyntax IfTrue { get; set; }

        public CtExpressionSyntax IfFalse { get; set; }
        
        public CtTernaryExpressionSyntax(CtExpressionSyntax test, CtExpressionSyntax ifTrue, CtExpressionSyntax ifFalse) : base(CtSyntaxKind.TernaryExpression)
        {
            TestCondition = test;
            IfTrue = ifTrue;
            IfFalse = ifFalse;
            test.Parent = this;
            ifTrue.Parent = this;
            ifFalse.Parent = this;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitTernaryExpression(this);
            TestCondition?.AcceptVisitor(visitor);
            IfTrue?.AcceptVisitor(visitor);
            IfFalse?.AcceptVisitor(visitor);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitTernaryExpression(this);
        }
        public override string ToString()
        {
            if (TestCondition != null && IfTrue != null && IfFalse != null)
                return $"{TestCondition} ? {IfTrue} : {IfFalse}";
            return "{empty}";
        }
    }
}