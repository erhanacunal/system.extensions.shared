﻿namespace System.Text.Parsers.Expressions
{
    public class CtParenthesizedExpressionSyntax : CtExpressionSyntax
    {
        public CtExpressionSyntax Expression { get; set; }
        
        public CtParenthesizedExpressionSyntax(CtExpressionSyntax expr) : base(CtSyntaxKind.ParenthesizedExpression)
        {
            Expression = expr;
            expr.Parent = this;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitParenthesizedExpression(this);
            Expression?.AcceptVisitor(visitor);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitParenthesizedExpression(this);
        }
    }
}