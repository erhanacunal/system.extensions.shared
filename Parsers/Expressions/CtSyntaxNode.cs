﻿namespace System.Text.Parsers.Expressions
{
    public abstract class CtSyntaxNode
    {
        public CtSyntaxKind Kind { get; set; }
        public CtSyntaxNode Parent { get; set; }

        protected CtSyntaxNode(CtSyntaxKind kind)
        {
            Kind = kind;
        }
    }
}