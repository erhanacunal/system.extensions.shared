﻿namespace System.Text.Parsers.Expressions
{
    public abstract class CtSyntaxVisitor<TResult> : ICtSyntaxVisitor<TResult>
    {
        public virtual TResult VisitPrimitiveExpression(CtPrimitiveExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitParenthesizedExpression(CtParenthesizedExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitIdentiferExpression(CtIdentifierExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitBinaryExpression(CtBinaryExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitUnaryExpression(CtUnaryExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitTernaryExpression(CtTernaryExpressionSyntax expr)
        {
            return default(TResult);
        }

        public virtual TResult VisitMethodInvocationExpression(CtInvocationExpressionSyntax expr)
        {
            return default(TResult);
        }
    }
}