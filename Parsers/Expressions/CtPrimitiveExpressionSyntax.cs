﻿namespace System.Text.Parsers.Expressions
{
    public class CtPrimitiveExpressionSyntax : CtExpressionSyntax
    {

        public object Value { get; set; }

        public CtPrimitiveExpressionSyntax(object value) : base(CtSyntaxKind.PrimitiveExpression)
        {
            Value = value;
        }
        
        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitPrimitiveExpression(this);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitPrimitiveExpression(this);
        }

        public override string ToString()
        {
            if (Value == null)
                return "null";
            if (Value is string)
                return $"'{Value}'";
            return Value.ToString();
        }
    }
}