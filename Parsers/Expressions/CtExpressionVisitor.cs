﻿namespace System.Text.Parsers.Expressions
{
    public abstract class CtExpressionVisitor : ICtSyntaxVisitor
    {
        public virtual void VisitPrimitiveExpression(CtPrimitiveExpressionSyntax expr)
        {

        }

        public virtual void VisitParenthesizedExpression(CtParenthesizedExpressionSyntax expr)
        {

        }

        public virtual void VisitIdentiferExpression(CtIdentifierExpressionSyntax expr)
        {

        }

        public virtual void VisitBinaryExpression(CtBinaryExpressionSyntax expr)
        {

        }

        public virtual void VisitUnaryExpression(CtUnaryExpressionSyntax expr)
        {

        }

        public virtual void VisitTernaryExpression(CtTernaryExpressionSyntax expr)
        {

        }

        public virtual void VisitMethodInvocationExpression(CtInvocationExpressionSyntax expr)
        {

        }
    }
}
