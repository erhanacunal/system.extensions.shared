﻿using System;
using System.Collections.Generic;

namespace System.Text.Parsers.Expressions
{
    public class CtInvocationExpressionSyntax : CtExpressionSyntax
    {
        public string Name { get; set; }

        public List<CtExpressionSyntax> Parameters { get; }

        public Type TargetType { get; set; }
        
        public CtInvocationExpressionSyntax(string name) : base(CtSyntaxKind.InvocationExpression)
        {
            Name = name;
            Parameters = new List<CtExpressionSyntax>();
        }

        public CtInvocationExpressionSyntax(Type targetType, string methodName)
            : this(methodName)
        {
            Name = methodName;
            TargetType = targetType;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitMethodInvocationExpression(this);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitMethodInvocationExpression(this);
        }

        public override string ToString()
        {
            return $"{Name}({string.Join(", ", Parameters)})";
        }
    }
}