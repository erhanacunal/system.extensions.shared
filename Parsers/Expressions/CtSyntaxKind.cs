﻿namespace System.Text.Parsers.Expressions
{
    public enum CtSyntaxKind
    {
        None,
        PrimitiveExpression,
        ParenthesizedExpression,
        IdentifierExpression,
        BinaryExpression,
        UnaryExpression,
        TernaryExpression,
        InvocationExpression,
        ForEachStatement,
        BlockStatement
    }
}