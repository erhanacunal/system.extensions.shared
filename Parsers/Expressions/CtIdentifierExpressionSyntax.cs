﻿namespace System.Text.Parsers.Expressions
{
    public class CtIdentifierExpressionSyntax : CtExpressionSyntax
    {
        public string Name { get; set; }

        public CtIdentifierExpressionSyntax(string name) : base(CtSyntaxKind.IdentifierExpression)
        {
            Name = name;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitIdentiferExpression(this);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitIdentiferExpression(this);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}