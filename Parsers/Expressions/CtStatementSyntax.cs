﻿using System.Collections.Generic;

namespace System.Text.Parsers.Expressions
{
    public abstract class CtStatementSyntax : CtSyntaxNode
    {
        protected CtStatementSyntax(CtSyntaxKind kind) : base(kind)
        {
        }
    }

    public sealed class CtBlockStatementSyntax : CtStatementSyntax
    {
        public List<CtStatementSyntax> Body { get; } = new List<CtStatementSyntax>();

        public CtBlockStatementSyntax() : base(CtSyntaxKind.BlockStatement)
        {

        }
    }

    public sealed class CtForEachStatementSyntax : CtStatementSyntax
    {
        public string VariableName { get; set; }
        public string LoopVariableName { get; set; }
        public CtBlockStatementSyntax Loop { get; } = new CtBlockStatementSyntax();
         

        public CtForEachStatementSyntax() : base(CtSyntaxKind.ForEachStatement)
        {
        }
    }

}