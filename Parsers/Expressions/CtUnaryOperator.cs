﻿namespace System.Text.Parsers.Expressions
{
    public enum CtUnaryOperator
    {
        Not,
        Minus,
        Plus
    }
}