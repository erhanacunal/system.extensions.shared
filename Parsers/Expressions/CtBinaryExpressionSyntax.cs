﻿namespace System.Text.Parsers.Expressions
{
    public class CtBinaryExpressionSyntax : CtExpressionSyntax
    {
        public CtExpressionSyntax Left { get; set; }

        public CtExpressionSyntax Right { get; set; }

        public CtBinaryOperator Operator { get; set; }
        
        public CtBinaryExpressionSyntax(CtExpressionSyntax left, CtExpressionSyntax right, CtBinaryOperator op) : base(CtSyntaxKind.BinaryExpression)
        {
            Left = left;
            Right = right;
            Operator = op;
            left.Parent = this;
            right.Parent = this;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitBinaryExpression(this);
            Left.AcceptVisitor(visitor);
            Right.AcceptVisitor(visitor);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitBinaryExpression(this);
        }

        string OperatorString()
        {
            switch (Operator)
            {
                case CtBinaryOperator.Add:
                    return "+";
                case CtBinaryOperator.Subtract:
                    return "-";
                case CtBinaryOperator.Multiply:
                    return "*";
                case CtBinaryOperator.Divide:
                    return "/";
                case CtBinaryOperator.And:
                    return "&&";
                case CtBinaryOperator.Or:
                    return "||";
                case CtBinaryOperator.Xor:
                    return "^";
                case CtBinaryOperator.Equal:
                    return "=";
                case CtBinaryOperator.NotEqual:
                    return "!=";
                case CtBinaryOperator.GreaterThan:
                    return ">";
                case CtBinaryOperator.GreaterThanOrEqual:
                    return ">=";
                case CtBinaryOperator.LessThan:
                    return "<";
                case CtBinaryOperator.LessThanOrEqual:
                    return "<=";
                case CtBinaryOperator.Modulo:
                    return "%";
                case CtBinaryOperator.Shr:
                    return ">>";
                case CtBinaryOperator.Shl:
                    return "<<";
                default:
                    return "";
            }
        }

        public override string ToString()
        {
            if (Left != null && Right != null)
            {
                return $"{Left} {OperatorString()} {Right}";
            }
            return "{empty}";
        }

    }
}