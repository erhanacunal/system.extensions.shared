﻿namespace System.Text.Parsers.Expressions
{
    public enum CtBinaryOperator
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        And,
        Or,
        Xor,
        Equal,
        NotEqual,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Modulo,
        Shr,
        Shl
    }
}