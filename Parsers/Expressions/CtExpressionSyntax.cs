﻿namespace System.Text.Parsers.Expressions
{
    public abstract class CtExpressionSyntax : CtSyntaxNode
    {
        protected CtExpressionSyntax(CtSyntaxKind kind) : base(kind)
        {
        }

        public abstract void AcceptVisitor(ICtSyntaxVisitor visitor);
        public abstract T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor);
    }
}