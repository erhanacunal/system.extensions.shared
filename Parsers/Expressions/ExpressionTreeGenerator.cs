﻿#if !MOBILE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Parsers;
using System.Utils;

namespace System.Text.Parsers.Expressions
{
    public class ExpressionTreeGenerator : CtSyntaxVisitor<Expression>
    {
        readonly List<MethodRecord> methodRecords = new List<MethodRecord>();

        public Dictionary<string, object> Variables { get; } = new Dictionary<string, object>();

        public void RegisterMethod(Type classType, object instance, string name, string alias)
        {
            methodRecords.Add(new MethodRecord(classType, instance, name, alias));
        }

        internal bool TryFindMethod(string name, out MethodRecord method)
        {
            method = MethodRecord.Null;
            foreach (var mi in methodRecords)
            {
                if (!mi.Alias.Equals(name, StringComparison.OrdinalIgnoreCase)) continue;
                method = mi;
                return true;
            }
            return false;
        }

        public override Expression VisitPrimitiveExpression(CtPrimitiveExpressionSyntax expr)
        {
            return Expression.Constant(expr.Value);
        }

        public override Expression VisitParenthesizedExpression(CtParenthesizedExpressionSyntax expr)
        {
            return expr.Expression.AcceptVisitor(this);
        }

        public override Expression VisitIdentiferExpression(CtIdentifierExpressionSyntax expr)
        {
            return Expression.Constant(Variables[expr.Name]);
        }

        private void ConvertLeftRight(CtExpressionSyntax left, CtExpressionSyntax right, out Expression leftExpr, out Expression rightExpr)
        {
            if (left == null || right == null)
                throw new ArgumentException("Left or Right is null");
            leftExpr = left.AcceptVisitor(this);
            rightExpr = right.AcceptVisitor(this);
            var leftType = leftExpr.Type;
            var rightType = rightExpr.Type;
            if (leftType == rightType) return;
            var convertType = typeof(Convert);
            if (TypeUtils.IsFloatingNumberType(leftType))
                rightExpr = Expression.Convert(rightExpr, leftType);
            else if (TypeUtils.IsFloatingNumberType(rightType))
                leftExpr = Expression.Convert(leftExpr, rightType);
            else if (TypeUtils.IsIntegerNumberType(leftType))
                rightExpr = Expression.Convert(rightExpr, leftType);
            else if (TypeUtils.IsIntegerNumberType(rightType))
                leftExpr = Expression.Convert(leftExpr, rightType);
            else if (leftType == typeof(string))
                rightExpr = Expression.Call(convertType, "ToString", null, rightExpr);
            else if (rightType == typeof(string))
                leftExpr = Expression.Call(convertType, "ToString", null, leftExpr);
            else
                throw new InvalidOperationException("Unknown type!");
        }

        public override Expression VisitBinaryExpression(CtBinaryExpressionSyntax expr)
        {
            Expression leftExpr, rightExpr;
            ConvertLeftRight(expr.Left, expr.Right, out leftExpr, out rightExpr);
            switch (expr.Operator)
            {
                case CtBinaryOperator.Add:
                    return Expression.Add(leftExpr, rightExpr);
                case CtBinaryOperator.Subtract:
                    return Expression.Subtract(leftExpr, rightExpr);
                case CtBinaryOperator.Multiply:
                    return Expression.Multiply(leftExpr, rightExpr);
                case CtBinaryOperator.Divide:
                    return Expression.Divide(leftExpr, rightExpr);
                case CtBinaryOperator.And:
                    if (TypeUtils.IsIntegerNumberType(leftExpr.Type))
                        return Expression.And(leftExpr, rightExpr);
                    return Expression.AndAlso(leftExpr, rightExpr);
                case CtBinaryOperator.Or:
                    if (TypeUtils.IsIntegerNumberType(leftExpr.Type))
                        return Expression.Or(leftExpr, rightExpr);
                    return Expression.OrElse(leftExpr, rightExpr);
                case CtBinaryOperator.Xor:
                    return Expression.ExclusiveOr(leftExpr, rightExpr);
                case CtBinaryOperator.Equal:
                    return Expression.Equal(leftExpr, rightExpr);
                case CtBinaryOperator.GreaterThan:
                    return Expression.GreaterThan(leftExpr, rightExpr);
                case CtBinaryOperator.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(leftExpr, rightExpr);
                case CtBinaryOperator.LessThan:
                    return Expression.LessThan(leftExpr, rightExpr);
                case CtBinaryOperator.LessThanOrEqual:
                    return Expression.LessThanOrEqual(leftExpr, rightExpr);
                case CtBinaryOperator.Modulo:
                    return Expression.Modulo(leftExpr, rightExpr);
                case CtBinaryOperator.Shr:
                    return Expression.RightShift(leftExpr, rightExpr);
                case CtBinaryOperator.Shl:
                    return Expression.LeftShift(leftExpr, rightExpr);
                case CtBinaryOperator.NotEqual:
                    return Expression.NotEqual(leftExpr, rightExpr);
                default:
                    throw new NotImplementedException();
            }
        }

        public override Expression VisitUnaryExpression(CtUnaryExpressionSyntax expr)
        {
            switch (expr.Operator)
            {
                case CtUnaryOperator.Not:
                    return Expression.Not(expr.TargetExpression.AcceptVisitor(this));
                case CtUnaryOperator.Minus:
                    return Expression.Negate(expr.TargetExpression.AcceptVisitor(this));
                case CtUnaryOperator.Plus:
                    return expr.TargetExpression.AcceptVisitor(this);
                default:
                    throw new NotImplementedException();
            }
        }

        public override Expression VisitTernaryExpression(CtTernaryExpressionSyntax expr)
        {
            return Expression.Condition(
                expr.TestCondition.AcceptVisitor(this),
                expr.IfTrue.AcceptVisitor(this),
                expr.IfFalse.AcceptVisitor(this));
        }

        public override Expression VisitMethodInvocationExpression(CtInvocationExpressionSyntax expr)
        {
            MethodRecord method;
            MethodInfo methodInfo = null;
            Type classType = null;
            object instance = null;
            if (expr.TargetType != null)
            {
                methodInfo = expr.TargetType.GetMethods().FirstOrDefault(_ => _.Name.Equals(expr.Name) && _.GetParameters().Length == expr.Parameters.Count);
                classType = expr.TargetType;
            }
            else if (TryFindMethod(expr.Name, out method))
            {
                methodInfo = method.ClassType.GetMethod(method.Name);
                instance = method.Instance;
                classType = method.ClassType;
            }
            if (methodInfo == null)
                throw new InvalidOperationException("method not found -> " + expr.Name);

            var methodParameters = methodInfo.GetParameters();

            var methodCallParameters = new List<Expression>();
            var paramQueue = new Queue<CtExpressionSyntax>(expr.Parameters);
            foreach (var methodParameter in methodParameters)
            {
                if (paramQueue.Count == 0)
                    throw new InvalidOperationException("Insufficient parameters");
                if (methodParameter.ParameterType.IsArray)
                {
                    var elements = new List<Expression>();
                    var elType = methodParameter.ParameterType.GetElementType();
                    while (paramQueue.Count > 0)
                    {
                        var parameter = paramQueue.Dequeue();
                        var paramExpression = parameter.AcceptVisitor(this);
                        CastAndAdd(elType, paramExpression, elements);
                    }
                    methodCallParameters.Add(Expression.NewArrayInit(elType, elements));
                }
                else
                    CastAndAdd(methodParameter.ParameterType, paramQueue.Dequeue().AcceptVisitor(this),
                        methodCallParameters);
            }
            return instance != null
                ? Expression.Call(Expression.Constant(instance), methodInfo, methodCallParameters.ToArray())
                : Expression.Call(classType, methodInfo.Name, null, methodCallParameters.ToArray());
        }

        private void CastAndAdd(Type requiredType, Expression expr, IList<Expression> targetList)
        {
            if (requiredType == typeof(object) || requiredType == expr.Type)
                targetList.Add(expr);
            else
                targetList.Add(Expression.Convert(expr, requiredType));
        }
    }
} 
#endif