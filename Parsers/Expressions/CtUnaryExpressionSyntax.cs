﻿namespace System.Text.Parsers.Expressions
{
    public class CtUnaryExpressionSyntax : CtExpressionSyntax
    {
        public CtExpressionSyntax TargetExpression { get; set; }

        public CtUnaryOperator Operator { get; set; }
        
        public CtUnaryExpressionSyntax(CtExpressionSyntax expr, CtUnaryOperator op) : base(CtSyntaxKind.UnaryExpression)
        {
            TargetExpression = expr;
            Operator = op;
            expr.Parent = this;
        }

        public override void AcceptVisitor(ICtSyntaxVisitor visitor)
        {
            visitor.VisitUnaryExpression(this);
            TargetExpression?.AcceptVisitor(visitor);
        }

        public override T AcceptVisitor<T>(ICtSyntaxVisitor<T> visitor)
        {
            return visitor.VisitUnaryExpression(this);
        }

        string OperatorString()
        {
            switch (Operator)
            {
                case CtUnaryOperator.Not:
                    return "!";
                case CtUnaryOperator.Minus:
                    return "-";
                default:
                    return "";
            }
        }

        public override string ToString()
        {
            return TargetExpression != null
                ? string.Concat(OperatorString(), TargetExpression)
                : "{empty}";
        }
    }
}