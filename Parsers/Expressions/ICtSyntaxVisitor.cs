﻿namespace System.Text.Parsers.Expressions
{
    public interface ICtSyntaxVisitor<out TResult>
    {
        TResult VisitPrimitiveExpression(CtPrimitiveExpressionSyntax expr);
        TResult VisitParenthesizedExpression(CtParenthesizedExpressionSyntax expr);
        TResult VisitIdentiferExpression(CtIdentifierExpressionSyntax expr);
        TResult VisitBinaryExpression(CtBinaryExpressionSyntax expr);
        TResult VisitUnaryExpression(CtUnaryExpressionSyntax expr);
        TResult VisitTernaryExpression(CtTernaryExpressionSyntax expr);
        TResult VisitMethodInvocationExpression(CtInvocationExpressionSyntax expr);
    }

    public interface ICtSyntaxVisitor
    {
        void VisitPrimitiveExpression(CtPrimitiveExpressionSyntax expr);
        void VisitParenthesizedExpression(CtParenthesizedExpressionSyntax expr);
        void VisitIdentiferExpression(CtIdentifierExpressionSyntax expr);
        void VisitBinaryExpression(CtBinaryExpressionSyntax expr);
        void VisitUnaryExpression(CtUnaryExpressionSyntax expr);
        void VisitTernaryExpression(CtTernaryExpressionSyntax expr);
        void VisitMethodInvocationExpression(CtInvocationExpressionSyntax expr);
    }
}