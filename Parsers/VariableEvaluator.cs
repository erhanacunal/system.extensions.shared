﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Text.Parsers
{
    public class VariableEvaluator
    {
        readonly Dictionary<string, object> variables;

        public VariableEvaluator(Dictionary<string, object> variables)
        {
            this.variables = variables;
        }

        public IDictionary<string, object> Variables => variables;

        public string Evaluate(string src)
        {
            if (string.IsNullOrWhiteSpace(src)) return src;
            var result = src;
            var lexer = new GenericLexer(new GenericLexerOptions { ParseQualifiedIdentifier = true }, src);

            var replacements = new List<Tuple<int, int, string>>();
            char ch;
            do
            {
                ch = lexer.NextChar();
                if (ch == '$' && lexer.NextChar() == '(')
                {
                    var idx = lexer.Position - 2;
                    lexer.NextToken();
                    string id;
                    if (lexer.CurrentToken.Kind == TokenKind.String)
                    {
                        id = lexer.CurrentToken.TokenString;
                        lexer.NextToken();
                    }
                    else
                        lexer.Expect(TokenKind.Identifier, out id);
                    var endIdx = lexer.CurrentToken.EndLocation.Offset;
                    lexer.Expect(TokenKind.RParen);
                    if (TryEvaluateFunction(id, out var value))
                    {
                        replacements.Add(Tuple.Create(idx, endIdx, Convert.ToString(value)));
                    }
                    else if (variables.TryGetValue(id, out value))
                    {
                        replacements.Add(Tuple.Create(idx, endIdx, Convert.ToString(value)));
                    }
                }
            } while (ch != CharReader.INVALID_CHARACTER);
            replacements.Reverse();
            return replacements.Aggregate(result,
                (current, item) => current.Remove(item.Item1, item.Item2 - item.Item1).Insert(item.Item1, item.Item3));
        }

        protected virtual bool TryEvaluateFunction(string function, out object value)
        {
            value = null;
            return false;
        }
    }
}
