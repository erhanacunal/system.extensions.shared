﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace System.Text.Parsers
{

    public enum TokenKind
    {
        None,
        Eof,
        String,
        Int16,
        Int32,
        Int64,
        Single,
        Double,
        Decimal,
        Boolean,
        Identifier,

        /*Punctutations*/

        /// <summary>
        /// ',' virgül
        /// </summary>
        Comma,
        /// <summary>
        /// ':' iki nokta üst üste
        /// </summary>
        Colon,
        /// <summary>
        /// ';' noktalı virgül
        /// </summary>
        Semicolon,
        Dot,
        DotDot,
        LParen,
        RParen,
        LCurlyBrace,
        RCurlyBrace,
        LBracket,
        RBracket,
        Plus,
        PlusPlus,
        Minus,
        MinusMinus,
        Multiply,
        Divide,
        Equal,
        /// <summary>
        /// &gt; büyüktür
        /// </summary>
        GreaterThan,
        GreaterThanGreaterThan,
        GreaterThanOrEqual,
        /// <summary>
        /// &lt; küçüktür
        /// </summary>
        LessThan,
        LessThanLessThan,
        LessThanOrEqual,
        EqualEqual,
        ExclamationEqual,
        Exclamation,
        AtSymbol,
        Question,
        QuestionQuestion,
        Ampersand,
        AmpersandAmpersand,
        Pipe,
        PipePipe,
        LessThanAndGreaterThan,
        PercentSign,
        Caret,
        Hash,
        BackSlash,
        Dollar,
        BadToken
    }
    [DebuggerDisplay("{Kind} @{StartLocation}|{EndLocation}")]
    public class Token
    {

        public static readonly Token Null = new NullTokenNode();

        public virtual bool IsNull { get; protected set; }

        public TokenLocation StartLocation { get; set; }
        public TokenLocation EndLocation { get; set; }

        public string TokenString { get; set; }

        public TokenKind Kind { get; protected set; }
        public int SubKind { get; set; }

        public Token()
        {
            IsNull = false;
        }

        public Token(TokenKind kind, TokenLocation start, TokenLocation end, string text = null)
        {
            Kind = kind;
            StartLocation = start;
            EndLocation = end;
            TokenString = text;
        }

        public static bool operator ==(Token token1, Token token2)
        {
            return Equals(token1, token2);
        }

        public static bool operator !=(Token token1, Token token2)
        {
            return !Equals(token1, token2);
        }

        public static implicit operator string(Token token)
        {
            var id = token as IdentifierToken;
            return id?.ToString() ?? token.TokenString;
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }

        public bool IsLiteralValue
        {
            get
            {
                switch (Kind)
                {
                    case TokenKind.String:
                    case TokenKind.Int16:
                    case TokenKind.Int32:
                    case TokenKind.Int64:
                    case TokenKind.Single:
                    case TokenKind.Double:
                    case TokenKind.Decimal:
                    case TokenKind.Boolean:
                        return true;
                    default:
                        return false;
                }
            }
        }

        sealed class NullTokenNode : Token
        {

            public NullTokenNode()
                : base(TokenKind.Eof, new TokenLocation(), new TokenLocation())
            {

            }
            public override bool IsNull => true;
        }
    }

    public class IdentifierToken : Token
    {

        public new static readonly IdentifierToken Null = new NullTokenNode();

        public IdentifierToken Target { get; set; }

        public bool IsQualified => Target != null;



        public IdentifierToken(TokenLocation start, TokenLocation end, string identifier)
            : base(TokenKind.Identifier, start, end)
        {
            TokenString = identifier;
        }

        public static implicit operator string(IdentifierToken id)
        {
            return id.ToString();
        }

        public override string ToString()
        {
            if (Target == null)
                return TokenString;
            return $"{Target}.{TokenString}";
        }

        sealed class NullTokenNode : IdentifierToken
        {

            public NullTokenNode()
                : base(new TokenLocation(), new TokenLocation(), string.Empty)
            {

            }
            public override bool IsNull => true;
        }

    }

    public enum DefaultFloatingPointType
    {
        Double,
        Decimal,
        Single
    }

    public enum DefaultIntegerType
    {
        Int16,
        Int32,
        Int64,
        Double,
        Decimal,
        Single
    }

    public sealed class GenericLexerOptions
    {
        /// <summary>
        /// Noktalı bir sayı ile karşılaşırsa hangi tipte token oluşturacağını verir veya ayarlar
        /// </summary>
        public DefaultFloatingPointType DefaultFloatingPointType { get; set; }
        /// <summary>
        /// Tam sayı ile karşılaşırsa hangi tipte token oluşturacağını verir veya ayarlar
        /// </summary>
        public DefaultIntegerType DefaultIntegerType { get; set; }

        /// <summary>
        /// Sayıların sonundaki sayı tipini belirten harfleri algılayıp algılamayacağını verir veya ayarlar
        /// </summary>
        public bool ParseNumberSuffix { get; set; }
        /// <summary>
        /// QualifierChar ile ayrılmış tanımlayıcıları algılayıp algılamayacağını verir veya ayarlar
        /// </summary>
        public bool ParseQualifiedIdentifier { get; set; }
        /// <summary>
        /// Tanımlayıcıları ayıran karakteri verir veya ayarlar
        /// </summary>
        public char QualiferChar { get; set; }
        /// <summary>
        /// &amp;&amp; == &lt;&lt; &gt;&gt; ?? || != gibi işaretleri algılayıp algılamayacağını verir veya ayarlar
        /// </summary>
        public bool ParsePunctuationPairs { get; set; }
        /// <summary>
        /// ParseLiteralValue yönteminde eğer bir identifier'e rastlanırsa bunu string olarak algılayıp algılamayacağını ayarlar
        /// </summary>
        public bool ParseIdentifierAsLiteralValue { get; set; }
        /// <summary>
        /// ##ID gibi tanımlayıcılara izin verilip vermeyeceğini verir veya ayarlar
        /// </summary>
        public bool AllowHashedIdentifiers { get; set; }
        /// <summary>
        /// $ işaretinin bir tanımlayıcı olarak algılanıp algılanmayacağını verir veya ayarlar
        /// </summary>
        public bool AllowDollarSignAsIdentifier { get; set; }
        /// <summary>
        /// - ile ayrılmış tanımlayıcıları okuyup okumayacağını verir veya ayarlar. örnek: http-equiv gibi
        /// </summary>
        public bool ParseDashedIdentifiers { get; set; }
        /// <summary>
        /// \t \r \n gibi kaçış sıralamalarını ayıklayıp ayıklayamayacağını verir veya ayarlar
        /// </summary>
        public bool ParseEscapeChars { get; set; }
        /// <summary>
        /// Dize'ler içinde \r \n gibi kaçış sıralamalarına izin verip verilmeyeceğini verir veya ayarlar
        /// </summary>
        public bool AllowCrLfInStrings { get; set; }
        /// <summary>
        /// [ ] parantezleri içinde bulunan tanımlayıcıları algılayıp algılamayacağını verir veya ayarlar
        /// </summary>
        public bool ParseBracketIdentifiers { get; set; }
        /// <summary>
        /// Eğer <see cref="ParseBracketIdentifiers"/> property'si true olarak ayarlanmışsa ve [] ile çevrelenmiş bir tanımlayıcı algılanırsa parantezleri kaldırır
        /// </summary>
        public bool RemoveBrackets { get; set; }
        /// <summary>
        /// Yorum satırlarını ayıklayıp ayıklamayacağını ayarlar
        /// </summary>
        public bool ParseComments { get; set; }
        /// <summary>
        /// Tanımadığı karakterleri geçer
        /// </summary>
        public bool SkipUnknownChars { get; set; }

        public GenericLexerOptions()
        {
            QualiferChar = '.';
            ParsePunctuationPairs = true;
            DefaultIntegerType = DefaultIntegerType.Int32;
            DefaultFloatingPointType = DefaultFloatingPointType.Double;
        }
    }

    /// <summary>
    /// Basit olarak bir string değeri ayıklamayı sağlar
    /// </summary>
    public class GenericLexer : CharReader
    {
        readonly GenericLexerOptions options;

        Token currentToken;
        string tokenString;

        public string TokenString => tokenString;

        public Token CurrentToken
        {
            get
            {
                EnsureStarted();
                return currentToken;
            }
        }

        public GenericLexer(GenericLexerOptions options, string source, int startIndex = 0)
            : base(source, startIndex)
        {
            this.options = options;
            currentToken = Token.Null;
        }

        protected override void OnSourceChanged()
        {
            base.OnSourceChanged();
            currentToken = Token.Null;
            tokenString = null;
        }

        protected bool ParseIdentifier(char ch, TokenLocation? start, IdentifierToken target)
        {
            if (!char.IsLetter(ch) && ch != '_' &&
                (!options.AllowDollarSignAsIdentifier || (options.AllowDollarSignAsIdentifier && ch != '$')))
                return false;
            if (!start.HasValue)
            {
                start = GetTextLocation();
                NextChar();
            }
            var sb = new StringBuilder();
            sb.Append(ch);
            while (char.IsLetterOrDigit(PeekChar()) || (PeekChar() == '_') || (options.ParseDashedIdentifiers && PeekChar() == '-') || (options.AllowDollarSignAsIdentifier && PeekChar() == '$'))
                sb.Append(NextChar());
            var result = new IdentifierToken(start.Value, GetTextLocation(), sb.ToString());
            currentToken = result;
            result.Target = target;
            if (!options.ParseQualifiedIdentifier) return true;
            SkipWhiteSpace();
            if (PeekChar() != options.QualiferChar) return true;
            NextChar();
            SkipWhiteSpace();
            ParseIdentifier(PeekChar(), null, result);
            return true;
        }

        public Token PeekToken()
        {
            PushState();
            var saveCurrentToken = currentToken;
            var saveTokenString = tokenString;

            var token = NextToken();

            PopState();
            currentToken = saveCurrentToken;
            tokenString = saveTokenString;

            return token;
        }

        TokenKind GetDefaultFloatingPointTypeTokenKind()
        {
            switch (options.DefaultFloatingPointType)
            {
                case DefaultFloatingPointType.Double:
                    return TokenKind.Double;
                case DefaultFloatingPointType.Decimal:
                    return TokenKind.Decimal;
                case DefaultFloatingPointType.Single:
                    return TokenKind.Single;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        TokenKind GetDefaultIntegerTypeTokenKind()
        {
            switch (options.DefaultIntegerType)
            {
                case DefaultIntegerType.Int16:
                    return TokenKind.Int16;
                case DefaultIntegerType.Int32:
                    return TokenKind.Int32;
                case DefaultIntegerType.Int64:
                    return TokenKind.Int64;
                case DefaultIntegerType.Double:
                    return TokenKind.Double;
                case DefaultIntegerType.Decimal:
                    return TokenKind.Decimal;
                case DefaultIntegerType.Single:
                    return TokenKind.Single;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public Token NextToken()
        {
            SkipWhiteSpace();
            tokenString = "";
            var startLocation = GetTextLocation();
            var ch = NextChar();
            if (ch == INVALID_CHARACTER)
            {
                currentToken = new Token(TokenKind.Eof, startLocation, GetTextLocation());
            }
            else if (char.IsNumber(ch))
            {
                tokenString += ch;
                while (char.IsNumber(PeekChar()))
                    tokenString += NextChar();
                if (PeekChar() == '.')
                {
                    NextChar();
                    tokenString += '.';
                    while (char.IsNumber(PeekChar()))
                        tokenString += NextChar();
                    if (options.ParseNumberSuffix)
                        switch (char.ToLower(PeekChar()))
                        {
                            case 'm':
                                currentToken = new Token(TokenKind.Decimal, startLocation, GetTextLocation(), tokenString);
                                NextChar();
                                break;
                            case 'f':
                                currentToken = new Token(TokenKind.Single, startLocation, GetTextLocation(), tokenString);
                                NextChar();
                                break;
                            case 'd':
                                currentToken = new Token(TokenKind.Double, startLocation, GetTextLocation(), tokenString);
                                NextChar();
                                break;
                            default:
                                currentToken = new Token(GetDefaultFloatingPointTypeTokenKind(), startLocation, GetTextLocation(), tokenString);
                                break;
                        }
                    else
                        currentToken = new Token(GetDefaultFloatingPointTypeTokenKind(), startLocation, GetTextLocation(), tokenString);

                }
                else
                {
                    if (options.ParseNumberSuffix)
                    {
                        switch (char.ToLower(PeekChar()))
                        {
                            case 'l':
                                currentToken = new Token(TokenKind.Int64, startLocation, GetTextLocation(), tokenString);
                                NextChar();
                                break;
                            default:
                                currentToken = new Token(GetDefaultIntegerTypeTokenKind(), startLocation, GetTextLocation(), tokenString);
                                break;
                        }
                    }
                    else
                        currentToken = new Token(GetDefaultIntegerTypeTokenKind(), startLocation, GetTextLocation(), tokenString);
                }
            }
            else if (ParseIdentifier(ch, startLocation, null))
            {
                tokenString = currentToken.ToString();
                currentToken = GetIdentifierTokenOverride(currentToken as IdentifierToken);
            }
            else if (ch == '\"' || ch == '\'')
            {
                currentToken = ParseStringLiterals(startLocation, ch, options.ParseEscapeChars, options.AllowCrLfInStrings);
            }
            else
            {
                TokenKind kind;
                switch (ch)
                {
                    case '{':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.LCurlyBrace, startLocation, GetTextLocation());
                        break;
                    case '}':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.RCurlyBrace, startLocation, GetTextLocation());
                        break;
                    case '(':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.LParen, startLocation, GetTextLocation());
                        break;
                    case ')':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.RParen, startLocation, GetTextLocation());
                        break;
                    case '[':
                        if (options.ParseBracketIdentifiers)
                        {
                            PushState();
                            SkipWhiteSpace();
                            if (char.IsLetter(PeekChar()))
                            {
                                tokenString = TakeUntil(']', '\r', '\n', '\t');
                                if (!options.RemoveBrackets)
                                    tokenString = string.Concat("[", tokenString, "]");
                                currentToken = new IdentifierToken(startLocation, GetTextLocation(), tokenString);
                                DropState();
                            }
                            else
                                PopState();
                        }
                        else
                        {
                            tokenString += ch;
                            currentToken = new Token(TokenKind.LBracket, startLocation, GetTextLocation());
                        }
                        break;
                    case ']':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.RBracket, startLocation, GetTextLocation());
                        break;
                    case '%':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.PercentSign, startLocation, GetTextLocation());
                        break;
                    case '^':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Caret, startLocation, GetTextLocation());
                        break;
                    case '\\':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.BackSlash, startLocation, GetTextLocation());
                        break;
                    case '.':
                        tokenString += ch;
                        kind = TokenKind.Dot;
                        if (options.ParsePunctuationPairs && PeekChar() == '.')
                        {
                            kind = TokenKind.DotDot;
                            NextChar();
                            tokenString += '.';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '?':
                        tokenString += ch;
                        kind = TokenKind.Question;
                        if (options.ParsePunctuationPairs && PeekChar() == '?')
                        {
                            kind = TokenKind.QuestionQuestion;
                            NextChar();
                            tokenString += '?';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '&':
                        tokenString += ch;
                        kind = TokenKind.Ampersand;
                        if (options.ParsePunctuationPairs && PeekChar() == '&')
                        {
                            kind = TokenKind.AmpersandAmpersand;
                            NextChar();
                            tokenString += '&';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '|':
                        tokenString += ch;
                        kind = TokenKind.Pipe;
                        if (options.ParsePunctuationPairs && PeekChar() == '|')
                        {
                            kind = TokenKind.PipePipe;
                            NextChar();
                            tokenString += '|';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '@':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.AtSymbol, startLocation, GetTextLocation());
                        break;
                    case ':':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Colon, startLocation, GetTextLocation());
                        break;

                    case ';':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Semicolon, startLocation, GetTextLocation());
                        break;
                    case ',':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Comma, startLocation, GetTextLocation());
                        break;
                    case '+':
                        tokenString += ch;
                        kind = TokenKind.Plus;
                        if (options.ParsePunctuationPairs && PeekChar() == '+')
                        {
                            kind = TokenKind.PlusPlus;
                            NextChar();
                            tokenString += '+';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '-':
                        tokenString += ch;
                        kind = TokenKind.Minus;
                        if (options.ParsePunctuationPairs && PeekChar() == '-')
                        {
                            kind = TokenKind.MinusMinus;
                            NextChar();
                            tokenString += '-';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '*':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Multiply, startLocation, GetTextLocation());
                        break;
                    case '/':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Divide, startLocation, GetTextLocation());
                        break;
                    case '=':
                        tokenString += ch;
                        kind = TokenKind.Equal;
                        if (options.ParsePunctuationPairs && PeekChar() == '=')
                        {
                            kind = TokenKind.EqualEqual;
                            NextChar();
                            tokenString += '=';
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '!':
                        tokenString += ch;
                        kind = TokenKind.Exclamation;
                        if (options.ParsePunctuationPairs && PeekChar() == '=')
                        {
                            kind = TokenKind.ExclamationEqual;
                            tokenString += '=';
                            NextChar();
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '>':
                        tokenString += ch;
                        kind = TokenKind.GreaterThan;
                        if (options.ParsePunctuationPairs && PeekChar() == '=')
                        {
                            tokenString += '=';
                            kind = TokenKind.GreaterThanOrEqual;
                            NextChar();
                        }
                        else if (options.ParsePunctuationPairs && PeekChar() == '>')
                        {
                            tokenString += '>';
                            kind = TokenKind.GreaterThanGreaterThan;
                            NextChar();
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '<':
                        kind = TokenKind.LessThan;
                        if (options.ParsePunctuationPairs && PeekChar() == '=')
                        {
                            tokenString += '=';
                            kind = TokenKind.LessThanOrEqual;
                            NextChar();
                        }
                        else if (options.ParsePunctuationPairs && PeekChar() == '>')
                        {
                            tokenString += '>';
                            kind = TokenKind.LessThanAndGreaterThan;
                            NextChar();
                        }
                        else if (options.ParsePunctuationPairs && PeekChar() == '<')
                        {
                            tokenString += '<';
                            kind = TokenKind.LessThanLessThan;
                            NextChar();
                        }
                        currentToken = new Token(kind, startLocation, GetTextLocation());
                        break;
                    case '#':
                        tokenString += ch;
                        if (options.AllowHashedIdentifiers)
                        {
                            PushState();
                            while (PeekChar() == '#')
                                tokenString += NextChar();
                            if (ParseIdentifier(NextChar(), startLocation, null))
                            {
                                tokenString = string.Concat(tokenString, currentToken.TokenString);
                                currentToken.TokenString = tokenString;
                                DropState();
                            }
                            else
                            {
                                PopState();
                                currentToken = new Token(TokenKind.Hash, startLocation, GetTextLocation());
                                tokenString = "#";
                            }
                        }
                        else
                            currentToken = new Token(TokenKind.Hash, startLocation, GetTextLocation());
                        break;
                    case '$':
                        tokenString += ch;
                        currentToken = new Token(TokenKind.Dollar, startLocation, GetTextLocation());
                        break;
                    default:
                        currentToken = NextTokenOverride(ch, startLocation);
                        break;
                }
            }
            return currentToken;
        }

        protected Token ParseStringLiterals(TokenLocation start, char quoteChar, bool parseEscapeChars, bool allowCrLfInStrings)
        {
            var sb = new StringBuilder();
            while (true)
            {
                var ch = PeekChar();
                if (ch == INVALID_CHARACTER)
                    throw new GenericLexerException("Unterminated string!");
                if (ch == '\\' && parseEscapeChars)
                {
                    ch = ParseEscapeSequence();
                    sb.Append(ch);
                }
                else if (ch == quoteChar)
                {
                    AdvanceChar();
                    if (allowCrLfInStrings && PeekChar() == quoteChar)
                        sb.Append(ch);
                    else
                        break;
                }
                else if (!allowCrLfInStrings && (ch == '\r' || ch == '\n'))
                    throw new GenericLexerException("CR/LF not allowed in strings!");
                else
                {
                    AdvanceChar();
                    sb.Append(ch);
                }
            }
            return new Token(TokenKind.String, start, GetTextLocation(), sb.ToString());
        }

        char ParseEscapeSequence()
        {
            var ch = NextChar();
            Debug.Assert(ch == '\\');
            ch = NextChar();
            switch (ch)
            {
                case '\'':
                case '"':
                case '\\':
                    break;
                // translate escapes as per C# spec 2.4.4.4
                case '0':
                    ch = '\u0000';
                    break;
                case 'a':
                    ch = '\u0007';
                    break;
                case 'b':
                    ch = '\u0008';
                    break;
                case 'f':
                    ch = '\u000c';
                    break;
                case 'n':
                    ch = '\u000a';
                    break;
                case 'r':
                    ch = '\u000d';
                    break;
                case 't':
                    ch = '\u0009';
                    break;
                case 'v':
                    ch = '\u000b';
                    break;
                default:
                    throw new GenericLexerException("Invalid character escape");
            }
            return ch;
        }

        protected virtual Token NextTokenOverride(char ch, TokenLocation startLocation)
        {
            if (!options.SkipUnknownChars)
                throw new GenericLexerException("Unknown character " + ch);
            return new Token(TokenKind.BadToken, startLocation, GetTextLocation());
        }

        protected virtual Token GetIdentifierTokenOverride(IdentifierToken identifier)
        {
            switch (identifier.TokenString.ToLowerInvariant())
            {
                case "true":
                    return new Token(TokenKind.Boolean, identifier.StartLocation, identifier.EndLocation, "true");
                case "false":
                    return new Token(TokenKind.Boolean, identifier.StartLocation, identifier.EndLocation, "false");
                default:
                    return identifier;
            }

        }

        void EnsureStarted()
        {
            if (currentToken == null || currentToken == Token.Null)
                NextToken();
        }

        public void Expect(TokenKind kind, bool advance = true)
        {
            if (CurrentToken.Kind != kind)
                throw new GenericLexerException(kind.ToString() + " expected!");
            if (advance)
                NextToken();
        }

        public void Expect(TokenKind kind, out string str, bool advance = true)
        {
            str = null;
            if (CurrentToken.Kind != kind)
                throw new GenericLexerException(kind + " expected!");
            str = kind == TokenKind.Identifier ? ((IdentifierToken)CurrentToken).ToString() : CurrentToken.TokenString;
            if (advance)
                NextToken();
        }

        public void ExpectIdentifier(string identifier, bool advance = true)
        {
            if (CurrentToken.Kind != TokenKind.Identifier || !identifier.Equals(CurrentToken.TokenString))
                throw new GenericLexerException($"'{identifier}' expected!");
            if (advance)
                NextToken();
        }

        public bool IsToken<T>(out T token) where T : Token
        {
            token = CurrentToken as T;
            return token != null;
        }

        public bool IsToken(TokenKind kind)
        {
            return CurrentToken.Kind == kind;
        }

        public bool IsIdentifierToken(string identifier)
        {
            return CurrentToken.Kind == TokenKind.Identifier && CurrentToken.TokenString.Equals(identifier);
        }

        public bool IsNextToken(TokenKind kind)
        {
            return NextToken().Kind == kind;
        }

        public bool IsNextToken<T>(out T token) where T : Token
        {
            token = NextToken() as T;
            return token != null;
        }

        public bool TrySkipToken(TokenKind kind)
        {
            if (CurrentToken.Kind != kind)
                return false;
            NextToken();
            return true;
        }

        public bool TryConsumeToken<T>(out T token) where T : Token
        {
            token = CurrentToken as T;
            if (token != null)
            {
                NextToken();
                return true;
            }
            return false;
        }

        public bool TryConsumeToken(TokenKind kind, out string str)
        {
            str = null;
            if (CurrentToken.Kind == kind)
            {
                str = CurrentToken.TokenString;
                NextToken();
                return true;
            }
            return false;
        }

        public void GetAllTokens(IList<Token> targetList)
        {
            while (true)
            {
                var token = NextToken();
                if (token.Kind == TokenKind.Eof)
                    break;
                targetList.Add(token);
            }
        }

        public void ReadIdentifierList(IList<string> targetList, TokenKind separator)
        {
            while (NextToken().Kind == TokenKind.Identifier)
            {
                targetList.Add(CurrentToken.TokenString);
                if (NextToken().Kind != separator)
                    break;
            }
        }

        public void ReadConstantList(IList<object> values, TokenKind separator)
        {
            object value;
            while (TryParseLiteralValue(out value))
            {
                values.Add(value);
                if (NextToken().Kind != separator)
                    break;
                else
                    NextToken();
            }
        }

        public static bool IsLiteralValue(TokenKind kind)
        {
            switch (kind)
            {
                case TokenKind.String:
                case TokenKind.Int16:
                case TokenKind.Int32:
                case TokenKind.Int64:
                case TokenKind.Single:
                case TokenKind.Double:
                case TokenKind.Decimal:
                case TokenKind.Boolean:
                    return true;
            }
            return false;
        }

        public object ParseLiteralValue(bool advance = true)
        {
            object value;
            if (!TryParseLiteralValue(out value))
                throw new GenericLexerException("Unknown literal value");
            if (advance)
                NextToken();
            return value;
        }

        public bool TryParseLiteralValue(out object value, bool advanceFirst = false)
        {
            if (advanceFirst)
                NextToken();
            value = null;
            switch (CurrentToken.Kind)
            {
                case TokenKind.String:
                    value = CurrentToken.TokenString;
                    break;
                case TokenKind.Int16:
                    value = CurrentToken.TokenString.ToInt16();
                    break;
                case TokenKind.Int32:
                    value = CurrentToken.TokenString.ToInt32();
                    break;
                case TokenKind.Int64:
                    value = CurrentToken.TokenString.ToInt64();
                    break;
                case TokenKind.Single:
                    value = Single.Parse(CurrentToken.TokenString);
                    break;
                case TokenKind.Double:
                    value = CurrentToken.TokenString.ToDouble();
                    break;
                case TokenKind.Decimal:
                    value = CurrentToken.TokenString.ToDecimal();
                    break;
                case TokenKind.Boolean:
                    value = "true".Equals(CurrentToken.TokenString, StringComparison.OrdinalIgnoreCase);
                    break;
                case TokenKind.Identifier:
                    if (options.ParseIdentifierAsLiteralValue)
                        value = CurrentToken.TokenString;
                    else
                        return false;
                    break;
                default:
                    return false;
            }
            return true;
        }


    }
    public sealed class GenericLexerException : Exception
    {

        public GenericLexerException(string message)
            : base(message)
        {

        }

    }
}
