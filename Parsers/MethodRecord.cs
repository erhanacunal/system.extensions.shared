﻿using System;

namespace System.Text.Parsers {
    struct MethodRecord {

        public static readonly MethodRecord Null = new MethodRecord { IsNull = true };

        public Type ClassType;
        public object Instance;
        public string Name;
        public bool IsNull { get; private set; }
        public string Alias;

        public MethodRecord( Type classType, object instance, string name, string alias )
            : this() {
            ClassType = classType;
            Instance = instance;
            Name = name;
            Alias = alias;
            IsNull = false;
        }
    }
}
